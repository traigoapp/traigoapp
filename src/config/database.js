const mysql = require( 'mysql' );
require('dotenv').config();

module.exports = class Database {
    constructor() {
        // Datos iniciales para entorno local
        let dataConnection = {
            host: 'localhost',
            user: 'root',
            password: '123',
            database: 'traigo',
            port: 3306
        };

        // Valida el tipo de entorno para saber la connexion a la base de datos
        if (process.env.NODE_ENV === 'production') {
            dataConnection.database = 'traigo';
            dataConnection.password = 'adminroot';
        } else if (process.env.NODE_ENV === 'test') {
            dataConnection.database = 'traigo_test';
            dataConnection.password = 'adminroot';
        }
        
        this.connection = mysql.createConnection(dataConnection);
    }
    query( sql, args ) {
        return new Promise( ( resolve, reject ) => {
            this.connection.query( sql, args, ( err, rows ) => {
                if ( err )
                    return reject( err );
                resolve( rows );
            } );
        } );
    }
    close() {
        return new Promise( ( resolve, reject ) => {
            this.connection.end( err => {
                if ( err )
                    return reject( err );
                resolve();
            } );
        } );
    }

    getConnection() {
        return this.connection;
    };
}