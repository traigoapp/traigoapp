const express = require('express');
const fs = require('fs');
const app = express();
const net = require('net');
let bodyParser = require('body-parser');
let router = express.Router();
let subdomain = require('express-subdomain');
app.locals.moment = require('moment');
require('dotenv').config();
app.use(subdomain('admin', router));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const webpush = require('web-push');

// VAPID keys should only be generated only once.
const vapidKeys = webpush.generateVAPIDKeys();
/*const publicVapidKey = process.env.PUBLIC_VAPID_KEY;
const privateVapidKey = process.env.PRIVATE_VAPID_KEY;*/

// Replace with your email
webpush.setVapidDetails('mailto:traigoapp@gmail.com', vapidKeys.publicKey, vapidKeys.privateKey);

let opts = {
	key: fs.readFileSync('ssl/private.key'),
	cert: fs.readFileSync('ssl/certificate.crt')
};

const server = require('http').Server(app);

const io = require('socket.io')(server);
const routesMod = require('./app/routes/routesMod');
const routesApp = require('./app/routes/routesApp');
const routesAdmin = require('./app/routes/routesAdmin');
const routesTienda = require('./app/routes/routesTienda');
const routerServicios = require('./app/routes/servicios');
const routerAPI = require('./app/routes/api');
const path = require('path');
const session = require('express-session');
const formidable = require('express-form-data');
const Database = require('./config/database');
let db = new Database(); // Instancia de base de datos promise
const connection = db.getConnection();
let RedisStore;
let redis;
let client
// Valida si se despliega el servidor para produccion
if (process.env.NODE_ENV === 'production') {
	redis = require('redis');
	RedisStore = require('connect-redis')(session);
	client = redis.createClient(); // this creates a new client
}

io.on("connect", function(socket){
	let clients = {};
	clients[socket.id] = socket;

	socket.on('disconnect', function() {
		console.log('Usuario disconectado IO');
		delete clients[socket.id];
		delete io.sockets.sockets[socket.id];
	});
	// Evento de conexion de usuario
	socket.on('userConnected', socket.join);
	// Evento de desconexion de usuario
	socket.on('userDisconnected', socket.leave);
	connection.query("select idPais, nombre from paises", function(err, res, field){
		if(err){
			connection.end();
			return;
		}
		socket.emit("paises", res);
	});
	socket.on("messages", function (data) {
		console.log(data);
		io.sockets.emit("msg", data);
	});

	socket.on("ordershop", function () {
		io.sockets.emit("order_shop");
	});

	connection.query("select idcategoria, nombre from categorias", function(err, res, field){
		if(err){
			connection.end();
			return;
		}
		socket.emit("categorias", res);
	});


	connection.query("select idcategoriaproducto, nombre from categoriasproductos", function(err, res, field){
		if(err){
			connection.end();
			return;
		}
		socket.emit("categoriasproductos", res);
	});


	connection.query("select idtienda, nombre from tiendas", function(err, res, field){
		if(err){
			connection.end();
			return;
		}
		socket.emit("tiendas", res);
	});

	socket.on("departamentos", function(data){
		var id = data.id;
		connection.query("select idDepartamento, nombre from departamentos where idPais = ?", [id], function(err, res, field){
			if(err){
				connection.end();
				return;
			}
			socket.emit("departamentos", res);
		})
	});

	socket.on("order_shop", function(id) {
		connection.query('select pd.*, ri.nombre as nombrerider, ri.apellido as apellidorider,'
						+ ' s.nombre as nombresucursal, s.numero_punto as numerosucursal, s.retenido as retiene,'
						+ ' s.tiempo_entrega as tiempo_entrega, s.costo_envio as costoenvio, ds.iddireccion'
						+ ' as iddireccionsucursal, ds.direccion as direccionsucursal, ds.notas as notasdireccionsucursal,'
						+ ' ds.latitude as latitudesucursal, ds.longitude as longitudesucursal, d.iddireccion'
						+ ' as iddireccionentrega, d.direccion as direccionentrega, d.notas as notasdireccionentrega'
						+ ' from pedidos_empresariales pd left join riders ri on ri.cedula = pd.idrider join direcciones d'
						+ ' on d.iddireccion = pd.iddireccion join sucursales s on s.idsucursal = pd.idsucursal'
						+ ' join direcciones ds on ds.iddireccion = s.iddireccion where pd.estado != ? and'
						+ ' pd.estado != ? and pd.estado != ? and pd.idpedido_empresarial = ?'
						+ ' order by pd.fecha desc', ['Entregado', 'Cancelado', 'Error', id],
							(err, rows, fields) => {
								if (err) {
									console.log(err);
									connection.end();
									return;
								} else {
									let order = rows[0]
									console.log('Orden creada');
									this.emit();
									io.emit('order_shop', { order });
								}
							});
	});

	socket.on("municipios", function(data){
		var id = data.id;
		connection.query("select idmunicipios, nombre from municipios where idDepartamento = ?", [id], function(err, res, field){
			if(err){
				connection.end();
				return;
			}
			socket.emit("municipios", res);
		})
	});

	socket.on("rider", function(data){
		var idrider = data.idrider;
		connection.query('select * from riders where cedula = ?'
			,[idrider], (err, rows, fields) => {
				if (rows) {
					if (rows.length == 0){
						socket.emit("actualizarRider", null);
					} else{
						socket.emit("actualizarRider", rows);
					}
				} else{
					socket.emit("actualizarRider", null);
				}
			});	
	});



	socket.on("riders", function(data){
		connection.query('select * from riders where activo = ?', ['S'], (err, rows, fields) => {
			socket.emit("mapariders", rows);
		});
	});

	socket.on('productotienda', function(data){
		var idproducto = data.idproducto;

		var elementos = [], selecciones = [], adiciones = [];
		var tempelementos;
		connection.query('select e.* from pclientes_productos pcp join pclientes_prod_elementos ppe on pcp.idpclientes_productos = ppe.idpclientes_productos join elementos e on e.idelemento = ppe.idelemento where pcp.idpclientes_productos = ?'
			,[idproducto], (err, rows, fields) => {
				if(err){
					console.log(err);
					socket.emit('renderproducto', {elementos: [], selecciones: [], adiciones: []})
				}else{
					tempelementos = rows;
					connection.query('select e.* from pclientes_productos pcp join elementos e on e.idproducto = pcp.idproducto where pcp.idpclientes_productos = ?'
						,[idproducto], (err, rows, fields) => {
							if(err){
								console.log(err);
								socket.emit('renderproducto', {elementos: [], selecciones: [], adiciones: []})
							}else{
								for(var i = 0; i < rows.length; i++){
									var correct = true;
									for(var j = 0; j < tempelementos.length; j++){
										if(tempelementos[j].idelemento == rows[i].idelemento){
											correct = false;
											break;
										}
									}
									if(correct){
										elementos.push(rows[i]);
									}
								}
								connection.query('select e.* from pclientes_productos pcp join pclientes_prod_adiciones ppe on pcp.idpclientes_productos = ppe.idpclientes_productos join adiciones e on e.idadicion = ppe.idadicion where pcp.idpclientes_productos = ?'
									,[idproducto], (err, rows, fields) => {
										if(err){
											console.log(err);
											socket.emit('renderproducto', {elementos: [], selecciones: [], adiciones: []})
										}else{
											adiciones = rows;
											connection.query('select e.*, f.nombre as nombrefactor from pclientes_productos pcp join pclientes_prod_selecciones ppe on pcp.idpclientes_productos = ppe.idpclientes_productos join selecciones e on e.idseleccion = ppe.idseleccion join factores f on f.idfactor = e.idfactor where pcp.idpclientes_productos = ?'
												,[idproducto], (err, rows, fields) => {
													if(err){
														console.log(err);
														socket.emit('renderproducto', {elementos: [], selecciones: [], adiciones: []});
													}else{
														selecciones = rows;
														socket.emit('renderproducto', {elementos: elementos, selecciones: selecciones, adiciones: adiciones});
													}
												});
										}
									});

							}
						});
				}
			});	

	});

});
app.use((req, res, next) => {
	req.io = io;
	next();
});
app.set('view engine', 'ejs');
app.set("views", path.join(__dirname, './app/views'));
app.set('etag', false);

app.use(formidable.parse({keepExtensions: true }));

app.use(session({
	store: (process.env.NODE_ENV === 'production')? new RedisStore({ host: 'localhost', port: 6379, client: client, ttl: 86400}) : null,
	secret: "jgxfj25545dg5f5",
	resave: true,
	saveUninitialized: false,
	cookie: {maxAge: new Date(Date.now() + 1 * 24 * 60 * 60 * 1000)} // 1 day
}));

app.use(express.static(path.join(__dirname, '../src/app/public')));
app.use('/static', express.static(path.join(__dirname, '../src/app/public')));
app.use('/api', routerAPI);

app.use('/servicios', routerServicios);
app.use('/moderador', routesMod);
app.use('/administrador', routesAdmin);
app.use('/tienda', routesTienda);
app.use('/', routesApp);

//para subirlo al servidor
//server.listen(process.argv[2], function(){
	//	console.log("Corriendo en el servidor - puerto:" + process.argv[2]);
	//});
	
	const port = (process.env.NODE_ENV === 'production')? 8001 : 8002;
	//para pruebas
	server.listen(port, function(){
		console.log("Corriendo en el servidor - puerto:" + port);
	});

/*	client.on('connect', function() {
	    console.log('Redis client connected');
	});

	client.on('error', function (err) {
	    console.log('Something went wrong redis' + err);
	});*/

	module.exports = app;
