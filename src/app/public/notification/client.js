const publicVapidKey = 'BItAhZu0DTitit8_ZkUezTNTJQ6Ynt_a8IrOdSF_0kTKjuracZvN9GCps4T9Bo7UGvX9RlaUuyXeKtHU0hBGkF8';

if ('serviceWorker' in navigator) {
  console.log('Registering service worker');
  run();
}

async function run() {
  console.log('Registering service worker');
  const registration = await navigator.serviceWorker.
    register('/static/workers/notification.js');
  console.log('Registered service worker');
  console.log('Registering push');
  const subscription = await registration.pushManager.
    subscribe({
      userVisibleOnly: true,
      applicationServerKey: urlBase64ToUint8Array(publicVapidKey)
    });
  console.log('Registered push');

  console.log('Sending push');
  await fetch('/administrador/notification', {
    method: 'POST',
    body: JSON.stringify(subscription),
    headers: {
      'content-type': 'application/json'
    }
  });
  console.log('Sent push');
}

// Boilerplate borrowed from https://www.npmjs.com/package/web-push#using-vapid-key-for-applicationserverkey
function urlBase64ToUint8Array(base64String) {
  const padding = '='.repeat((4 - base64String.length % 4) % 4);
  const base64 = (base64String + padding)
    .replace(/\-/g, '+')
    .replace(/_/g, '/');

  const rawData = window.atob(base64);
  const outputArray = new Uint8Array(rawData.length);

  for (let i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i);
  }
  return outputArray;
}
