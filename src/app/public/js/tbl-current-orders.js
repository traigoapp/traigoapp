
// Registro de componentes de mapas
Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyAKwCnkuKfwRNZUEDhXGDM6FHzsM9AD-Js',
        libraries: 'places'
        // region: 'CO',
        // language: 'es',
    },
    // Demonstrating how we can customize the name of the components
    installComponents: true,
});
// Componente select2
Vue.component('v-select', VueSelect.VueSelect);
// Buscador de autocompletado
Vue.component('v-autocomplete', Autocomplete);
// Componente para mapa
Vue.component('google-map', VueGoogleMaps.Map);
// Componente para el maker del mapa
Vue.component('google-marker', VueGoogleMaps.Marker);
// Componente de formulario para pedidos deseos
Vue.component('form-order-desire', {
    data() {
        return {
            // Lista de paises
            listCountries: [],
            // Lista de departamentos
            listDepartments: [],
            // Lista de ciudades
            listCities: [],
            // Lista de ciudades que tiene costos
            listCityCost: [],
            // CLientes registrados
            clientForm: {
                // Direccion del cliente
                address: {
                    cityId: null,
                    description: null,
                    notes: '',
                    position: {
                        lat: 4.535000,
                        lng: -75.675690
                    }
                },
                // Ciudad de donde se toman los costos
                cityIdCost: null,
                // Descripcion del deseo
                description: null,
                // Cliente seleccionado
                selected: null,
                // Cliente nuevo
                input: {
                    nombre: null,
                    correo: null,
                    telefono: null
                }
            }
        }
    },

    beforeMount() {
        // Obtiene las ciudades que tienen costos
        this.getCityCost();
        // Obtiene los paises
        this.getCountries();
    },

    methods: {

        /**
         * Inicializa los datos del fomrulario
         */
        initDataForm() {
            // CLientes registrados
            this.clientForm = {
                // Direccion del cliente
                address: {
                    cityId: null,
                    description: null,
                    notes: '',
                    position: {
                        lat: 4.535000,
                        lng: -75.675690
                    }
                },
                // Ciudad de donde se toman los costos
                cityIdCost: null,
                // Descripcion del deseo
                description: null,
                // Cliente seleccionado
                selected: null,
                // Cliente nuevo
                input: {
                    nombre: null,
                    correo: null,
                    telefono: null
                }
            };

            // Limpia el buscador de clientes
            this.$refs.clients.value = null;
            // Limpia el buscador de direciones
            this.$refs.gmap_autocomplete.$refs.input.value = null;
            // Centra el mapa en el punto inicial
            this.$refs.gmap.panTo(this.clientForm.address.position);

            // Obtiene las ciudades que tienen costos
            this.getCityCost();
            // Obtiene los paises
            this.getCountries();

        },

        /**
         * Crea un nuevo pedido deseo
         */
        createOrderDesire() {
            const dataForm = this.clientForm;
            // Valida si los datos del usuario son existentes
            (dataForm.selected) ? delete dataForm.input : delete dataForm.selected;
            // Envia informacion de pedido deseo
            axios({
                method: 'POST',
                url: `/administrador/orders/order-desire`,
                data: dataForm,
                responseType: 'json',
                headers: { 'Content-Type': 'application/json' }
            }).then((res) => {
                console.log(res, Function.prototype.getDesireOrders());
                // Cierra el modal
                $('#modalFormDesireOrders').modal('toggle');
                $.toast({
                    heading: 'Pedido creado',
                    text: 'El pedido deseo ha sido creado con exito.',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'success',
                    hideAfter: 3500, 
                    stack: 6
                });
                // Inicializa los datos del formulario
                this.initDataForm();
            }).catch((err) => {
                $.toast({
                    heading: 'Error',
                    text: 'Error al guardar los clientes',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
                console.log('Error al guardar los clientes', err);
            });
            
        },

        /**
         * Ubica la direccion buscada en el mapa
         *
         * @param {*} data datos de la direccion proporcionada por google
         */
        setPlace(data) {
            // Asigna Direccion
            this.clientForm.address.description = data.formatted_address;
            // Asigna Latitud
            this.clientForm.address.position.lat = data.geometry.location.lat();
            // Asigna Longitud
            this.clientForm.address.position.lng = data.geometry.location.lng();
            // Centra el mapa en la ubicacion obtenida
            this.$refs.gmap.panTo(this.clientForm.address.position);
        },

        /**
         * Evento al presionar el mapa, para obtener la ubicacion seleccionada
         *
         * @param {*} data datos de ubicacion
         */
        clickMap(data) {

            // Inicializa clase de Geocorder para obtener la direccion  por latitud y longitud
            let geocoder = new google.maps.Geocoder();

            // Centra el mapa en la ubicacion obtenida
            this.$refs.gmap.panTo(data.latLng);

            const that = this;

            // Obtiene los datos de la direccion
            geocoder.geocode({ latLng: data.latLng }, (results, status) => {
                // Valida si la informacion es correcta
                if (status == google.maps.GeocoderStatus.OK) {
                    // Valida si hay resultados
                    if (results[0]) {
                        // Asigna Direccion
                        that.clientForm.address.description = results[0].formatted_address;
                        // Asigna Latitud
                        that.clientForm.address.position.lat = results[0].geometry.location.lat();
                        // Asigna Longitud
                        that.clientForm.address.position.lng = results[0].geometry.location.lng();

                    } else {
                        // Inicializa los datos de la direccion
                        that.clientForm.address.description = null;
                        that.clientForm.address.position.lat = null;
                        that.clientForm.address.position.lng = null;
                    }
                } else {
                    // Inicializa los datos de la direccion
                    that.clientForm.address.description = null;
                    that.clientForm.address.position.lat = null;
                    that.clientForm.address.position.lng = null;
                }
            });
        },

        /**
         * Obtiene el valor de retorno del objeto filtrado
         * 
         * @param {Object} client objecto de cliente
         */
        getResultValue(client) {
            return `${client.correo} | ${client.nombre} | ${client.telefono}`;
        },

        /**
         * Evento cuando se selecciona un cliente del buscador
         *
         * @param {Object} result datos del cliente seleccionado
         */
        clientsSubmit(client) {
            this.clientForm.selected = client;
        },

        /**
         * Busca los clientes cuando se ingresa un caracter en el buscador
         *
         * @param {String} input campo de filtro por correo
         */
        async search(input) {
            // Valida si la longitud del filtro es mayor a 2
            if (input.length < 2) { return [] }
            // Obtiene los clientes existentes filtrados
            return await this.chargeClients(input);
        },

        /**
         * Obtiene los clientes registrados con filtro por correo
         *
         * @param {String} input correo del cliente
         *
         * @returns datos de los clientes
         */
        chargeClients(input) {
            // Obtiene los clientes registrados
            return axios({
                method: 'GET',
                url: `/administrador/orders/get-clients?email=${input}`,
                responseType: 'json',
                headers: { "Content-Type": "application/json" }
            }).then((res) => {
                return res.data.clients;
            }).catch((err) => {
                console.log('Error al cargar los clientes', err);
            });
        },

        /**
         * Obtiene las ciudades de tiene un costo asignado
         */
        getCityCost() {
            // Obtiene todos los pedidos en curso
            axios({
                method: 'GET',
                url: `/administrador/orders/get-city-costs`,
                responseType: 'json',
                headers: { 'Content-Type': 'application/json' }
            })
            .then((request) => {
                // Lista de ciudades que tiene costo
                this.listCityCost = request.data;
            })
            .catch(err => {
                console.log(err);
            });
        },

        /**
         * Obtiene todos los paises
         */
        getCountries() {
            // Obtiene todos los pedidos en curso
            axios({
                method: 'GET',
                url: `/administrador/get-countries`,
                responseType: 'json',
                headers: { 'Content-Type': 'application/json' }
            })
            .then((request) => {
                // Lista de paises
                this.listCountries = request.data;
            })
            .catch(err => {
                console.log(err);
            });
        },
        /**
         * Obtiene todos los departamentos por pais
         */
        getDepartmentByCountry(countryId) {
            // Obtiene todos los departamentos por ciudad
            axios({
                method: 'GET',
                url: `/administrador/get-departments-by-country/${countryId}`,
                responseType: 'json',
                headers: { 'Content-Type': 'application/json' }
            })
            .then(request => {
                // Lista de departamentos
                this.listDepartments = request.data;
            })
            .catch(err => {
                console.log(err);
            });
        },
        /**
         * Obtiene todas las ciudades por departamento
         */
        getCityByDepartment(departmentId) {
            axios({
                method: 'GET',
                url: `/administrador/get-city-by-department/${departmentId}`,
                responseType: 'json',
                headers: { 'Content-Type': 'application/json' }
            })
            .then(request => {
                // Lista de ciudades
                this.listCities = request.data;
            })
            .catch(err => {
                console.log(err);
            });
        }

    }
});
Vue.component('form-order-entrust-address', {
    data() {
        return {}
     }
});
// Componente de formulario para pedidos deseos
Vue.component('form-order-entrust', {
    data() {
        return {
            // Lista de paises
            listCountries: [],
            // Lista de departamentos
            listDepartments: [],
            // Lista de ciudades
            listCities: [],
            // Lista de ciudades que tiene costos
            listCityCost: [],
            // Clientes registrados
            clientForm: {
                // Direccion origen de recogida cliente
                originAddress: {
                    cityId: null,
                    description: null,
                    notes: '',
                    position: {
                        lat: 4.535000,
                        lng: -75.675690
                    }
                },
                // Direccion final de destino
                destinationAddress: {
                    cityId: null,
                    description: null,
                    notes: '',
                    position: {
                        lat: 4.535000,
                        lng: -75.675690
                    }
                },
                // Paradas adicionales del pedido
                stops: [],
                // Ciudad de donde se toman los costos
                cityIdCost: null,
                // Descripcion del deseo
                description: null,
                // Cliente seleccionado
                selected: null,
                // Cliente nuevo
                input: {
                    nombre: null,
                    correo: null,
                    telefono: null
                }
            }
        }
    },

    beforeMount() {
        // Obtiene las ciudades que tienen costos
        this.getCityCost();
        // Obtiene los paises
        this.getCountries();
    },

    methods: {

        /**
         * Agrega una nueva parada
         */
        newStop() {
            this.clientForm.stops.push({
                cityId: null,
                description: null,
                notes: '',
                position: {
                    lat: 4.535000,
                    lng: -75.675690
                }
            })
        },

        /**
         * Inicializa los datos del fomrulario
         */
        initDataForm() {
            // CLientes registrados
            this.clientForm = {
                // Direccion del cliente
                address: {
                    cityId: null,
                    description: null,
                    notes: '',
                    position: {
                        lat: 4.535000,
                        lng: -75.675690
                    }
                },
                // Ciudad de donde se toman los costos
                cityIdCost: null,
                // Descripcion del deseo
                description: null,
                // Cliente seleccionado
                selected: null,
                // Cliente nuevo
                input: {
                    nombre: null,
                    correo: null,
                    telefono: null
                }
            };

            // Limpia el buscador de clientes
            this.$refs.clients.value = null;
            // Limpia el buscador de direciones
            this.$refs.gmap_autocomplete.$refs.input.value = null;
            // Centra el mapa en el punto inicial
            this.$refs.gmap.panTo(this.clientForm.address.position);

            // Obtiene las ciudades que tienen costos
            this.getCityCost();
            // Obtiene los paises
            this.getCountries();

        },

        /**
         * Crea un nuevo pedido deseo
         */
        createOrderDesire() {
            const dataForm = this.clientForm;
            // Valida si los datos del usuario son existentes
            (dataForm.selected) ? delete dataForm.input : delete dataForm.selected;
            // Envia informacion de pedido deseo
            axios({
                method: 'POST',
                url: `/administrador/orders/order-desire`,
                data: dataForm,
                responseType: 'json',
                headers: { 'Content-Type': 'application/json' }
            }).then((res) => {
                console.log(res, Function.prototype.getDesireOrders());
                // Cierra el modal
                $('#modalFormDesireOrders').modal('toggle');
                $.toast({
                    heading: 'Pedido creado',
                    text: 'El pedido deseo ha sido creado con exito.',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'success',
                    hideAfter: 3500, 
                    stack: 6
                });
                // Inicializa los datos del formulario
                this.initDataForm();
            }).catch((err) => {
                $.toast({
                    heading: 'Error',
                    text: 'Error al guardar los clientes',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
                console.log('Error al guardar los clientes', err);
            });
            
        },

        /**
         * Ubica la direccion buscada en el mapa
         *
         * @param {*} data datos de la direccion proporcionada por google
         */
        setPlace(data) {
            // Asigna Direccion
            this.clientForm.address.description = data.formatted_address;
            // Asigna Latitud
            this.clientForm.address.position.lat = data.geometry.location.lat();
            // Asigna Longitud
            this.clientForm.address.position.lng = data.geometry.location.lng();
            // Centra el mapa en la ubicacion obtenida
            this.$refs.gmap.panTo(this.clientForm.address.position);
        },

        /**
         * Evento al presionar el mapa, para obtener la ubicacion seleccionada
         *
         * @param {*} data datos de ubicacion
         */
        clickMap(data) {

            // Inicializa clase de Geocorder para obtener la direccion  por latitud y longitud
            let geocoder = new google.maps.Geocoder();

            // Centra el mapa en la ubicacion obtenida
            this.$refs.gmap.panTo(data.latLng);

            const that = this;

            // Obtiene los datos de la direccion
            geocoder.geocode({ latLng: data.latLng }, (results, status) => {
                // Valida si la informacion es correcta
                if (status == google.maps.GeocoderStatus.OK) {
                    // Valida si hay resultados
                    if (results[0]) {
                        // Asigna Direccion
                        that.clientForm.address.description = results[0].formatted_address;
                        // Asigna Latitud
                        that.clientForm.address.position.lat = results[0].geometry.location.lat();
                        // Asigna Longitud
                        that.clientForm.address.position.lng = results[0].geometry.location.lng();

                    } else {
                        // Inicializa los datos de la direccion
                        that.clientForm.address.description = null;
                        that.clientForm.address.position.lat = null;
                        that.clientForm.address.position.lng = null;
                    }
                } else {
                    // Inicializa los datos de la direccion
                    that.clientForm.address.description = null;
                    that.clientForm.address.position.lat = null;
                    that.clientForm.address.position.lng = null;
                }
            });
        },

        /**
         * Obtiene el valor de retorno del objeto filtrado
         * 
         * @param {Object} client objecto de cliente
         */
        getResultValue(client) {
            return `${client.correo} | ${client.nombre} | ${client.telefono}`;
        },

        /**
         * Evento cuando se selecciona un cliente del buscador
         *
         * @param {Object} result datos del cliente seleccionado
         */
        clientsSubmit(client) {
            this.clientForm.selected = client;
        },

        /**
         * Busca los clientes cuando se ingresa un caracter en el buscador
         *
         * @param {String} input campo de filtro por correo
         */
        async search(input) {
            // Valida si la longitud del filtro es mayor a 2
            if (input.length < 2) { return [] }
            // Obtiene los clientes existentes filtrados
            return await this.chargeClients(input);
        },

        /**
         * Obtiene los clientes registrados con filtro por correo
         *
         * @param {String} input correo del cliente
         *
         * @returns datos de los clientes
         */
        chargeClients(input) {
            // Obtiene los clientes registrados
            return axios({
                method: 'GET',
                url: `/administrador/orders/get-clients?email=${input}`,
                responseType: 'json',
                headers: { "Content-Type": "application/json" }
            }).then((res) => {
                return res.data.clients;
            }).catch((err) => {
                console.log('Error al cargar los clientes', err);
            });
        },

        /**
         * Obtiene las ciudades de tiene un costo asignado
         */
        getCityCost() {
            // Obtiene todos los pedidos en curso
            axios({
                method: 'GET',
                url: `/administrador/orders/get-city-costs`,
                responseType: 'json',
                headers: { 'Content-Type': 'application/json' }
            })
            .then((request) => {
                // Lista de ciudades que tiene costo
                this.listCityCost = request.data;
            })
            .catch(err => {
                console.log(err);
            });
        },

        /**
         * Obtiene todos los paises
         */
        getCountries() {
            // Obtiene todos los pedidos en curso
            axios({
                method: 'GET',
                url: `/administrador/get-countries`,
                responseType: 'json',
                headers: { 'Content-Type': 'application/json' }
            })
            .then((request) => {
                // Lista de paises
                this.listCountries = request.data;
            })
            .catch(err => {
                console.log(err);
            });
        },
        /**
         * Obtiene todos los departamentos por pais
         */
        getDepartmentByCountry(countryId) {
            // Obtiene todos los departamentos por ciudad
            axios({
                method: 'GET',
                url: `/administrador/get-departments-by-country/${countryId}`,
                responseType: 'json',
                headers: { 'Content-Type': 'application/json' }
            })
            .then(request => {
                // Lista de departamentos
                this.listDepartments = request.data;
            })
            .catch(err => {
                console.log(err);
            });
        },
        /**
         * Obtiene todas las ciudades por departamento
         */
        getCityByDepartment(departmentId) {
            axios({
                method: 'GET',
                url: `/administrador/get-city-by-department/${departmentId}`,
                responseType: 'json',
                headers: { 'Content-Type': 'application/json' }
            })
            .then(request => {
                // Lista de ciudades
                this.listCities = request.data;
            })
            .catch(err => {
                console.log(err);
            });
        }

    }
});

let vue = new Vue({
    el: '#app',
    data: {
        // Datos de actualizacion de ordenes concurrente
        ordersInProgress: null,
        f_sucursal_name: '',
        f_rider_name: '', // 
        f_municipio_name: '', // 
        s_sucursal: [], // Lista de sucursales
        s_riders: [], // Lista de riders activos
        s_municipio: [], // Lista de municipios
        count_pe: 0, // Cantidad de pedidos en curso

        // Datos de modal de modificacion de pedido empresarial
        riders: [], // Lista  de riders activos
        order: {}, // Orden a editar
        states: [], // Estados de pedido
        tam_maletin: [], // Lista de tamaño de maletines

        // Campos de texto del modal
        mf_rider: 'Sin Rider', // Valor rider
        mf_state: null, // Estado del pedido
        mf_size_bag: 'Cualquier tamaño', // Tamaño de maletin
        mf_adjust: null, // Valor de ajuste
        mf_is_adjust: false, // Verifica si la orden ya tiene un ajuste
        mf_adjust_description: null, // Descripcion de ajuste
        mf_evidence: null, // Evidencia (Tienda especial)

        limit_orders_riders: 0 // Limite de pedidos empresariales disponibles para visualisar riders
    },
    methods: {
        /**
         *  Carga los pedido en curso inicialmente 
         */
        chargeOrders() {
            // Obtiene todos los pedidos en curso
            axios({
                method: 'GET',
                url: '/administrador/getBusinessOrders',
                responseType: 'json',
                headers: { "Content-Type": "application/json" }
            })
                .then(res => {

                    // Limite de pedidos que piede nver los riders
                    this.limit_orders_riders = res.data.limit;

                    /**
                     * Inicializa las listas de filtro
                     */
                    this.s_sucursal = [];
                    this.s_riders = [];
                    this.s_municipio = [];

                    /**
                     * Agrega elementos unicos a una lista
                     * @param {*} list lista de elementos
                     * @param {*} element elemento a agregar
                     */
                    function insertUniqueElement(list, element) {
                        // VErifica que solo se ingresen elementos unicos
                        if (list.indexOf(element) === -1) {
                            // Agrega el elemento sin duplicidad
                            list.push(element);
                        }
                    }

                    // Lista de pedidos en curso
                    this.ordersInProgress = res.data.orders.map(item => {
                        // Verifica si existe un rider asociado al pedido
                        if (!item.idrider) {
                            item.nombrerider = 'SIN RIDER';
                        }

                        // Inserta riders
                        insertUniqueElement(this.s_riders, item.nombrerider);

                        // Inserta sucursales
                        insertUniqueElement(this.s_sucursal, item.nombresucursal);
                        // Iserta municipios
                        insertUniqueElement(this.s_municipio, item.municipio);

                        return item;
                    });

                    // Cantida de pedidos actuales
                    this.count_pe = this.ordersInProgress.length;

                    // Ordena la listas de filtro
                    this.s_sucursal.sort();

                })
                .catch(err => {
                    console.log('Error al obtener los pedidos empresariales', err);
                    this.ordersInProgress = null;
                });
        },
        /**
         * Filtra todos las ordenes por las coincnidencias
         */
        ordersInProgressFilter() {

            // Se verifica si existe una lista de órdenes
            if (this.ordersInProgress) {

                // Filtros a ejecutar por el criterio ingresado
                const filters = {
                    nombresucursal: [(this.f_sucursal_name) ? this.f_sucursal_name.toUpperCase() : ''],
                    nombrerider: [(this.f_rider_name) ? this.f_rider_name.toUpperCase() : ''],
                    municipio: [(this.f_municipio_name) ? this.f_municipio_name.toUpperCase() : '']
                }

                // Obtenemos las llaves de los filtros
                const filterKeys = Object.keys(filters);
                // Filtra todas las considecias por por los filtros dispoibles
                return this.ordersInProgress.filter((item) => {
                    // Por cada filtro ingresado verificamos si lo incluye en el elemento de la clave
                    return filterKeys.every((key) => {
                        return item[key].toUpperCase().includes(filters[key]);
                    });
                });

            }
        },
        /**
         * Carga los datos del modal de edicion de pedido empresarial 
         */
        chargeModalFormEditOrderBusiness(order_id) {
            // Obtiene todos los pedidos en curso
            axios({
                method: 'GET',
                url: `/administrador/order-business/${order_id}/edit`,
                responseType: 'json',
                headers: { "Content-Type": "application/json" }
            })
                .then(request => {
                    this.order = request.data.order;
                    this.mf_rider = this.order.idrider;
                    this.mf_state = this.order.estado;
                    this.mf_size_bag = this.order.tamanio_maletin;
                    this.riders = request.data.riders;
                    this.states = request.data.states;
                    this.tam_maletin = request.data.tam_maletin;
                    this.mf_is_adjust = request.data.has_adjust;

                    this.riders.unshift({ cedula: null, nombre: 'SIN RIDER' });

                    $('#modalEditBusinessOrders').modal();
                })
                .catch(err => {
                    console.log('Error al obtener el pedido empresarial', err);
                });
        },
        /**
         * 
         */
        sendEditBusinessOrder(order_id) {

            let form_data = {
                rider: (this.mf_rider) ? this.mf_rider : 'Sin Rider',
                state: this.mf_state,
                evidence: this.mf_evidence,
                size_bag: this.mf_size_bag,
                adjust_value: this.mf_adjust,
                adjust_description: this.mf_adjust_description
            }

            if (this.mf_evidence) {
                form_data = new FormData();
                form_data.append('rider', this.mf_rider);
                form_data.append('state', this.mf_state);
                if (this.mf_evidence) {
                    form_data.append('evidence', this.mf_evidence);
                }
                if (this.mf_size_bag) {
                    form_data.append('size_bag', this.mf_size_bag);
                }
                if (this.mf_adjust) {
                    form_data.append('adjust_value', this.mf_adjust);
                }
                if (this.mf_adjust_description) {
                    form_data.append('adjust_description', this.mf_adjust_description);
                }
            }
            //console.log('DATA', form_data);
            // Edita el pedido empresarial
            axios({
                method: 'PUT',
                url: `/administrador/order-business/${order_id}`,
                responseType: 'json',
                headers: { 'Content-Type': (this.mf_evidence) ? 'multipart/form-data' : 'application/json' },
                data: form_data
            }).then((response) => {
                //console.log('Actualiza con exito', response);
                this.chargeOrders();
                $('#modalEditBusinessOrders').modal('toggle');
            }).catch(err => {
                console.log('Error al actualizar el pedido empresarial', err);
                $('#modalEditBusinessOrders').modal('toggle');
            })
        },
        /**
         * Abre modal para crear un pedido deseo
         */
        newDesire() {
            // Abre el modal de pedidos deseo
            $('#modalFormDesireOrders').modal();
            // Sube la elevacion del buscador de direcciones de google
            $('.pac-container').css('z-index', '9999');
            // $('#modalEditBusinessOrders').modal('toggle');
        },
        /**
         * Abre modal para crear un pedido encomienda
         */
        newEntrust() {
            // Abre el modal de pedidos encomienda
            $('#modalFormEntrustOrders').modal();
            // Sube la elevacion del buscador de direcciones de google
            $('.pac-container').css('z-index', '9999');
            // $('#modalEditBusinessOrders').modal('toggle');
        },
        /**
         * Procesa la el archivo de evidencia
         */
        processFileModal() {
            this.mf_evidence = event.target.files[0];
        }
    }
});



var socket = io.connect('/');//wss://admin.traigo.com.co
socket.on('order_shop', function (data) {
    vue.chargeOrders();
    //socket.disconnect();
});

// Obtiene los pedido empresariales inicialmente
vue.chargeOrders();
// vue.chargeClients();
