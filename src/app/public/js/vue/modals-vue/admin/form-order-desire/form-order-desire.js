
export default {
    template: `
    <form novalidate v-on:submit.prevent="sendEditBusinessOrder(order.idpedido_empresarial); return false;" enctype="multipart/form-data">
        <div class="form-body">
            <!--/row-->
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <h5>Rider <span class="text-danger">*</span></h5>
                        <div class="controls">
                            <v-select :options="riders" :reduce="riders => riders.cedula" label="nombre" v-model="mf_rider"></v-select>
                            <small class="form-control-feedback">
                                Selector de riders.
                            </small>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <h5>Estado <span class="text-danger">*</span></h5>
                        <div class="controls">
                            <select class="form-control custom-select select2" v-model="mf_state"
                                required>
                                <option :selected="(state == order.estado)? true : false" v-for="state in states">{{ state }}</option>
                            </select>
                            <small class="form-control-feedback">
                                Selector de estados.
                            </small>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <h5>Tamaño maletín <span class="text-danger">*</span></h5>
                        <div class="controls">
                            <select class="form-control custom-select" v-model="mf_size_bag">
                                <option :selected="(tam.id == order.idrider)? true : false" v-for="tam in tam_maletin" :value="tam.id">{{ tam.name}}</option>
                            </select>
                            <small class="form-control-feedback">
                                Selector de tamaño de maletines.
                            </small>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" v-if="!mf_is_adjust">
                <div class="col-md-6">
                    <div class="form-group">
                        <h5>Valor ajuste </h5>
                        <div class="controls">
                            <input type="number" class="form-control" v-model="mf_adjust">
                        </div>
                        <small class="form-control-feedback">
                            Ingrese el valor del ajuste para éste pedido
                        </small>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <h5>Descripción ajuste</h5>
                        <div class="controls">
                            <textarea class="form-control" placeholder="Ingrese el motivo del ajuste" aria-invalid="false" rows="3" v-model="mf_adjust_description"></textarea>
                        </div>
                        <small class="form-control-feedback">
                            Ingrese el valor del ajuste para éste pedido
                        </small>
                    </div>
                </div>
            </div>
            
            <div class="row" v-if="order.tipoSucursal === 2 && (order.evidencia !== null || order.evidencia !== '')">
                <div class="col-md-10">
                    <div class="form-group">
                        <h5>Evidencia fotográfica <span class="text-danger">*</span></h5>
                        <div class="controls">
                            <span v-if="mf_evidence">Archivo seleccionado: {{mf_evidence.name}}</span>
                            <input type="file" class="form-control" @change="processFileModal($event)" accept="image/x-png,image/gif,image/jpeg,image/jpg">
                        </div>
                        <small class="form-control-feedback"> Seleccione la evidencia de éste pedido </small>
                    </div>
                </div>
            </div>
        </div>
        <!--/row-->
        <hr>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <button type="submit" class="btn btn-success">
                                <i class="fa fa-check"></i>
                                Guardar
                            </button>
                            <button type="button" class="btn btn-inverse" data-dismiss="modal">Cancelar</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-6"> </div>
            </div>
        </div>
    </form>
    `,
    data() {
        return {
            message: 'Oh hai from the component'
        }
    }
}