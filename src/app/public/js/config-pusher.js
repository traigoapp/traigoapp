
// Enable pusher logging - don't include this in production
Pusher.logToConsole = true;

var pusher = new Pusher('e687d1825c8d4e611925', {
  cluster: 'us2',
  forceTLS: true
});

var channel = pusher.subscribe('my-channel');
channel.bind('my-event', function(data) {
    console.log('Datos recibidos', data)
  alert(JSON.stringify(data));
});