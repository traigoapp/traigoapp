let vue = new Vue({
    el: '#riders_form',
    data: {
        mCountry: null,
        mDepartment: null,
        mCity: null,
        mListCountries: [],
        mListDepartments: [],
        mListCities: []
    },
    methods: {
        /**
         * Obtiene todos los paises
         */
        getCountries() {
            // Obtiene todos los pedidos en curso
            axios({
                method: 'GET',
                url: `/administrador/get-countries`,
                responseType: 'json',
                headers: { 'Content-Type': 'application/json' }
            })
            .then(request => {
                // Lista de paises
                this.mListCountries = request.data;
            })
            .catch(err => {
                console.log(err);
            });
        },
        /**
         * Obtiene todos los departamentos por pais
         */
        getDepartmentByCountry() {
            // Obtiene todos los departamentos por ciudad
            axios({
                method: 'GET',
                url: `/administrador/get-departments-by-country/${this.mCountry}`,
                responseType: 'json',
                headers: { 'Content-Type': 'application/json' }
            })
            .then(request => {
                // Lista de departamentos
                this.mListDepartments = request.data;
            })
            .catch(err => {
                console.log(err);
            });
        },
        /**
         * Obtiene todos los departamentos por pais
         */
        getCityByDepartment() {
            // Obtiene todos los departamentos por ciudad
            axios({
                method: 'GET',
                url: `/administrador/get-city-by-department/${this.mDepartment}`,
                responseType: 'json',
                headers: { 'Content-Type': 'application/json' }
            })
            .then(request => {
                // Lista de ciudades
                this.mListCities = request.data;
            })
            .catch(err => {
                console.log(err);
            });
        }

    }
});

vue.getCountries();