console.log('Loaded service worker!');

// Show Notification
self.addEventListener('push', ev => {
  const data = ev.data.json();
  //console.log('Got push', data);
  self.registration.showNotification(data.title, {
    body: data.message,
    icon: '/static/img/favicon.png'
  });
});
