const auth = require('../../middlewares/auth');
// Permite manejar la informacion de las rutas
const router = require('express').Router();
// Controlador de utilidades generales de la aplicacion
const utilsController = require('../utils.controller');
// Acceso a modelos de sequelize
const models = require('../../models');
// Modelo pedidos empresariales
const PedidosEmpresariales = models.pedidos_empresariales;
// Modelo Riders
const Riders = models.riders;
// Modelo configuraciones
const Configuraciones = models.configuraciones;
// Operaciones de sequelize
const op = models.Sequelize.Op;

let urlSocket = 'http://localhost:8002'

// Valida el tipo de entorno para saber la url del socket
if (process.env.NODE_ENV === 'production') {
	urlSocket = 'https://admin.traigo.com.co'; 
} else if (process.env.NODE_ENV === 'test') {
	urlSocket = 'http://traigo.com.co:8002'; 
}

const io = require('socket.io-client')(urlSocket);

/**
 * Obtiene lista de pedidos en curso
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns lsita de pedidos en curso
 */
router.get('/obtenerpedidosencurso', auth, (req, res) => {

	let correo = req.query.usuario;

	let deseos, pedidos, mandados;
	connection.query('select pd.*, d.direccion as direccionentrega, d.notas as notasdireccionentrega ' +
		'from pedidos_deseos pd join direcciones d on d.iddireccion = pd.iddireccion join clientes c ' +
		'on c.correo = pd.correocliente where pd.estado != ? and pd.estado != ? ' +
		'and pd.estado != ? and c.correo = ? order by pd.fecha desc',
		['Entregado', 'Cancelado', 'Error', correo], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(err);
			} else {
				deseos = rows;
				for (let i = 0; i < deseos.length; i++) {
					deseos[i].type = 'deseo';
				}
				connection.query('select pd.*, c.*, do.direccion as direccionorigen, do.notas as notasdireccionorigen, ' +
					'do.latitude as latitudedireccionorigen, do.longitude as longitudedireccionorigen, dd.direccion ' +
					'as direcciondestino, dd.notas as notasdirecciondestino, dd.latitude as latitudedirecciondestino, ' +
					'dd.longitude as longitudedirecciondestino from pedidos_encomiendas pd join direcciones do ' +
					'on do.iddireccion = pd.iddireccionorigen join direcciones dd on dd.iddireccion = pd.iddirecciondestino ' +
					'join clientes c on c.correo = pd.correocliente where pd.estado != ? and pd.estado != ? ' +
					'and pd.estado != ? and c.correo = ? order by pd.fecha desc',
					['Entregado', 'Cancelado', 'Error', correo], (err, rows, fields) => {
						if (err) {
							console.log(err);
							res.send(err);
						} else {
							mandados = rows;
							for (let i = 0; i < mandados.length; i++) {
								mandados[i].type = 'mandado';
							}
							connection.query('select pd.*, c.*, d.*, ss.idsucursal as idsucursal, ss.nombre as nombresucursal, ' +
								'ss.tiempo_entrega as tiempo_entrega, count(p.idpclientes_productos) as cantidadproductos ' +
								'from pedidos_clientes pd join clientes c on c.correo = pd.correocliente join direcciones d ' +
								'on d.iddireccion = pd.iddireccion join pclientes_productos p on pd.idpedido_clientes = p.idpedido_clientes ' +
								'join productos pro on pro.idproducto = p.idproducto join sucursales ss on ss.idsucursal = pro.idsucursal ' +
								'where pd.estado != ? and pd.estado != ? and pd.estado != ? and c.correo = ? ' +
								'group by pd.idpedido_clientes, ss.idsucursal, ss.nombre, ss.tiempo_entrega order by pd.fecha desc',
								['Entregado', 'Cancelado', 'Error', correo], (err, rows, fields) => {
									if (err) {
										console.log(err);
										res.send(err);
									} else {
										pedidos = rows;
										for (let i = 0; i < pedidos.length; i++) {
											pedidos[i].type = 'tienda';
										}
										let todos = deseos.concat(pedidos, mandados);

										for (let i = 0; i < (todos.length - 1); i++) {
											for (let j = 0; j < (todos.length - i - 1); j++) {
												if (todos[j].fecha < todos[j + 1].fecha) {
													k = todos[j + 1];
													todos[j + 1] = todos[j];
													todos[j] = k;
												}
											}
										}
										res.send(todos);
									}
								});
						}
					});
			}
		});
});

/**
 * Cambia el estado del pedido empresarial
 */
router.post('/change-business-order-status', (req, res) => {
	let riderId = req.body.rider_id;
	let orderId = req.body.order_id;
	let state = req.body.state;
	
	// Busca el pedido empresarial por el identificador 
	PedidosEmpresariales.findOne({
		where: {
			idpedido_empresarial: orderId,
			[op.or] : [
				{ idrider : null},
				{ idrider : riderId}
			]
		}
	})
	.then(async (pe) => {

		// Verifica si existe el pedido empresarial
		if (pe) {

			// Valida si el estado es Rider en camino y Valida la cantidad de pedidos empresariales que puede llevar el rider
			if (state === 'Rider en camino' && !(await validateLimitBusinessOrderRider(riderId))) {
				return res.status(409).json({
					success: false,
					message: '¡Ups!, ha superado el máximo de pedidos que puede llevar.'
				});
			}

			pe.idrider = riderId;
			pe.estado = state;

			// Actualiza los datos del pedido en curso
			pe.save()	
			.then ((result, affect) => {

				if (io) {
					console.log("Emite cambio de estado");
					io.emit("ordershop");
				}

				res.status(200).json({
					success: true,
					message: 'Estado actualizado con éxito'
				});
			})
			.catch(err => {
				console.log('Error al actualizar el estado del pedido empresarial', err);
				res.status(400).json({
					success: false,
					message: 'Error al actualizar el estado'
				});
			});
			
		} else {
			res.status(200).json({
				success: false,
				message: 'No se encuentra el pedido empresarial o el registro'
			});
		}
	})
	.catch(err => {
		console.log(err);
		res.status(200).json({
			success: false,
			message: 'Error al buscar el pedido empresarial'
		});
	});
	
});

/**
 * Obtiene la cantidad de pedidos empresariales en curso del rider
 *
 * @param {*} riderId 
 */
function getQtyCurrentBussinessOrdersRider(riderId) {
	return PedidosEmpresariales.count({
		where: {
			idrider: riderId,
			[op.not]: {
				[op.or]: [
					{estado: 'Entregado'},
					{estado: 'Error'},
					{estado: 'Cancelado'}
				]
			}
		}
	});
}

/**
 * Valida si el rider puede tomar mas pedidos empresariales
 *
 * @param {*} riderId identificador del rider
 * @param {*} qtyOrders cantidad actual de pedidos empresariales asignados
 * 
 * @returns true si puede tomar mas pedios | false de lo contrario
 */
function validateLimitBusinessOrderRider(riderId) {

	return Riders.findOne({
		attributes: ['limitePedidos'],
		where: {
			cedula: riderId
		}
	}).then(async (rider) => {

		// Obtiene la cantidad de pedidos empresariales actuales del rider
		const qtyBussinesOrdersRider = await getQtyCurrentBussinessOrdersRider(riderId);
		console.log(rider.limitePedidos, qtyBussinesOrdersRider);
		
		// Valida si el limite de pedidos del rider es mayor a la cantidad de pedidos tomados
		if (rider.limitePedidos > qtyBussinesOrdersRider) {
			return true;
		}
		return false;
	});
}

module.exports = router;
