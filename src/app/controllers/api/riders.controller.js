require('dotenv').config();
// Permite manejar la informacion de las rutas
const router = require('express').Router();
// Controlador de utilidades generales de la aplicacion
const utilsController = require('../utils.controller');
// Acceso a modelos de sequelize
const models = require('../../models');
// Manejo de fechas
const moment = require('moment');
// Manejo de encriptacion de contraseñas
const bcrypt = require('bcrypt');
// Manejo de archivos
const fs = require('fs');
// Json web token
const jwt = require('jsonwebtoken');

/**
 * Valida el token de peticiones del rider
 */
router.get('/validate-token', (req, res) => {
	const token = req.query.token;
	try {
		// Valida token
		const decoded = jwt.verify(token, process.env.PRIVATE_JWT_KEY);
		// Actualiza token
		req.user_app = decoded;
		// Envia informacion de token valido
		res.status(200).send({success: true, message: 'Token válido.', code: 2000});
	} catch (err) {
		console.log('Token invalido', err);
		// Envia informacion de token invalido
		res.status(400).send({success: false, message: 'Token inválido.', code: 1000});
	}
});

/**
 * Login riders
 */
router.post('/login', (req, res) => {

	const user = req.body.username;
	const pass = req.body.password;

	console.log(req.body);

	if (!user || !pass) {
		return res.status(402).json({ error: 'Datos incompletos' });
	}

    models.sequelize
        .query(`SELECT ri.*, di.direccion as direccion, di.idmunicipio as idmunicipio, di.notas as notas 
                FROM riders ri JOIN direcciones di ON ri.iddireccion=di.iddireccion WHERE ri.cedula = ?`, 
                { replacements: [user], type: models.sequelize.QueryTypes.SELECT })
        .then(rows => {	// Datos del rider
			// Verifica si no se encuentra el rider
			if (rows.length === 0) {
				console.log('Error no se encuentra el rider', rows);
				return res.status(401).json({ error: 'No está registrado en nuestra plataforma.' });
			}

			// Rider obtenido
			const user = rows[0];

			// Se valida si la contraseña es correcta
			if (bcrypt.compareSync(pass, user.contrasenia)) {
				// Se verifica si el rider está en lista negra
				if (user.listanegra === 'S') {
					return res.status(403).json({ error: 'No está habilitado para usar esta aplicación', message: user });
				}
				// Se crea el token con los datos de usuario
				jwt.sign({ user }, process.env.PRIVATE_JWT_KEY, (err, token) => {
                    // Valida si hay error al crear el token
					if (err) {
						return res.status(400).json({ error: 'Error al crear el token' });
					}

					/**
					 * Se Organiza el request para el envio de los datos
					 */
					user.cedula = parseInt(user.cedula)
					user.listanegra = utilsController.validateBoolean(user.listanegra);
					user.activo = utilsController.validateBoolean(user.activo);
					user.direction = { // Organiza datos de la direccion del rider
						id: user.iddireccion,
						address: user.direccion,
						notes: user.notas,
						city_id: user.idmunicipio
					}

					// Se calcula la edad del rider
					user.edad = moment().diff(moment(user.fecha_nacimiento), 'years');

					// Se envía el token creado
					user.token = token;

					// Se eliminan datos innecesarios
					delete user.direccion;
					delete user.idmunicipio;
					delete user.notas;
					delete user.contrasenia;

					res.status(200).json(user);
				});
			} else { // Error cuando la contraseña no coincide
				res.status(402).json({ error: 'Contraseña invalida' });
			}
		}).catch(err => { // Error al obtener el rider
			console.log('Error login al obtener el rider', err);
			res.status(400).json({ error: err });
		});
});

module.exports = router;
