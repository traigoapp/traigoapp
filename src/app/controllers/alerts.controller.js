// Permite manejar la informacion de las rutas
const router = require('express').Router();
// Controlador de utilidades generales de la aplicacion
const utilsController = require('./utils.controller');
// Acceso a modelos de sequelize
const models = require('../models');
// Modelo de alertas
const Alertas = models.alertas;
// Modelo de historial alertas
const HistorialAlertas = models.historial_alertas;
// Modelo de tiendas historial alertas
const TiendasHistorialAlertas = models.tiendas_historial_alertas;
// Manejo de archivos
const fs = require('fs');

/** CRUD ALERTS INIT**/
// (INDEX) 
router.get('/', (req, res) => {
	// Pagina index de alertas
	res.render('admin/pages/alerts/index');
});
// Obtiene los tipos de alertas
router.get('/types', (req, res) => {
	res.status(200).json(utilsController.getConstant('TIPO_ALERTA'));
});

// Obtiene las alertas existentes
router.get('/all', (req, res) => {

	// Busca todas las alertas existentes
	Alertas.findAll()
	.then(alertas => {
		res.status(200).json({alertas, tiposAlerta: utilsController.getConstant('TIPO_ALERTA')});
	})
	.catch(err => {
		console.log('Error al obtener alertas', err);
		res.status(400).json('Error al obtener todas las alertas');
	});
	
});

// (STORE) guarda una nueva alerta
router.post('/store', (req, res) => {
	// Tipo de alerta
	const tipo = req.body.tipo;
	// Nombre de alerta
	const nombre = req.body.nombre;
	// Imagen de alerta
	const fileImg = req.files.img;

	// Organiza la imagen recibida
	const extension = fileImg.name.split('.').pop(); // Obtiene la exprension del archivo
	const nameImg = Math.random().toString(36).slice(2); // Nombre aletorio de la imagen
	const urlImg = `src/app/public/img/alertas/${nameImg}.${extension}`; // Direccion url de la ubicacion de la imge
	fs.renameSync(fileImg.path, urlImg); // REnombra la ubicacion de la image
	const img = `${nameImg}.${extension}`;
	
	// Crea una nueva alerta
	Alertas.create({ nombre, tipo, img })
	.then((alert) => {
		res.status(200).json(alert);
	})
	.catch(err => {
		console.log('Error al crear una nuvea alerta', err);
		res.status(400).json('Error al crear la alerta');
	});

});

// (UPDATE) Modifica los costos actuales
router.put('/:id', async (req, res) => {

	// Identificador de la alerta
	const id = req.params.id;
	// Nombre de alerta
	const nombre = req.body.nombre;
	// Tipo de alerta
	const tipo = req.body.tipo;
	// Imagen de alerta
	const fileImg = (req.files && req.files.img)? req.files.img : null;
	// Datos a actualizar
	let dataUpdate = { nombre, tipo };

	// Verifica si existe una imagen para actualizar
	if (fileImg) {
		// Organiza la imagen recibida
		const extension = fileImg.name.split('.').pop(); // Obtiene la exprension del archivo
		const nameImg = Math.random().toString(36).slice(2); // Nombre aletorio de la imagen
		const urlImg = `src/app/public/img/alertas/${nameImg}.${extension}`; // Direccion url de la ubicacion de la imge
		fs.renameSync(fileImg.path, urlImg); // REnombra la ubicacion de la image
		img = `${nameImg}.${extension}`;
		dataUpdate.img = img;
	}

	// Busca la alerta por id y obtiene el nombre de la imagen
	const oldImg = await Alertas.findOne({ attributes: ['img'], where: { id }, raw: true });

	// Crea una nueva alerta
	Alertas.update(dataUpdate, { where: { id } })
	.then((alert) => {
		// Elimina la imagen anterior
		if(fileImg && oldImg.img) {
			utilsController.deleteFile(`src/app/public/img/alertas/${oldImg.img}`);
		}
		res.status(200).json(alert);
	})
	.catch(err => {
		console.log('Error al modificar la alerta' + id, err);
		res.status(400).json('Error al crear la alerta');
	});
});

// Elimina una alerta
router.delete('/:id', (req, res) => {
	const id = req.params.id;

	// Elimina una alerta
	Alertas.destroy({ where: { id } })
	.then(() => {
		res.status(200).json();
	})
	.catch(err => {
		console.log(`Error al eliminar la tienda ${idtienda}`, err);
		res.status(400).json('Error al eliminar la alerta');
	});
});

/** CRUD ALERTS FINISH**/

/**
 * Publica una nueva alerta para las tiendas indicadas
 */
router.post('/publish', (req, res) => {
	// Lista de tiendas a publicar la alerta
	const shops = req.body.shops;
	// Alerta a publicar
	const alert = req.body.alert;
	// Verifica si la seleccion fue para todas las tiendas
	const isAllShopSelected = req.body.isAllShopSelected;
	// Usuario que hace la publicacion
	const user = req.session.user;
	// Tipo de publicacion
	const type = (isAllShopSelected)? 
					utilsController.getConstantByName('TIPO_PUBLICACION_ALERTA', 'all').id :
					utilsController.getConstantByName('TIPO_PUBLICACION_ALERTA', 'selected').id;
	
	// Inicio de transaccion
	models.sequelize.transaction((t) => {
		// Crea un nuevo historial de publicacion de alerta
		return HistorialAlertas.create({
			alertas_id: alert,
			encargado_id: Number(user.usuario.cedula),
			encargado_rol: user.type,
			tipo_envio: String(type)
		}, {transaction: t}).then(historialAlerta => {
			// Inicializa lista de insercion de tiendas para la relacion con historial y tiendas
			let shopsInsert = [];
			// Se recorren los ids de la tienda
			shops.forEach(shopId => {
				shopsInsert.push({tiendas_id: shopId, historial_alertas_id: historialAlerta.id});
			});
			// Se crea la relacion con tiendas e historial
			return TiendasHistorialAlertas.bulkCreate(shopsInsert, {transaction: t});
		});
	}).then(async (response) => {
		// Identificadores de tiendas e historial guardado
		const tiendasResponse = response.map((data) => data.toJSON());
		// Busca el historial de la tienda por el idntificador de alerta
		const historialAlerta = await HistorialAlertas.findOne({
			where: {
				id: tiendasResponse[0].historial_alertas_id
			},
			raw: true
		});
		// Alerta a notificar | publicar
		const alerta = await Alertas.findOne({where: {id: historialAlerta.alertas_id}});
		// Recorre todos los identifcador de las tiedas
		shops.forEach(shopId => {
			// Emite la notificacion | publicacion de alerta a todas las tiendas correspondientes
			req.io.sockets.to(shopId).emit('publish_alert', {alert: alerta});
		});
		res.status(200).json();
	}).catch(err => {
		console.log('Error al publicar una alerta', err);
		res.status(400).json();
	});
});

module.exports = router;
