
// Constantes de la aplicacion
const CONSTANTS = require('../routes/constants');
// Manejo de archivos
const fs = require('fs');

module.exports = {
    /**
     * Obtiene la constante inficada
     */
    getConstant: (constant) => {
        return CONSTANTS[constant]
    },
    /**
     * Obtiene el elemento del la lista de constantes
     * @param {*} constant 
     * @param {*} id 
     */
    getConstantById: (constant, id) => {
        return  CONSTANTS[constant][CONSTANTS[constant].findIndex(x => x.id == id)];
    },
    /**
     * Obtiene la constante por el nombre
     * @param {*} constant lista de contantes
     * @param {*} name 
     */
    getConstantByName: (constant, name) => {
        return CONSTANTS[constant][CONSTANTS[constant].findIndex(x => x.name == name)];
    },
    /**
     * Elimina un archivo
     * @param {string} path ruta completa del archivo a eliminar
     */
    deleteFile(path) {
        fs.unlink(path, (err) => {
            if (err) throw err;
            console.log('Archivo eliminado', path);
        });   
    },
    /**
     * Valida si el parametro es S = true o N = false
     * @param {*} param 
     */
    validateBoolean(param) {
        return (param === 'S') ? true : false;
    }
}