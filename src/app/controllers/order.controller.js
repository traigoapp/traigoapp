// Permite manejar la informacion de las rutas
const router = require('express').Router();

const bcrypt = require('bcrypt');

// Acceso a modelos de sequelize
const models = require('../models');

const Clientes = models.clientes; // new require for db object clientes
const ClienteDireccion = models.cliente_direccion; // new require for db object cliente direccion
const Costos = models.costos; // new require for db object costos
const Direcciones = models.direcciones; // new require for db object direcciones
const PedidosDeseos = models.pedidos_deseos; // new require for db object pedidos deseos
const Municipios = models.municipios; // new require for db object pedidos deseos

/**
 * Crea un nuevo pedido deseo
 */
router.post('/order-desire', (req, res) => {

    console.log(req.body);
    const orderDesire = req.body;

    // Inicio de transaccion
	models.sequelize.transaction((trans) => {
        
        // Crea los datos de la nueva direccion
        return Direcciones.create({
            direccion: orderDesire.address.description,
            idmunicipio: orderDesire.address.cityId,
            notas: orderDesire.address.notes,
            latitude: orderDesire.address.position.lat,
            longitude: orderDesire.address.position.lng
        }, {transaction: trans})
        .then(async (address) => {
            let email;
            // Valida si el cliente es para crear
            if (orderDesire.input) {
                // Encripta la contrasena basado en el telefono
                const password = bcrypt.hashSync(orderDesire.input.telefono, 10);
                // Asigna correo a ingresar
                email = orderDesire.input.correo;
                // Crea un nuevo cliente
                await Clientes.create({
                    correo: email,
                    nombre: orderDesire.input.nombre,
                    telefono: orderDesire.input.telefono,
                    contrasenia: password,
                }, {transaction: trans});
            } else {
                // Asigna correo seleccionado
                email = orderDesire.selected.correo;
            }

            // Agrega la direccion a el cliente
            ClienteDireccion.create({
                iddireccion: address.iddireccion,
                correocliente: email
            }, {transaction: trans});

            // Busca costos por ciudad
            const cost = await Costos.findOne({ where: { idmunicipio: orderDesire.cityIdCost }, raw: true });

            // Crea un nuevo pedido deseo
            return PedidosDeseos.create({
                costo_envio: cost.costo_envio_deseo,
                comision_cliente: cost.comision_cliente_deseo,
                fecha: new Date(),
                estado: 'En proceso',
                iddireccion: address.iddireccion,
                deseo: orderDesire.description,
                comision_entregada: 'N',
                correocliente: email
            }, {transaction: trans});

        }).then((response) => {
            res.status(200).json(response);
        }).catch(err => {
            console.log('Error al crear un pedido deseo', err);
            res.status(400).json('Error al crear un pedido deseo');
        });
        
    });

});

/**
 * Obtiene las ciudades que tienen costos asignados
 */
router.get('/get-city-costs', (req, res) => {

    // Obtiene los municipios que tienen costos asignados
    models.sequelize.query('SELECT m.idmunicipios AS id, m.nombre AS name FROM costos c JOIN municipios m ON c.idmunicipio = m.idmunicipios',
    {type: models.sequelize.QueryTypes.SELECT })
    .then((cities) => {
        res.status(200).json(cities);
    })
    .catch((err) => {
        console.log('Error al obtner las ciudades.', err);
        res.status(400).json('Error al obtner las ciudades.');
    });
    

});

/**
 * Obtiene los clientes registrados
 */
router.get('/get-clients', (req, res) => {

    const findEmail = req.query.email;

    // Operaciones de tabla
    let op = models.Sequelize.Op;

    // Obtiene todos los clientes
	Clientes.findAll({
        attributes: ['correo', 'nombre', 'telefono'],
        where : {
            correo: {
                [op.like]: `${findEmail}%`
            }
        }
    })
	.then((clients) => {
		console.error('-----Clientes', clients);
		
		res.status(200).json({clients});
	})
	.catch((err) => {
		console.log('Error al obtener clientes', err);
		res.status(400).json('Error al obtener todas los clientes');
	});    

});


module.exports = router;
