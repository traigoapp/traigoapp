// Permite manejar la informacion de las rutas
const router = require('express').Router();
// Controlador de utilidades generales de la aplicacion
const utilsController = require('./utils.controller');
// Acceso a modelos de sequelize
const models = require('../models');
// Modelo de alertas
const Tiendas = models.tiendas;
// Manejo de archivos
const fs = require('fs');

// Obtiene todas las tiendas
router.get('/all', (req, res) => {
    // Consulta las tiendas disponibles
    Tiendas.findAll().then(tiendas => {
        res.status(200).json({tiendas, tipoSucursales: utilsController.getConstant('TIPO_SUCURSAL')});
    }).catch(err => {
        console.log('Error al obtener alertas', err);
		res.status(400).json('Error al obtener todas las alertas');

    });
});


// Visualiza el listado completo de tiendas
router.get('/', (req, res) => {

	const info = req.query.info;
    // Consulta las tiendas disponibles
	Tiendas.findAll().then(tiendas => {
		res.render('admin/pages/shops/index', {
			tiendas,
			info
		});
	}).catch(err => {
		console.log(err);
		res.redirect('/administrador');
	});
});

// Visualiza el listado completo de tiendas
router.get('/create', (req, res) => {
	res.render('admin/pages/shops/create');
});

// Visualiza el formulario para la edicion de la tienda
router.get('/:id/edit', (req, res) => {

	let info = req.query.info;
	let idtienda = req.params.id;

	// Busca la tienda por el id
	Tiendas.findOne({
		where: {
			idtienda
		}
	})
	.then(tienda => {
		res.render('admin/pages/shops/edit', {
			tienda,
			info
		});
	})
	.catch(err => {
		console.log(err);
		res.redirect('/administrador');
	});

});

// Crea una nueva tienda
router.post('/', (req, res) => {
	let nombre = req.body.nombre;
	let descripcion = req.body.descripcion;

	let foto = req.files.imagen;
	let imagen= 'default.png';

	direccionImg = 'src/app/public/img/tienda/default.png';
	
	if (foto.name != '') {
		let extension = foto.name.split('.').pop();
		let direccionImg = 'src/app/public/img/tienda/' + nombre + '.' + extension;
		fs.renameSync(foto.path, direccionImg);
		imagen = nombre + '.' + extension;
	}

	// Crea una nueva tienda
	Tiendas.create({
		nombre,
		descripcion,
		imagen
	})
	.then(() => {
		res.redirect('/administrador/tiendas?info=correct');
	})
	.catch(err => {
		console.log('Error al crear una tienda', err);
		res.redirect('/administrador/tiendas?info=err');
	});

});

// Actualiza una tienda existente
router.put('/:id', (req, res) => {
	let idtienda = req.params.id;
	let nombre = req.body.nombre;
	let descripcion = req.body.descripcion;
	let imagenanterior = req.body.imagenanterior;

	let foto = req.files.imagen;
	let name = imagenanterior;

	let direccionImg = 'src/app/public/img/tienda/default.png';

	if (foto.name != '') {
		let extension = foto.name.split('.').pop();
		direccionImg = 'src/app/public/img/tienda/' + nombre + '.' + extension;
		fs.renameSync(foto.path, direccionImg);
		name = nombre + '.' + extension;
	}

	// Busca una tienda por el id
	Tiendas.findByPk(idtienda)
	.then(tienda => {
		// Verifica si la tienda existe
		if (!tienda) {
			console.log('Tienda no encontrada');
			return res.redirect('/administrador/tiendas/'+ idtienda +'/edit?info=err');
		}

		tienda.nombre = nombre;
		tienda.descripcion = descripcion;
		tienda.imagen = name;

		// Actualiza los datos de la tienda
		return tienda.save();
	})
	.then(() => {
		res.redirect('/administrador/tiendas?info=correct');
	})
	.catch(err => {
		console.log(`Error al actualizar la tienda ${idtienda}`, err);
		res.redirect(`/administrador/tiendas/${idtienda}/edit?info=err`);
	});

});

/**
 * Elimina una tienda
 */
router.get('/delete/:id', (req, res) => {
	let idtienda = req.params.id;

	// Elimina una tienda
	Tiendas.destroy({
		where: { 
			idtienda
		} 
	})
	.then(() => {
		res.redirect('/administrador/tiendas?info=eliminated');
	})
	.catch(err => {
		console.log(`Error al eliminar la tienda ${idtienda}`, err);
		res.redirect('/administrador/tiendas?info=err');
	});

});


module.exports = router;
