module.exports = {
    /**
     * Obtiene la constante por el nombre
     */
    getConstant(constant, name) {
        return constant[constant.findIndex(x => x.name == name)];
    }
};

