module.exports = {
    TIPO_SUCURSAL: [
        { id: 1, name: 'Normal' },
        { id: 2, name: 'Especial' },
        { id: 3, name: 'Prepagada' }
    ],
    TIPO_MALETIN: [
        { id: null, name: 'Cualquier tamaño', description: '' },
        { id: 1, name: 'Grande', description: '42 X 42 X 42' },
        { id: 2, name: 'Mediano', description: '32 X 32 X 48' },
        { id: 3, name: 'Pequeño', description: '26 X 26 X 40' }
    ],
    TIPO_CONTRATO: [
        { id: 1, name: 'Normal' },
        { id: 2, name: 'Solo empresarial' },
        { id: 3, name: 'Solo tienda' }
    ],
    TIPO_PLAN: [
        { id: 1, name: 'Por defecto' },
        { id: 2, name: 'Auxilio' },
        { id: 3, name: 'Ilimitado' }
        // { id: 4, name: 'Personalizado' },
    ],
    ESTADO_TIENDAS_PLANES: [ // Estados de planes de tienda
        { id: 1, name: 'Activo' },
        { id: 2, name: 'Inactivo'}, // Finalizado 
        { id: 3, name: 'Pendiente' },
        { id: 4, name: 'Cancelado' }
    ],
    ESTADO_HISTORIAL_PLANES: [ // Estados de historial de planes de cada tienda
        { id: 1, name: 'En curso' },
        { id: 2, name: 'Finalizado' },
        { id: 3, name: 'Pendiente' },
        { id: 4, name: 'Cancelado' }
    ],
    TIPO_ALERTA: [ // Tipos de alerta
        { id: 1, name: 'warning', description: 'Advertencia' },
        { id: 2, name: 'info', description: 'Información' },
        { id: 3, name: 'promo', description: 'Promoción' }
    ],
    TIPO_PUBLICACION_ALERTA: [ // Estados de publicacion de alertas en el historial de alertas
        { id: 1, name: 'all', description: 'Todas las tiendas' },
        { id: 2, name: 'selected', description: 'Tiendas en especifico' },
        { id: 3, name: 'type', description: 'Por tipo de tienda' },
    ]

}
