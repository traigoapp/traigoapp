const express = require('express');
const router = express.Router();
const fs = require('fs');
const nodemailer = require('nodemailer');
require('dotenv').config();
const jwt = require('jsonwebtoken');

const bcrypt = require('bcrypt');
const Constants = require('./constants');

const Database = require('../../config/database');
let db = new Database(); // Instancia de base de datos promise
const connection = db.getConnection();

let urlSocket = 'http://localhost:8002'

// Valida el tipo de entorno para saber la url del socket
if (process.env.NODE_ENV === 'production') {
	urlSocket = 'https://admin.traigo.com.co'; 
} else if (process.env.NODE_ENV === 'test') {
	urlSocket = 'http://traigo.com.co:8002'; 
}

const io = require('socket.io-client')(urlSocket);
//const io = require('socket.io-client')('https://admin.traigo.com.co');

const models = require('../models');
const Ajustes = models.ajustes; // new require for db object ajustes
const PedidosEmpresariales = models.pedidos_empresariales; // new require for db object direcciones
const Sucursales = models.sucursales; // new require for db object sucursales
const Riders = models.riders; // new require for db object riders
const Direcciones = models.direcciones; // new require for db object direcciones
const Configuraciones = models.configuraciones; // new require for db object configuraciones
const Planes = models.planes; // new require for db object planes
const HistorialTiendasPlanes = models.historial_tiendas_planes; // new require for db object historial tiendas planes
const TiendasPlanes = models.tiendas_planes; // new require for db object tiendas planes

router.get('/encrptr', (req, res) => {
	let data = req.query.data;
	let hash = bcrypt.hashSync(data, 10);
	res.send(hash);
});

/**
 * Permite encriptar las contraseñas de las tablas
 * RIDERS, SUCURSALES, MODERADORES, ADMINISTRADORES y CLIENTES
 * 
 * @param req	parametros
 * @param res	opciones de respuesta
 */
router.get('/encriptar', (req, res) => {
	let promises = [];
	let namePromises = [];
	
	// Verifica la existencia del query params asocuado a la tabña deseada a converir
	if (req.query.r) {
		promises.push(db.query('SELECT * FROM riders'));
		namePromises.push({ name: 'riders', type: 'cedula' });
	}
	if (req.query.s) {
		promises.push(db.query('SELECT * FROM sucursales'));
		namePromises.push({ name: 'sucursales', type: 'idsucursal' });
	}
	if (req.query.m) {
		promises.push(db.query('SELECT * FROM moderadores'));
		namePromises.push({ name: 'moderadores', type: 'cedula' });
	}
	if (req.query.a) {
		promises.push(db.query('SELECT * FROM administradores'));
		namePromises.push({ name: 'administradores', type: 'cedula' });
	}
	if (req.query.c) {
		promises.push(db.query('SELECT * FROM clientes'));
		namePromises.push({ name: 'clientes', type: 'correo' });
	}

	// Verifica si no viene query params, se agregan todos para la encriptacion
	if (Object.keys(req.query).length === 0) {
		promises.push(db.query('SELECT * FROM riders'));
		namePromises.push({ name: 'riders', type: 'cedula' });

		promises.push(db.query('SELECT * FROM sucursales'));
		namePromises.push({ name: 'sucursales', type: 'idsucursal' });

		promises.push(db.query('SELECT * FROM moderadores'));
		namePromises.push({ name: 'moderadores', type: 'cedula' });

		promises.push(db.query('SELECT * FROM administradores'));
		namePromises.push({ name: 'administradores', type: 'cedula' });

		promises.push(db.query('SELECT * FROM clientes'));
		namePromises.push({ name: 'clientes', type: 'correo' });
	}

	// Ejecuta las consultas agregadas
	Promise.all(promises)
	.then( values => { 
		for (let i = 0; i < values.length; i++) {
			const element = values[i];
			let cond = namePromises[i].type;
			let table = namePromises[i].name;

			for (let j = 0; j < element.length; j++) {
				const item = element[j];
				let pass = item.contrasenia;
				let condRes = item.cedula;

				if (cond === 'idsucursal') {
					condRes = item.idsucursal;
				} else if (cond === 'correo') {
					condRes = item.correo;
				}

				// Crea el hash del password
				let hash = bcrypt.hashSync(pass, 10);
				db.query('UPDATE '+ table +' SET contrasenia = ? WHERE '+ cond +' = ?', [hash, condRes])
				.catch(err => {
					console.log(err);
					res.send(err);
				});
			}
		}
		console.log('Termina encriptacion');
		res.send('Termina encriptacion');
	})
	.catch( err => {
		console.log(err);
		res.send(err);
	});

});

/**
 * Actuaiza las configuraciones adicionales en la actualizacion del software
 */
router.get('/configuraciones', (req, res) => {

	// Lista de promesas
	let promises = [];
	// Actualiza el tipo de sucursal a normal = 1
	promises.push(db.query('UPDATE sucursales SET tipo = ?', [1]));

	// Ejecuta las promesas
	Promise.all(promises)
	.then(() => {
		console.log('Termina actualizacion de tipo sucursales');
		res.send('Termina actualizacion de tipo sucursales');
	})
	.catch(err => {
		console.log(err);
		res.send(err);
	});
	
});


/**
 * Obtiene el estado del rider por cedula
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns envia estado y la lista negra
 */
router.get('/obtenerestadorider', (req, res) => {
	let cedula = req.query.cedula;
	connection.query('select * from riders where cedula = ?',
		[cedula], (err, rows, fields) => {
			let rider = rows[0];
			if (err) {
				res.send(err);
			} else {
				let listanegra = false;
				if (rider.listanegra == 'S') {
					listanegra = true;
				}

				if (rider.activo == 'S') {
					res.send({
						estado: true,
						listanegra: listanegra
					});
				} else {
					res.send({
						estado: false,
						listanegra: listanegra
					});
				}
			}
		})
});

/**
 * Actualza la ubicacion actual de rider
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns error o correcto
 */
router.post('/actualizarcoordenadasrider', (req, res) => {
	let cedula = req.body.cedula;
	let latitude = req.body.latitude;
	let longitude = req.body.longitude;

	connection.query('update riders set latitude = ?, longitude = ? where cedula = ?',
		[latitude, longitude, cedula], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(err);
			} else {
				res.send({
					okey: 'okey'
				});
			}
		});
});

/**
 *  Login riders
 */
router.post('/login', (req, res) => {
	
	let user =  req.body.username;
	let pass =  req.body.password;

	if (user === 'undefined' || pass === 'undefined') {
		return res.status(402).json({error: 'Datos incompletos'});
	}

	db.query('SELECT * FROM riders WHERE cedula = ?', [user])
		.then(rows => {	// Datos del rider
			// Verifica si no se encuentra el rider
			if (rows.length === 0) {
				res.status(401).json({error: 'No está registrado en nuestra plataforma.'});
			}

			const user = rows[0];
			// Se valida si la contraseña es correcta
			if (bcrypt.compareSync(pass, user.contrasenia)) {
				// Se verifica si el rider está en lista negra
				if (user.listanegra === 'S') {
					res.status(403).json({error: 'No está habilitado para usar esta aplicación', message: user});
				}
				// Se crea el token con los datos de usuario
				jwt.sign({user}, process.env.PRIVATE_JWT_KEY, (err, token) => {
					if (err) { // Error al crear el token
						res.status(400).json({error: 'Error al crear el token'});
					}
					// Se envía el token creado
					user.token = token;
					res.status(200).json({message: user});
				});
			} else { // Error cuando la contraseña no coincide
				res.status(402).json({error: 'Contraseña invalida'});
			}
		}).catch(err => { // Error al obtener el rider
			console.log(err);
			res.status(400).json({error: err});
		});
});


/**
 * Inicio de session para el cliente
 */
router.post('/logincliente', (req, res) => {
	let user = req.body.username;
	let pass = req.body.password;

	if (user === 'undefined' || pass === 'undefined') {
		return res.status(402).json({error: 'Datos incompletos'});
	}

	db.query('SELECT * FROM clientes WHERE correo = ?', [user])
	.then(rows => {
		// Verifica si no se encuentra el cliente
		if (rows.length === 0) {
			res.status(401).json({error: 'No está registrado en nuestra plataforma.'});
		} else {
			const user = rows[0];
			// Se valida si la contraseña es correcta
			if (bcrypt.compareSync(pass, user.contrasenia)) {
				res.status(200).json({message: user});	
			} else { // Error cuando la contraseña no coincide
				res.status(402).json({error: 'Contraseña invalida'});
			}
		}
	}).catch(err => { // Error al obtener el cliente
		console.log(err);
		res.status(400).json({error: err});
	});
	
});


/**
 * Ejemplo de uso de recursos con token
 */
router.get('/exampleToken', verifyToken, (req, res) => {
	jwt.verify(req.token, process.env.PRIVATE_JWT_KEY, (err, authData) => {
		if (err) {
			res.sendStatus(403);
		} else {
			res.json({
				msm: 'Oki Doki',
				authData
			});
		}
	});
});

/**
 * Middleware para verificar el token enviado
 * 
 * Authotization: Bearer <token>  --Expresion del token [HEADER]
 * 
 * https://www.npmjs.com/package/jsonwebtoken
 * 
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
function verifyToken (req, res, next) {
	// Get auth token header value
	const bearerHeader = req.headers['authorization'];
	// Check if bearer is undefined
	if (typeof bearerHeader !== 'undefined') {
		// Split token space
		const bearer = bearerHeader.split(' ');
		// get token
		const token = bearer[1];
		// Send token
		req.token = token;
		// Next middleware
		next();
	} else {
		// Forbidden
		res.sendStatus(403);
	}

}

/**
 * Busca y obtiene el rider por la cedula (id)
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns envia los datos del rider
 */
router.get('/obtenerrider', (req, res) => {
	let usuario = req.query.cedula;

	connection.query('select * from riders where cedula = ?', [usuario], (err, rows, fields) => {
		if (err) {
			res.send(err);
		} else {
			res.send(rows);
		}
	});
});

/**
 * Cambia el estado del rider [Activo] - [Inactivo]
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns error o correcto
 */
router.post('/cambiarestadorider', (req, res) => {
	let cedula = req.body.cedula;
	let estado = req.body.estado;

	connection.query('update riders set activo = ? where cedula = ?',
		[estado, cedula], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(err);
			} else {
				res.send({
					okey: 'okey'
				});
			}
		});
});

/**
 * Obtiene todos los productos de la tienda
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns prodcutos del tienda
 */
router.get('/obtenerproductos', (req, res) => {
	let idsucursal = req.query.tienda;
	connection.query('select * from productos p where p.idsucursal = ? and p.activo = ? and p.stock = ?',
		[idsucursal, 'S', 'S'], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(err);
			} else {
				res.send(rows);
			}
		});
});

/**
 * Obtiene las categorias de los productos
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns ecategorias de preductos
 */
router.get('/obtenercategoriasproductos', (req, res) => {
	connection.query('select * from categoriasproductos', (err, rows, fields) => {
		if (err) {
			console.log(err);
			res.send(err);
		} else {
			res.send(rows);
		}
	});
});

/**
 * Obtiene las tiendas disponibles
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns lista de tiendas
 */
router.get('/obtenertiendas', (req, res) => {
	let idcategoria = req.query.categoria;

	connection.query('SELECT s.*, (s.hora_inicial <= current_time() ' +
		'AND s.hora_final > current_time()) AS abierto, t.imagen AS imagen ' +
		'FROM sucursales s JOIN tiendas t ON t.idtienda = s.idtienda ' +
		'WHERE idcategoria = ? AND soloempresarial = ?',
		[idcategoria, 'N'], (err, rows, fields) => {
			if (err) {
				res.send(err);
			} else {
				res.send(rows);
			}
		});
})

function agregarImagenesDeseo(i, foto, idpedido, next) {
	if (foto.name != '') {
		let extension = foto.name.split('.').pop();
		let direccionImg = 'src/app/public/img/pedidosdeseos/' + idpedido + '-' + i + '.' + extension;
		fs.renameSync(foto.path, direccionImg);
		name = idpedido + '-' + i + '.' + extension;
		connection.query('insert into fotos (idpedido_deseo, foto) values(?, ?)',
			[idpedido, name], (err, rows, fields) => {
				if (err) {
					next(err);
				} else {
					next();
				}
			});
	}
}

function agregarImagenesMandado(i, foto, idpedido, next) {
	if (foto.name != '') {
		let extension = foto.name.split('.').pop();
		let direccionImg = 'src/app/public/img/pedidosmandados/' + idpedido + '-' + i + '.' + extension;
		fs.renameSync(foto.path, direccionImg);
		name = idpedido + '-' + i + '.' + extension;
		connection.query('insert into fotos (idpedido_encomienda, foto) values(?, ?)',
			[idpedido, name], (err, rows, fields) => {
				if (err) {
					next(err);
				} else {
					next();
				}
			});
	}
}

function agregarParada(parada, idpedido, costoparada, next) {
	connection.query('insert into paradas (iddireccion, costo, idpencomienda) values(?,?,?)',
		[parada.iddireccion, costoparada, idpedido], (err, rows, fields) => {
			next(err);
		});
}

/**
 * Permite crear un pedido mandado
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns error o correcto
 */
router.post('/realizarpedidomandado', (req, res) => {
	let images = req.files;
	let paradas = JSON.parse(req.body.paradas);

	let dirOrigen = JSON.parse(req.body.direccionOrigen);
	let dirDestino = JSON.parse(req.body.direccionDestino);

	let costo_envio = req.body.costo_envio;
	let iddireccionorigen = dirOrigen.iddireccion;
	let iddirecciondestino = dirDestino.iddireccion;
	let comision_rider = req.body.comision_rider;
	let mandado = req.body.mandado;
	let correo = req.body.correo;
	let costoparada = req.body.costoparada;

	let imageslength = req.body.imageslength;
	connection.query('insert into pedidos_encomiendas (costo_envio, estado, iddireccionorigen, iddirecciondestino, comision_rider, comision_entregada, mandado, correocliente, fecha) values(?,?,?,?,?,?,?,?,?)',
		[costo_envio, 'En proceso', iddireccionorigen, iddirecciondestino, comision_rider, 'N', mandado, correo, new Date()], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(err);
			} else {
				let idpedido = rows.insertId;
				for (let i = 0; i < imageslength; i++) {
					agregarImagenesMandado(i, images['picture' + i], idpedido, (err) => {
						if (err) {
							console.log(err);
							return res.send(err);
						}
					});
				}
				for (let j = 0; j < paradas.length; j++) {
					agregarParada(paradas[j], idpedido, costoparada, (err) => {
						if (err) {
							console.log(err);
							return res.send(err);
						}
					});
				}
				res.send({
					okey: 'yes'
				});
			}
		});
});


/**
 * Permite crear un pedido deseo
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns error o correcto
 */
router.post('/realizarpedidodeseo', (req, res) => {
	let images = req.files;
	let imageslength = req.body.imageslength;
	let correo = req.body.correo;
	let costo_envio = req.body.costo_envio;
	let comision = req.body.comision;
	let deseo = req.body.deseo;
	let direccion = JSON.parse(req.body.direccion);

	connection.query('insert into pedidos_deseos (costo_envio, comision_cliente, fecha, estado, iddireccion, deseo, comision_entregada, correocliente) values(?,?,?,?,?,?,?,?)',
		[costo_envio, comision, new Date(), 'En proceso', direccion.iddireccion, deseo, 'N', correo], (err, rows, fields) => {
			if (err) {
				res.send(err);
			} else {
				for (let i = 0; i < imageslength; i++) {
					agregarImagenesDeseo(i, images['picture' + i], rows.insertId, (err) => {
						if (err) {
							console.log(err);
						}
					});
				}
				res.send({
					okey: 'yes'
				});
			}
		});
});

/**
 * Obtiene los costos de traigo
 * TODO falta configurar en la aplicacion de riders, obtner por ciudad
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns error o correcto
 */
router.get('/obtenercostos', (req, res) => {
	let idmunicipio = req.query.idmunicipio;
	let iddireccion = req.query.iddireccion;

	if (idmunicipio) {
		connection.query('select * from costos where idmunicipio = ?',[idmunicipio], (err, rows, fields) => {
			if (err) {
				res.send(err);
			} else if (rows[0]) {
				res.send(rows[0]);
			} else {
				getCosts(req, res);
			}
		});
	} else if (iddireccion) {
		connection.query('select idmunicipio from direcciones where iddireccion = ?',[iddireccion], (err, municipio, fields) => {
			if (err) {
				res.send(err);
			} else {
				if (municipio.length === 0) {
					return res.send("Datos erroneos 2");
				}
				connection.query('select * from costos where idmunicipio = ?',[municipio[0].idmunicipio], (err, rows, fields) => {
					if (err) {
						res.send(err);
					} else if (rows[0]) {
						res.send(rows[0]);
					} else {
						getCosts(req, res);
					}
				});
			}
		});
	} else {
		getCosts(req, res);
	}
	
});

// Obtiene los primeros costos es decir el de armenia
function getCosts (req, res) {
	connection.query('select * from costos', (err, rows, fields) => {
		if (err) {
			res.send(err);
		} else {
			res.send(rows[0]);
		}
	});
}

/**
 * Permite gregar una nueva dirección
 * TODO verificar agregar direccion son quemar el municipio
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns error o correcto
 */
router.post('/agregardireccion', (req, res) => {

	let correo = req.body.correo;
	let latitude = req.body.latitude;
	let longitude = req.body.longitude;
	let direccion = req.body.direccion;
	let notas = req.body.notas;
	let municipio = req.body.municipio;

	connection.query('insert into direcciones (idmunicipio, latitude, longitude, direccion, notas) values(?, ?,?,?,?)',
		[municipio, latitude, longitude, direccion, notas], (err, rows, fields) => {
			if (err) {
				res.send(err);
			} else {
				let iddireccion = rows.insertId;
				connection.query('insert into cliente_direccion (iddireccion, correocliente) values(?, ?)',
					[iddireccion, correo], (err, rows, fields) => {
						if (err) {
							console.log(err);
							res.send(err);
						} else {
							connection.query('select * from direcciones d join cliente_direccion cd on d.iddireccion = cd.iddireccion where cd.correocliente = ?', [correo], (err, rows, fields) => {
								if (err) {
									res.send(err);
								} else {
									res.send(rows);
								}
							});
						}
					});
			}
		});
});

/**
 * Obtiene todas las categorias de la tienda
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns lista de categorias
 */
router.get('/obtenercategorias', (req, res) => {
	connection.query('select * from categorias', (err, rows, fields) => {
		if (err) {
			res.send(err);
		} else {
			res.send(rows);
		}
	});
});

/**
 * Obtiene la direccion del cliente por el correo (id cliente)
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns direccion del cliente
 */
router.get('/obtenerdireccionescliente', (req, res) => {
	let correo = req.query.correo;
	connection.query('select * from direcciones d join cliente_direccion cd on d.iddireccion = cd.iddireccion where cd.correocliente = ?',
		[correo], (err, rows, fields) => {
			if (err) {
				res.send(err);
			} else {
				res.send(rows);
			}
		});
});

/**
 * Obtiene lista de pedidos en curso
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns lsita de pedidos en curso
 */
router.get('/obtenerpedidosencurso', (req, res) => {

	let correo = req.query.usuario;

	let deseos, pedidos, mandados;
	connection.query('select pd.*, d.direccion as direccionentrega, d.notas as notasdireccionentrega ' +
		'from pedidos_deseos pd join direcciones d on d.iddireccion = pd.iddireccion join clientes c ' +
		'on c.correo = pd.correocliente where pd.estado != ? and pd.estado != ? ' +
		'and pd.estado != ? and c.correo = ? order by pd.fecha desc',
		['Entregado', 'Cancelado', 'Error', correo], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(err);
			} else {
				deseos = rows;
				for (let i = 0; i < deseos.length; i++) {
					deseos[i].type = 'deseo';
				}
				connection.query('select pd.*, c.*, do.direccion as direccionorigen, do.notas as notasdireccionorigen, ' +
					'do.latitude as latitudedireccionorigen, do.longitude as longitudedireccionorigen, dd.direccion ' +
					'as direcciondestino, dd.notas as notasdirecciondestino, dd.latitude as latitudedirecciondestino, ' +
					'dd.longitude as longitudedirecciondestino from pedidos_encomiendas pd join direcciones do ' +
					'on do.iddireccion = pd.iddireccionorigen join direcciones dd on dd.iddireccion = pd.iddirecciondestino ' +
					'join clientes c on c.correo = pd.correocliente where pd.estado != ? and pd.estado != ? ' +
					'and pd.estado != ? and c.correo = ? order by pd.fecha desc',
					['Entregado', 'Cancelado', 'Error', correo], (err, rows, fields) => {
						if (err) {
							console.log(err);
							res.send(err);
						} else {
							mandados = rows;
							for (let i = 0; i < mandados.length; i++) {
								mandados[i].type = 'mandado';
							}
							connection.query('select pd.*, c.*, d.*, ss.idsucursal as idsucursal, ss.nombre as nombresucursal, ' +
								'ss.tiempo_entrega as tiempo_entrega, count(p.idpclientes_productos) as cantidadproductos ' +
								'from pedidos_clientes pd join clientes c on c.correo = pd.correocliente join direcciones d ' +
								'on d.iddireccion = pd.iddireccion join pclientes_productos p on pd.idpedido_clientes = p.idpedido_clientes ' +
								'join productos pro on pro.idproducto = p.idproducto join sucursales ss on ss.idsucursal = pro.idsucursal ' +
								'where pd.estado != ? and pd.estado != ? and pd.estado != ? and c.correo = ? ' +
								'group by pd.idpedido_clientes, ss.idsucursal, ss.nombre, ss.tiempo_entrega order by pd.fecha desc',
								['Entregado', 'Cancelado', 'Error', correo], (err, rows, fields) => {
									if (err) {
										console.log(err);
										res.send(err);
									} else {
										pedidos = rows;
										for (let i = 0; i < pedidos.length; i++) {
											pedidos[i].type = 'tienda';
										}
										let todos = deseos.concat(pedidos, mandados);

										for (let i = 0; i < (todos.length - 1); i++) {
											for (let j = 0; j < (todos.length - i - 1); j++) {
												if (todos[j].fecha < todos[j + 1].fecha) {
													k = todos[j + 1];
													todos[j + 1] = todos[j];
													todos[j] = k;
												}
											}
										}
										res.send(todos);
									}
								});
						}
					});
			}
		});
});

/**
 * Obtiene todo los pedidos que estan en proceso
 * RIDERS
 * Filtrar por tipo de maletin, tipo de sucursal pedidos de la misma surucrsal especial
 * 
 * 
 */
router.get('/obtenerpedidosenproceso', async (req, res) => {

	let appcode = req.query.appcode;
	let idrider = req.query.rider;
	//	let iddirection = req.query.direction;

	/*if (appcode != 9) {
		return res.send({ code: 1, err: 'Ups! Debes actualizar la aplicación para tomar pedidos' });
	}*/

	let dataRider;

	Riders.findByPk(idrider).then(rider => {
		dataRider = rider;
		return Direcciones.findByPk(rider.iddireccion);
	}).then(async address => {
		// Verifica si existe la direccion
		if (!address) {
			console.log('Error direccion', idrider);
			res.send({ code: 1, err: 'Datos erroneos' });
		}

		let deseos, pedidos, mandados, empresariales;

		const conditions = ['Entregado', 'Cancelado', 'Error', address.idmunicipio];

		deseos = models.sequelize.query(`select pd.*, d.direccion as direccionentrega, d.notas as notasdireccionentrega, 
			d.latitude as latitudeentrega, d.longitude as longitudeentrega from pedidos_deseos pd 
			join direcciones d on d.iddireccion = pd.iddireccion where pd.estado != ? and pd.estado != ?
			and pd.estado != ? and d.idmunicipio = ? and isnull(idrider) order by pd.fecha desc`,
			{ replacements: conditions, type: models.sequelize.QueryTypes.SELECT });

		mandados = models.sequelize.query(`select pd.*, do.direccion as direccionorigen, do.notas as notasdireccionorigen, 
			do.latitude as latitudedireccionorigen, do.longitude as longitudedireccionorigen, dd.direccion as direcciondestino, 
			dd.notas as notasdirecciondestino, dd.latitude as latitudedirecciondestino, dd.longitude as longitudedirecciondestino 
			from pedidos_encomiendas pd join direcciones do on do.iddireccion = pd.iddireccionorigen
			join direcciones dd on dd.iddireccion = pd.iddirecciondestino where pd.estado != ? and pd.estado != ?
			and pd.estado != ? and dd.idmunicipio = ? and isnull(idrider) order by pd.fecha desc`, 
			{ replacements: conditions, type: models.sequelize.QueryTypes.SELECT });

		pedidos = models.sequelize.query(`select pd.*, c.*, d.*, ss.idsucursal as idsucursal, ss.nombre as nombresucursal, 
			ss.tiempo_entrega as tiempo_entrega, count(p.idpclientes_productos) as cantidadproductos 
			from pedidos_clientes pd join clientes c on c.correo = pd.correocliente join direcciones d 
			on d.iddireccion = pd.iddireccion join pclientes_productos p on pd.idpedido_clientes = p.idpedido_clientes 
			join productos pro on pro.idproducto = p.idproducto join sucursales ss on ss.idsucursal = pro.idsucursal 
			where pd.estado != ? and pd.estado != ? and pd.estado != ? and d.idmunicipio = ? and isnull(idrider) group by pd.idpedido_clientes, 
			ss.idsucursal, ss.nombre, ss.tiempo_entrega order by pd.fecha asc`,
			{ replacements: conditions, type: models.sequelize.QueryTypes.SELECT });

			
		let limit_orders = await Configuraciones.findAll().then(config => config[0].limit_business_orders);

		conditions.push(limit_orders);

		empresariales = models.sequelize.query(`select pd.*, s.tipo as tipoSucursal, s.nombre as nombresucursal, s.numero_punto as numerosucursal, 
			s.retenido as retiene, s.tiempo_entrega as tiempo_entrega, s.costo_envio as costoenvio, 
			ds.iddireccion as iddireccionsucursal, ds.direccion as direccionsucursal, ds.notas as notasdireccionsucursal, 
			ds.latitude as latitudesucursal, ds.longitude as longitudesucursal, d.iddireccion as iddireccionentrega, 
			d.direccion as direccionentrega, d.notas as notasdireccionentrega from pedidos_empresariales pd 
			join direcciones d on d.iddireccion = pd.iddireccion join sucursales s on s.idsucursal = pd.idsucursal 
			join direcciones ds on ds.iddireccion = s.iddireccion where pd.estado != ? and pd.estado != ? 
			and pd.estado != ? and ds.idmunicipio = ? and isnull(idrider) order by pd.fecha asc limit ?`, 
			{ replacements: conditions, type: models.sequelize.QueryTypes.SELECT });

		return Promise.all([deseos, mandados, pedidos, empresariales]);

	}).then( async result => {
		let sucursal;

		// Se obtienen todos los pedidos del rider
		await getOrdersRider(idrider)
		.then( orders => {
			// Pedidos empresariales
			const empresariales = orders[3];
			for (let i = 0; i < empresariales.length; i++) {
				// Verifica si es una sucursal Especial
				if (empresariales[i].tipoSucursal === Constants.TIPO_SUCURSAL[1].id) {
					sucursal = empresariales[i].idsucursal;
				}
			}
		});

		const orderType = ['deseo', 'mandado', 'tienda', 'empresariales'];
		let todos = [];

		// Validamos si hay pedidos tomads con la sucursal tipo 2
		if (sucursal) {
			let order = result[3]; // Solo pedidos empresariales
			for (let j = 0; j < order.length; j++) {
				order[j].type = orderType[3];
				// Verificamos si la sucursal de la orden tomada es igual
				// a la sucursal de las ordenes en curso
				if (sucursal === order[j].idsucursal) {
					todos.push(order[j]);
				}
			}
		} else {
			// Recorremos todos los pedidos disponibles y retornamos todos en orden descendente
			for (let i = 0; i < result.length; i++) {
				let order = result[i];
				for (let j = 0; j < order.length; j++) {
					order[j].type = orderType[i];
					// se verifica si el tipo de pedido es empresarial
					if (order[j].type === 'empresariales') {
						// Verificamos si la orden solicita un maletin en especifico
						if (order[j].tamanio_maletin) {
							// Verificamos si el rider tiene el maletin solicitado
							if (order[j].tamanio_maletin === dataRider.tamanio_maletin) {
								todos.push(order[j]);	
							}
						} else {
							todos.push(order[j]);
						}
					} else {
						todos.push(order[j]);
					}
				}
			}
		}
		// Ordenar por fechas
		let order = mergeSortTopDown(todos.slice());
		res.send(order);
	}).catch(err => {
		console.log(err);
		res.send(err);
	});

});


// Ordenar fechas
// top-down implementation
function mergeSortTopDown(array) {
  if (array.length < 2) {
    return array;
  }

  let middle = Math.floor(array.length / 2);
  let left = array.slice(0, middle);
  let right = array.slice(middle);

  return mergeTopDown(mergeSortTopDown(left), mergeSortTopDown(right));
}

function mergeTopDown(left, right) {
  let array = [];

  while (left.length && right.length) {
    if (left[0].fecha < right[0].fecha) {
      array.push(left.shift());
    } else {
      array.push(right.shift());
    }
  }
  return array.concat(left.slice()).concat(right.slice());
}

//console.log(mergeSortTopDown(array.slice())); // => [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ]


router.get('/contarpedidostomados', (req, res) => {
	let cedula = req.query.cedula;
	let total = 0;
	connection.query('select count(*) as total from pedidos_deseos pd  where pd.estado != ? and pd.estado != ? and pd.estado != ? and idrider = ?', ['Entregado', 'Cancelado', 'Error', cedula], (err, rows, fields) => {
		if (err) {
			res.send(err);
		} else {
			total += rows[0].total;
			connection.query('select count(*) as total from pedidos_encomiendas pd  where pd.estado != ? and pd.estado != ? and pd.estado != ? and idrider = ?', ['Entregado', 'Cancelado', 'Error', cedula], (err, rows, fields) => {
				if (err) {
					res.send(err);
				} else {
					total += rows[0].total;
					connection.query('select count(*) as total from pedidos_clientes pd where pd.estado != ? and pd.estado != ? and pd.estado != ? and idrider = ?', ['Entregado', 'Cancelado', 'Error', cedula], (err, rows, fields) => {
						if (err) {
							res.send(err);
						} else {
							total += rows[0].total;
							connection.query('select count(*) as total from pedidos_empresariales pd  where pd.estado != ? and pd.estado != ? and pd.estado != ?  and idrider = ?', ['Entregado', 'Cancelado', 'Error', cedula], (err, rows, fields) => {
								if (err) {
									console.log(err);
									res.send(err);
								} else {
									total += rows[0].total;
									res.send({ total: total });
								}
							});
						}
					});
				}
			});
		}
	});
});

/**
 * Sin uso
 */
router.get('/obtenerpedidos', (req, res) => {
	let deseos, pedidos, mandados, empresariales;
	connection.query('select pd.*, d.direccion as direccionentrega, d.notas as notasdireccionentrega, d.latitude as latitudeentrega, d.longitude as longitudeentrega from pedidos_deseos pd join direcciones d on d.iddireccion = pd.iddireccion where pd.estado != ? and pd.estado != ? and pd.estado != ? and isnull(idrider) order by pd.fecha desc', ['Entregado', 'Cancelado', 'Error'], (err, rows, fields) => {
		if (err) {
			res.send(err);
		} else {
			deseos = rows;
			for (let i = 0; i < deseos.length; i++) {
				deseos[i].type = 'deseo';
			}
			connection.query('select pd.*, do.direccion as direccionorigen, do.notas as notasdireccionorigen, do.latitude as latitudedireccionorigen, do.longitude as longitudedireccionorigen, dd.direccion as direcciondestino, dd.notas as notasdirecciondestino, dd.latitude as latitudedirecciondestino, dd.longitude as longitudedirecciondestino from pedidos_encomiendas pd join direcciones do on do.iddireccion = pd.iddireccionorigen join direcciones dd on dd.iddireccion = pd.iddirecciondestino where pd.estado != ? and pd.estado != ? and pd.estado != ? and isnull(idrider) order by pd.fecha desc', ['Entregado', 'Cancelado', 'Error'], (err, rows, fields) => {
				if (err) {
					console.log(err);
					res.send(err);
				} else {
					mandados = rows;

					for (let i = 0; i < mandados.length; i++) {
						mandados[i].type = 'mandado';
					}
					connection.query('select pd.*, c.*, d.*, ss.idsucursal as idsucursal, ss.nombre as nombresucursal, ss.tiempo_entrega as tiempo_entrega, count(p.idpclientes_productos) as cantidadproductos from pedidos_clientes pd join clientes c on c.correo = pd.correocliente join direcciones d on d.iddireccion = pd.iddireccion join pclientes_productos p on pd.idpedido_clientes = p.idpedido_clientes join productos pro on pro.idproducto = p.idproducto join sucursales ss on ss.idsucursal = pro.idsucursal where pd.estado != ? and pd.estado != ? and pd.estado != ? and isnull(idrider) group by pd.idpedido_clientes, ss.idsucursal, ss.nombre, ss.tiempo_entrega order by pd.fecha desc', ['Entregado', 'Cancelado', 'Error'], (err, rows, fields) => {
						if (err) {
							res.send(err);
						} else {
							pedidos = rows;
							for (let i = 0; i < pedidos.length; i++) {
								pedidos[i].type = 'tienda';
							}

							connection.query('select pd.*, s.nombre as nombresucursal, s.numero_punto as numerosucursal, s.retenido as retiene, s.tiempo_entrega as tiempo_entrega, s.costo_envio as costoenvio, ds.iddireccion as iddireccionsucursal, ds.direccion as direccionsucursal, ds.notas as notasdireccionsucursal, ds.latitude as latitudesucursal, ds.longitude as longitudesucursal, d.iddireccion as iddireccionentrega, d.direccion as direccionentrega, d.notas as notasdireccionentrega from pedidos_empresariales pd join direcciones d on d.iddireccion = pd.iddireccion join sucursales s on s.idsucursal = pd.idsucursal join direcciones ds on ds.iddireccion = s.iddireccion where pd.estado != ? and pd.estado != ? and pd.estado != ?  and isnull(idrider) order by pd.fecha desc', ['Entregado', 'Cancelado', 'Error'], (err, rows, fields) => {
								if (err) {
									console.log(err);
									res.send(err);
								} else {
									empresariales = rows;

									for (let i = 0; i < empresariales.length; i++) {
										empresariales[i].type = 'empresariales';
									}

									let todos = deseos.concat(pedidos, mandados, empresariales);
									for (let i = 0; i < (todos.length - 1); i++) {
										for (let j = 0; j < (todos.length - i - 1); j++) {
											if (todos[j].fecha > todos[j + 1].fecha) {
												k = todos[j + 1];
												todos[j + 1] = todos[j];
												todos[j] = k;
											}
										}
									}
									res.send(todos);
								}
							});
						}
					});

				}
			});
		}
	});
});

/**
 * Obtiene lista de pedidos del rider
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns lsita de pedidos del rider
 */
router.get('/obtenerpedidosrider', (req, res) => {

	let cedula = req.query.cedula;

	/*if (cedula === 'undefined') {
		console.log(err);
		return res.send(err);
	}*/
	getOrdersRider(cedula)
	.then(result => {
		const orderType = ['deseo', 'mandado', 'tienda', 'empresariales'];
		let todos = [];

		for (let i = 0; i < result.length; i++) {
			let order = result[i];
			for (let j = 0; j < order.length; j++) {
				order[j].type = orderType[i];
				todos.push(order[j]);
			}
		}
		// Ordenar por fechas
		let order = mergeSortTopDown(todos.slice());
		res.send(order);
	})
	.catch(err => {
		console.log(err);
		res.send(err);
	});
	
});

/**
 * Obtiene todos los peidos asignados del rider
 * 
 * @param cedula identificador del rider
 * @returns promises  ['deseo', 'mandado', 'tienda', 'empresariales']
 */
function getOrdersRider (cedula) {
	let deseos, pedidos, mandados, empresariales;

	const conditions = ['Entregado', 'Cancelado', 'Error', cedula];

	deseos = db.query('select pd.*, d.direccion as direccionentrega, d.notas as notasdireccionentrega, d.latitude as latitudeentrega, d.longitude as longitudeentrega from pedidos_deseos pd join direcciones d on d.iddireccion = pd.iddireccion where pd.estado != ? and pd.estado != ? and pd.estado != ? and idrider = ? order by pd.fecha desc', conditions);
	mandados = db.query('select pd.*, do.direccion as direccionorigen, do.notas as notasdireccionorigen, do.latitude as latitudedireccionorigen, do.longitude as longitudedireccionorigen, dd.direccion as direcciondestino, dd.notas as notasdirecciondestino, dd.latitude as latitudedirecciondestino, dd.longitude as longitudedirecciondestino from pedidos_encomiendas pd join direcciones do on do.iddireccion = pd.iddireccionorigen join direcciones dd on dd.iddireccion = pd.iddirecciondestino where pd.estado != ? and pd.estado != ? and pd.estado != ? and idrider = ? order by pd.fecha desc', conditions);
	pedidos = db.query('select pd.*, c.*, d.*, ss.idsucursal as idsucursal, ss.nombre as nombresucursal, ss.tiempo_entrega as tiempo_entrega, count(p.idpclientes_productos) as cantidadproductos from pedidos_clientes pd join clientes c on c.correo = pd.correocliente join direcciones d on d.iddireccion = pd.iddireccion join pclientes_productos p on pd.idpedido_clientes = p.idpedido_clientes join productos pro on pro.idproducto = p.idproducto join sucursales ss on ss.idsucursal = pro.idsucursal where pd.estado != ? and pd.estado != ? and pd.estado != ? and idrider = ? group by pd.idpedido_clientes, ss.idsucursal, ss.nombre, ss.tiempo_entrega order by pd.fecha desc', conditions);
	empresariales = db.query('select pd.*, s.tipo as tipoSucursal, s.nombre as nombresucursal, s.numero_punto as numerosucursal, s.retenido as retiene, s.tiempo_entrega as tiempo_entrega, s.costo_envio as costoenvio, s.tipo as tipoSucursal, ds.iddireccion as iddireccionsucursal, ds.direccion as direccionsucursal, ds.notas as notasdireccionsucursal, ds.latitude as latitudesucursal, ds.longitude as longitudesucursal, d.iddireccion as iddireccionentrega, d.direccion as direccionentrega, d.notas as notasdireccionentrega from pedidos_empresariales pd join direcciones d on d.iddireccion = pd.iddireccion join sucursales s on s.idsucursal = pd.idsucursal join direcciones ds on ds.iddireccion = s.iddireccion where pd.estado != ? and pd.estado != ? and pd.estado != ?  and idrider = ? order by pd.fecha desc', conditions);

	return Promise.all([deseos, mandados, pedidos, empresariales]);
}

/**
 * Obtiene cliente por correo
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns cliente
 */
router.get('/obtenercliente', (req, res) => {
	let correo = req.query.correo;

	connection.query('select * from clientes where correo = ?', [correo], (err, rows, fields) => {
		if (err) {
			res.send(err);
		} else {
			res.send(rows);
		}
	});
});



/**
 * Se crea un nuevo cliente
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns direccion del cliente
 */
router.post('/agregarcliente', (req, res) => {

	let nombre = req.body.nombre;
	let correo = req.body.correo;
	let telefono = req.body.telefono;
	let contrasenia = req.body.contrasenia;
	let latitude = req.body.latitude;
	let longitude = req.body.longitude;
	let direccion = req.body.direccion;
	let notas = req.body.notas;
	let municipio = req.body.municipio;

	let iddireccion;

	let direccionObj = { idmunicipio: municipio, latitude: latitude, longitude: longitude, direccion: direccion, notas: notas };
	connection.query('insert into direcciones (idmunicipio, latitude, longitude, direccion, notas) values(?,?,?,?,?)',
		[municipio, latitude, longitude, direccion, notas], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(err);
			} else {
				iddireccion = rows.insertId;
				direccionObj['iddireccion'] = iddireccion;
				let hash = bcrypt.hashSync(contrasenia, 10);
				connection.query('insert into clientes (nombre, telefono, correo, contrasenia) values(?, ?, ?, ?)',
					[nombre, telefono, correo, hash], (err, rows, fields) => {
						if (err) {
							console.log(err);
							res.send(err);
						} else {
							connection.query('insert into cliente_direccion (iddireccion, correocliente) values(?, ?)',
								[iddireccion, correo], (err, rows, fields) => {
									if (err) {
										console.log(err);
										res.send(err);
									} else {
										connection.query('select * from clientes where correo = ?', [correo], (err, respuesta, fields) => {
											if (err) {
												console.log(err);
												res.send(err);
											} else {
												respuesta.push(direccionObj);
												res.send(respuesta);
											}
										});
									}
								});
						}
					});
			}
		});
});

/**
 * Obtiene la sucursal por id
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns sucursal
 */
router.get('/obtenersucursal', (req, res) => {
	let idsucursal = req.query.idsucursal;
	connection.query('select * from sucursales s join direcciones d on d.iddireccion = s.iddireccion where idsucursal = ?',
		[idsucursal], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(err);
			} else {
				res.send(rows);
			}
		});
});

router.post('/actualizarpedidoempresarial', (req, res) => {

	let id = req.body.id;
	let address_id = req.body.address_id;
	let customerName = req.body.customerName;
	let customerPhone = req.body.customerPhone;
	let value = req.body.value;
	let direction = req.body.direction;
	
	//let evidencia = req.body.evidencia; // Foto de la evidencia

	db.query('UPDATE pedidos_empresariales SET nombre_cliente = ?, telefono = ?, valor = ?' +
	' WHERE idpedido_empresarial = ?', [customerName, customerPhone, value, id])
	.then(() => {
		return db.query('UPDATE direcciones SET direccion = ? WHERE iddireccion = ?', [direction, address_id]);
	})
	.then(() => {
		res.status(200).json({message: 'Pedido actualizado con exito'});
	})
	.catch(err => {
		console.log(err);
		res.status(402).json({error: err});
	});
	
});

/**
 * Cambia el estado de pedido empresarial
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns pedido actualizado
 */
router.post('/cambiarestadopedidoempresarial', (req, res) => {
	
	let idrider = req.body.cedularider;
	let idpedidoempresarial = req.body.idpedidoempresarial;
	let estado = req.body.estado;

	let op = models.Sequelize.Op;
/*
	// Busca el pedido emrpesarial por el identificador 
	PedidosEmpresariales.findOne({
		where: {
			idpedido_empresarial: idpedidoempresarial,
			[op.or] : [
				{ idrider : null},
				{ idrider : idrider}
			]
		}
	})
	.then(pe => {

		// Verifica si existe el pedido empresarial
		if (pe) {

			pe.idrider = idrider;
			pe.estado = estado;

			// Actualiza los datos del pedido en curso
			pe.save()	
			.then ((result, affect) => {

				if (io) {
					console.log("Emite cambio de estado");
					io.emit("ordershop");
				}
				console.log('REs ' + result + ' UP ' + affect );

				res.send({
					okey: 'yes',
					update: affect
				});
			})
			.catch(err => {
				console.log(err);
				res.send(err);
			});
			
		} else {
			res.send('No se encuentra el pedido');
		}
	})
	.catch(err => {
		console.log(err);
		res.send(err);		
	});
*/
	
	connection.query('update pedidos_empresariales set idrider = ?, estado = ? where idpedido_empresarial = ? and (isnull(idrider) or idrider = ?)',
		[idrider, estado, idpedidoempresarial, idrider], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(err);
			} else {
				if (io) {
					console.log("Emite cambio de estado");
					io.emit("ordershop");
				}
				res.send({
					okey: 'yes',
					update: rows.affectedRows
				});
			}
		});
		
});

/**
 * Cambia el estado de pedido del cliente
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns pedido actualizado
 */
router.post('/cambiarestadopedidocliente', (req, res) => {
	let idrider = req.body.cedularider;
	let idpedido = req.body.idpedidocliente;
	let estado = req.body.estado;
	connection.query('update pedidos_clientes set idrider = ?, estado = ? where idpedido_clientes = ? and (isnull(idrider) or idrider = ?)',
		[idrider, estado, idpedido, idrider], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(err);
			} else {
				res.send({
					okey: 'yes',
					update: rows.affectedRows
				});
			}
		});
});

/**
 * Cambia el estado de pedido encomienda
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns pedido actualizado
 */
router.post('/cambiarestadopedidoencomienda', (req, res) => {
	let idrider = req.body.cedularider;
	let idpedidoencomienda = req.body.idpedidoencomienda;
	let estado = req.body.estado;
	connection.query('update pedidos_encomiendas set idrider = ?, estado = ? where idpedido_encomienda = ? and (isnull(idrider) or idrider = ?)',
		[idrider, estado, idpedidoencomienda, idrider], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(err);
			} else {
				res.send({
					okey: 'yes',
					update: rows.affectedRows
				});
			}
		});
});

/**
 * Cambia el estado de pedido deseo
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns pedido actualizado
 */
router.post('/cambiarestadopedidodeseo', (req, res) => {

	let idrider = req.body.cedularider;
	let idpedidodeseo = req.body.idpedidodeseo;
	let estado = req.body.estado;
	connection.query('update pedidos_deseos set idrider = ?, estado = ? where idpedido_deseo = ? and (isnull(idrider) or idrider = ?)',
		[idrider, estado, idpedidodeseo, idrider], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(err);
			} else {
				res.send({
					okey: 'yes',
					update: rows.affectedRows
				});
			}
		});
});

/**
 * Obtiene pedido empresarial por id
 * [DEPRECATED]
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns pedido empresarial
 */
router.get('/obtenerestadopedidoempresarial', (req, res) => {
	let idpedido = req.query.pedido;

	connection.query('select * from pedidos_empresariales where idpedido_empresarial = ?',
		[idpedido], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(err);
			} else {
				res.send({ estado: rows[0] }.estado);
			}
		});
});

/**
 * Obtiene pedido empresarial por id
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns pedido empresarial
 */
router.get('/get-state-business-order/:id', (req, res) => {

	const id = req.params.id;

	// Obtiene el pedido empresarial por el id
	PedidosEmpresariales.findOne({
		attributes: ['estado', 'idrider'],
		where: {
			idpedido_empresarial: id
		}
	}).then(pe => {
		res.send(pe);
	}).catch(err => {
		console.log(`Error al obtener el pedido empresarial: ${id}`, err);
		res.send(err);
	});
});

/**
 * Obtiene pedido deseo por id
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns pedido deseo
 */
router.get('/obtenerestadopedidodeseo', (req, res) => {
	let idpedido = req.query.pedido;
	connection.query('select * from pedidos_deseos where idpedido_deseo = ?',
		[idpedido], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(err);
			} else {
				res.send({ estado: rows[0] }.estado);
			}
		});
});

/**
 * Obtiene pedido cliente por id
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns pedido cliente
 */
router.get('/obtenerestadopedidocliente', (req, res) => {
	let idpedido = req.query.pedido;
	connection.query('select * from pedidos_clientes where idpedido_clientes = ?',
		[idpedido], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(err);
			} else {
				res.send({ estado: rows[0] }.estado);
			}
		});
});

/**
 * Obtiene los productos del pedido del cliente
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns productos del pedido cliente
 */
router.get('/obtenerproductospedido', (req, res) => {
	let idpedido = req.query.pedido;
	connection.query('select pp.*, p.* from pclientes_productos pp join productos p on pp.idproducto = p.idproducto where pp.idpedido_clientes = ?',
		[idpedido], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(err);
			} else {
				res.send(rows);
			}
		});
});

/**
 * Obtiene el valor total del pedio del cliente
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns total del cliente
 */
router.get('/obtenertotalpedidocliente', (req, res) => {
	let idpedido = req.query.pedido;
	let totalelementos = 0, totalselecciones = 0, totaladiciones = 0, totalproductos = 0;
	connection.query('select sum(e.precio) as totalelementos from pclientes_productos pp join pclientes_prod_elementos ppe on pp.idpclientes_productos = ppe.idpclientes_productos join elementos e on e.idelemento = ppe.idelemento where pp.idpedido_clientes = ? group by pp.idpedido_clientes',
		[idpedido], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(ee);
			} else {
				if (rows.length != 0) {
					totalelementos = rows[0].totalelementos;
				}
				connection.query('select sum(a.precio) as totaladiciones from pclientes_productos pp join pclientes_prod_adiciones ppe on pp.idpclientes_productos = ppe.idpclientes_productos join adiciones a on a.idadicion = ppe.idadicion where pp.idpedido_clientes = ? group by pp.idpedido_clientes',
					[idpedido], (err, rows, fields) => {
						if (err) {
							console.log(err);
							res.send(ee);
						} else {
							if (rows.length != 0) {
								totaladiciones = rows[0].totaladiciones;
							}
							connection.query('select sum(a.precio) as totalselecciones from pclientes_productos pp join pclientes_prod_selecciones ppe on pp.idpclientes_productos = ppe.idpclientes_productos join selecciones a on a.idseleccion = ppe.idseleccion where pp.idpedido_clientes = ? group by pp.idpedido_clientes',
								[idpedido], (err, rows, fields) => {
									if (err) {
										console.log(err);
										res.send(ee);
									} else {
										if (rows.length != 0) {
											totalselecciones = rows[0].totalselecciones;
										}
										connection.query('select sum(p.precio) as totalproductos from pclientes_productos pp join productos p on p.idproducto = pp.idproducto where pp.idpedido_clientes = ? group by pp.idpedido_clientes',
											[idpedido], (err, rows, fields) => {
												if (err) {
													console.log(err);
													res.send(err);
												} else {
													res.send({
														total: totaladiciones + totalelementos + totalselecciones + rows[0].totalproductos
													});
												}
											});
									}
								});
						}
					});
			}
		});
});

/**
 * Obtiene el estado del pedido encomienda
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns pedido o error
 */
router.get('/obtenerestadopedidoencomienda', (req, res) => {
	let idpedido = req.query.pedido;
	connection.query('select * from pedidos_encomiendas where idpedido_encomienda = ?',
		[idpedido], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(err);
			} else {
				res.send({ estado: rows[0] }.estado);
			}
		});
});

/**
 * Obtiene todos los pedidos realizados por el rider
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns pedido del cliente
 */
router.get('/obtenerpedidosrealizadosrider', (req, res) => {
	let cedula = req.query.cedularider;

	let deseos, pedidos, mandados, empresariales;
	connection.query('select pd.*, \'deseo\' AS type, sum(f.valor) as totalfacturas from pedidos_deseos pd join facturas f on f.idpedido_deseos = pd.idpedido_deseo where pd.idrider = ? and (pd.estado = ? or pd.estado = ?) and pd.comision_entregada = ? group by pd.idpedido_deseo',
		[cedula, 'Entregado', 'Error', 'N'], (err, rows, fields) => {
			if (err) {
				console.log(err);
				return res.send(err);
			}
			deseos = rows;

			connection.query('select pe.*, \'mandado\' AS type, sum(p.costo) as totalparadas from pedidos_encomiendas pe left join paradas p on p.idpencomienda = pe.idpedido_encomienda where pe.idrider = ? and (pe.estado = ? or pe.estado = ?) and pe.comision_entregada = ? group by pe.idpedido_encomienda',
				[cedula, 'Entregado', 'Error', 'N'], (err, rows, fields) => {
					if (err) {
						console.log(err);
						return res.send(err);
					}
					mandados = rows;

					connection.query('select *, \'tienda\' AS type from pedidos_clientes pc where pc.idrider = ? and (pc.estado = ? or pc.estado = ?)',
						[cedula, 'Entregado', 'Error'], (err, rows, fields) => {
							if (err) {
								console.log(err);
								return res.send(err);
							}
							pedidos = rows;

							connection.query('select pe.*, \'empresariales\' AS type, IFNULL(aj.valor, 0) AS valor_ajuste from pedidos_empresariales pe LEFT JOIN ajustes aj ON aj.id = pe.ajustes_id where pe.idrider = ? and (pe.estado = ? or pe.estado = ?) and pe.comision_entregada = ?',
								[cedula, 'Entregado', 'Error', 'N'], async (err, rows, fieds) => {
									if (err) {
										console.log(err);
										return res.send(err);
									}
									empresariales = rows;

									let todos = deseos.concat(pedidos, mandados, empresariales);
									//console.log(mandados);
									res.send(todos);
								});
						});
				});
		});
});

/**
 * Obtiene todos los pedidos realizados no pagados del rider
 * 
 * @author Jhoan Sebastian Chilito Sanchez
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 */
router.get('/get-orders-rider/:id', (req, res) => {

	const id = req.params.id;

	// Obtiene los pedidos no pagados actuales del rider
	getCurrentOrdersAccountRider(id)
	.then(orders => {
		res.send(orders[0].concat(orders[1], orders[2], orders[3]));
	})
	.catch(err => {
		console.log(`Error al obtener la cuenta del rider: ${id}`, err);
		res.send(err);
	});

});

/**
 * Obtiene el valor actual de los pedido sin pagar y adicionales
 * 
 * @author Jhoan Sebastian Chilito Sanchez
 * @param {*} rider_id identificador del rider
 * @param {*} init_date fecha inicial intervalo
 * @param {*} end_date fecha final intervalo
 * @returns PedidosDeseo[0]
 * 			PedidosClientes[1]
 * 			PedidosEmpresariales[2]
 * 			PedidosEncomiendas[3] JOIN con ajustes 
 */
async function getCurrentOrdersAccountRider(rider_id, init_date, end_date, commision = 'N') {
	// Tipos de pedidos
	let deseos, encomiendas, clientes, empresariales;
	// Codiciones de consulta
	let condiciones;
	// Verificador de intervalo de fechas
	let isRange = false;
	// Consulta adicionada para intervalo de fechas
	let queryRange = '';

	// Condiciones de los filtros para las siguientes consultas
	condiciones = [rider_id, 'Entregado', 'Error', commision];

	// Verifica si vienen fechas de rango de filtro
	if (init_date && end_date) {
		// Se habilita rango de fechas para filtrar
		isRange = true;
		// Query adicional para filtro de fechas
		queryRange = `fecha BETWEEN '${init_date}' AND '${end_date}'`;
	}

	// Obtiene todos los pedidos deseos por rider
	deseos = models.sequelize.query(`SELECT pd.*, 'deseo' AS type, SUM(f.valor) AS totalfacturas FROM pedidos_deseos pd JOIN facturas f
	 ON f.idpedido_deseos = pd.idpedido_deseo WHERE pd.idrider = ? AND (pd.estado = ? OR pd.estado = ?) AND pd.comision_entregada = ?
	  ${isRange? `AND pd.${queryRange}` : ''} GROUP BY pd.idpedido_deseo`, { replacements: condiciones, type: models.sequelize.QueryTypes.SELECT });
	
	// Obtiene todos los pedidos realizados a una tienda por rider
	clientes = models.sequelize.query('SELECT *, \'tienda\' AS type FROM pedidos_clientes pc WHERE pc.idrider = ? AND (pc.estado = ? OR pc.estado = ?) AND pc.comision_entregada = ?',
	{ replacements: condiciones, type: models.sequelize.QueryTypes.SELECT });

	let op = models.Sequelize.Op;

	// Condiciones de pedido empresariales
	let conditionsBusinessOrder = {
		idrider: rider_id,
		comision_entregada: commision,
		[op.or]: [
			{estado: 'Entregado'},
			{estado: 'Error'}
		]
	};

	// Verifica si se hce el filtro por intervalo de fechas
	if (isRange) {
		// Se agrega la condicion de rango fechas
		conditionsBusinessOrder.fecha = {
			[op.between]: [
				init_date,
				end_date
			]
		}
	}

	// Obtiene todos los pedidos empresariales por rider
	empresariales = PedidosEmpresariales.findAll({
		include: [
			{model: Ajustes, as: 'ajuste'}
		],
		where: conditionsBusinessOrder
	});

	// Obtiene todos los pedidos encomienda por rider
	encomiendas = models.sequelize.query(`SELECT pe.*, 'mandado' AS type, SUM(p.costo) AS totalparadas FROM pedidos_encomiendas pe 
	 LEFT JOIN paradas p ON p.idpencomienda = pe.idpedido_encomienda WHERE pe.idrider = ? AND (pe.estado = ? OR pe.estado = ?) 
	 AND pe.comision_entregada = ? ${isRange? `AND pe.${queryRange}` : ''} GROUP BY pe.idpedido_encomienda`, { replacements: condiciones, type: models.sequelize.QueryTypes.SELECT });

	// Se ejecutan todas las consultas
	return Promise.all([deseos, clientes, empresariales, encomiendas]);
}


/**
 * Busca las solicitudes no pagadas
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns id sulicitud | solicitud actual
 */
router.post('/solicitarpago', (req, res) => {
	let cedularider = req.body.cedularider;

	connection.query('select * from solicitudes where idrider = ? and pagada = ?',
		[cedularider, 'N'], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(err);
			} else if (rows.length > 0) {
				res.send({ current: rows.length });
			} else {
				connection.query('insert into solicitudes (idrider, fecha, pagada) values(?,?,?)',
					[cedularider, new Date(), 'N'], (err, rows, fields) => {
						res.send({
							idsolicitud: rows.insertId
						});
					});
			}
		});
});

/**
 * Obtiene los factores, elemento y adiciones de un producto
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns id sulicitud | solicitud actual
 */
router.get('/obtenerfactoreselementosadiciones', (req, res) => {
	let idproducto = req.query.idproducto;
	let response = [];
	let factores = [], elementos = [], adiciones = [];
	connection.beginTransaction((err) => {
		if (err) {
			console.log(err);
		} else {
			connection.query('select * from factores f where f.idproducto = ?', [idproducto], (err, rows, fields) => {
				if (err) {
					connection.rollback(() => {
						console.log(err);
						res.send(err);
					});
				} else {
					factores = rows;
					for (let i = 0; i < factores.length; i++) {
						factores[i].selecciones = [];
					}
					connection.query('select f.*, s.idseleccion as idseleccion, s.nombre as nombreseleccion, s.precio as precioseleccion from factores f join selecciones s on s.idfactor = f.idfactor where f.idproducto = ?', [idproducto], (err, rows, fields) => {
						if (err) {
							connection.rollback(() => {
								console.log(err);
								res.send(err);
							});
						} else {
							for (let i = 0; i < factores.length; i++) {
								for (let j = 0; j < rows.length; j++) {
									if (factores[i].idfactor == rows[j].idfactor) {
										factores[i].selecciones.push({
											idseleccion: rows[j].idseleccion,
											nombreseleccion: rows[j].nombreseleccion,
											precioseleccion: rows[j].precioseleccion
										});
									}
								}
							}
							connection.query('select * from elementos where idproducto = ?', [idproducto], (err, rows, fields) => {
								if (err) {
									connection.rollback(() => {
										console.log(err);
										res.send(err);
									});
								} else {
									elementos = rows;
									connection.query('select * from adiciones where idproducto = ?', [idproducto], (err, rows, fields) => {
										if (err) {
											connection.rollback(() => {
												console.log(err);
												res.send(err);
											});
										} else {
											adiciones = rows;
											connection.commit((err) => {
												if (err) {
													console.log(err);
													res.send(err);
												} else {
													response.push(factores);
													response.push(elementos);
													response.push(adiciones);
													res.send(response);
												}
											});
										}
									});
								}
							});
						}
					});
				}
			});
		}
	});
});


/**
 * Inserta un nuevo pedido del cliente
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns error | ok
 */
router.post('/realizarpedidocliente', (req, res) => {
	let carrito = JSON.parse(req.body.carrito);
	let correocliente = req.body.correocliente;
	let costo_envio = req.body.costo_envio;
	let comision_rider = req.body.comision_rider;
	let iddireccion = req.body.iddireccion;
	let idpedido;

	connection.beginTransaction((err) => {
		if (err) {
			console.log(err);
			res.send(err);
		} else {
			connection.query('insert into pedidos_clientes (costo_envio, comision_rider, fecha, estado, iddireccion, comision_entregada, correocliente) values(?,?,?,?,?,?,?)',
				[costo_envio, comision_rider, new Date(), 'En proceso', iddireccion, 'N', correocliente], (err, rows, fields) => {
					if (err) {
						connection.rollback(() => {
							console.log(err);
							res.send(err);
						});
					} else {
						idpedido = rows.insertId;
						let values = [];
						for (let i = 0; i < carrito.length; i++) {
							let curr = [];
							curr.push(idpedido, carrito[i].idproducto);
							values.push(curr);
						}
						connection.query('insert into pclientes_productos (idpedido_clientes, idproducto) values ?',
							[values], (err, rows, fields) => {
								if (err) {
									connection.rollback(() => {
										console.log(err);
										res.send(err);
									});
								} else {
									let lastId = rows.insertId;
									let valuesElementos = [], valuesAdiciones = [], valuesSelecciones = [];
									for (let i = 0; i < carrito.length; i++) {
										for (let j = 0; j < carrito[i].elementos.length; j++) {
											let curr = [];
											curr.push(carrito[i].elementos[j].idelemento, lastId + i);
											valuesElementos.push(curr);
										}

										for (let j = 0; j < carrito[i].adiciones.length; j++) {
											let curr = [];
											curr.push(carrito[i].adiciones[j].idadicion, lastId + i);
											valuesAdiciones.push(curr);
										}

										for (let j = 0; j < carrito[i].factores.length; j++) {
											let curr = [];
											curr.push(carrito[i].factores[j].selecciones[carrito[i].factores[j].indexSelected].idseleccion, lastId + i);
											valuesSelecciones.push(curr);
										}
									}
									connection.query('insert into pclientes_prod_elementos (idelemento, idpclientes_productos) values ?',
										[valuesElementos], (err, rows, fields) => {
											if (err && valuesElementos.length != 0) {
												connection.rollback(() => {
													console.log(err);
													res.send(err);
												});
											} else {
												connection.query('insert into pclientes_prod_adiciones (idadicion, idpclientes_productos) values ?',
													[valuesAdiciones], (err, rows, fields) => {
														if (err && valuesAdiciones.length != 0) {
															connection.rollback(() => {
																console.log(err);
																res.send(err);
															});
														} else {
															connection.query('insert into pclientes_prod_selecciones (idseleccion, idpclientes_productos) values ?',
																[valuesSelecciones], (err, rows, fields) => {
																	if (err && valuesSelecciones.length != 0) {
																		connection.rollback(() => {
																			console.log(err);
																			res.send(err);
																		});
																	} else {
																		connection.commit((err) => {
																			if (err) {
																				console.log(err);
																				res.send(err);
																			} else {
																				res.send({
																					okey: 'yes'
																				});
																			}
																		});
																	}
																});
														}
													});
											}
										});
								}
							});
					}
				})
		}
	});
});

/**
 * Obtiene las paradas
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns error | paradas
 */
router.get('/obtenerparadas', (req, res) => {
	connection.query('select * from paradas p join direcciones d on p.iddireccion = d.iddireccion', (err, rows, fields) => {
		if (err) {
			console.log(err);
			res.send(err);
		} else {
			res.send(rows);
		}
	});
});

/**
 * Obtiene las paradas del pedido
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns error | paradas
 */
router.get('/obtenerparadaspedido', (req, res) => {
	let idpedido = req.query.pedido;
	connection.query('select * from paradas p join direcciones d on p.iddireccion = d.iddireccion where p.idpencomienda = ?', [idpedido], (err, rows, fields) => {
		if (err) {
			console.log(err);
			res.send(err);
		} else {
			res.send(rows);
		}
	});
});

/**
 * Obtiene la direccion del pedido encomienda
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns error | direccion pedio encomienda
 */
router.get('/obtenerdireccionesencomienda', (req, res) => {
	let idpedido = req.query.pedido;
	let pedido;
	connection.query('select * from pedidos_encomiendas where idpedido_encomienda = ?', [idpedido], (err, rows, fields) => {
		if (err) {
			console.log(err);
			res.send(err);
		} else {
			pedido = rows[0];
			let direccionrecogida, direccionentrega;
			connection.query('select * from direcciones where iddireccion = ?',
				[pedido.iddireccionorigen], (err, rows, fields) => {
					if (err) {
						console.log(err);
						res.send(err);
					} else {
						direccionrecogida = rows[0];
						connection.query('select * from direcciones where iddireccion = ?',
							[pedido.iddirecciondestino], (err, rows, fields) => {
								if (err) {
									console.log(err);
									res.send(err);
								} else {
									direccionentrega = rows[0];
									let response = [];
									response.push(direccionrecogida, direccionentrega);
									res.send(response);
								}
							});
					}
				});
		}
	});
});

/**
 * Obtiene las facturas del pedido deseo
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns error | facturas
 */
router.get('/obtenerfacturas', (req, res) => {
	let idpedido = req.query.pedido;
	connection.query('select * from facturas where idpedido_deseos = ?',
		[idpedido], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(err);
			} else {
				res.send(rows);
			}
		});
});

/**
 * Agrega una nueva factura
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns error | ok
 */
router.post('/agregarfactura', (req, res) => {
	let idpedido = req.body.idpedido;
	let cantidadFacturas = req.body.cantidadFacturas;
	let foto = req.files.fotofactura;
	let valor = req.body.valor;
	let extension = foto.name.split('.').pop();
	let direccionImg = 'src/app/public/img/facturas/' + idpedido + '-' + cantidadFacturas + '.' + extension;
	fs.renameSync(foto.path, direccionImg);
	name = idpedido + '-' + cantidadFacturas + '.' + extension;

	connection.query('insert into facturas (foto, valor, idpedido_deseos) values(?, ?, ?)',
		[name, valor, idpedido], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(err);
			} else {
				res.send({
					okey: 'yes'
				});
			}
		});
});

/**
 * Obtiene el clente del pedido deseo
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns error | cliente
 */
router.get('/obtenerclientepedidodeseo', (req, res) => {
	let idpedido = req.query.pedido;
	connection.query('select c.* from pedidos_deseos pd join clientes c on c.correo = pd.correocliente where pd.idpedido_deseo = ?',
		[idpedido], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(err);
			} else {
				res.send(rows[0]);
			}
		});
});


/**
 * Obtiene el clente del pedido encomienda
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns error | cliente
 */
router.get('/obtenerclientepedidoencomienda', (req, res) => {
	let idpedido = req.query.pedido;
	connection.query('select c.* from pedidos_encomiendas pd join clientes c on c.correo = pd.correocliente where pd.idpedido_encomienda = ?',
		[idpedido], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(err);
			} else {
				res.send(rows[0]);
			}
		});
});

/**
 * Obtiene el clente del pedido clientes
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns error | cliente
 */
router.get('/obtenerclientepedidoclientes', (req, res) => {
	let idpedido = req.query.pedido;
	connection.query('select c.* from pedidos_clientes pd join clientes c on c.correo = pd.correocliente where pd.idpedido_clientes = ?',
		[idpedido], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(err);
			} else {
				res.send(rows[0]);
			}
		});
});

/**
 * Permite recuperar la contraseña del cliente
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns error | ok
 */
router.post('/recuperarcontrasenia', (req, res) => {
	let correo = req.body.correo;
	console.log('Hola');
	connection.query('select * from clientes where correo = ?', [correo], (err, rows, fields) => {
		if (err) {
			console.log(err);
			res.send(err);
		} else if (rows.length == 0) {
			res.send({
				notfound: 404
			})
		} else {
			let transporter = nodemailer.createTransport({
				service: 'gmail',
				auth: {
					user: 'traigoapp@gmail.com',
					pass: 'traigo*.*/20*18'
				}
			});
			let mailOptions = {
				from: 'traigoapp@gmail.com',
				to: rows[0].correo,
				subject: 'Traigo: Recuperación de contraseña',
				text: 'Hola ' + rows[0].nombre + ', nos enteramos de que olvidaste tu contraseña :( en Traigo estamos felices de tenerte! Por eso no queremos que pierdas tu acceso.\nTu contraseña es: ' + rows[0].contrasenia
			}

			transporter.sendMail(mailOptions, (err, info) => {
				if (err) {
					console.log(err);
					res.send(err);
				} else {
					res.send({
						correct: 'yes'
					});
				}
			});
		}
	});
});

/**
 * Permite obtener todos los departamentos
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns error | departamentos
 */
router.get('/departamentos', (req, res) => {
	connection.query('SELECT d.idDepartamento as id, d.nombre FROM departamentos d',
	(err, rows, fields) => {
		if (err) {
			console.log(err);
			res.send(err);
		} else {
			res.send(rows);
		}
	});
});

/**
 * Permite obtener todos las ciudades por id
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns error | ciudades
 */
router.get('/ciudades/:departamento', (req, res) => {
	let idDepartamento = req.params.departamento;
	connection.query('SELECT m.idmunicipios as id, m.nombre FROM municipios m WHERE idDepartamento = ?',
	[idDepartamento], (err, rows, fields) => {
		if (err) {
			console.log(err);
			res.send(err);
		} else {
			res.send(rows);
		}
	});
});


module.exports = router;
