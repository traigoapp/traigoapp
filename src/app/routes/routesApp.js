const express = require('express');
const router = express.Router();
const sesion = require('../middlewares/sesionApp');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

require('dotenv').config();

const Database = require('../../config/database');
let db = new Database(); // Instancia de base de datos promise
const connection = db.getConnection();

router.use(sesion);

router.get('/', (req, res) => {
	db.query('SELECT * FROM configuraciones')
	.then(rows => {
		res.render('admin/pages/auth/login', {
			img: rows[0].img_login
		});
	})
	.catch(err => {
		console.log(err);
		res.render('admin/pages/auth/login', {
			img: 'default.jpeg'
		});
	});
	
});

router.get('/login', (req, res) => {
	let fail = req.query.fail;
	
	db.query('SELECT * FROM configuraciones')
	.then(rows => {
		if (fail === 'block') {
			
			let message = req.query.message;
			res.render('admin/pages/auth/login', { 
				fail : fail,
				message : (message)? message : 'Cuenta inhabilitada',
				img: rows[0].img_login
			});
		} else {
			res.render('admin/pages/auth/login', {
				fail : fail,
				message: '',
				img: rows[0].img_login
			});
		}
		
	})
	.catch(err => {
		console.log(err);
		res.render('admin/pages/auth/login', {
			img: 'default.jpeg',
			fail : fail,
		});
	});

});

/**
 * Restablecer la contraseña del rider
 * actualiza la nueva contraseña
 */
router.post('/reset-password', (req, res) => {
	
	// Token de verificacion
	let token = req.body.token;
	let password = req.body.password;
	let passwordc = req.body.password_confirm;

	try {
		// Verifica si el token es valido o ha expirado
		let rider = jwt.verify(token, process.env.PRIVATE_JWT_KEY);

		// Verifica si ha diligenciado dotos los datos
		if (password && passwordc) {
			// Verifica si las contraseñas ingresadas son iguales
			if (password === passwordc) {
				// Se crea la contraseña encriptada
				let hash = bcrypt.hashSync(password, 10);		
				// Actualiza el rider con la nueva contraseña
				db.query('UPDATE riders SET contrasenia = ? WHERE cedula = ?', [hash, rider.cedula])
				.then(() => {
					res.render('admin/pages/auth/password-msg', {
						msg: {
							title: 'Perfecto',
							description: 'La contraseña se ha actualizado con exito.',
							info: ''
						}
					});
				})
				.catch(err => {
					console.log(err);
					return confictRedirect('Error al actuaizar la contraseña.');	
				});
			} else {
				return confictRedirect('Las contraseñas no coinciden.');
			}
		} else {
			return confictRedirect('Debe diligenciar todos los datos.');
		}

		/**
		 * Envia mensaje de error a la interfaz de recuperar contraseña
		 * @param {*} rider 
		 * @param {*} msg 
		 */
		function confictRedirect(msg) {
			res.redirect(`/reset-password/${token}?info=${msg}`);
		}

	} catch (err) { // err
		console.log(err);
		res.render('admin/pages/auth/password-msg',{
			msg: {
				title: 'Ahh',
				description: 'El tiempo de espera a expirado',
				info: 'Vuelve a solicitar el cambio de contraseña.'
			}
		});
	}

});
/**
 * Restablecer la contraseña del rider
 */
router.get('/reset-password/:token', (req, res) => {
	
	// Token de verificacion
	let token = req.params.token;
	let info = req.query.info;

	try {
		// Verifica si el token es valido o ha expirado
		let rider = jwt.verify(token, process.env.PRIVATE_JWT_KEY);
		rider.token = token;
		res.render('admin/pages/auth/recover-password', {
			rider: rider,
			info: info
		});
	} catch (err) { // err
		console.log('Error validacion',err);
		res.render('admin/pages/auth/password-msg', {
			msg: {
				title: 'Ahh',
				description: 'El tiempo de espera a expirado',
				info: 'Vuelve a solicitar el cambio de contraseña.'
			}
		});
	}
});

/**
 * Verficacion de autenticacion de usuario
 */
router.post('/login', (req, res) => {
	let type = req.body.type;
	let usuario = req.body.usuario;
	let contrasenia = req.body.contrasenia;

	if(!(type && usuario && contrasenia)) {
		return res.redirect('/login?fail=err');
	}

	//if(usuario == 'traigoapp' && contrasenia == 'traigo*.*/20*18'){
	//	req.session.user = {type: 'administrador', usuario: 'traigo', contrasenia: 'traigo*.*/20*18'};
	//	return res.redirect('/administrador');
	//}

	//if(usuario == 'tienda' && contrasenia == 'traigo*.*/20*18'){
		//req.session.user = {type: 'tienda', usuario: 'traigo', contrasenia: 'traigo*.*/20*18'};
		//return res.redirect('/tienda');
	//}

	if (type === 'admin') {
		getTypeUser('administradores', 'administrador', usuario, contrasenia, req, res);
	} else if (type === 'moderator') {
		getTypeUser('moderadores', 'moderador', usuario, contrasenia, req, res);
	} else if (type === 'shop') {
		db.query('SELECT su.*, ti.imagen, di.*  FROM sucursales su LEFT JOIN tiendas ti ON su.idtienda = ti.idtienda LEFT JOIN direcciones di ON su.iddireccion = di.iddireccion WHERE rut = ?', [usuario])
		.then(rows => {
			us = rows[0];
			if (us) {
				if (us.rut) {
					if (bcrypt.compareSync(contrasenia, us.contrasenia)) {
						// Convierte a booleano el valor de ida y vuelta
						us.ida_vuelta = (us.ida_vuelta == '1');
						if (us.listanegra === 'S') {
							return res.redirect('/login?fail=block&message=' + us.descripcion_listanegra);
						}
						req.session.user = {type: 'tienda', usuario: us};
						return res.redirect('/tienda');
					}
				}
			}
			console.log(contrasenia, us.contrasenia);
			return res.redirect('/login?fail=err');
		}).catch(err => {
			console.log(err);
			return res.redirect('/login?fail=err');
		});
	}
});

/**
 * Verifica el tipo de usuario que intenta ingresar al sistema
 * @param {String} table nombre de la tabla para la consulta
 * @param {String} type typo de usuario para la sesión
 */
function getTypeUser(table, type, usuario, contrasenia, req, res) {
	db.query('SELECT * FROM ' + table + ' WHERE cedula = ?', [usuario])
		.then(rows => {
			us = rows[0];
			/*bcrypt.compare(contrasenia, us.contrasenia)
			.then( samePassword => {
				console.log(samePassword);*/
				if (bcrypt.compareSync(contrasenia, us.contrasenia)) {
					connection.query('UPDATE ' + table + ' SET ultimo_acceso = ? WHERE cedula = ?', 
						[new Date(), rows[0].cedula], (err, rows, fields) => {
							if (err) {
								console.log(err);
								return res.redirect('/login?fail=err');
							} else {
								req.session.user = {
									type: type,
									usuario: us
								};
								return res.redirect('/administrador');
							}
					});
				} else {
					console.log(contrasenia, us.contrasenia);
					return res.redirect('/login?fail=err');
				}
			//});
		})
		.catch(err => {
			console.log(err);
			return res.redirect('/login?fail=err');
		});
}

module.exports = router;