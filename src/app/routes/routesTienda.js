const express = require('express');
const router = express.Router();
const sesion = require('../middlewares/sesionTienda');
const moment = require('moment');
let bodyParser = require('body-parser');
let Pusher = require('pusher');
const webpush = require('web-push');
let LocalStorage = require('node-localstorage').LocalStorage;
localStorage = new LocalStorage('./scratch');

let urlSocket = 'http://localhost:8002'

// Valida el tipo de entorno para saber la url del socket
if (process.env.NODE_ENV === 'production') {
	urlSocket = 'https://admin.traigo.com.co'; 
} else if (process.env.NODE_ENV === 'test') {
	urlSocket = 'http://traigo.com.co:8002'; 
}
const io = require('socket.io-client')(urlSocket);
router.use(sesion);
const app = express();
const Database = require('../../config/database');
let db = new Database(); // Instancia de base de datos promise
const connection = db.getConnection();
const Constants = require('./constants');

const models = require('../models');
//const sequelize = models.index;
const Tiendas = models.tiendas; // new require for db object tiendas
const Planes = models.planes; // new require for db object planes
const TiendasPlanes = models.tiendas_planes; // new require for db object tiendas planes
const HistorialTiendasPlanes = models.historial_tiendas_planes; // new require for db object historial tiendas planes
const Direcciones = models.direcciones; // new require for db object direcciones
const PedidosEmpresariales = models.pedidos_empresariales; // new require for db object direcciones
const Sucursales = models.sucursales; // new require for db object tiendas

const orderController = require('../controllers/order.controller');

app.use(require('body-parser').json());
/**
 * This module let us use HTTP verbs such as PUT or DELETE 
 * in places where they are not supported
 */ 
let methodOverride = require('method-override')

/**
 * using custom logic to override method
 * 
 * there are other ways of overriding as well
 * like using header & using query value
 */ 
router.use(methodOverride(function (req, res) {
  if (req.body && typeof req.body === 'object' && '_method' in req.body) {
    // look in urlencoded POST bodies and delete it
    let method = req.body._method
    delete req.body._method
    return method
  }
}));
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

router.get('/', (req, res) => {
	res.render('tienda/pages/home')
});

router.get('/cerrarsesion', (req, res) => {
	req.session.user = undefined;
	res.redirect('/');
});

/**
 * Obtiene los pedidos empresariales en curso por id de la sucursal
 * 
 * @param officeId identificador de la sucursal
 */
function getCurrentOrdersByOffice(officeId) {

	return models.sequelize.query(`SELECT pe.idpedido_empresarial AS pe_id, pe.nombre_cliente, pe.valor, pe.estado, pe.datafono, di.direccion, di.notas AS notas_direccion,
		pe.nombre_cliente, pe.telefono, pe.tamanio_maletin AS tam_maletin, DATE_FORMAT(pe.fecha,'%H:%i:%s') hora, r.foto_perfil, r.cedula AS ri_id,
        CONCAT(r.nombre, ' ', r.apellido) AS rider, r.latitude, r.longitude, r.tipo_vehiculo FROM pedidos_empresariales pe LEFT JOIN riders r ON r.cedula = pe.idrider 
        LEFT JOIN direcciones di ON pe.iddireccion = di.iddireccion WHERE idsucursal = ? AND pe.estado != ? AND pe.estado != ? AND pe.estado != ?`,
        { replacements: [officeId, 'Entregado', 'Error', 'Cancelado'], type: models.sequelize.QueryTypes.SELECT })
        .then((orderBussines) => {
			for (let i = 0; i < orderBussines.length; i++) {
				orderBussines[i].tam_maletin = getConstantById(Constants.TIPO_MALETIN, orderBussines[i].tam_maletin).name;
				orderBussines[i].color = getColorState(orderBussines[i].estado);
			}
            return orderBussines;
        }).catch((err) => {
            console.log('Error al obtener los pedidos en curso de la sucursal', err);
            return [];
        });
};

/**
 * Obtiene los datos del pedido por el ID
 *
 * @param {*} orderId 
 */
function getOrderById(orderId) {
	return models.sequelize.query(`SELECT pe.idpedido_empresarial AS pe_id, pe.nombre_cliente, pe.valor, pe.estado, pe.datafono, di.direccion, di.notas AS notas_direccion,
		pe.nombre_cliente, pe.telefono, r.foto_perfil, r.cedula AS ri_id,
        CONCAT(r.nombre, ' ', r.apellido) AS rider, r.latitude, r.longitude, r.tipo_vehiculo FROM pedidos_empresariales pe LEFT JOIN riders r ON r.cedula = pe.idrider 
        LEFT JOIN direcciones di ON pe.iddireccion = di.iddireccion WHERE idpedido_empresarial = ?`,
		{ replacements: [orderId], type: models.sequelize.QueryTypes.SELECT })
		.then((order) => {
			if (order.length > 0) {
				order[0].color = getColorState(order[0].estado);
			}
			return order;
		})
		.catch((err) => {
			console.log('Error al obtener el pedido', err);
			return [];
		});
}
/**
 * Obtiene el color del estado del pedido
 *
 * @param state estado de pedido
 * @returns color de estado de pedido
 */
function getColorState(state) {
	if (state === 'Error') {
		return 'label color-state-error';
	} else if (state === 'Rider en camino') {
		return 'label color-state-onrute'; // Amarillo
	} else if (state === 'Rider con productos') {
		return 'label color-state-products'; // Verde
	} else if (state === 'Rider donde el cliente') {
		return 'label color-state-oncustomer'; // Gris cañeria
	} else if (state === 'Rider en la tienda') {
		return 'label color-state-onshop'; // Azul 
	} else if (state === 'En proceso') {
		return 'label color-state-process'; // Rojo
	} else {
		return 'label color-state-other'; // Super duper ultra rojo
	}
}

/**
 * Obtiene todas las tiendas
 */
router.get('/get-current-orders', async (req, res) => {
	// Datos de la sucursal en sesion
	const sucursal = req.session.user.usuario;
	const listCurrentOrders = await getCurrentOrdersByOffice(sucursal.idsucursal);

	res.status(200).json({orders: listCurrentOrders});
});

router.get('/realizarpedido', async (req, res) => {
	let info = req.query.info;

	// Datos de la sucursal en sesion
	const sucursal = req.session.user.usuario;

	// Datos de respuesta
	let data = {
		info: info,
		maletines: Constants.TIPO_MALETIN
	}

	// Verifica si la sucursal es de tipo especial
	if (res.locals.usuario.usuario.tipo === getConstant(Constants.TIPO_SUCURSAL, 'Especial').id) { 
		res.render('tienda/pages/realizarpedidoespecial', {
			info: info,
			maletines: Constants.TIPO_MALETIN
		});	
	} 
	// Verifica si la sucursal es de tipo Prepagada
	else if (res.locals.usuario.usuario.tipo === getConstant(Constants.TIPO_SUCURSAL, 'Prepagada').id) { 
		
		// Se asigna un nuevo objeto para saber el tipo de tienda
		data.tienda = {
			tipo: sucursal.tipo
		}

		// Busca el estado actuvo de tiendas planes
		const estado_activo = getConstant(Constants.ESTADO_TIENDAS_PLANES, 'Activo').id;

		// Se busca la tienda asociada a la sucursal
		await Tiendas.findOne({
			where: {
				idtienda: sucursal.idtienda
			}
		})
		.then(async t => {

			// Verifica si hay elementos activos
			if (t) {

				// Operaciones del ORM
				let op = models.Sequelize.Op;

				// Se asigna el valor obtenido a objeto de datos a enviar
				data.tienda.plan = await TiendasPlanes.findOne({
					where: {
						tiendas_id: sucursal.idtienda,
						estado: estado_activo	// Se filtra por estado activo
					},
					raw: true
				});
				// console.log(data.tienda);

				// Valida si tiene un plan activo
				if (!data.tienda.plan) {
					return res.redirect('/tienda/listapedidos?info=limit');
				}

				// estado historial en curso
				const EST_ENCURSO = getConstant(Constants.ESTADO_HISTORIAL_PLANES, 'En curso').id;
				// estado historial en pendiente
				const EST_PENDIENTE = getConstant(Constants.ESTADO_HISTORIAL_PLANES, 'Pendiente').id;

				// Cuenta todo el historial de planes de la tienda pendientes
				await HistorialTiendasPlanes.findAll({
					where: {
						tiendas_planes_tiendas_id: sucursal.idtienda,
						[op.or]: [
							{estado: EST_ENCURSO}, // Planes En curso
							{estado: EST_PENDIENTE} // Planes pendientes
						]
					}
				})
				.then(htp => {
/*
					// Contador de pedidos pendientes
					let cont_pendientes = 0;

					// Cantidad de pedidos pendientes
					let cant_pendientes = 0;

					let htp_activo;

					// Se recorren todos los elementos del historial
					for (const key in htp) {
						if (htp.hasOwnProperty(key)) {
							const element = htp[key];
							
							// Se verifica si el estado del historial es En curso
							if (element.estado == EST_ENCURSO) {
								//data.tienda.plan.activo = element;
								htp_activo = element;
							} 
							// Se verifica si el estado del historial es pendiente
							else if (element.estado == EST_PENDIENTE) {
								
								cont_pendientes += 1;

								cant_pendientes += element.cant_pedidos + element.adicionales;
							}
						}
					}

					// Porcentaje de pedidos usados
					const pct_restantes = (parseInt(data.tienda.plan.total_pedidos) / (parseInt(htp_activo.cant_pedidos) + parseInt(htp_activo.adicionales))) * 100;

					// Porcentaje de pedidos restantes
					const pct_usados = 100 - pct_restantes;

					// se asignan la cantidad de elementos pendientes
					data.tienda.plan.cont_pendientes = cont_pendientes;
					data.tienda.plan.cant_pendientes = cant_pendientes;

					data.tienda.plan.usados = parseInt(htp_activo.cant_pedidos) + parseInt(htp_activo.adicionales) - parseInt(data.tienda.plan.total_pedidos);
					data.tienda.plan.pct_usados = pct_usados;
					data.tienda.plan.restantes = data.tienda.plan.total_pedidos;
					data.tienda.plan.pct_restantes = pct_restantes;*/

					//console.log('Tienda', data);

					res.render('tienda/pages/realizarpedido', data);

				})
				.catch(err => {
					console.log('Error al buscar el historial de planes de la tienda', err);
					return res.redirect('/tienda/listapedidos?info=err');		
				});
				
			} else {
				return res.redirect('/tienda/listapedidos?info=limit');
			}
			
		})
		.catch(err => {
			console.log('Error al buscar la tienda', err);
			return res.redirect('/tienda/listapedidos?info=err');
		});
	} else {
		res.render('tienda/pages/realizarpedido', data);
	}	
	
});

/**
 * Obtiene las cantidades existentes y de planes de las tiendas prepagadas
 */
router.get('/get-qty-orders-prepaid', async (req, res) => {

	// Datos de la sucursal en sesion
	const sucursal = req.session.user.usuario;
	
	// Se asigna un nuevo objeto para saber el tipo de tienda
	let data = {
		cont_pendientes: 0,
		cant_pendientes: 0,
		usados: 0,
		pct_usados: 0,
		restantes: 0,
		pct_restantes: 0
	};

	// Busca el estado actuvo de tiendas planes
	const estado_activo = getConstant(Constants.ESTADO_TIENDAS_PLANES, 'Activo').id;

	// Se busca la tienda asociada a la sucursal
	await Tiendas.findOne({
		where: {
			idtienda: sucursal.idtienda
		}
	})
	.then(async t => {

		// Verifica si hay elementos activos
		if (t) {

			// Operaciones del ORM
			let op = models.Sequelize.Op;

			// Se asigna el valor obtenido a objeto de datos a enviar
			const tp = await TiendasPlanes.findOne({
				attributes: ['total_pedidos', 'planes_id'],
				where: {
					tiendas_id: sucursal.idtienda,
					estado: estado_activo	// Se filtra por estado activo
				},
				raw: true
			});

			// Valida si la sucursal cuenta con un plan activo
			if (tp) {
				// Busca el plan asociado a la tienda
				const planes = await Planes.findOne({
					where: { id: tp.planes_id }, raw: true
				});
				// Asigna el tipo de plan del plan
				tp.tipo_plan = planes.tipo;
			} else {
				return res.status(200).json(data);
			}

			// estado historial en curso
			const EST_ENCURSO = getConstant(Constants.ESTADO_HISTORIAL_PLANES, 'En curso').id;
			// estado historial en pendiente
			const EST_PENDIENTE = getConstant(Constants.ESTADO_HISTORIAL_PLANES, 'Pendiente').id;

			// Cuenta todo el historial de planes de la tienda pendientes
			await HistorialTiendasPlanes.findAll({
				where: {
					tiendas_planes_tiendas_id: sucursal.idtienda,
					[op.or]: [
						{estado: EST_ENCURSO}, // Planes En curso
						{estado: EST_PENDIENTE} // Planes pendientes
					]
				}
			})
			.then(htp => {

				// Contador de pedidos pendientes
				let cont_pendientes = 0;

				// Cantidad de pedidos pendientes
				let cant_pendientes = 0;

				// Historial de plan activo
				let htp_activo;

				// Se recorren todos los elementos del historial
				for (const key in htp) {
					if (htp.hasOwnProperty(key)) {
						const element = htp[key];
						
						// Se verifica si el estado del historial es En curso
						if (element.estado == EST_ENCURSO) {
							//data.tienda.plan.activo = element;
							htp_activo = element;
						} 
						// Se verifica si el estado del historial es pendiente
						else if (element.estado == EST_PENDIENTE) {
							
							cont_pendientes += 1;

							cant_pendientes += element.cant_pedidos + element.adicionales;
						}
					}
				}

				// Porcentaje de pedidos usados
				const pct_restantes = (parseInt(tp.total_pedidos) / (parseInt(htp_activo.cant_pedidos) + parseInt(htp_activo.adicionales))) * 100;

				// Porcentaje de pedidos restantes
				const pct_usados = 100 - pct_restantes;

				// se asignan la cantidad de elementos pendientes
				data.cont_pendientes = cont_pendientes;
				data.cant_pendientes = cant_pendientes;

				data.usados = parseInt(htp_activo.cant_pedidos) + parseInt(htp_activo.adicionales) - parseInt(tp.total_pedidos);
				data.pct_usados = pct_usados;
				data.restantes = tp.total_pedidos;
				data.pct_restantes = pct_restantes;

				// Valida si el tipo del plan activo es ilimitado
				if (tp.tipo_plan == getConstant(Constants.TIPO_PLAN, 'Ilimitado').id) {
					data.pct_restantes = 100;
					data.pct_usados = 100;
					data.restantes = 'Ilimitado';
					data.usados = 'Plan vencible';
				}

				// res.render('tienda/pages/realizarpedido', data);
				res.status(200).json(data);

			})
			.catch(err => {
				console.log('Error al buscar el historial de planes de la tienda', err);
				return res.status(400).json('Error al buscar el historial de planes de la tienda.');
				// return res.redirect('/tienda/listapedidos?info=err');
			});
			
		} else {
			return res.status(400).json('No se ha encontrado la tienda.');
			// return res.redirect('/tienda/listapedidos?info=limit');
		}
		
	})
	.catch(err => {
		console.log('Error al buscar la tienda', err);
		return res.status(400).json('No se ha encontrado la tienda.');
	});
});

/**
 * Permite cancelar un pedido 
 */
router.get('/cancelarpedido', async (req, res) => {

	let idpedido = req.query.id;

	// Verifica si existe el elemento
	if (idpedido) {

		// Inicializacion de transaccion activa
		let trans = await models.sequelize.transaction();

		try {
			// Busca el pedido empresarial por el id
			await PedidosEmpresariales.findByPk(idpedido, {transaction: trans})
			.then(async pedido => {

				// Verifica si existe el pedido
				if (pedido) {

					let nuevoEstado;

					if (pedido.estado != 'En proceso' && pedido.estado != 'Rider en la tienda') {
						nuevoEstado = 'Error';
					} else {
						nuevoEstado = 'Cancelado';
					}

					// Actualiza el estado del pedido
					pedido.estado = nuevoEstado;

					// Guarda la información actualizada
					await pedido.save()
					.then(async () => {

						// Datos de la sucursal en sesion
						let sucursal = res.locals.usuario.usuario;

						// Verifica si el estado es cancelado
						if (nuevoEstado == 'Cancelado') {
							// Verifica si la sucursal es prepagada
							if (sucursal.tipo === getConstant(Constants.TIPO_SUCURSAL, 'Prepagada').id ) {

								// Busca las tiendas planes que tienen estado activo
								await TiendasPlanes.findAll({
									where: {
										tiendas_id: sucursal.idtienda,
										estado: getConstant(Constants.ESTADO_TIENDAS_PLANES, 'Activo').id
									}
								}, {transaction: trans})
								.then(async tp => {
									// Verifica si hay elementos
									if (tp.length > 0) {
										// Tienda plan activo
										let tiendaPlan = tp[0];

										// Se suma el pedido cancelado al plan activo
										tiendaPlan.total_pedidos = parseInt(tiendaPlan.total_pedidos) + 1;

										// Guarda la acualizacion del saldo de pedidos + 1
										await tiendaPlan.save()
										.catch(async err => {
											// Devuelve los cambios realizados
											await trans.rollback();

											console.log('Error al actualizar el saldo del plan activo.', err);
											res.status(400).json('Error al cancelar el pedido.');
											// res.redirect('/tienda/listapedidos?info=err');		
										});
									} else {
										// Selecciona el ultimo elemento activo, elemento inactivo más reciente
										await models.sequelize.query(
											'SELECT * FROM tiendas_planes WHERE updated_at = (SELECT MIN(updated_at) FROM tiendas_planes WHERE estado = $1)',
										{ bind: [getConstant(Constants.ESTADO_TIENDAS_PLANES, 'Inactivo').id], type: models.sequelize.QueryTypes.SELECT })
										.then(async tpi => {

											// Verifica si no hay elementos
											if (tpi.length > 0) {

												// Datos de la tienda plan filtrada
												let tiendaPlan = tpi[0];

												// Actualiza el saldo y estado de la tienda plan inactiva
												await TiendasPlanes.update(
													{ 
														total_pedidos: parseInt(tiendaPlan.total_pedidos) + 1,
														estado: getConstant(Constants.ESTADO_TIENDAS_PLANES, 'Activo').id
													},
													{
														where: {
															tiendas_id: tiendaPlan.tiendas_id,
															planes_id: tiendaPlan.planes_id
														}
													}, {transaction: trans}
												);

												// Busca el historial con actualizacion mas reciente de estado finalizado
												await HistorialTiendasPlanes.findAll({
													where: {
														tiendas_planes_planes_id: tiendaPlan.planes_id,
														tiendas_planes_tiendas_id: tiendaPlan.tiendas_id,
														estado: getConstant(Constants.ESTADO_HISTORIAL_PLANES, 'Finalizado').id
													},
													order: [['updatedAt']], // Los mas nuevos
													limit: 1
												}, {transaction: trans})
												.then(async htp => {
													// Verifica si hay elementos
													if (htp.length > 0) {
														let historial = htp[0];

														historial.estado = getConstant(Constants.ESTADO_HISTORIAL_PLANES, 'En curso').id;

														await historial.save()
														//.then(async () => await trans.commit());

													} else {
														await trans.rollback();
														console.log('No se ecuentra un historial para actualizar');
														res.status(400).json('Error al cancelar el pedido.');
														// res.redirect('/tienda/listapedidos?info=err');
													}
												});

											} else {
												console.log('No hay elementos inactivos');
												res.status(400).json('Error al cancelar el pedido.');
												// res.redirect('/tienda/listapedidos?info=err');
											}
										})
										.catch(async err => {
											// Devuelve los cambios realizados
											await trans.rollback();

											console.log(err);
											// res.redirect('/tienda/listapedidos?info=err');
											res.status(400).json('Error al cancelar el pedido.');
										});
									}
								});
							} 
						}

						// Guarda todos los cambios
						await trans.commit();
						console.log('CAncela pedido');
						
						// Emite evento para actualizar los pedidos empresariales
						if (io) {
							console.log('Cancela pedido de la tienda', res.locals.usuario.usuario.idsucursal);
							io.emit("ordershop");
						}
						res.status(200).json('Cancela pedido de la tienda.');
						// res.redirect('/tienda/listapedidos?info=eliminated');
						
					});
				} else {
					console.log('No se encuentra el elemento');
					res.status(400).json('Error al cancelar el pedido.');
					// res.redirect('/tienda/listapedidos?info=err');
				}
			});			
		} catch (err) {
			// Devuelve los cambios realizados
			await trans.rollback();

			console.log('Error en la transaccion', err);
			//res.redirect('/tienda/listapedidos?info=err');
			res.status(400).json('Error al cancelar el pedido.');

		}		
	} else {
		console.log('No provee un identificador');
		res.status(400).json('Error al cancelar el pedido.');
		//res.redirect('/tienda/listapedidos?info=err');
	}
	
});

// Lista de todos los pedidos
router.get('/listapedidos', (req, res) => {
	let info = req.query.info;

	let date = new Date()

	let day = date.getDate()
	let month = date.getMonth() + 1
	let year = date.getFullYear()

	let current = year + '-' + month + '-' + day;

	db.query('SELECT nd.title, nd.description, date_format(n.created_at,\'%h:%i:%s\') created_at FROM notifications n JOIN notifications_detail nd ON n.notifications_detail_id = nd.id WHERE DATE(n.created_at) = ?',[current])
	.then(rows => {
		req.session.user.notifications = rows;
	})
	.catch(err => {
		console.log(err);
	});

	connection.query('select * from pedidos_empresariales where estado != ? and estado != ? and estado != ? and idsucursal = ? order by fecha asc',
		['Entregado', 'Cancelado', 'Error', res.locals.usuario.usuario.idsucursal], (err, rows, fields) => {
			if (err) {
				console.log(err);
				return res.redirect('/tienda');
			} else {
				for(let i= 0; i < rows.length; i++) {
					let date = new Date(rows[i].fecha).toLocaleString();
					let date_split =  date.split(" ");
					rows[i].fecha = date_split[0];
					rows[i].hora = date_split[1];
				}
				res.render('tienda/pages/listapedidos', {
					info: info,
					pedidos: rows
				});
			}
		});
});

router.get('/listatraigo', (req, res) => {
	let info = req.query.info;

	connection.query('select pc.*, r.nombre as nombre, r.apellido as apellido, r.telefono as telefono from pedidos_clientes as pc join (select pcp.idpedido_clientes from productos p join pclientes_productos pcp on p.idproducto = pcp.idproducto where p.idsucursal = ? group by pcp.idpedido_clientes) as suc on suc.idpedido_clientes = pc.idpedido_clientes left join riders r on r.cedula = pc.idrider where pc.estado = ? or pc.estado = ? or pc.estado = ?',
		[res.locals.usuario.usuario.idsucursal, 'En proceso', 'Tomado por el Rider', 'Rider obteniendo productos'], (err, rows, fields) => {
			if (err) {
				console.log(err);
				return res.redirect('/tienda');
			} else {
				res.render('tienda/pages/listatraigo', {
					info: info,
					pedidos: rows
				});
			}
		});
});

router.get('/pedido', (req, res) => {
	let idpedido = req.url.split('?')[1].split('=')[1];
	connection.query('select pe.*, r.nombre as nombrerider, r.apellido as apellidorider, r.telefono as telefonorider, r.latitude as latituderider, r.longitude as longituderider, d.direccion as direccionpedido, d.notas as notasdireccionpedido from pedidos_empresariales pe left join riders r on r.cedula = pe.idrider join direcciones d on d.iddireccion = pe.iddireccion where idpedido_empresarial = ?',
		[idpedido], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.redirect('/tienda/listapedidos');
			} else {
				res.render('tienda/pages/pedido', {
					pedido: rows[0]
				});
			}
		});
});

router.get('/pedidotraigo', (req, res) => {
	let idpedido = req.url.split('?')[1].split('=')[1];
	let totalelementos = 0, totalselecciones = 0, totaladiciones = 0, totalproductos = 0; 
	connection.query('select sum(e.precio) as totalelementos from pclientes_productos pp join pclientes_prod_elementos ppe on pp.idpclientes_productos = ppe.idpclientes_productos join elementos e on e.idelemento = ppe.idelemento where pp.idpedido_clientes = ? group by pp.idpedido_clientes',
		[idpedido], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(ee);
			} else {
				if (rows.length != 0) {
					totalelementos = rows[0].totalelementos;
				}
				connection.query('select sum(a.precio) as totaladiciones from pclientes_productos pp join pclientes_prod_adiciones ppe on pp.idpclientes_productos = ppe.idpclientes_productos join adiciones a on a.idadicion = ppe.idadicion where pp.idpedido_clientes = ? group by pp.idpedido_clientes',
					[idpedido], (err, rows, fields) => {
						if (err) {
							console.log(err);
							res.send(ee);
						} else {
							if (rows.length != 0) {
								totaladiciones = rows[0].totaladiciones;
							}
							connection.query('select sum(a.precio) as totalselecciones from pclientes_productos pp join pclientes_prod_selecciones ppe on pp.idpclientes_productos = ppe.idpclientes_productos join selecciones a on a.idseleccion = ppe.idseleccion where pp.idpedido_clientes = ? group by pp.idpedido_clientes',
								[idpedido], (err, rows, fields) => {
									if (err) {
										console.log(err);
										res.send(ee);
									} else {
										if (rows.length != 0) {
											totalselecciones = rows[0].totalselecciones;
										}
										connection.query('select sum(p.precio) as totalproductos from pclientes_productos pp join productos p on p.idproducto = pp.idproducto where pp.idpedido_clientes = ? group by pp.idpedido_clientes',
											[idpedido], (err, rows, fields) => {
												if (err) {
													console.log(err);
													res.send(err);
												} else {
													let total = totaladiciones + totalelementos + totalselecciones + rows[0].totalproductos;
													connection.query('select p.*, pcp.idpclientes_productos as id from pclientes_productos pcp join productos p on p.idproducto = pcp.idproducto where pcp.idpedido_clientes = ?',
														[idpedido], (err, rows, fields) => {
															if (err) {
																console.log(err);
																res.redirect('/tienda/listapedidos');
															} else {	
																res.render('tienda/pages/pedidotraigo', {
																	productos: rows, total: total
																});
															}
														});			
												}
											});
									}
								});
						}
					});
			}
		});
});

router.get('/historial', (req, res) => {
	let info = req.query.info;
	res.render('tienda/pages/historial', {
		info: info
	});
});

router.post('/historialfiltro', (req, res) => {
	let info = req.query.info;

	let request = req.body;

	let dateFilterBefore = request.before.fecha;// + ' ' + request.before.hora;
	let dateFilterAfter = request.after.fecha;// + ' ' + request.after.hora;

	connection.query("SELECT pd.*, r.nombre AS nombre_rider, r.apellido AS apellido_rider FROM pedidos_empresariales pd JOIN riders r ON r.cedula = pd.idrider WHERE " +
	"pd.idsucursal = ? AND pd.fecha >= ? AND pd.fecha <= ? ORDER BY fecha DESC",
	//connection.query('select * from pedidos_empresariales where idsucursal = ? order by fecha desc', 
		[res.locals.usuario.usuario.idsucursal, dateFilterBefore, dateFilterAfter], (err, rows, fields) => {
			res.contentType('json');
			if (err) {
				console.log(err);
				res.send({ err: JSON.stringify(err) });
			} else {
				for (let i = 0; i < rows.length; i++) {
					// Convierte los valor de la fecha a un formato mas entendible
					let date = new Date(rows[i].fecha).toLocaleString();
					let date_split =  date.split(" ");
					rows[i].nombre_rider = rows[i].nombre_rider + ' ' + rows[i].apellido_rider;
					rows[i].fecha = date_split[0];
					rows[i].hora = date_split[1];
					/*  Colores estado
						#F4D095 -> naranja = Error
						#FCBEBE -> rojo = Cancelado
						#C9F7C9 -> verde = Entregado */
					if (rows[i].estado === 'Entregado') {
						rows[i].color = '#C9F7C9'; // Verde tortuga ninja
					} else if (rows[i].estado === 'Cancelado') {
						rows[i].color = '#FCBEBE'; // Rojo que te cojo
					} else if (rows[i].estado === 'Error') { 
						rows[i].color = '#F4D095'; // Naranja mecanico
					}
				}
				res.send(rows);
			}
	});
});

router.post('/agregarpedido', async (req, res) => {

	let idsucursal = req.session.user.usuario.idsucursal;
	let valor = req.body.valor;
	let numero_factura = req.body.numero_factura;
	let notas_adicionales = req.body.notas_adicionales;
	let nombre = req.body.nombre;
	let telefono = req.body.telefono;
	let direccion = req.body.direccion;
	let notas_direccion = req.body.notas_direccion;

	let retenido = res.locals.usuario.usuario.retenido;
	let datafono = (req.body.datafono ? 'S' : 'N');
	let idDireccion;

	let maletin = req.body.maletin? req.body.maletin : null;

	let costo_envio = res.locals.usuario.usuario.costo_envio;

	let queryOrder = {};
	let querysDirection = {};

	const idaVuelta = req.body.ida_vuelta;
	let direccionesIdaVuelta = null;

	// Transaccion para el proceso de insercion y validacion de nuevos pedidos
	let trans = await models.sequelize.transaction({ autocommit: false });

	let data;

	try {
		// Se obtienen los constos y los datos de la sucursal
		const cost = await models.sequelize.query('SELECT * FROM costos c INNER JOIN sucursales s ' +
		' ON c.idmunicipio = s.idmunicipio WHERE s.idsucursal = $1', 
		{ bind: [idsucursal], type: models.sequelize.QueryTypes.SELECT });

		// Obtiene la suscursal de tipo especial
		const sucursalEspecial = getConstant(Constants.TIPO_SUCURSAL, 'Especial').id;
		// Obtiene la suscursal de tipo prepagada
		const sucursalPrepagada = getConstant(Constants.TIPO_SUCURSAL, 'Prepagada').id;
		// Obtiene el valor de la comision del rider
		comision_rider_empresarial = cost[0].comision_rider_empresarial;
		// Inicializacion de atributos de insercion del pedido
		queryOrder =  {
			costo_envio: costo_envio,
			idsucursal: idsucursal,
			retenido: retenido,
			pagado: 'N',
			comision_entregada: 'N',
			pagado_sucursal: 'N',
			nombre_cliente: nombre,
			notas: notas_adicionales,
			valor: valor,
			numero_factura: numero_factura,
			telefono: telefono,
			estado: 'En proceso',
			fecha: new Date(),
			datafono: datafono,
			tamanio_maletin: maletin,
			comision_rider: comision_rider_empresarial,
			ida_vuelta: idaVuelta
		};

		// Se organiza el objeto para creacion de nueva direccion
		querysDirection = {
			direccion: direccion, 
			idmunicipio: res.locals.usuario.usuario.idmunicipio,
			notas: notas_direccion
		};

		// Identificador de direccion no existente de las direcciones ida y vuelta
		let indexAddress;

		// Valida si se habilta el ida y vuelta para el pedido a crear
		if (idaVuelta === true) {
			// Asigna el valor de ida y vuelta
			queryOrder.valor_ida_vuelta = cost[0].valor_ida_vuelta;

			// Se asigna las direcciones de ida y vuelta 
			// La posicion [0] es a la que el rider debe llegar y regresar (ida y vuelta)
			// La posicion [1] es la de destino
			// La [0] puede ser la de la tienda o la del cliente
			direccionesIdaVuelta = req.body.direciones_ida_vuelta;

			// Index de la direccion que se debe crear | Valida si la primera direccion no existe
			indexAddress = (direccionesIdaVuelta[0].id === null) ? 0 : 1;

			// Asigna datos de la direccion a crear
			querysDirection.direccion = direccionesIdaVuelta[indexAddress].name;
			querysDirection.notas = direccionesIdaVuelta[indexAddress].notes;
		}

		// verifica si es un pedido especial
		if (res.locals.usuario.usuario.tipo === sucursalEspecial) { 
			
			queryOrder.valor = 0.0;
			querysDirection.direccion = '';
			querysDirection.notas = '';

			delete queryOrder.nombre_cliente;
			delete queryOrder.notas_adicionales;
			delete queryOrder.telefono;

		// verifica si es un pedido prepagado
		} else if (res.locals.usuario.usuario.tipo === sucursalPrepagada) { 
				
			/**
			 * Identificador de la tienda
			 */
			const tienda_id = res.locals.usuario.usuario.idtienda;

			/**
			 * Busca el historial por estado, tiendas, y/o plan
			 *
			 * @param {*} plan_id | opcional
			 * @param {*} tienda_id 
			 * @param {*} estado 
			 */
			function getHistorialTiendasPlanes(trans, estado, tienda_id, plan_id) {

				/**
				 * Condiciones para el where
				 */
				let condiciones = {
					tiendas_planes_tiendas_id: tienda_id,
					estado: estado
				};

				// Verifica si va a filtrar por plan
				if (plan_id) {
					condiciones.tiendas_planes_planes_id = plan_id;
				}

				// Se buscan todo el historial con las condiciones preestablecidas
				return HistorialTiendasPlanes.findAll({
					where: condiciones,
					order: [['createdAt']]
				}, {transaction: trans});
			}

			// Busca el plan activo asignado a la tienda
			const tiendap = await TiendasPlanes.findOne({
				where: {
					tiendas_id: tienda_id,
					estado: getConstant(Constants.ESTADO_TIENDAS_PLANES, 'Activo').id
				}
			}, {transaction: trans});

			/**
			 * Actualiza el historial de tiendas planes 
			 * cuando se habilita un estado pendiente
			 * @param {*} historial_id 
			 */
			async function updateHistorialTiendasPlanes(historial_id, tienda_id, trans, tiendaPlan) {
							
				try {

					// Actaliza el estado del historial activo
					await HistorialTiendasPlanes.update(
						{ estado: getConstant(Constants.ESTADO_HISTORIAL_PLANES, 'Finalizado').id }, 
						{ 
							where: { 
								tiendas_planes_tiendas_id: tienda_id,
								estado: getConstant(Constants.ESTADO_HISTORIAL_PLANES, 'En curso').id
							}
						}, {transaction: trans}
					);
					
					// Datos de actualizacion de pedido a activar
					const dataToUpdate = {
						estado: getConstant(Constants.ESTADO_HISTORIAL_PLANES, 'En curso').id
					};

					// Valida si el plan a activar es vencible
					if (tiendaPlan && tiendaPlan.vencible) {
						// Calcular las fechas de inicio y vencimiento con los dias habilitados
						dataToUpdate.fecha_inicio = new Date();
						dataToUpdate.fecha_vencimiento = moment().add(tiendaPlan.dias_habilitados, 'days').calendar();
					}

					// Actaliza el estado del historial pendiente
					await HistorialTiendasPlanes.update(
						//{ estado: getConstant(Constants.ESTADO_HISTORIAL_PLANES, 'En curso').id }, 
						dataToUpdate,
						{ 
							where: {
								id: historial_id
							}
						}, {transaction: trans}
					);

					return true;

				} catch (err) {
					console.log('Error al actualizar el historial de tiendas planes', err);
					await trans.rollback();
					return false;
				}
			}

			// Verifica si exite el elemento
			if (tiendap) {
				// const plan = await Planes.findByPk(tiendap.planes_id);
				// Valida si el plan es vencible
				if (tiendap.vencible === true) {
					//plan.tipo == getConstant(Constants.TIPO_PLAN, 'Ilimitado').id;
					// Fecha de fencimiento
					let fechaVencimiento = moment(tiendap.fecha_vencimiento, 'YYYY-MM-DD');
					// Fecha actual
					let fechaHoy = moment().startOf('day');
					// Obtiene el plan asignado
					const plan = await Planes.findByPk(tiendap.planes_id);
					// Valida si el plan es ilimitado
					const esIlimitado = (plan.tipo == getConstant(Constants.TIPO_PLAN, 'Ilimitado').id);
					// Valida si se excede de la fecha de vencimiento
					if (fechaVencimiento.diff(fechaHoy, 'days') < 0 || ( !esIlimitado && tiendap.total_pedidos == 0 ) ) {
						// Se obtien el historial pendiente de la tienda plan con estado activo
						const htp = await getHistorialTiendasPlanes(
							trans,
							getConstant(Constants.ESTADO_HISTORIAL_PLANES, 'Pendiente').id,
							tienda_id, 
							tiendap.planes_id
						);
						// Cuando NO hay renovaciones o planes pendientes del plan activo
						if (htp.length === 0) {
							// Obiene todos los historiales pendientes
							const htp2 = await getHistorialTiendasPlanes(
								trans,
								getConstant(Constants.ESTADO_HISTORIAL_PLANES, 'Pendiente').id,
								tienda_id
							);
							// Verificar si hay pedidos pendientes
							if (htp2.length > 0) {
								// Se obtiene el historial pendiente por orden de ingreso
								const historial = htp2[0];

								// Actualiza el estado del plan activo a finalizado por el plan pendiente a activo
								const isUpdate = await updateHistorialTiendasPlanes(historial.id, tienda_id, trans, historial);

								// Verifica si el historial actualizo los estados con exito
								if (isUpdate) {

									try {
										// Se actualiza la tienda plan con estado activo a inactivo
										await TiendasPlanes.update(
											{ 
												estado: getConstant(Constants.ESTADO_TIENDAS_PLANES, 'Inactivo').id,
												total_pedidos: 0 
											},
											{
												where: {
													tiendas_id: historial.tiendas_planes_tiendas_id,
													estado: getConstant(Constants.ESTADO_TIENDAS_PLANES, 'Activo').id
												}
											}, {transaction: trans}
										);

										// Datos para actualizar la cativacion de tienda plan
										const dataToUpdate = {
											estado: getConstant(Constants.ESTADO_TIENDAS_PLANES, 'Activo').id,
											total_pedidos: parseInt(historial.cant_pedidos) + parseInt(historial.adicionales) - 1
										};

										// Valida si el plan a activar es vencible
										if (historial && historial.vencible) {
											// Calcular las fechas de inicio y vencimiento con los dias habilitados
											dataToUpdate.fecha_inicio = new Date();
											dataToUpdate.fecha_vencimiento = moment().add(historial.dias_habilitados, 'days').calendar();
											// Valida si la canidad de pedidos del plan a asignar es cero (Plan Ilimitado)
											if ((parseInt(historial.cant_pedidos) + parseInt(historial.adicionales)) === 0) {
												dataToUpdate.total_pedidos = 0;
											}
										}
										// Se actualiza la tienda plan con estado pendiente a activo
										await TiendasPlanes.update(
											dataToUpdate,
											{
												where: {
													tiendas_id: historial.tiendas_planes_tiendas_id,
													planes_id: historial.tiendas_planes_planes_id
												}
											}, {transaction: trans}
										);

									} catch(err) { // Cuando ocurre un Error al actualizar el estado y total de pedidos de la tienda plan
										// Devuelve cambios de actualización
										await trans.rollback();
										console.log('Error actualizar el estado y total de pedidos de la tienda plan', err);
										
										// return res.redirect('/tiendas/listapedidos?info=err');
										return res.status(400).json('Error al guardar el pedido.');
									}
								} else { // Cuando la actualizacion de los estados del historial hay errores
									return res.status(400).json('Error al guardar el pedido.');
									// return res.redirect('/tiendas/listapedidos?info=err');
								}
							} else { // Cuando no hay renovaciones o planes pendientes

								// Actualiza el plan en curso a inactivo
								await TiendasPlanes.update(
									{ 
										estado: getConstant(Constants.ESTADO_TIENDAS_PLANES, 'Inactivo').id,
										total_pedidos: 0
									},
									{
										where: {
											planes_id: tiendap.planes_id,
											tiendas_id: tiendap.tiendas_id
										}
									}, {transaction: trans}
								);

								// Actualiza el historial del plan en curso a finalizado
								await HistorialTiendasPlanes.update(
									{ estado: getConstant(Constants.ESTADO_HISTORIAL_PLANES, 'Finalizado').id },
									{
										where: {
											tiendas_planes_planes_id: tiendap.planes_id,
											tiendas_planes_tiendas_id: tiendap.tiendas_id,
											estado: getConstant(Constants.ESTADO_HISTORIAL_PLANES, 'En curso').id
										}
									}, {transaction: trans});

								console.log('Se ha exedido el limite de pedidos.');
								await trans.commit();

								// return res.status(400).end();
								return res.status(400).json('Se ha exedido el limite de pedidos.').end();
								// await res.redirect('/tiendas/listapedidos?info=limit');
							}
						} else { // cuando existe un historial pendiente del estado actual activo

							let historial = htp[0];

							// Actualiza el estado del plan activo finalizado por el plan pendiente
							const isUpdate = updateHistorialTiendasPlanes(historial.id, tienda_id, trans, historial);

							// Verifica si el historial actualizo los estados con exito
							if (isUpdate) {

								// Datos de actualizacion de tienda plan
								const dataToUpdate = {
									total_pedidos: (historial.vencible)? 
										parseInt(historial.cant_pedidos) : parseInt(historial.cant_pedidos) + parseInt(historial.adicionales) - 1,
									adicionales: historial.adicionales
								};
								// Valida si el plan a activar es vencible
								if (historial && historial.vencible) {
									// Calcular las fechas de inicio y vencimiento con los dias habilitados
									dataToUpdate.fecha_inicio = new Date();
									dataToUpdate.fecha_vencimiento = moment().add(historial.dias_habilitados, 'days').calendar();
								}

								// Se actualiza la tienda plan con estado pendiente a activo
								await TiendasPlanes.update(
									dataToUpdate,
									{
										where: {
											tiendas_id: historial.tiendas_planes_tiendas_id,
											planes_id: historial.tiendas_planes_planes_id
										}
									}, {transaction: trans}
								);
							} else { // Si no actualiza el historial
								console.log(`No se encuentra el plan activo de la tienda ${tienda_id}`);
								await trans.rollback();
								return res.status(400).json('Error al realizar el pedido.');
								// return res.redirect('/tiendas/listapedidos?info=err');
							}
						}
					}
				}
				// Verifica si la cantidad de pedidos disponibles es 1
				else if (tiendap.total_pedidos == 0) {
					// Se obtien el historial de la tienda plan con estado activo
					const htp = await getHistorialTiendasPlanes(
						trans,
						getConstant(Constants.ESTADO_HISTORIAL_PLANES, 'Pendiente').id,
						tienda_id, 
						tiendap.planes_id
					)
					// Cuando no hay renovaciones o planes pendientes del plan activo
					if (htp.length === 0) {
						// Obiene todos los historiales pendientes
						const htp2 = await getHistorialTiendasPlanes(
							trans,
							getConstant(Constants.ESTADO_HISTORIAL_PLANES, 'Pendiente').id,
							tienda_id
						);
						// Verifica si hay planes pendientes
						if (htp2.length > 0) {
							// Se obtiene el historial pendiente por orden de ingreso
							const historial = htp2[0];

							// Actualiza el estado del plan activo finalizado por el plan pendiente
							const isUpdate = await updateHistorialTiendasPlanes(historial.id, tienda_id, trans);

							// Verifica si el historial actualizó los estados con exito
							if (isUpdate) {

								// Se actualiza la tienda plan con estado activo a inactivo
								await TiendasPlanes.update(
									{ estado: getConstant(Constants.ESTADO_TIENDAS_PLANES, 'Inactivo').id },
									{
										where: {
											tiendas_id: historial.tiendas_planes_tiendas_id,
											estado: getConstant(Constants.ESTADO_TIENDAS_PLANES, 'Activo').id
										}
									}, {transaction: trans}
								);

								// Se actualiza la tienda plan con estado pendiente a activo
								await TiendasPlanes.update(
									{ 
										estado: getConstant(Constants.ESTADO_TIENDAS_PLANES, 'Activo').id,
										total_pedidos: parseInt(historial.cant_pedidos) + parseInt(historial.adicionales) - 1
									},
									{
										where: {
											tiendas_id: historial.tiendas_planes_tiendas_id,
											planes_id: historial.tiendas_planes_planes_id
										}
									}, {transaction: trans}
								);

							} else { // Cuando la actualizacion de los estados del historial hay errores
								return res.status(400).json('Error al guardar el pedido.');
								// return res.redirect('/tiendas/listapedidos?info=err');
							}
						} else { // Cuando no hay renovaciones o planes pendientes

							tiendap.estado = getConstant(Constants.ESTADO_TIENDAS_PLANES, 'Inactivo').id,

							await tiendap.save();

							await HistorialTiendasPlanes.update(
								{ estado: getConstant(Constants.ESTADO_HISTORIAL_PLANES, 'Finalizado').id },
								{
									where: {
										tiendas_planes_planes_id: tiendap.planes_id,
										tiendas_planes_tiendas_id: tiendap.tiendas_id,
										estado: getConstant(Constants.ESTADO_HISTORIAL_PLANES, 'En curso').id
									}
								}, {transaction: trans}
							);

							await trans.commit();

							return res.status(400).json('Se ha exedido el limite de pedidos.');
							// await res.redirect('/tiendas/listapedidos?info=limit');
						}

					} else { // cuando existe un historial pendiente del estado actual activo

						let historial = htp[0];

						// Actualiza el estado del plan activo finalizado por le plan pendiente
						const isUpdate = updateHistorialTiendasPlanes(historial.id, tienda_id, trans);

						// Verifica si el historial actualizo los estados con exito
						if (isUpdate) {

							// Se actualiza la tienda plan con estado pendiente a activo
							await TiendasPlanes.update(
								{ 
									total_pedidos: parseInt(historial.cant_pedidos) + parseInt(historial.adicionales) - 1,
									adicionales: historial.adicionales
								},
								{
									where: {
										tiendas_id: historial.tiendas_planes_tiendas_id,
										planes_id: historial.tiendas_planes_planes_id
									}
								}, {transaction: trans}
							);
						} else { // Si no actualiza el historial
							console.log(`No se encuentra el plan activo de la tienda ${tienda_id}`);
							return res.status(400).json('Error al realizar el pedido.');
							// return res.redirect('/tiendas/listapedidos?info=err');
						}
					}
				} else {

					await TiendasPlanes.update(
						{ total_pedidos: parseInt(tiendap.total_pedidos) - 1 }, 
						{ 
							where: { 
								tiendas_id: tiendap.tiendas_id,
								planes_id: tiendap.planes_id
							}
						}, {transaction: trans}
					);

					// Actualiza el salo de pedidos disponibles
					/*await tiendap.update({
						total_pedidos: parseInt(tiendap.total_pedidos) - 1
					})
					.catch(err => {
						console.log('Error en la actualización total de pedidos de tiendas planes', err);
						return res.redirect(`/tiendas/listapedidos?info=limit`);
					});*/
				}
			} else {
				console.log(`No se encuentra el plan activo de la tienda ${tienda_id}`);
				return res.status(400).json('Error al guardar el pedido.');
				// res.redirect('/tiendas/listapedidos?info=err');
			}

		}

		// Se crea una nueva direccion para el pedido empresarial
		const direction = await Direcciones.create(querysDirection, {transaction: trans});			
		
		// Se obtiene el id de la nueva dirección
		queryOrder.iddireccion = direction.iddireccion;

		// Valida si se habilta el ida y vuelta para el pedido a crear
		if (idaVuelta === true) {
			// Agrega el id de la direccion nueva (creada)
			direccionesIdaVuelta[indexAddress].id = direction.iddireccion;
			// Asigna el id de la direccion de ida y vuelta
			queryOrder.id_direccion_ida_vuelta = direccionesIdaVuelta[0].id;
		}
		// Se crea el nuevo pedido empresarial
		data = await PedidosEmpresariales.create(queryOrder, {transaction: trans});	
	} catch(err) {
		console.log('Error al crear el pedido empresarial', err);
		await trans.rollback();
		return res.status(400).json('Error al realizar el pedido.');
	}


	// Confirma todos los cambios de actualización
	await trans.commit();

	/*let order = rows[0]
	let pusher = new Pusher({
		appId: process.env.PUSHER_APP_ID,
		key: process.env.PUSHER_APP_KEY,
		secret: process.env.PUSHER_APP_SECRET,
		cluster: process.env.PUSHER_APP_CLUSTER
	});*/
	// pusher.trigger('notifications', 'order_shop', order, req.headers['x-socket-id']);
	// Obtiene la subcripcion de las notificaciones
	const subscription = localStorage.getItem('notification');
	if (subscription) {
		const payload = JSON.stringify({
			title: 'Nuevo',
			message: 'Pedido empresarial'
		});
		//console.log(JSON.parse(localStorage.getItem('notification')));
		await webpush.sendNotification(JSON.parse(subscription), payload)
		.then(() => {
			//getOrderById(orderBussines[0].pe_id);;
			// Emite evento para actualizar los pedidos empresariales
			if (io) {
				console.log('Emite notificacion order ', data.idpedido_empresarial);
				io.emit("ordershop");
			}
		})
		.catch(error => {
			console.error(error.stack);
			return res.status(400).json('Error al enviar la notificacion de pedido.');
		});
	}
	// Obtiene los datos del pedido realizado
	let order = await getOrderById(data.idpedido_empresarial);
	order = (order.length > 0)? order[0] : null;
	res.status(200).json({msg: 'Pedido realizado exitosamente.', order});
	// res.redirect('/tienda/listapedidos?info=correct');

});

/**
 * Obtiene la constante por el nombre
 */
function getConstant(constant, name) {
	return constant[constant.findIndex(x => x.name == name)];
}
/**
 * Obtiene la constante por el nombre
 */
function getConstantById(constant, id) {
	return constant[constant.findIndex(x => x.id == id)];
}

module.exports = router;
