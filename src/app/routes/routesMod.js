const express = require('express');
const router = express.Router();
const Database = require('../../config/database');
let db = new Database(); // Instancia de base de datos promise
const connection = db.getConnection();
const sesion = require('../middlewares/sesionMod');

router.use(sesion);

router.get("/login", function(req, res){
	res.render("app/index")
});

router.get("/prueba", function(req, res){

	connection.query('select * from categorias', function(err, rows, fields){
		if(err)
			console.log(err);
		else
			res.send(rows);
	});
	
});

module.exports = router;