const express = require('express');
const router = express.Router();
const sesion = require('../middlewares/sesionAdmin');
const webpush = require('web-push');
const Database = require('../../config/database');
const moment = require('moment');

const Pusher = require('pusher');
require('dotenv').config();

let channels_client = new Pusher({
  appId: '871814',
  key: 'e687d1825c8d4e611925',
  secret: 'f6a8e5befba2c04a113f',
  cluster: 'us2',
  encrypted: true
});

let db = new Database(); // Instancia de base de datos promise
const connection = db.getConnection();
let LocalStorage = require('node-localstorage').LocalStorage;
localStorage = new LocalStorage('./scratch');
let bodyParser = require('body-parser');
let fs = require('fs');
const bcrypt = require('bcrypt');

const models = require('../models');

const Ajustes = models.ajustes; // new require for db object ajustes
const Configuraciones = models.configuraciones; // new require for db object configuraciones
const Departamentos = models.departamentos; // new require for db object departamentos
const Direcciones = models.direcciones; // new require for db object direcciones
const HistorialTiendasPlanes = models.historial_tiendas_planes; // new require for db historial_planes_sucursales 
const Municipios = models.municipios; // new require for db object municipios
const Paises = models.paises; // new require for db object paises
const PedidosEmpresariales = models.pedidos_empresariales; // new require for db object predidos empresariales
const Planes = models.planes; // new require for db object planes
const Riders = models.riders; // new require for db object riders
const Sucursales = models.sucursales; // new require for db object sucrusales
const Solicitudes = models.solicitudes; // new require for db object solicitudes
const Tiendas = models.tiendas; // new require for db object planes
const TiendasPlanes = models.tiendas_planes; // new require for db object planes

const sharp = require('sharp');

let urlSocket = 'http://localhost:8002'

// Valida el tipo de entorno para saber la url del socket
if (process.env.NODE_ENV === 'production') {
	urlSocket = 'https://admin.traigo.com.co'; 
} else if (process.env.NODE_ENV === 'test') {
	urlSocket = 'http://traigo.com.co:8002'; 
}

const io = require('socket.io-client')(urlSocket);

const Constants = require('./constants');

// var http = require('http').Server(express);
// var io = require('socket.io')(http);
/**
 * This module let us use HTTP verbs such as PUT or DELETE 
 * in places where they are not supported
 */ 
let methodOverride = require('method-override')
 
/**
 * using custom logic to override method
 * 
 * there are other ways of overriding as well
 * like using header & using query value
 */ 
router.use(methodOverride(function (req, res) {
  if (req.body && typeof req.body === 'object' && '_method' in req.body) {
    // look in urlencoded POST bodies and delete it
    let method = req.body._method
    delete req.body._method
    return method
  }
}))
 
router.use(sesion);
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

webpush.setVapidDetails('mailto:traigoapp@gmail.com', process.env.PUBLIC_VAPID_KEY, process.env.PRIVATE_VAPID_KEY);

// Controlador de utilidades generales de la aplicacion
const utilsController = require('../controllers/utils.controller');
// Controlador de alertas
router.use('/alertas', require('../controllers/alerts.controller'));
// Controlador de tiendas
router.use('/tiendas', require('../controllers/shops.controller'));

router.use('/orders', require('../controllers/order.controller'));


router.get('/', (req, res) => {
	res.render('admin/pages/home');
});

router.get('/solicitudes', (req, res) => {

	// Busca todas las solicitudes pendientes
	Solicitudes.findAll({
		where: {
			pagada: 'N'
		},
		include: [
			{
				model: Riders, as: 'rider',
				attributes: ['nombre', 'apellido']
			}
		]
	}).then(solicitudes => {
		
		for (let i= 0; i < solicitudes.length; i++) {
			
			let date = new Date(solicitudes[i].fecha).toLocaleString();
			let date_split = date.split(" ");
			solicitudes[i].fecha = date_split[0];
			solicitudes[i].hora = date_split[1];
		}

		res.render('admin/pages/solicitudes', {info: undefined, solicitudes: solicitudes});
	})
	.catch(err => {
		console.log('Error al buscar todas las solicitudes', err);
		res.redirect('/?info=err');
	});
});

router.get('/solicitud', (req, res) => {
	let id = req.query.id;
	let info = req.query.info;

	Solicitudes.findOne({
		where: {
			idsolicitud: id
		}, 
		include: [
			{
				model: Riders, as: 'rider',
				attributes: ['nombre', 'apellido', 'cedula', 'telefono']
			}
		]
	})
	.then( sol => {
		// Verfica si se encuentra la solicitud
		if (sol) {
			// Busca todas las solicitudes del rider
			Solicitudes.findAll({
				where: {
					idrider: sol.idrider
				},
				order: [['fecha']]
			})
			.then( async solicitudes => {

				let solicitud, anterior, fechaanterior;
				let minDate = new Date('2017-01-01');

				for (let i = 0; i < solicitudes.length; i++) {
					let soli = solicitudes[i];
					if (soli.idsolicitud == id) {
						solicitud = soli;
						solicitud.rider = sol.rider;
						anterior = i - 1;
					}
				}

				if (anterior == -1) {
					fechaanterior = minDate;
				} else {
					fechaanterior = solicitudes[anterior].fecha;
				}

				const cedula = solicitud.idrider, 
					  fechasolicitud = solicitud.fecha;

				// Obtiene pedidos del rider por intervalo de fechas
				const accountOrdersRider = getCurrentOrdersAccountRider(cedula, fechaanterior, fechasolicitud, solicitud.pagada);
				
				return Promise.all([accountOrdersRider, solicitud]);

			})
			.then(async data => {

				// Lista de ordenes
				const orders = data[0];
				
				// Info solicitud
				const request = data[1];

				let deseos = orders[0], 
					tiendas = orders[1], 
					empresariales = orders[2],
					encomiendas = orders[3], 
					solicitud = request;

				let todos = deseos.concat(tiendas, encomiendas, empresariales);
		
				let cuenta = 0;

				for (let i = 0; i < todos.length; i++) {
					if (todos[i].type == 'deseo') {
						//if(todos[i].comision_entregada == 'N'){
							cuenta -= todos[i].totalfacturas * todos[i].comision_cliente / 100;
						//}
					} else if(todos[i].type == 'mandado') {
						//if(todos[i].comision_entregada == 'N'){
							cuenta -= (todos[i].totalparadas + todos[i].costo_envio) * todos[i].comision_rider / 100;
						//}
					} else if(todos[i].type == 'tienda') {
						//if(todos[i].comision_entregada == 'N'){
							cuenta -= (todos[i].costo_envio) * todos[i].comision_rider / 100;
						//}
					} else {
						//if(todos[i].comision_entregada == 'N'){
							// console.log('sucursal id', todos[i].idsucursal);
							const sucursal = await Sucursales.findOne({attributes: ['nombre'], where: {idsucursal: todos[i].idsucursal}});
							// console.log('sucursal', sucursal);
							todos[i].nombresucursal = sucursal.nombre;

							if (todos[i].retenido == 'S') {
								cuenta += (todos[i].costo_envio + todos[i].valor_ida_vuelta) * (100 - todos[i].comision_rider) / 100;
							} else {
								cuenta -= (todos[i].costo_envio + todos[i].valor_ida_vuelta) * todos[i].comision_rider / 100;
							}
							if (todos[i].ajuste) {
								cuenta += todos[i].ajuste.valor;
							}
						//}
					}

				}
		
				res.render('admin/pages/solicitud', {
					info: info,
					solicitud: solicitud,
					cuenta: cuenta,
					deseos: deseos,
					empresariales: empresariales,
					tiendas: tiendas, 
					mandados: encomiendas,
					//ajustes: ajustes
				});
			})
			.catch(err => {
				console.log('Error al buscar los pedidos de la solicitud', err);
				res.redirect('/solicitudes/?info=err');
			});

		} else {
			res.redirect('/solicitudes/?info=err');
		}
	})
	.catch(err => {
		console.log('Error al buscar la solicitud: ' + id, err);
		res.redirect('/solicitudes/?info=err');
	});

	/*connection.query('select * from solicitudes s join riders r on r.cedula = s.idrider where s.idsolicitud = ?', [id], (err, rows, fields) => {
		if(err){
			console.log(err);
		}else{
			idrider = rows[0].idrider;
			connection.query('select * from solicitudes s join riders r on r.cedula = s.idrider where s.idrider = ? order by fecha', [idrider], (err, rows, fields) => {
				if(err){
					console.log(err);
				}else{

					for(let i = 0; i < rows.length; i++){
						if(rows[i].idsolicitud == id){
							solicitud = rows[i];
							anterior = i - 1;
						}
					}
					if(anterior == -1){
						fechaanterior = minDate;
					}else{
						fechaanterior = rows[anterior].fecha;
					}

					let cedula = solicitud.idrider, fechasolicitud = solicitud.fecha;
					let deseos, pedidos, mandados, empresariales;
					connection.query('select pd.*, sum(f.valor) as totalfacturas from pedidos_deseos pd join facturas f on f.idpedido_deseos = pd.idpedido_deseo where pd.idrider = ? and (pd.estado = ? or pd.estado = ?) and pd.fecha <= ? and pd.fecha > ? group by pd.idpedido_deseo'
						,[cedula, 'Entregado', 'Error', fechasolicitud, fechaanterior], (err, rows, fields) => {
							if(err){
								console.log(err);
								return res.send(err);
							}
							deseos = rows;
							for(let i = 0; i < deseos.length; i++){
								deseos[i].type = 'deseo';
							}
							connection.query('select pe.*, sum(p.costo) as totalparadas from pedidos_encomiendas pe join paradas p on p.idpencomienda = pe.idpedido_encomienda where pe.idpedido_encomienda = ? and (pe.estado = ? or pe.estado = ?) and pe.fecha <= ? and pe.fecha > ? group by pe.idpedido_encomienda'
								,[cedula, 'Entregado', 'Error', fechasolicitud, fechaanterior], (err, rows, fields) => {
									if(err){
										console.log(err);
										return res.send(err);
									}
									mandados = rows;
									for(let i = 0; i < mandados.length; i++){
										mandados[i].type = 'mandado';
									}
									connection.query('select * from pedidos_clientes pc where pc.idrider = ? and (pc.estado = ? or pc.estado = ?) and pc.fecha <= ? and pc.fecha > ?'
										,[cedula, 'Entregado', 'Error', fechasolicitud, fechaanterior], (err, rows, fields) => {
											if(err){
												console.log(err);
												return res.send(err);
											}
											pedidos = rows;
											for(let i = 0; i < pedidos.length; i++){
												pedidos[i].type = 'tienda';
											}
											connection.query('select pe.*, s.nombre as nombresucursal  from pedidos_empresariales pe join sucursales s on s.idsucursal = pe.idsucursal where pe.idrider = ? and (pe.estado = ? or pe.estado = ?) and pe.fecha <= ? and pe.fecha > ?'
												,[cedula, 'Entregado', 'Error', fechasolicitud, fechaanterior], (err, rows, fieds) => {
													if(err){
														console.log(err);
														return res.send(err);
													}
													empresariales = rows;
													for(let i = 0; i < empresariales.length; i++){
														empresariales[i].type = 'empresariales';
													}
													let todos = deseos.concat(pedidos, mandados, empresariales);


													let cuenta = 0;

													for(let i = 0; i < todos.length; i++){
														if(todos[i].type == 'deseo'){
															//if(todos[i].comision_entregada == 'N'){
																cuenta -= todos[i].totalfacturas * todos[i].comision_cliente / 100;
															//}
														}else if(todos[i].type == 'mandado'){
															//if(todos[i].comision_entregada == 'N'){
																cuenta -= (todos[i].totalparadas + todos[i].costo_envio) * todos[i].comision_rider / 100;
															//}
														}else if(todos[i].type == 'tienda'){
															//if(todos[i].comision_entregada == 'N'){
																cuenta -= (todos[i].costo_envio) * todos[i].comision_rider / 100;
															//}
														}else{
															//if(todos[i].comision_entregada == 'N'){
																if(todos[i].retenido == 'S'){
																	cuenta += (todos[i].costo_envio) * (100 - todos[i].comision_rider) / 100;
																}else{
																	cuenta -= (todos[i].costo_envio) * todos[i].comision_rider / 100;
																}

															//}
														}
													}

													res.render('admin/pages/solicitud', {info: info, solicitud: solicitud, cuenta:cuenta, deseos: deseos, empresariales: empresariales, tiendas: pedidos, mandados: mandados});
												});
										});
								});
						});
				}
				
			});
		}
	
	});*/

});

router.get("/realizarpagosolicitud", (req, res) => {
	
	let idrider = req.query.idrider;
	let idsolicitud = req.query.idsolicitud;
	let fechaSolicitud;

	// Busca la solicitud a pagar por el id
	/*Solicitud.findByPk(idsolicitud)
	.then(solicitud=> {
		// Verifica si la solicitud existe
		if (solicitud) {
			res.redirect(`/administrador/solicitud?id=${idsolicitud}&info=err`);
		}
	})
	.catch(err => {
		res.redirect('/administrador/solicitud?id='+ idsolicitud + '&info=err');
	});*/

	connection.beginTransaction((err) => {
		if(err){
			res.redirect('/administrador/solicitud?id='+ idsolicitud + '&info=err');
		}else{
			connection.query('select * from solicitudes where idrider = ? and pagada = ?', [idrider, 'N'], (err, rows, fields) => {
				if(err){
					connection.rollback(() => {
						res.redirect('/administrador/solicitud?id='+ idsolicitud + '&info=err');
					});
				}else{
					fechaSolicitud = rows[0].fecha;
					connection.query('update pedidos_empresariales set comision_entregada = ? where idrider = ? and fecha < ?'
						,['S', idrider, fechaSolicitud], (err, rows, fields) => {
							if(err){
								connection.rollback(() => {
									res.redirect('/administrador/solicitud?id='+ idsolicitud + '&info=err');
								});
							}else{
								connection.query('update pedidos_deseos set comision_entregada = ? where idrider = ? and fecha < ?'
									,['S', idrider, fechaSolicitud], (err, rows, fields) => {
										if(err){
											connection.rollback(() => {
												res.redirect('/administrador/solicitud?id='+ idsolicitud + '&info=err');
											});
										}else{
											connection.query('update pedidos_encomiendas set comision_entregada = ? where idrider = ? and fecha < ?'
												,['S', idrider, fechaSolicitud], (err, rows, fields) => {
													if(err){
														connection.rollback(() => {
															res.redirect('/administrador/solicitud?id='+ idsolicitud + '&info=err');
														});
													}else{
														connection.query('update pedidos_clientes set comision_entregada = ? where idrider = ? and fecha < ?'
															,['S', idrider, fechaSolicitud], (err, rows, fields) => {
																if(err){
																	connection.rollback(() => {
																		res.redirect('/administrador/solicitud?id='+ idsolicitud + '&info=err');
																	});
																}else{
																	connection.query('update solicitudes set pagada = ? where idrider = ?', ['S', idrider], (err, rows, fields) => {
																		if(err){
																			connection.rollback(() => {
																				res.redirect('/administrador/solicitud?id='+ idsolicitud + '&info=err');
																			})
																		}else{
																			connection.commit((err) => {
																				if(err){
																					connection.rollback(() => {
																						res.redirect('/administrador/solicitud?id='+ idsolicitud + '&info=err');
																					})
																				}else{
																					res.redirect('/administrador/solicitudes?info=correct');
																				}
																			});
																		}
																	})
																}
															});
													}
												});
										}
									});
							}
						})
				}
			});
			
		}
	});
});

// Visualiza el mapa con lo riders en curso
router.get('/mapariders', (req, res) => {
	let info = undefined;
	res.render('admin/pages/riders/map',  {info: info});
});

// Cancela un pedido deseo por id
router.get('/cancelarpedidodeseo', (req, res) => {
	let id = req.query.id;
	connection.query('SELECT * FROM pedidos_deseos WHERE idpedido_deseo = ?',
		[id], (err, rows, fields) => {
			if (err) {
				console.log(err);
				return res.redirect('/administrador/pedidosencurso?info=err');
			} else {
				let pedido = rows[0];
				let nuevoEstado;
				if(pedido.estado != 'En proceso' && pedido.estado != 'Rider en la tienda'){
					nuevoEstado = 'Error';
				} else {
					nuevoEstado = 'Cancelado';
				}
				connection.query('UPDATE pedidos_deseos SET estado = ? WHERE idpedido_deseo = ?',
					[nuevoEstado, id], (err, rows, fields) => {
						if (err) {
							console.log(err);
							return res.redirect('/administrador/pedidosencurso?info=err');
						} else {
							return res.redirect('/administrador/pedidosencurso?info=eliminated');
						}
					});
			}
		});
});

router.get('/cancelarpedidomandado', (req, res) => {
	let id = req.query.id;

	connection.query('select * from pedidos_encomiendas where idpedido_encomienda = ?', [id], 
		(err, rows, fields) => {
			if(err){
				console.log(err);
				return res.redirect('/administrador/pedidosencurso?info=err');
			}else{
				var pedido = rows[0];
				var nuevoEstado;
				if(pedido.estado != 'En proceso' && pedido.estado != 'Rider en el sitio de recogida'){
					nuevoEstado = 'Error';
				}else{
					nuevoEstado = 'Cancelado';
				}
				connection.query('update pedidos_encomiendas set estado = ? where idpedido_encomienda = ?'
					,[nuevoEstado, id],(err, rows, fields) => {
						if(err){
							console.log(err);
							return res.redirect('/administrador/pedidosencurso?info=err');
						}else{
							return res.redirect('/administrador/pedidosencurso?info=eliminated');
						}
					});
			}
			
		});
});

// Cancela un pedido empresarial por id
router.get('/cancelarpedido', async (req, res) => {
	
	let idpedido = req.query.id;
	
	// Inicializacion de transaccion activa
	let trans = await models.sequelize.transaction();

	// Busca el pedido emrpesarial por el identificador 
	await PedidosEmpresariales.findOne({
		where : {
			idpedido_empresarial: idpedido
		}
	}, {trasaction: trans})
	.then(async pe => {

		// Verifica si existe el pedido empresarial
		if (pe) {

			let nuevoEstado;

			if (pe.estado != 'En proceso' && pe.estado != 'Rider en la tienda') {
				nuevoEstado = 'Error';
			} else {
				nuevoEstado = 'Cancelado';
			}

			pe.estado = nuevoEstado;

			// Actualiza los datos del pedido en curso
			await pe.save()
			.then (async () => {

				if (nuevoEstado === 'Cancelado') {
					
					await Sucursales.findByPk(pe.idsucursal)
					.then(async sucursal => {

						// Verifica si existe la sucursal
						if (sucursal) {

							// Verifica si la sucursal es prepagada
							if (sucursal.tipo === getConstant(Constants.TIPO_SUCURSAL, 'Prepagada').id ) {

								// Busca las tiendas planes que tienen estado activo
								await TiendasPlanes.findAll({
									where: {
										tiendas_id: sucursal.idtienda,
										estado: getConstant(Constants.ESTADO_TIENDAS_PLANES, 'Activo').id
									}
								}, {transaction: trans})
								.then(async tp => {
									// Verifica si hay elementos
									if (tp.length > 0) {
										// Tienda plan activo
										let tiendaPlan = tp[0];

										// Se suma el pedido cancelado al plan activo
										tiendaPlan.total_pedidos = parseInt(tiendaPlan.total_pedidos) + 1;

										try {
											// Guarda la acualizacion del saldo de pedidos + 1
											await tiendaPlan.save()
											.then(async () => await trans.commit());

										} catch (err) {
											// Devuelve los cambios realizados
											await trans.rollback();

											console.log('Error al actualizar el saldo del plan activo.', err);
											res.redirect('/tienda/listapedidos?info=err');		
										}
										
									} else {
										// Selecciona el ultimo elemento activo, elemento inactivo más reciente
										await models.sequelize.query(
											'SELECT * FROM tiendas_planes WHERE updated_at = (SELECT MIN(updated_at) FROM tiendas_planes WHERE estado = $1)',
										{ bind: [getConstant(Constants.ESTADO_TIENDAS_PLANES, 'Inactivo').id], type: models.sequelize.QueryTypes.SELECT })
										.then(async tpi => {

											// Verifica si no hay elementos
											if (tpi.length > 0) {

												// Datos de la teind plan filtrada
												let tiendaPlan = tpi[0];

												try {
													// Actualiza el saldo y estado de la tienda plan inactiva
													await TiendasPlanes.update(
														{ 
															total_pedidos: parseInt(tiendaPlan.total_pedidos) + 1,
															estado: getConstant(Constants.ESTADO_TIENDAS_PLANES, 'Activo').id
														},
														{
															where: {
																tiendas_id: tiendaPlan.tiendas_id,
																planes_id: tiendaPlan.planes_id
															}
														}, {transaction: trans}
													);

													// Busca el historial con actualizacion mas reciente de estado finalizado
													await HistorialTiendasPlanes.findAll({
														where: {
															tiendas_planes_planes_id: tiendaPlan.planes_id,
															tiendas_planes_tiendas_id: tiendaPlan.tiendas_id,
															estado: getConstant(Constants.ESTADO_HISTORIAL_PLANES, 'Finalizado').id
														},
														order: [['updatedAt']], // Los mas nuevos
														limit: 1
													})
													.then(async htp => {
														// Verifica si hay elementos
														if (htp.length > 0) {
															let historial = htp[0];

															historial.estado = getConstant(Constants.ESTADO_HISTORIAL_PLANES, 'En curso').id;

															await historial.save()
															.then(async () => await trans.commit());

														} else {
															await trans.rollback();
															console.log('No se ecuentra un historial para actualizar');
															res.redirect('/tienda/listapedidos?info=err');
														}
													});
												} catch (err) {
													// Devuelve los cambios realizados
													await trans.rollback();

													console.log('Error al efectuar la reactivacion del plan', err);
													res.redirect('/tienda/listapedidos?info=err');
												}

											} else {
												console.log('No hay elementos inactivos');
												res.redirect('/tienda/listapedidos?info=err');
											}
										});
									}
								});
							}
						} else  {
							console.log('No se encuentra la sucursal del pedido a eliminar');
							res.redirect('/tienda/listapedidos?info=err');	
						}
					});
				} else {
					await trans.commit();
				}

				return res.redirect('/administrador/pedidosencurso?info=eliminated');
			})
			.catch(err => {
				console.log('Error al cancelar el pedido: ' + idpedido, err);
				return res.redirect('/administrador/pedidosencurso?info=err');
			});
			
		} else {
			console.log('No se encuentra el pedido empresarial: ' + idpedido);
			return res.redirect('/administrador/pedidosencurso?info=err');
		}
	})
	.catch(err => {
		console.log(err);
		return res.redirect('/administrador/pedidosencurso?info=err');
	});

});

router.get('/pedidodeseo', (req, res) => {
	const id = req.query.id;
	connection.query('select pd.*, d.direccion as direccionentrega, d.notas as notasdireccionentrega, d.latitude as latitudeentrega, d.longitude as longitudeentrega, ri.nombre as nombrerider, ri.apellido as apellidorider, ri.telefono as telefonorider, c.nombre as nombrecliente, c.telefono as telefonocliente from pedidos_deseos pd left join riders ri on ri.cedula = pd.idrider join clientes c on c.correo = pd.correocliente join direcciones d on d.iddireccion = pd.iddireccion where idpedido_deseo = ?'
		,[id],(err, rows, fields) => {
			if(err){
				console.log(err);
				send(err);
			}else{
				connection.query('select * from facturas where idpedido_deseos = ?', [id],(err, facturas, fields) => {
					if (err) {
						console.log(err);
						send(err);
					}else{
						res.render('admin/pedidodeseo', {pedido: rows[0], facturas: facturas});
					}
				});
				
			}
		});
});


// Configura las notificaciones
router.post('/notification', (req, res) => {
	const subscription = req.body;
	res.status(201).json({});
	const payload = JSON.stringify({ title: 'Registro', message: 'Notificaciones activadas' });

	localStorage.setItem('notification', JSON.stringify(subscription));
	console.info(subscription);
  	//console.log(JSON.parse(localStorage.getItem('notification')));
	/*webpush.sendNotification(subscription, payload).catch(error => {
	  console.error(error.stack);
	});*/
});

/**
 * Actualiza el limite de pedidos de todos lo riders
 */
router.post('/modificarlimiteriders', (req, res) => {
	let limite = (req.body.limite.trim() == '')? 0 : req.body.limite;

	db.query('UPDATE riders SET limitePedidos = ?', [limite])
	.then(() => {
		res.redirect('/administrador/riders?info=limitcorrect');
	})
	.catch(err => {
		console.log(err);
		res.redirect('/administrador/riders?info=err');
	});
	
});


// Pedidos actuales empresariales
router.get('/getBusinessOrders', (req, res) => {

	// Obtiene el id de la orden
	const order_id = req.query.id;

	// Se habilita la respuesta pra enviar JSONs
	res.contentType('json');

	// Condiciones de consulta pedidos empresariaes
	let condicions = ['Error', 'Entregado', 'Cancelado'];

	// Verifica si se filtra de forma independiente
	if (order_id) {
		// Agrega el id del pedido al arreglo de condiciones
		condicions.push(order_id);
	}

	// Consulta de pedidos empresariales
	models.sequelize.query(`SELECT pd.idpedido_empresarial, pd.valor, pd.nombre_cliente, pd.telefono, pd.tamanio_maletin,
	datafono, pd.fecha, pd.estado, pd.idrider, CONCAT(ri.nombre, " ", ri.apellido) AS nombrerider, IF(pd.ida_vuelta='1', "S", "N") AS ida_vuelta,
	pd.iddireccion, pd.id_direccion_ida_vuelta, s.iddireccion AS suc_address, s.nombre AS nombresucursal, s.numero_punto AS numerosucursal,
	s.retenido AS retiene, s.tiempo_entrega AS tiempo_entrega, d.direccion AS direccionentrega,
	d.notas AS notasdireccionentrega, mu.nombre AS municipio FROM pedidos_empresariales pd LEFT JOIN riders ri ON ri.cedula = pd.idrider
	JOIN direcciones d ON d.iddireccion = pd.iddireccion JOIN sucursales s ON s.idsucursal = pd.idsucursal JOIN municipios mu ON mu.idmunicipios = d.idmunicipio 
	WHERE pd.estado != ? AND pd.estado != ? AND pd.estado != ? ${order_id? 'AND pd.idpedido_empresarial = ?' : '' } order by pd.fecha desc`,
	{ replacements: condicions, type: models.sequelize.QueryTypes.SELECT })
	.then(async pe => {
		for (let i = 0; i < pe.length; i++) {

			// Inicializa color Super duper ultra rojo
			let color = 'state-other';
			// Asigna el color correspondiente dependiendo del estado
			if (pe[i].estado === 'Error') {
				color = 'state-error';
			} else if (pe[i].estado === 'Rider en camino') {
				color = 'state-onrute'; // Amarillo
			} else if (pe[i].estado === 'Rider con productos') {
				color = 'state-products'; // Verde
			} else if (pe[i].estado === 'Rider donde el cliente') {
				color = 'state-oncustomer'; // Gris cañeria
			} else if (pe[i].estado === 'Rider en la tienda') {
				color = 'state-onshop'; // Azul 
			} else if (pe[i].estado === 'Rider regresando a la tienda') {
				color = 'state-return-shop'; // Morado lila
			} else if (pe[i].estado === 'Rider regresando donde el cliente') {
				color = 'state-return-customer'; // Naranja
			} else if (pe[i].estado === 'En proceso') {
				color = 'state-process'; // Rojo
			}

			// Asigna el color del estado
			pe[i].color = color;
			// Tipo de pedido
			pe[i].type = 'orders';
			// Convierte los valor de la fecha a un formato mas entendible
			pe[i].fecha = new Date(pe[i].fecha).toLocaleString();
			// Asigna el tipo de maletin del pedido
			pe[i].tamanio_maletin = (pe[i].tamanio_maletin)? findConstantById(Constants.TIPO_MALETIN, pe[i].tamanio_maletin).name : 'Cualquier tamaño';
		}
		const limit = await Configuraciones.findAll().then(config => {return config[0].limit_business_orders});
		res.status(200).json({orders: pe, limit});
	})
	.catch(err => {
		console.log(err);
		res.send({
			err: JSON.stringify(err) 
		});
	});

});

// Pedidos actuales encomienda (mandados)
router.get('/getOrderingOrders', (req, res) => {
	
	let orders;
	// Obtiene los pedidos tipo mandado
	connection.query('select count(distinct(pa.idparada)) as cantidadparadas, pd.*, do.direccion as direccionorigen, do.notas as notasdireccionorigen, do.latitude as latitudedireccionorigen, do.longitude as longitudedireccionorigen, dd.direccion as direcciondestino, dd.notas as notasdirecciondestino, dd.latitude as latitudedirecciondestino, dd.longitude as longitudedirecciondestino, ri.nombre as nombrerider, ri.apellido as apellidorider, c.nombre as nombrecliente, c.telefono as telefonocliente, mu.nombre as municipio from pedidos_encomiendas pd join direcciones do on do.iddireccion = pd.iddireccionorigen join direcciones dd on dd.iddireccion = pd.iddirecciondestino join clientes c on c.correo = pd.correocliente join municipios mu on mu.idmunicipios = do.idmunicipio left join paradas pa on pa.idpencomienda = pd.idpedido_encomienda left join riders ri on ri.cedula = pd.idrider where pd.estado != ? and pd.estado != ? and pd.estado != ? group by pd.idpedido_encomienda order by pd.fecha desc', ['Entregado', 'Cancelado', 'Error'],				
					(err, rows, fields) => {
			res.contentType('json');
			if (err) {
				console.log(err);
				res.send({
					err: JSON.stringify(err) 
				});
			} else {
				orders = rows;
				for (let i = 0; i < orders.length; i++) {
					orders[i].type = 'mandado';
					if (orders[i].estado === 'Error') {
						orders[i].color = 'state-error';
					} else if (orders[i].estado === 'Rider en camino') {
						orders[i].color = 'state-onrute'; // Amarillo
					} else if (orders[i].estado === 'Rider con productos') {
						orders[i].color = 'state-products'; // Verde
					} else if (orders[i].estado === 'Rider donde el cliente') {
						orders[i].color = 'state-oncustomer'; // Gris cañeria
					} else if (orders[i].estado === 'Rider en la tienda') {
						orders[i].color = 'state-onshop'; // Azul 
					} else if (orders[i].estado === 'En proceso') {
						orders[i].color = 'state-process'; // Rojo
					} else if (orders[i].estado === 'Rider regresando a la tienda') {
						orders[i].color = 'state-return-shop'; // Morado lila
					} else if (orders[i].estado === 'Rider regresando donde el cliente') {
						orders[i].color = 'state-return-customer'; // Naranja
					} else {
						orders[i].color = 'state-other'; // Super duper ultra rojo
					}
				}
				res.send(orders);
			}
	});
});

// Pedidos actuales deseo
router.get('/getDesireOrders', (req, res) => {

	let orders;
	connection.query('select pd.*, d.direccion as direccionentrega, d.notas as notasdireccionentrega, d.latitude as latitudeentrega, d.longitude as longitudeentrega, ri.nombre as nombrerider, ri.apellido as apellidorider, c.nombre as nombrecliente, c.telefono as telefonocliente, mu.nombre as municipio from pedidos_deseos pd left join riders ri on ri.cedula = pd.idrider join clientes c on c.correo = pd.correocliente join direcciones d on d.iddireccion = pd.iddireccion join municipios mu on mu.idmunicipios = d.idmunicipio where pd.estado != ? and pd.estado != ? and pd.estado != ? order by pd.fecha desc', ['Entregado', 'Cancelado', 'Error'],
		(err, rows, fields) => {
			res.contentType('json');
			if (err) {
				console.log(err);
				res.send({
					err: JSON.stringify(err) 
				});
			} else {
				orders = rows;
				for(let i = 0; i < orders.length; i++){
					orders[i].type = 'deseo';
					// Convierte los valor de la fecha a un formato mas entendible
					let date = new Date(orders[i].fecha).toLocaleString();
					orders[i].fecha = date;
					if (orders[i].estado === 'Error') {
						orders[i].color = 'state-error';
					} else if (orders[i].estado === 'Rider en camino') {
						orders[i].color = 'state-onrute'; // Amarillo
					} else if (orders[i].estado === 'Rider con productos') {
						orders[i].color = 'state-products'; // Verde
					} else if (orders[i].estado === 'Rider donde el cliente') {
						orders[i].color = 'state-oncustomer'; // Gris cañeria
					} else if (orders[i].estado === 'Rider en la tienda') {
						orders[i].color = 'state-onshop'; // Azul 
					} else if (orders[i].estado === 'En proceso') {
						orders[i].color = 'state-process'; // Rojo
					} else if (orders[i].estado === 'Rider regresando a la tienda') {
						orders[i].color = 'state-return-shop'; // Morado lila
					} else if (orders[i].estado === 'Rider regresando donde el cliente') {
						orders[i].color = 'state-return-customer'; // Naranja
					} else {
						orders[i].color = 'state-other'; // Super duper ultra rojo
					}
				}
				res.send(orders);
			}
		});
});

// Obtiene y visualiza los pedidos en curso
router.get('/pedidosencurso', (req, res) => {

	let date = new Date()

	let day = date.getDate()
	let month = date.getMonth() + 1
	let year = date.getFullYear()

	let current = year + '-' + month + '-' + day;

	db.query('SELECT nd.title, nd.description, date_format(n.created_at,\'%h:%i:%s\') created_at FROM notifications n JOIN notifications_detail nd ON n.notifications_detail_id = nd.id WHERE DATE(n.created_at) = ?',[current])
	.then(rows => {
		req.session.user.notifications = rows;
	})
	.catch(err => {
		console.log(err);
	});

	let deseos, pedidos, mandados, empresariales;

	// Obtiene los pedidos tipo deseo
	connection.query('select pd.*, d.direccion as direccionentrega, d.notas as notasdireccionentrega, d.latitude as latitudeentrega, d.longitude as longitudeentrega, ri.nombre as nombrerider, ri.apellido as apellidorider, c.nombre as nombrecliente, c.telefono as telefonocliente, mu.nombre as municipio from pedidos_deseos pd left join riders ri on ri.cedula = pd.idrider join clientes c on c.correo = pd.correocliente join direcciones d on d.iddireccion = pd.iddireccion join municipios mu on mu.idmunicipios = d.idmunicipio where pd.estado != ? and pd.estado != ? and pd.estado != ? order by pd.fecha desc', ['Entregado', 'Cancelado', 'Error'],
		(err, rows, fields) => {
			if (err) {
				res.send(err);
			} else {
				deseos = rows;
				//console.log(deseos);
				for(let i = 0; i < deseos.length; i++){
					deseos[i].type = 'deseo';
					// Convierte los valor de la fecha a un formato mas entendible
					let date = new Date(deseos[i].fecha).toLocaleString();
					deseos[i].fecha = date;
					if (deseos[i].estado === 'Error') {
						deseos[i].color = 'state-error';
					} else if (deseos[i].estado === 'Rider en camino') {
						deseos[i].color = 'state-onrute'; // Amarillo
					} else if (deseos[i].estado === 'Rider con productos') {
						deseos[i].color = 'state-products'; // Verde
					} else if (deseos[i].estado === 'Rider donde el cliente') {
						deseos[i].color = 'state-oncustomer'; // Gris cañeria
					} else if (deseos[i].estado === 'Rider en la tienda') {
						deseos[i].color = 'state-onshop'; // Azul 
					} else if (deseos[i].estado === 'En proceso') {
						deseos[i].color = 'state-process'; // Rojo
					} else {
						deseos[i].color = 'state-other'; // Super duper ultra rojo
					}
					/*let date_split =  date.split(" ");
					deseos[i].fecha = date_split[0];
					deseos[i].hora = date_split[1];*/
				}
				// Obtiene los pedidos tipo mandado
				connection.query('select count(distinct(pa.idparada)) as cantidadparadas, pd.*, do.direccion as direccionorigen, do.notas as notasdireccionorigen, do.latitude as latitudedireccionorigen, do.longitude as longitudedireccionorigen, dd.direccion as direcciondestino, dd.notas as notasdirecciondestino, dd.latitude as latitudedirecciondestino, dd.longitude as longitudedirecciondestino, ri.nombre as nombrerider, ri.apellido as apellidorider, c.nombre as nombrecliente, c.telefono as telefonocliente, mu.nombre as municipio from pedidos_encomiendas pd join direcciones do on do.iddireccion = pd.iddireccionorigen join direcciones dd on dd.iddireccion = pd.iddirecciondestino join clientes c on c.correo = pd.correocliente join municipios mu on mu.idmunicipios = do.idmunicipio left join paradas pa on pa.idpencomienda = pd.idpedido_encomienda left join riders ri on ri.cedula = pd.idrider where pd.estado != ? and pd.estado != ? and pd.estado != ? group by pd.idpedido_encomienda order by pd.fecha desc', ['Entregado', 'Cancelado', 'Error'],				
					(err, rows, fields) => {
					if (err) {
						console.log(err);
						res.send(err);
					} else {
						mandados = rows;
						//console.log(mandados);
						for (let i = 0; i < mandados.length; i++) {
							mandados[i].type = 'mandado';
							if (mandados[i].estado === 'Error') {
								mandados[i].color = 'state-error';
							} else if (mandados[i].estado === 'Rider en camino') {
								mandados[i].color = 'state-onrute'; // Amarillo
							} else if (mandados[i].estado === 'Rider con productos') {
								mandados[i].color = 'state-products'; // Verde
							} else if (mandados[i].estado === 'Rider donde el cliente') {
								mandados[i].color = 'state-oncustomer'; // Gris cañeria
							} else if (mandados[i].estado === 'Rider en la tienda') {
								mandados[i].color = 'state-onshop'; // Azul 
							} else if (mandados[i].estado === 'En proceso') {
								mandados[i].color = 'state-process'; // Rojo
							} else {
								mandados[i].color = 'state-other'; // Super duper ultra rojo
							}
						}
						// Obtiene los pedidos cliente
						connection.query('select * from pedidos_clientes pd where pd.estado != ? and pd.estado != ? and pd.estado != ? order by pd.fecha desc', ['Entregado', 'Cancelado', 'Error'],
						(err, rows, fields) => {
							if (err) {
								res.send(err);
							} else {
								pedidos = rows;
								//console.log(pedidos);
								for (let i = 0; i < pedidos.length; i++) {
									pedidos[i].type = 'tienda';
								}
								// Obtiene los pedidos empresariales
								connection.query('select pd.*, ri.nombre as nombrerider, ri.apellido as apellidorider, s.nombre as nombresucursal, s.numero_punto as numerosucursal, s.retenido as retiene, s.tiempo_entrega as tiempo_entrega, s.costo_envio as costoenvio, ds.iddireccion as iddireccionsucursal, ds.direccion as direccionsucursal, ds.notas as notasdireccionsucursal, ds.latitude as latitudesucursal, ds.longitude as longitudesucursal, d.iddireccion as iddireccionentrega, d.direccion as direccionentrega, d.notas as notasdireccionentrega, mu.nombre as municipio from pedidos_empresariales pd left join riders ri on ri.cedula = pd.idrider join direcciones d on d.iddireccion = pd.iddireccion join sucursales s on s.idsucursal = pd.idsucursal join municipios mu on mu.idmunicipios = d.idmunicipio join direcciones ds on ds.iddireccion = s.iddireccion where pd.estado != ? and pd.estado != ? and pd.estado != ? order by pd.fecha desc', ['Entregado', 'Cancelado', 'Error'],
									(err, rows, fields) => {
										if (err) {
											console.log(err);
											res.send(err);
										} else {
											empresariales = rows;
											//console.log(empresariales);
											for (let i = 0; i < empresariales.length; i++) {
												if (empresariales[i].estado === 'Error') {
													empresariales[i].color = 'state-error';
												} else if (empresariales[i].estado === 'Rider en camino') {
													empresariales[i].color = 'state-onrute'; // Amarillo
												} else if (empresariales[i].estado === 'Rider con productos') {
													empresariales[i].color = 'state-products'; // Verde
												} else if (empresariales[i].estado === 'Rider donde el cliente') {
													empresariales[i].color = 'state-oncustomer'; // Gris cañeria
												} else if (empresariales[i].estado === 'Rider en la tienda') {
													empresariales[i].color = 'state-onshop'; // Azul 
												} else if (empresariales[i].estado === 'En proceso') {
													empresariales[i].color = 'state-process'; // Rojo
												} else if (empresariales[i].estado === 'Rider regresando a la tienda') {
													empresariales[i].color = 'state-return-shop'; // Morado lila
												} else if (empresariales[i].estado === 'Rider regresando donde el cliente') {
													empresariales[i].color = 'state-return-customer'; // Naranja
												} else {
													empresariales[i].color = 'state-other'; // Super duper ultra rojo
												}
												empresariales[i].type = 'empresariales';
												// Convierte los valor de la fecha a un formato mas entendible
												let date = new Date(empresariales[i].fecha).toLocaleString();
												empresariales[i].fecha = date;
											}
											//let todos = deseos.concat(pedidos, mandados, empresariales);
											res.render('admin/pages/orders/pedidosencurso', {
												info: undefined,
												pedidostienda: pedidos,
												pedidosdeseos: deseos,
												pedidosmandados: mandados,
												pedidosempresariales: empresariales
											});
										}
								});
							}
						});
					}
				});
			}
	});
});

// Filtro que permite obtener los pedidos en historial en un intervalo de fechas
router.get('/historial-planes', (req, res) => {

	let fechaInicio = req.query.start;
	let fechaFin = req.query.end;
	let tienda = req.query.tienda;

	console.log('Entra historial planes filtro');

	// Verifica si se ingresan los valores necesarios para el filtro
	if (!(fechaInicio && fechaFin)) {
		return res.status(400).json('Ingrese la fecha inicio y fecha fin para efectuar el filtro.');
	}

	// Consulta para filtrar por planes de pedido en intervalo de fechas
	let query = 'SELECT tp.total_pedidos AS pedidos_disponibles, tp.adicionales, tp.estado, tp.created_at, t.nombre AS nombre_tienda, p.nombre AS nombre_pedido,' +
	' p.num_pedidos AS pedidos_plan FROM tiendas_planes tp JOIN historial_tiendas_planes htp ON tp.tiendas_id = htp.tiendas_planes_tiendas_id AND tp.planes_id = htp.tiendas_planes_planes_id JOIN tiendas t ON tp.tiendas_id = t.idtienda' +
	' JOIN planes p ON tp.planes_id = p.id WHERE tp.created_at >= ? AND tp.created_at <= ? ';

	// Condiciones para el filtro
	let condiciones = [
		fechaInicio,
		fechaFin
	]

	// Verifica si tambien se filtra por tienda
	if (tienda) {
		// Se agrega un nuevo filtro por id de tienda
		query += 'AND tp.tiendas_id = ?';
		// Se agrega el id de la tienda a las condiciones
		condiciones.push(tienda);
	}

	// Se obtiene el filtro de datos para las tiendas planes
	models.sequelize.query(query, { replacements: condiciones, type: models.sequelize.QueryTypes.SELECT })
	.then(filtro => {
		console.log('Se realiza el filtro', filtro);
		res.status(200).json(filtro);
	})
	.catch(err => {
		console.log('Error al filtrar por tienda plan.', err);
		res.status(400).json('Error al filtrar');
	});
	

/*
	connection.query("select pd.*, ri.nombre as nombrerider, ri.apellido as apellidorider, s.nombre as nombresucursal, s.numero_punto as numerosucursal, s.retenido as retiene, s.tiempo_entrega as tiempo_entrega, s.costo_envio as costoenvio, ds.iddireccion as iddireccionsucursal, ds.direccion as direccionsucursal, ds.notas as notasdireccionsucursal, ds.latitude as latitudesucursal, ds.longitude as longitudesucursal, d.iddireccion as iddireccionentrega, d.direccion as direccionentrega, d.notas as notasdireccionentrega from pedidos_empresariales pd left join riders ri on ri.cedula = pd.idrider join direcciones d on d.iddireccion = pd.iddireccion join sucursales s on s.idsucursal = pd.idsucursal join direcciones ds on ds.iddireccion = s.iddireccion where pd.fecha >= '" + dateFilterBefore + "' AND pd.fecha <= '" + dateFilterAfter + "'", (err, rows, fields) => {
		res.contentType('json');
		if(err){
			console.log(err);
			res.send({ err: JSON.stringify(err) });
		}else{
			orders = rows;
			for(let i = 0; i < orders.length; i++){
				orders[i].type = 'empresariales';

				// Convierte los valor de la fecha a un formato mas entendible
				let date = new Date(orders[i].fecha).toLocaleString();
				let date_split =  date.split(" ");
				orders[i].fecha = date_split[0];
				orders[i].hora = date_split[1];
				orders[i].nombre_rider = orders[i].nombrerider + ' ' + orders[i].apellidorider;
				orders[i].direccion_completa = orders[i].direccionentrega + ' ' + orders[i].notasdireccionentrega;
				if (orders[i].estado === 'Error') {
					orders[i].color = 'state-error';
				} else if (orders[i].estado === 'Rider en camino') {
					orders[i].color = 'state-onrute'; // Amarillo
				} else if (orders[i].estado === 'Rider con productos') {
					orders[i].color = 'state-products'; // Verde
				} else if (orders[i].estado === 'Rider en la tienda') {
					orders[i].color = 'state-onshop'; // Azul 
				} else if (orders[i].estado === 'En proceso') {
					orders[i].color = 'state-process'; // Rojo
				} else {
					orders[i].color = 'state-other'; // Super duper ultra rojo
				}
			}
  			res.send(orders);
		}
	});*/
});

// Filtro que permite obtener los pedidos en historial en un intervalo de fechas
router.post('/historialpedidosfiltro', (req, res) => {
	let orders;
	let request = req.body;

	let dateFilterBefore = request.before.fecha;// + ' ' + request.before.hora;
	let dateFilterAfter = request.after.fecha;// + ' ' + request.after.hora;

	connection.query("select pd.*, ri.nombre as nombrerider, ri.apellido as apellidorider, s.nombre as nombresucursal, s.numero_punto as numerosucursal, s.retenido as retiene, s.tiempo_entrega as tiempo_entrega, s.costo_envio as costoenvio, ds.iddireccion as iddireccionsucursal, ds.direccion as direccionsucursal, ds.notas as notasdireccionsucursal, ds.latitude as latitudesucursal, ds.longitude as longitudesucursal, d.iddireccion as iddireccionentrega, d.direccion as direccionentrega, d.notas as notasdireccionentrega from pedidos_empresariales pd left join riders ri on ri.cedula = pd.idrider join direcciones d on d.iddireccion = pd.iddireccion join sucursales s on s.idsucursal = pd.idsucursal join direcciones ds on ds.iddireccion = s.iddireccion where pd.fecha >= '" + dateFilterBefore + "' AND pd.fecha <= '" + dateFilterAfter + "'", (err, rows, fields) => {
		res.contentType('json');
		if(err){
			console.log(err);
			res.send({ err: JSON.stringify(err) });
		}else{
			orders = rows;
			for(let i = 0; i < orders.length; i++){
				orders[i].type = 'empresariales';

				// Convierte los valor de la fecha a un formato mas entendible
				let date = new Date(orders[i].fecha).toLocaleString();
				let date_split =  date.split(" ");
				orders[i].fecha = date_split[0];
				orders[i].hora = date_split[1];
				orders[i].nombre_rider = orders[i].nombrerider + ' ' + orders[i].apellidorider;
				orders[i].direccion_completa = orders[i].direccionentrega + ' ' + orders[i].notasdireccionentrega;
				if (orders[i].estado === 'Error') {
					orders[i].color = 'state-error';
				} else if (orders[i].estado === 'Rider en camino') {
					orders[i].color = 'state-onrute'; // Amarillo
				} else if (orders[i].estado === 'Rider con productos') {
					orders[i].color = 'state-products'; // Verde
				} else if (orders[i].estado === 'Rider en la tienda') {
					orders[i].color = 'state-onshop'; // Azul 
				} else if (orders[i].estado === 'En proceso') {
					orders[i].color = 'state-process'; // Rojo
				} else if (orders[i].estado === 'Rider regresando a la tienda') {
					orders[i].color = 'state-return-shop'; // Morado lila
				} else if (orders[i].estado === 'Rider regresando donde el cliente') {
					orders[i].color = 'state-return-customer'; // Naranja
				} else {
					orders[i].color = 'state-other'; // Super duper ultra rojo
				}
			}
  			res.send(orders);
		}
	});
});

// Visualiza el historial de pedidos (deseos, pedidos, mandados)
router.get('/historialpedidos', (req, res) => {
	let deseos, pedidos, mandados;
	connection.query('select pd.*, d.direccion as direccionentrega, d.notas as notasdireccionentrega, d.latitude as latitudeentrega, d.longitude as longitudeentrega, ri.nombre as nombrerider, ri.apellido as apellidorider, c.nombre as nombrecliente, c.telefono as telefonocliente from pedidos_deseos pd left join riders ri on ri.cedula = pd.idrider join clientes c on c.correo = pd.correocliente join direcciones d on d.iddireccion = pd.iddireccion order by pd.fecha desc',(err, rows, fields) =>{
		if(err){
			res.send(err);
		}else{
			deseos = rows;
			for(let i = 0; i < deseos.length; i++){
				deseos[i].type = 'deseo';
				// Convierte los valor de la fecha a un formato mas entendible
				let date = new Date(deseos[i].fecha).toLocaleString();
				let date_split =  date.split(" ");
				deseos[i].fecha = date_split[0];
				deseos[i].hora = date_split[1];
			}
			connection.query('select pd.*, do.direccion as direccionorigen, do.notas as notasdireccionorigen, do.latitude as latitudedireccionorigen, do.longitude as longitudedireccionorigen, dd.direccion as direcciondestino, dd.notas as notasdirecciondestino, dd.latitude as latitudedirecciondestino, dd.longitude as longitudedirecciondestino from pedidos_encomiendas pd join direcciones do on do.iddireccion = pd.iddireccionorigen join direcciones dd on dd.iddireccion = pd.iddirecciondestino order by pd.fecha desc',(err, rows, fields) =>{
				// console.log(rows); // Innecesario
				if(err){
					console.log(err);
					res.send(err);
				}else{
					mandados = rows;
					
					for(let i = 0; i < mandados.length; i++){
						mandados[i].type = 'mandado';
					}
					connection.query('select * from pedidos_clientes pd order by pd.fecha desc',(err, rows, fields) =>{
						if(err){
							res.send(err);
						}else{
							pedidos = rows;
							for(let i = 0; i < pedidos.length; i++){
								pedidos[i].type = 'tienda';
							}
							res.render('admin/pages/orders/historialpedidos', {info: undefined,pedidostienda: pedidos, pedidosdeseos: deseos, pedidosmandados: mandados});
						}
					});
				}
			});
		}
	});
});


router.get('/order-business/:id/edit', (req, res) => {

	// Id de la tienda
	const id = req.params.id;

	// Pedido empresarial
	let order = models.sequelize.query('SELECT pe.*, r.nombre AS nombrerider, r.apellido AS apellidorider, s.tipo AS tipoSucursal' +
		' FROM pedidos_empresariales pe LEFT JOIN riders r ON r.cedula = pe.idrider' +
		' INNER JOIN sucursales s ON pe.idsucursal = s.idsucursal WHERE idpedido_empresarial = ?', 
		{ replacements: [id], type: models.sequelize.QueryTypes.SELECT });

	// Obtiene los riders activos
	let	riders = Riders.findAll({
		attributes: ['cedula', [models.sequelize.fn('CONCAT', models.sequelize.col('nombre'), ' ', models.sequelize.col('apellido')), 'nombre']],
		where: {
			listanegra: 'N',
			activo: 'S'
		}
	});

	// Se ejecutan las promesas
	Promise.all([order, riders])
	.then(rows => {
		// Estados de pedidos empresariales
		const states = ['En proceso', 'Rider en camino', 'Rider en la tienda', 'Rider con productos',
		'Rider donde el cliente', 'Entregado', 'Error', 'Cancelado'];
		
		// Verifica si ya posee un ajuste
		const has_adjust = (rows[0][0].ajustes_id)? true : false;

		// Evia los datos recolectados a la vista
		res.send({
			order: rows[0][0],
			riders: rows[1],
			states,
			tam_maletin: Constants.TIPO_MALETIN,
			has_adjust
		});
	})
	.catch(err => {
		console.log(err);
		res.send(err);
	});
});

// Visualiza formulario para alterar el estado del pedido en curso
router.get('/pedidoempresarial', (req, res) => {
	let id = req.query.id;

/*	let order = db.query('SELECT pe.*, r.nombre AS nombrerider, r.apellido AS apellidorider, s.tipo AS tipoSucursal' +
		' FROM pedidos_empresariales pe LEFT JOIN riders r ON r.cedula = pe.idrider' +
		' INNER JOIN sucursales s ON pe.idsucursal = s.idsucursal WHERE idpedido_empresarial = ?', [id]);

	let	riders = db.query('SELECT * FROM riders WHERE listanegra = ? and activo = ?', ['N', 'S']);

*/
	let order = models.sequelize.query('SELECT pe.*, r.nombre AS nombrerider, r.apellido AS apellidorider, s.tipo AS tipoSucursal' +
		' FROM pedidos_empresariales pe LEFT JOIN riders r ON r.cedula = pe.idrider' +
		' INNER JOIN sucursales s ON pe.idsucursal = s.idsucursal WHERE idpedido_empresarial = ?', 
		{ replacements: [id], type: models.sequelize.QueryTypes.SELECT });

	let	riders = Riders.findAll({
		attributes: ['cedula', 'nombre', 'apellido'],
		where: {
			listanegra: 'N',
			activo: 'S'
		}
	});

	Promise.all([order, riders])
		.then(rows => {
			let states = ['En proceso', 'Rider en camino', 'Rider en la tienda', 'Rider con productos',
			'Rider donde el cliente', 'Entregado', 'Error', 'Cancelado'];
			
			let hasAjuste = (rows[0][0].ajustes_id)? true : false;

			res.render('admin/pages/orders/pedidoempresarial', {
				pedido: rows[0][0],
				riders: rows[1],
				estados: states,
				tam_maletin: Constants.TIPO_MALETIN,
				hasAjuste: hasAjuste
			});
		})
		.catch(err => {
			console.log(err);
			res.send(err);
		});
});

/**
 * Permite editar un pedido deseo
 */
router.get('/pedidodeseo', (req, res) => {
	let id = req.query.id;
	connection.query('select pd.*, d.direccion as direccionentrega, d.notas as notasdireccionentrega, d.latitude as latitudeentrega, d.longitude as longitudeentrega, ri.nombre as nombrerider, ri.apellido as apellidorider, ri.telefono as telefonorider, c.nombre as nombrecliente, c.telefono as telefonocliente from pedidos_deseos pd left join riders ri on ri.cedula = pd.idrider join clientes c on c.correo = pd.correocliente join direcciones d on d.iddireccion = pd.iddireccion where idpedido_deseo = ?',
		[id], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(err);
			} else {
				connection.query('select * from facturas where idpedido_deseos = ?', [id],(err, facturas, fields) => {
					if (err) {
						console.log(err);
						send(err);
					} else {
						connection.query('SELECT * FROM riders WHERE listanegra = ? and activo = ?', ['N', 'S'], (err, riders, fields) => {
							let states = ['En proceso' ,'Tomado por el Rider', 'Rider en el sitio de recogida', 'Rider recorriendo paradas',
							'Paradas recorridas', 'Rider en el sitio de entrega', 'Entregado', 'Cancelado', 'Error'];
							
							res.render('admin/pages/orders/pedidodeseo', {
								pedido: rows[0],
								facturas: facturas,
								riders: riders,
								estados: states
							});
						});
						
					}
				});
				
			}
		});
});

/**
 * Permite editar un pedido encomienda
 */
router.get('/pedidoencomienda', (req, res) => {
	let id = req.query.id;
	connection.query('select pe.*, r.nombre as nombrerider, r.apellido as apellidorider from pedidos_encomiendas pe left join riders r on r.cedula = pe.idrider where idpedido_encomienda = ?'
		,[id],(err, rows, fields) => {
			if(err){
				console.log(err);
				res.send(err);
			}else{
				connection.query('SELECT * FROM riders WHERE listanegra = ? and activo = ?', ['N', 'S'], (err, riders, fields) => {
					if(err){
						console.log(err);
						res.send(err);
					}else{
						let states = ['En proceso', 'Rider en camino', 'Rider en la tienda', 'Rider con productos',
							'Rider donde el cliente', 'Entregado', 'Cancelado', 'Error'];
						res.render('admin/pages/orders/pedidoencomienda', {
							pedido: rows[0],
							riders: riders,
							estados: states
						});
					}
					
				});
				
			}
		});
});


router.put('/order-business/:id', (req, res) => {

	let idpedido = req.params.id;
	let idrider = req.body.rider;
	let estado = req.body.state;
	let foto = (req.files)? req.files.evidence: null;
	
	let maletin = req.body.size_bag;

	let valor_ajuste = req.body.adjust_value;
	let detalle_ajuste = req.body.adjust_description;

	console.log('BODY', req.body);
	let name = null;
	
	if (idrider == ('Sin Rider')) {
		idrider = null;
	}

	if (foto) {
		let extension = foto.name.split('.').pop();
		let direccionImg = 'src/app/public/img/orders/' + idpedido + '.' + extension;
		fs.renameSync(foto.path, direccionImg);
		name = idpedido + '.' + extension;
	}

	// Objeto de actualizacion de pedidos
	let update = { 
		evidencia: name,
		estado: estado,
		idrider: idrider,
		tamanio_maletin: (maletin)? maletin : null
	}
	

	console.log('VAlor ajuste', valor_ajuste);

	// Verificar si hay un ajuste a insertar
	if (valor_ajuste) {
		console.log('Entra ajsute');

		const usuario = req.session.user;

		// Crea un nuevo ajuste
		Ajustes.create({
			valor: valor_ajuste,
			detalle: detalle_ajuste,
			encargado_id: parseInt(usuario.usuario.cedula),
			encargado_rol: usuario.type
		})
		//}, {transaction: trans})
		.then( (ajuste) => {
			update.ajustes_id = ajuste.id;

			// Actualiza el pedido emrpesarial
			updatePedidoEmpresarial(update, idpedido, res);
		})
		.catch(async err => {
			//await trans.rollback();
			console.log('Error al crear un ajuste', err);
			res.status(400).json(err);
		});
	} else {
		// Actualiza el pedido emrpesarial
		updatePedidoEmpresarial(update, idpedido, res);
	}

	/**
	 * Actuliza el pedido empresarial
	 */
	function updatePedidoEmpresarial(update, idpedido, res) {

		// Actualza un pedido empresarial
		PedidosEmpresariales.findOne({
			where: {
				idpedido_empresarial: idpedido
			}
		})
		.then(async pe => {

			// Se obtiene la sucursal asociada al pedido
			let sucursal = await Sucursales.findOne({
				attributes: ['idtienda', 'tipo'],
				where: {
					idsucursal: pe.idsucursal
				}
			});

			// Se recorren todas las claves disponibles
			Object.keys(update).forEach((item) => {
				pe[item] = update[item];
			});

			// Guarda la actualizacion de los valores
			pe.save()
			.then(async () => {
				console.info('Entra guardado cambios');
				// Verifica si el estado a cambiar es Cancelado
				if (estado == 'Cancelado') {
					console.info('Entra estado Cancelado');
					// Verifica si el tipo de sucursal es Prepagada
					if (sucursal.tipo === getConstant(Constants.TIPO_SUCURSAL, 'Prepagada').id ) {

						// Busca las tiendas planes que tienen estado activo
						await TiendasPlanes.findOne({
							where: {
								tiendas_id: sucursal.idtienda,
								estado: getConstant(Constants.ESTADO_TIENDAS_PLANES, 'Activo').id
							}
						})// {transaction: trans}
						.then(async tp => {
							
							// Verifica si existe el elemento
							if (tp) {
								console.info('Entra estado activo', tp);
								// Tienda plan activo
								let tiendaPlan = tp;
			
								// Se suma el pedido cancelado al plan activo
								tiendaPlan.total_pedidos = parseInt(tiendaPlan.total_pedidos) + 1;
			
								// Guarda la acualizacion del saldo de pedidos + 1
								await tiendaPlan.save()
								.then(() => res.status(200).end())
								.catch(async err => {
									// Devuelve los cambios realizados
									//await trans.rollback();
									console.log('Error al actualizar el saldo del plan activo.', err);
									res.status(400).json(err);
									//res.redirect('/tienda/listapedidos?info=err');		
								});
							} else {
								// Selecciona el ultimo elemento activo, elemento inactivo más reciente
								await models.sequelize.query(
									'SELECT * FROM tiendas_planes WHERE updated_at = (SELECT MIN(updated_at) FROM tiendas_planes WHERE estado = $1)',
								{ bind: [getConstant(Constants.ESTADO_TIENDAS_PLANES, 'Inactivo').id], type: models.sequelize.QueryTypes.SELECT })
								.then(async tpi => {
			
									// Verifica si no hay elementos
									if (tpi.length > 0) {
			
										// Datos de la tienda plan filtrada
										let tiendaPlan = tpi[0];
			
										// Actualiza el saldo y estado de la tienda plan inactiva
										await TiendasPlanes.update(
											{ 
												total_pedidos: parseInt(tiendaPlan.total_pedidos) + 1,
												estado: getConstant(Constants.ESTADO_TIENDAS_PLANES, 'Activo').id
											},
											{
												where: {
													tiendas_id: tiendaPlan.tiendas_id,
													planes_id: tiendaPlan.planes_id
												}
											}//, {transaction: trans}
										);
			
										// Busca el historial con actualizacion mas reciente de estado finalizado
										await HistorialTiendasPlanes.findAll({
											where: {
												tiendas_planes_planes_id: tiendaPlan.planes_id,
												tiendas_planes_tiendas_id: tiendaPlan.tiendas_id,
												estado: getConstant(Constants.ESTADO_HISTORIAL_PLANES, 'Finalizado').id
											},
											order: [['updatedAt']], // Los mas nuevos
											limit: 1
										})//, {transaction: trans})
										.then(async htp => {
											// Verifica si hay elementos
											if (htp.length > 0) {
												let historial = htp[0];
			
												historial.estado = getConstant(Constants.ESTADO_HISTORIAL_PLANES, 'En curso').id;
			
												await historial.save();
												//.then(async () => await trans.commit());
												console.log('Actualiza pedido empresarial');
												await res.status(200).end();
												//res.redirect('/administrador/pedidosencurso');
			
											} else {
												//await trans.rollback();
												console.log('No se ecuentra un historial para actualizar');
												//res.send(err);
												res.status(400).json(err);
											}
										});
			
									} else {
										console.log('No hay elementos inactivos');
										res.status(400).json(err);
									}
								})
								.catch(async err => {
									// Devuelve los cambios realizados
									//await trans.rollback();
			
									console.log(err);
									res.status(400).json(err);
								});
							}
						});
					} else {
						res.status(200).end();
						//res.redirect('/administrador/pedidosencurso');
					}	
				} else {
					await res.status(200).end();
					//res.redirect('/administrador/pedidosencurso');
				}
			});

		})
		.catch(err => {
			console.log('Error actualizar pedido empresarial', err);
//			res.send(err);
			res.status(400).json(err);
		});
	}

});

router.put('/modificarpedidoempresarial/:id', (req, res) => {
	let idpedido = req.params.id;
	let idrider = req.body.rider;
	let estado = req.body.estado;
	let foto = req.files.evidencia;
	
	let maletin = req.body.maletin;

	let valor_ajuste = req.body.adjust_value;
	let detalle_ajuste = req.body.adjust_description;

	let name = null;
	
	if (idrider == 'Sin Rider') {
		idrider = null;
	}

	if (foto) {

		let extension = foto.name.split('.').pop();
		let direccionImg = 'src/app/public/img/orders/' + idpedido + '.' + extension;
		fs.renameSync(foto.path, direccionImg);
		name = idpedido + '.' + extension;
		//const readStream = fs.createReadStream(direccionImg);
		/*sharp(direccionImg)
		.resize(200)
		.toFile(idpedido + '.' + extension)
		.then( data => {
			console.log('Info', data);
			
		 })
		.catch( err => { 
			console.log('Error', err);
		 });
		*/
	}

	// Objeto de actualizacion de pedidos
	let update = { 
		evidencia: name,
		estado: estado,
		idrider: idrider,
		tamanio_maletin: (maletin)? maletin : null
	}

	console.log('Valor ajuste', valor_ajuste);

	// Verificar si hay un ajuste a insertar
	if (valor_ajuste) {

		const usuario = req.session.user;

		// Crea un nuevo ajuste
		Ajustes.create({
			valor: valor_ajuste,
			detalle: detalle_ajuste,
			encargado_id: parseInt(usuario.usuario.cedula),
			encargado_rol: usuario.type
		})
		//}, {transaction: trans})
		.then( (ajuste) => {
			update.ajustes_id = ajuste.id;

			// Actualiza el pedido emrpesarial
			updatePedidoEmpresarial(update, idpedido, res);
		})
		.catch(async err => {
			//await trans.rollback();
			console.log('Error al crear un ajuste', err);
			res.send(err);
		});
	} else {
		// Actualiza el pedido emrpesarial
		updatePedidoEmpresarial(update, idpedido, res);
	}

	/**
	 * Actuliza el pedido empresarial
	 */
	function updatePedidoEmpresarial(update, idpedido, res) {

		console.log('update3', update);

		// crea un nuevo pedido empresarial
		/*PedidosEmpresariales.update(
			update,
			{
				where: {
					idpedido_empresarial: idpedido
				}
			}
		).then(() => {
			res.redirect('/administrador/pedidosencurso');
		})
		.catch(err => {
			console.log('Error actualizar pedido empresarial', err);
			res.send(err);
		});*/

		// Actualza un pedido empresarial
		PedidosEmpresariales.findOne({
			where: {
				idpedido_empresarial: idpedido
			}
		})
		.then(async pe => {

			// Se obtiene la sucursal asociada al pedido
			let sucursal = await Sucursales.findOne({
				attributes: ['idtienda', 'tipo'],
				where: {
					idsucursal: pe.idsucursal
				}
			});

			// Se recorren todas las claves disponibles
			Object.keys(update).forEach((item) => {
				pe[item] = update[item];
			});

			// Guarda la actualizacion de los valores
			pe.save()
			.then(async () => {
				console.info('Entra guardado cambios');
				// Verifica si el estado a cambiar es Cancelado
				if (estado == 'Cancelado') {
					console.info('Entra estado Cancelado');
					// Verifica si el tipo de sucursal es Prepagada
					if (sucursal.tipo === getConstant(Constants.TIPO_SUCURSAL, 'Prepagada').id ) {

						// Busca las tiendas planes que tienen estado activo
						await TiendasPlanes.findOne({
							where: {
								tiendas_id: sucursal.idtienda,
								estado: getConstant(Constants.ESTADO_TIENDAS_PLANES, 'Activo').id
							}
						})// {transaction: trans}
						.then(async tp => {
							
							// Verifica si existe el elemento
							if (tp) {
								console.info('Entra estado activo', tp);
								// Tienda plan activo
								let tiendaPlan = tp;
			
								// Se suma el pedido cancelado al plan activo
								tiendaPlan.total_pedidos = parseInt(tiendaPlan.total_pedidos) + 1;
			
								// Guarda la acualizacion del saldo de pedidos + 1
								await tiendaPlan.save()
								.then(() => res.redirect('/administrador/pedidosencurso'))
								.catch(async err => {
									// Devuelve los cambios realizados
									//await trans.rollback();
									console.log('Error al actualizar el saldo del plan activo.', err);
									res.redirect('/tienda/listapedidos?info=err');		
								});
							} else {
								// Selecciona el ultimo elemento activo, elemento inactivo más reciente
								await models.sequelize.query(
									'SELECT * FROM tiendas_planes WHERE updated_at = (SELECT MIN(updated_at) FROM tiendas_planes WHERE estado = $1)',
								{ bind: [getConstant(Constants.ESTADO_TIENDAS_PLANES, 'Inactivo').id], type: models.sequelize.QueryTypes.SELECT })
								.then(async tpi => {
			
									// Verifica si no hay elementos
									if (tpi.length > 0) {
			
										// Datos de la tienda plan filtrada
										let tiendaPlan = tpi[0];
			
										// Actualiza el saldo y estado de la tienda plan inactiva
										await TiendasPlanes.update(
											{ 
												total_pedidos: parseInt(tiendaPlan.total_pedidos) + 1,
												estado: getConstant(Constants.ESTADO_TIENDAS_PLANES, 'Activo').id
											},
											{
												where: {
													tiendas_id: tiendaPlan.tiendas_id,
													planes_id: tiendaPlan.planes_id
												}
											}//, {transaction: trans}
										);
			
										// Busca el historial con actualizacion mas reciente de estado finalizado
										await HistorialTiendasPlanes.findAll({
											where: {
												tiendas_planes_planes_id: tiendaPlan.planes_id,
												tiendas_planes_tiendas_id: tiendaPlan.tiendas_id,
												estado: getConstant(Constants.ESTADO_HISTORIAL_PLANES, 'Finalizado').id
											},
											order: [['updatedAt']], // Los mas nuevos
											limit: 1
										})//, {transaction: trans})
										.then(async htp => {
											// Verifica si hay elementos
											if (htp.length > 0) {
												let historial = htp[0];
			
												historial.estado = getConstant(Constants.ESTADO_HISTORIAL_PLANES, 'En curso').id;
			
												await historial.save()
												//.then(async () => await trans.commit());
												res.redirect('/administrador/pedidosencurso');
			
											} else {
												//await trans.rollback();
												console.log('No se ecuentra un historial para actualizar');
												res.send(err);
											}
										});
			
									} else {
										console.log('No hay elementos inactivos');
										res.send(err);
									}
								})
								.catch(async err => {
									// Devuelve los cambios realizados
									//await trans.rollback();
			
									console.log(err);
									res.send(err);
								});
							}
						});
					} else {
						res.redirect('/administrador/pedidosencurso');
					}	
				} else {
					res.redirect('/administrador/pedidosencurso');
				}
			});

		})
		.catch(err => {
			console.log('Error actualizar pedido empresarial', err);
			res.send(err);
		});
		
	}

	
	// TODO Ajustes

/*	connection.query('update pedidos_empresariales set estado = ?, idrider = ?, evidencia = ? where idpedido_empresarial = ?', [estado, idrider, name, idpedido], (err, rows, fields) => {
		if (err) {
			console.log(err);
			res.send(err);
		} else {
			return res.redirect('/administrador/pedidosencurso');
		}
	});
*/
});

router.put('/modificarpedidodeseo/:id', (req, res) => {
	let idpedido = req.params.id;
	let idrider = req.body.rider;
	let estado = req.body.estado;

	if(idrider == 'Sin Rider'){
		idrider = null;
	}

	connection.query('update pedidos_deseos set estado = ?, idrider = ? where idpedido_deseo = ?', [estado, idrider, idpedido], (err, rows, fields) => {
		if(err){
			res.send(err);
		}else{
			return res.redirect('/administrador/pedidosencurso');
		}
	});

});

router.put('/modificarpedidoencomienda/:id', (req, res) => {
	let idpedido = req.params.id;
	let idrider = req.body.rider;
	let estado = req.body.estado;

	if(idrider == 'Sin Rider'){
		idrider = null;
	}

	connection.query('update pedidos_encomiendas set estado = ?, idrider = ? where idpedido_encomienda = ?', [estado, idrider, idpedido], (err, rows, fields) => {
		if(err){
			res.send(err);
		}else{
			return res.redirect('/administrador/pedidosencurso');
		}
	});

});

/** CRUD CONFIG INIT**/
// (INDEX) Obtiene todas las configuracion
router.get('/configuraciones', (req, res) => {
	let info = req.query.info;

	db.query('SELECT * FROM configuraciones')
	.then(rows => {
		res.render('admin/pages/config/edit', {
			config: rows,
			info: info
		});
	})
	.catch(err => {
		console.log(err);
		res.redirect('/administrador');
	});
});
// (INDEX) Obtiene todas las configuracion
router.put('/configuraciones/update', (req, res) => {
	
	let file = req.files.imagen;
	let name = 'default.png';

	if (file.name != '') {
		let extension = file.name.split('.').pop();
		let direccionImg = 'src/app/public/img/config/inicio.' + extension;
		fs.renameSync(file.path, direccionImg);
		name = 'inicio.' + extension;
		
		db.query('SELECT * FROM configuraciones')
		.then(rows => {
			if (rows.length > 0) {
				return db.query('UPDATE configuraciones SET img_login = ?', [name]);
			} else {
				return db.query('INSERT INTO configuraciones (img_login) VALUES(?)', [name]);
			}
		})
		.then(() => {
			res.redirect('/administrador/configuraciones?info=correct');
		})
		.catch(err => {
			console.log(err);
			res.redirect('/administrador/configuraciones?info=err')
		});
	} else {
		res.redirect('/administrador/configuraciones?info=updated');
	}

	
});

/**
 * Actualiza el limite de pedidos en curso a mostrar 
 */
router.get('/orders-limit-update/:limit', (req, res) => {
	// limite a actualizar
	const limit = req.params.limit;

	// Actualiza el limite en configuraciones
	models.sequelize.query(`UPDATE configuraciones SET limit_business_orders = ${limit}`)
		.then(() => {
			res.status(200).json('Limite actualizado con exito.');
		}).catch(err => {
			console.log('Error al actualizar el limite.', err);
			res.status(400).json('Error al actualizar el limite.');
		});

});
/*router.get('/configuraciones', (req, res) => {
	let info = req.query.info;

	db.query('SELECT * FROM configuraciones')
	.then(rows => {
		res.render('admin/pages/config/index', {
			config: rows,
			info: info
		});
	})
	.catch(err => {
		console.log(err);
		res.redirect('/administrador/configuraciones');
	});
});*/
// (CREATE) Visualiza el formulario para agregar una configuracion
/*router.get('/configuraciones/create', (req, res) => {
	res.render('admin/pages/config/create');
});*/
// (SHOW) Permite visualizar una configuracion
/*router.get('/configuraciones/:id', (req, res) => {
});*/
// (EDIT) Permite editar una configuracion
/*router.get('/configuraciones/:id/edit', (req, res) => {
	let id = req.params.id;
	let info = req.query.info;

	db.query('SELECT * FROM configuraciones WHERE id = ?', [id])
	.then(rows => {
		res.render('admin/pages/config/edit', {
			config: rows[0],
			info: info
		});	
	})
	.catch(err => {
		console.log(err);
		res.redirect('/configuraciones?info=err');
	});
});*/
// (UPDATE) Permite editar una configuracion [actualizar cuando se inserten mas campos]
/*router.put('/configuraciones/:id', (req, res) => {

	let id = req.params.id;
	let file = req.files.imagen;
	let name = 'default.png';
	
	if (file.name != '') {
		let extension = file.name.split('.').pop();
		let direccionImg = 'src/app/public/img/config/inicio.' + extension;
		fs.renameSync(file.path, direccionImg);
		name = 'inicio.' + extension;
	} 
	res.redirect('/administrador/configuraciones?info=updated');
});*/
// (STORE) Agrega una nueva configuracion
/*router.post('/configuraciones', (req, res) => {

	let file = req.files.imagen;
	let name = 'default.png';
	
	if (file.name != '') {
		let extension = file.name.split('.').pop();
		let direccionImg = 'src/app/public/img/config/inicio.' + extension;
		fs.renameSync(file.path, direccionImg);
		name = 'inicio.' + extension;
	}

	db.query('INSERT INTO configuraciones (img_login) VALUES(?)', [name])
	.then(() => {
		res.redirect('/administrador/configuraciones?info=correct');
	})
	.catch(err => {
		console.log(err);
		res.redirect('/administrador/configuraciones/create?info=err')
	});

});*/
// (DESTROY) Elimina una configuracion
/*router.get('/configuraciones/delete/:id', (req, res) => {
});*/
/** CRUD CONFIG FINISH**/

/** CRUD COSTS INIT**/
// (INDEX) Obtiene los costos actuales
router.get('/costos', (req, res) => {

	let info = req.query.info;

	connection.query('SELECT c.*, m.nombre AS municipio FROM costos c JOIN municipios m ON c.idmunicipio = m.idmunicipios', (err, costos, fields) => {
		if (err) {
			console.log(err);
			res.redirect('/administrador/costos');
		} else {
			res.render('admin/pages/costs/index', {
				costos: costos,
				info: info
			});
		}
	});
});

// (UPDATE) Modifica los costos actuales
router.put('/costos/:id', (req, res) => {
	let id = req.params.id;
	let costo_envio_deseo = req.body.costo_envio_deseo;
	let comision_cliente_deseo = req.body.comision_cliente_deseo;
	let costo_envio_cliente = req.body.costo_envio_cliente;
	let comision_rider_cliente = req.body.comision_rider_cliente;
	let costo_envio_mandado = req.body.costo_envio_mandado;
	let costo_paradas_mandado = req.body.costo_paradas_mandado;
	let comision_rider_mandado = req.body.comision_rider_mandado;
	let comision_rider_empresarial = req.body.comision_rider_empresarial;
	let idmunicipio = req.body.municipio;

	connection.query('UPDATE costos SET costo_envio_deseo = ?, comision_cliente_deseo = ?, costo_envio_cliente = ?, comision_rider_cliente = ?, costo_envio_mandado = ?, costo_paradas_mandado = ?, comision_rider_mandado = ?, comision_rider_empresarial = ?, idmunicipio = ? WHERE idcosto = ?' ,
	[costo_envio_deseo, comision_cliente_deseo, costo_envio_cliente, comision_rider_cliente, costo_envio_mandado, costo_paradas_mandado, comision_rider_mandado, comision_rider_empresarial, idmunicipio, id], 
		(err, rows, fields) => {
			if (err) {
				console.log(err);
				res.redirect('/administrador/costos?info=err')
			} else {
				res.redirect('/administrador/costos?info=correct');
			}
		});
});

// (CREATE) Visualiza el formulario para agregar un nuevo costo
router.get('/costos/create', (req, res) => {
	res.render('admin/pages/costs/create');
});

// (EDIT) Permite editar un costo
router.get('/costos/:id/edit', (req, res) => {

	let id = req.params.id;
	let info = req.query.info;

	connection.query('SELECT * FROM costos WHERE idcosto = ?', [id],
	 	(err, rows, fields) => {
			if (err) {
				console.log(err);
				res.redirect('/costos?info=err');
			} else {
				if (rows[0].idmunicipio) {
					connection.query('SELECT idmunicipios, d.idPais, m.idDepartamento FROM departamentos d, municipios m, paises p WHERE d.idDepartamento = m.idDepartamento AND m.idmunicipios = ?',
					[rows[0].idmunicipio], (err, dir, fields) => {
						if (err) {
							console.log(err);
							res.redirect('/costos?info=err');
						} else {
							res.render('admin/pages/costs/edit', {
								costo: rows[0],
								direccion: dir[0],
								info: info
							});	
						}
					});
				} else {
					res.render('admin/pages/costs/edit', {
						costo: rows[0],
						info: info
					});	
				}
				
			}
		});
});

/**
 * Crea nuevos costos para la ciudad predeterminada
 */
router.post('/costos', (req, res) => {
	let costo_envio_deseo = req.body.costo_envio_deseo;
	let comision_cliente_deseo = req.body.comision_cliente_deseo;
	let costo_envio_cliente = req.body.costo_envio_cliente;
	let comision_rider_cliente = req.body.comision_rider_cliente;
	let costo_envio_mandado = req.body.costo_envio_mandado;
	let costo_paradas_mandado = req.body.costo_paradas_mandado;
	let comision_rider_mandado = req.body.comision_rider_mandado;
	let comision_rider_empresarial = req.body.comision_rider_empresarial;
	let idmunicipio = req.body.municipio;

	connection.query('INSERT INTO costos (costo_envio_deseo, comision_cliente_deseo, costo_envio_cliente, comision_rider_cliente, costo_envio_mandado, costo_paradas_mandado, comision_rider_mandado, comision_rider_empresarial, idmunicipio) VALUES(?,?,?,?,?,?,?,?,?)',
	[costo_envio_deseo, comision_cliente_deseo, costo_envio_cliente, comision_rider_cliente, costo_envio_mandado, costo_paradas_mandado, comision_rider_mandado, comision_rider_empresarial, idmunicipio], 
		(err, rows, fields) => {
			if (err) {
				console.log(err);
				res.redirect('/administrador/costos/create?info=err')
			} else {
				res.redirect('/administrador/costos?info=correct');
			}
		});
});

/** CRUD CATEGORY PRODUCT INIT**/
// (INDEX) Obtiene todas las categorías de productos
router.get('/categoriaproductos', (req, res) => {
	let info = req.query.info;

	connection.query('select * from categoriasproductos', (err, rows, fields) =>{
		if (err) {
			console.log(err);
			res.redirect('/administrador');
		} else {
			res.render('admin/pages/categories/products/index', {
				categorias: rows,
				info: info
			});
		}
	});
});
// (CREATE) Visualiza el formulario para agregar una categoría
router.get('/categoriaproductos/create', (req, res) => {
	res.render('admin/pages/categories/products/create');
});
/**
	// (SHOW) Permite visualizar un...
	router.get('/categoriaproductos/:id', (req, res) => {
	});
	// (EDIT) Permite editar una sucursal
	router.get('/categoriaproductos/:id/edit', (req, res) => {
	});
	// (UPDATE) Permite editar un...
	router.put('/categoriaproductos/:id', (req, res) => {
	});
*/
// (STORE) Agrega una nueva categoría
router.post('/categoriaproductos', (req, res) => {
	let nombre = req.body.nombre;
	let descripcion = req.body.descripcion;

	connection.query('insert into categoriasproductos (nombre, descripcion) values(?,?)',
		[nombre, descripcion], (err, rows, fields) => {
			if (err) {
				console.log(err);
				return res.redirect('/administrador/categoriaproductos?info=err');
			} else {
				return res.redirect('/administrador/categoriaproductos?info=correct');
			}
		});
});
// (DESTROY) Elimina una categoria
router.get('/categoriaproductos/delete/:id', (req, res) => {
	let id = req.params.id;

	connection.query('delete from categoriasproductos where idcategoriaproducto = ?',
		[id], (err, rows, fields) => {
			if (err) {
				console.log(err);
				return res.redirect('/administrador/categoriaproductos?info=err');
			} else {
				return res.redirect('/administrador/categoriaproductos?info=eliminated');
			}
	});
});
/** CRUD CATEGORY PRODUCT FINISH**/


/** CRUD CATEGORY SHOPS INIT**/
// (INDEX) Obtiene todas las categorías de tiendas
router.get('/categoriatiendas', (req, res) => {
	let info = req.query.info;

	connection.query('select * from categorias', (err, rows, fields) =>{
		if (err) {
			console.log(err);
			res.redirect('/administrador');
		} else {
			res.render('admin/pages/categories/shops/index', {
				categorias: rows,
				info: info
			});
		}
	});
});
// (CREATE) Visualiza el formulario para agregar una categoría
router.get('/categoriatiendas/create', (req, res) => {
	res.render('admin/pages/categories/shops/create');
});
/**
	// (SHOW) Permite visualizar un...
	router.get('/categoriaproductos/:id', (req, res) => {
	});
	// (EDIT) Permite editar una sucursal
	router.get('/categoriaproductos/:id/edit', (req, res) => {
	});
	// (UPDATE) Permite editar un...
	router.put('/categoriaproductos/:id', (req, res) => {
	});
*/
// (STORE) Agrega una nueva categoría
router.post('/categoriatiendas', (req, res) => {
	let nombre = req.body.nombre;
	let descripcion = req.body.descripcion;
	let imagen = req.body.imagen;
	let foto = req.files.imagen;
	let direccionImg;
	let name;

	direccionImg = 'src/app/public/img/categoria/default.png';
	name = 'default.png';
	
	if (foto.name != '') {
		let extension = foto.name.split('.').pop();
		let direccionImg = 'src/app/public/img/categoria/' + nombre + '.' + extension;
		fs.renameSync(foto.path, direccionImg);
		name = nombre + '.' + extension;
	}

	connection.query('insert into categorias (nombre, descripcion, imagen) values(?,?,?)',
		[nombre, descripcion, name], (err, rows, fields) => {
			if (err) {
				console.log(err);
				return res.redirect('/administrador/categoriatiendas?info=err');
			} else {
				return res.redirect('/administrador/categoriatiendas?info=correct');
			}
		});
});
// (DESTROY) Elimina una categoria
router.get('/categoriatiendas/delete/:id', (req, res) => {
	let id = req.params.id;

	connection.query('delete from categorias where idcategoria = ?', [id], (err, rows, fields) => {
		if (err) {
			console.log(err);
			return res.redirect('/administrador/categoriatiendas?info=err');
		} else {
			return res.redirect('/administrador/categoriatiendas?info=eliminated');
		}
	});
});
/** CRUD CATEGORY SHOP FINISH**/

/** CRUD EXAMPLE INIT**/
// (INDEX) Obtiene tod...
router.get('/example', (req, res) => {
});
// (CREATE) Visualiza el formulario para agregar un...
router.get('/example/create', (req, res) => {
});
// (SHOW) Permite visualizar un...
router.get('/example/:id', (req, res) => {
});
// (EDIT) Permite editar una sucursal
router.get('/example/:id/edit', (req, res) => {
});
// (UPDATE) Permite editar un...
router.put('/example/:id', (req, res) => {
});
// (STORE) Agrega una nueva...
router.post('/example', (req, res) => {
});
// (DESTROY) Elimina una sucursal
router.get('/example/delete/:id', (req, res) => {
});
/** CRUD EXAMPLE FINISH**/

/** CRUD ORDER STATE INIT**/
// (INDEX) Obtiene todos los estados de pedido
router.get('/estados', (req, res) => {
	let info = req.query.info;

	db.query('SELECT * FROM estados_pedido')
	.then(rows => {
		res.render('admin/pages/order_state/index', {
			estados: rows,
			info: info
		});
	}).catch(err => {
		console.log(err);
		res.redirect('/administrador');
	});
});
// (CREATE) Visualiza el formulario para agregar un estado de pedido
router.get('/estados/create', (req, res) => {
	// Se obtiene los contextos de pedido disponibles
	db.query('SELECT * FROM contexto_pedidos')
		.then( rows => {
			res.render('admin/pages/order_state/create', {
				tipos: rows,
			});
		}).catch( err => {
			console.log(err);
			res.redirect('/administrador');
		});
});
// (SHOW) Permite visualizar un estado de pedido
router.get('/estados/:id', (req, res) => {
});
// (EDIT) Permite editar un estado de pedido
router.get('/estados/:id/edit', (req, res) => {
	let id = req.params.id;

	db.query('SELECT * FROM estados_pedido WHERE id = ?', [id])
		.then(async estado => {
			await db.query('SELECT * FROM contexto_pedidos')
				.then( async tipos => {
					await db.query('SELECT * FROM contexto_estados_pedido WHERE estados_pedido_id = ?', [id])
					.then( ctx => {

						for (let i = 0; i < tipos.length; i++) {
							const t = tipos[i];
							for (let j = 0; j < ctx.length; j++) {
								const c = ctx[j];
								if (c.contexto_pedidos_id === t.id) {
									tipos[j].selected = true;
								}
							}
						}

						res.render('admin/pages/order_state/edit', {
							estado: estado[0],
							tipos: tipos
						});
					});
				});
		}).catch(err => {
			console.log(err);
			res.redirect('/administrador/estados/' + id + '?info=err');
		});
});
// (UPDATE) Permite editar un estado de pedido
router.put('/estados/:id', (req, res) => {

	let id = req.params.id;
	let estado_rider = req.body.estado_rider;
	let estado_cliente = req.body.estado_cliente;
	let tiempo_minimo = req.body.tiempo_minimo;
	let tiempo_maximo = req.body.tiempo_maximo;
	let tiempo_demanda = req.body.tiempo_demanda;
	let descripcion = req.body.descripcion;
	let color = req.body.color;

	let estado_anterior = req.body.anterior;
	let contextos = req.body.contexto;

	db.query('UPDATE estados_pedido set estado_rider = ?, estado_cliente = ?, tiempo_minimo = ?, tiempo_maximo = ?, ' +
	'tiempo_demanda = ?, descripcion = ?, color = ? WHERE id = ?', [estado_rider, estado_cliente, tiempo_minimo, tiempo_maximo, tiempo_demanda, descripcion, color, id])
		.then(() => { return db.query('DELETE FROM contexto_estados_pedido WHERE estados_pedido_id = ? ', [id]) })
		.then(async () => {
			for (let i = 0; i < contextos.length; i++) {
				const item = contextos[i];
				if (await !insertOrderStateContext(item, id, estado_anterior) ) {
					res.redirect('/administrador/estados/'+ id +'/edit?info=err');
				}
			}
			res.redirect('/administrador/estados?info=correct');
		}).catch( err => {
			console.log(err);
			res.redirect('/administrador/estados/'+ id +'/edit?info=err');
		});
});
// (STORE) Agrega un nuevo estado de pedido
router.post('/estados', (req, res) => {

	let estado_rider = req.body.estado_rider;
	let estado_cliente = req.body.estado_cliente;
	let tiempo_minimo = req.body.tiempo_minimo;
	let tiempo_maximo = req.body.tiempo_maximo;
	let tiempo_demanda = req.body.tiempo_demanda;
	let descripcion = req.body.descripcion;
	let color = req.body.color;

	let estado_anterior = (req.body.anterior)? req.body.anterior : null;
	let contextos = req.body.contexto;

	db.query('INSERT INTO estados_pedido (estado_rider, estado_cliente, tiempo_minimo, tiempo_maximo, tiempo_demanda, descripcion, color) ' +
	'VALUES(?,?,?,?,?,?,?)', [estado_rider, estado_cliente, tiempo_minimo, tiempo_maximo, tiempo_demanda, descripcion, color])
		.then( async rows => {
			for (let i = 0; i < contextos.length; i++) {
				const item = parseInt(contextos[i]);
				if (await !insertOrderStateContext(item, rows.insertId, estado_anterior) ) {
					res.redirect('/administrador/estados/create?info=err');
				}
			}
			res.redirect('/administrador/estados?info=correct');
		}).catch( err => {
			console.log(err);
			res.redirect('/administrador/estados/create?info=err');
		});
	
});
// (DESTROY) Elimina un estado
router.get('/estados/delete/:id', (req, res) => {

	let id = req.params.id;

	db.query('DELETE FROM estados_pedido WHERE id = ?', [id])
	.then(() => {
		return db.query('DELETE FROM contexto_estados_pedido WHERE estados_pedido_id = ? ', [id]);
	}).then(() => {
		res.redirect('/administrador/estados?info=eliminated');
	}).catch(err => {
		console.log(err);
		res.redirect('/administrador/estados?info=err');
	});

});

/**
 * Inserta nuevas ordenes asociadas a los contextos
 * 
 * @param context Contexto del estado
 * @param state	Estado de pedido
 * @param last	Estado anterior
 * 
 * @returns boolean | false error | true exito
 */
function insertOrderStateContext(context, state, last) {
	db.query('INSERT INTO contexto_estados_pedido (contexto_pedidos_id, estados_pedido_id, estado_anterior) ' +
	'VALUES(?,?,?)', [context, state, last])
	.catch(err => {
		console.log(err);
		return false;
	});
	return true;
};

// Se obtienen los estados de orden
router.get('/getOrderStatus', (req, res) => {

	let ids = JSON.parse(req.query.ids);
	
	// Se obtiene los estados dependiendo de los contextos de pedido disponibles
	db.query('SELECT DISTINCT ep.* FROM estados_pedido ep INNER JOIN contexto_estados_pedido cep ' +
	'ON cep.estados_pedido_id = ep.id WHERE cep.contexto_pedidos_id IN (?)', [ids])
		.then( rows => {		
			res.send(rows);
		}).catch( err => {
			res.send({
				err: JSON.stringify(err)
			});
		});
});


/** CRUD SUCURSAL */
// (INDEX) Obtiene todas las sucrusales
router.get('/sucursales', (req, res) => {

	let info = req.query.info;

	models.sequelize
		.query('SELECT s.nombre, s.idsucursal, s.hora_inicial, s.hora_final, s.tiempo_entrega, s.numero_punto, t.nombre AS nombretienda, c.nombre AS ' +
			' categoria, CONCAT(d.direccion, \', \', d.notas) AS ' +
			' direccion FROM sucursales s JOIN tiendas t ON ' +
			' s.idtienda = t.idtienda JOIN categorias c ON ' +
			' c.idcategoria = s.idcategoria JOIN direcciones d ON ' +
			' d.iddireccion = s.iddireccion ORDER BY t.nombre ASC', 
			{type: models.sequelize.QueryTypes.SELECT})
		.then(sucursales => {
			res.render('admin/pages/offices/index', {
				sucursales,
				info
			});
		})
		.catch(err => {
			console.log(err);
			res.redirect('/administrador');
		});
});

// (CREATE) Visualiza el formulario para agregar una nueva sucursal
router.get('/sucursales/create', (req, res) => {
	res.render('admin/pages/offices/create', {
		tipoSucursal: Constants.TIPO_SUCURSAL
	});
});

// (EDIT) Visualiza el formulario para editar una sucursal
router.get('/sucursales/:id/edit', (req, res) => {
	let id = req.params.id;
	let info = req.query.info;

	// Busca la sucursal por el id
	/*Sucursales.findOne({
		where: {
			idsucursal: id
		}
	})
	.then(sucursal => {

		// Busca la direccion completa de la sucursal
		models.sequelize.
			query('SELECT d.latitude, d.longitude, d.notas, d.direccion, d.iddireccion, ' +
				  ' m.idmunicipios, de.iddepartamento, de.idpais FROM direcciones d JOIN ' +
				  ' municipios m ON d.idmunicipio = m.idmunicipios JOIN departamentos de' +
				  ' ON de.iddepartamento = m.iddepartamento WHERE iddireccion = ?',
				  { replacements: [sucursal.iddireccion], type: models.sequelize.QueryTypes.SELECT })
			.then(direccion => {
				console.log(direccion);
				res.render('admin/pages/offices/edit', {
					sucursal,
					direccion,
					info,
					tipoSucursal: Constants.TIPO_SUCURSAL
				});
			});

	})
	.catch(err => {
		console.log(`Error al buscar la sucursal ${id}`, err);	
		res.redirect('/sucursales?info=err');
	});*/

	connection.query('SELECT s.* FROM sucursales s WHERE s.idsucursal = ?', [id],
	 	(err, rows, fields) => {
			if (err) {
				console.log(err);
				res.redirect('/sucursales?info=err');
			} else {
				let iddireccion = rows[0].iddireccion;
				let sucursal = rows[0];
				connection
					.query('SELECT d.latitude, d.longitude, d.notas, d.direccion, d.iddireccion, ' +
						' m.idmunicipios, de.iddepartamento, de.idpais FROM direcciones d JOIN ' +
						' municipios m ON d.idmunicipio = m.idmunicipios JOIN ' +
						' departamentos de ON de.iddepartamento = m.iddepartamento WHERE iddireccion = ?', 
						[iddireccion], (err, rows, fields) => {
							if (err) {
								console.log(err);
								res.redirect('/sucursales?info=err');
							} else {
								res.render('admin/pages/offices/edit', {
									sucursal: sucursal,
									direccion: rows[0],
									info: info,
									tipoSucursal: Constants.TIPO_SUCURSAL
								});	
							}
						});
			}
		});
});

// (SHOW) Permite visualizar una sucursal
router.get('/sucursales/:id', (req, res) => {
});

// (UPDATE) Permite editar una sucursal
router.put('/sucursales/:id', (req, res) => {
	let idsucursal = req.params.id;
	let nombreanterior = req.body.nombreanterior;

	let nombre = req.body.nombre;
	let idtienda = req.body.tienda;
	let categoria = req.body.categoria;
	let rut = req.body.rut;
	let empresarial = req.body.empresarial;
	let retenido = (req.body.retenido == 'on' ? 'S' : 'N');
	let bajo_lluvia = (req.body.lluvia == 'on' ? 1 : 0);
	let ida_vuelta = (req.body.ida_vuelta == 'on' ? true : false);
	let valor_ida_vuelta = req.body.valor_ida_vuelta;

	let latitude = req.body.latitude;
	let longitude = req.body.longitude;

	let tipo = req.body.tipo;

	if (empresarial == 'Normal') {
		empresarial = 'N';
	} else {
		empresarial = 'Y';
	}

	let municipio = req.body.municipio;
	let direccion = req.body.direccion;
	let notas = req.body.notas;
	let hora_inicial = req.body.hora_inicial;
	let hora_final = req.body.hora_final;

	let tiempo_entrega = req.body.entrega;
	let numero_punto = req.body.numero_punto;
	let costo_envio = req.body.costo_envio;
	let comision = req.body.comision;
	let cedula = req.body.cedula;
	let nombre_dueno = req.body.nombre_dueno;
	let apellido_dueno = req.body.apellido_dueno;
	let numero_privado = req.body.numero_privado;
	let cuenta_bancaria = req.body.cuenta_bancaria;

	let listanegra = req.body.listanegra;
	let descripcion_listanegra = req.body.descripcion_listanegra;

	// Inicio de transaccion
	models.sequelize.transaction((t) => {
		// Busca la sucursal por el id
		return Sucursales.findOne({
			where: {
				idsucursal
			}
		}, {transaction: t}).then(sucursal => {
			// Actualiza direccion
			return Direcciones.update(
				{ 
					direccion: direccion,
					idmunicipio: municipio,
					notas,
					latitude,
					longitude
				},
				{
					where: {
						iddireccion: sucursal.iddireccion
					}
				}, {transaction: t}
			);
		}).then(() => {
			// Actualiza sucursal
			return Sucursales.update(
				{ 
					listanegra, descripcion_listanegra, retenido,
					nombre, rut, cuenta_bancaria, cedula, numero_privado,
					numero_punto, idtienda, idmunicipio: municipio, costo_envio,
					comision, nombre_duenio: nombre_dueno, apellido_duenio: apellido_dueno,
					idcategoria:categoria, hora_inicial, hora_final, tiempo_entrega,
					soloempresarial: empresarial, tipo, bajo_lluvia, ida_vuelta,
					valor_ida_vuelta
				},
				{
					where: {
						idsucursal
					}
				}, {transaction: t}
			);
		}).then(() => {
			// Confirmacion de actualizacion completa
			return res.redirect('/administrador/sucursales?info=update');	
		});
	}).catch(err => {
		console.log(`Error al actualizar la sucursal ${idsucursal}`, err);
		return res.redirect(`/administrador/sucursales/${idsucursal}/edit?info=err`);
	});

});

// (STORE) Agrega una nueva sucursal
router.post('/sucursales', (req, res) => {
	let nombre = req.body.nombre;
	let idtienda = req.body.tienda;
	let categoria = req.body.categoria;
	let rut = req.body.rut;
	let empresarial = req.body.empresarial;
	let contrasenia = rut;
	let retenido = (req.body.retenido == 'on' ? 'S' : 'N');
	let bajo_lluvia = (req.body.lluvia == 'on' ? 1 : 0);
	let ida_vuelta = (req.body.ida_vuelta == 'on' ? 1 : 0);
	let valor_ida_vuelta = req.body.valor_ida_vuelta;

	let latitude = req.body.latitude;
	let longitude = req.body.longitude;

	if (empresarial == 'Normal') {
		empresarial = 'N';
	} else {
		empresarial = 'Y';
	}

	let municipio = req.body.municipio;
	let direccion = req.body.direccion;
	let notas = req.body.notas;
	let hora_inicial = req.body.hora_inicial;
	let hora_final = req.body.hora_final;
	let tiempo_entrega = req.body.entrega;
	let numero_punto = req.body.numero_punto;
	let costo_envio = req.body.costo_envio;
	let comision = req.body.comision;
	let cedula = req.body.cedula;
	let nombre_dueno = req.body.nombre_dueno;
	let apellido_dueno = req.body.apellido_dueno;
	let numero_privado = req.body.numero_privado;
	let cuenta_bancaria =  req.body.cuenta_bancaria;

	let tipo = req.body.tipo;

	// Inicio de transaccion
	models.sequelize.transaction((t) => {
		// Crea una direccion para la sucursal
		return Direcciones.create({
			direccion, idmunicipio: municipio,
			notas, latitude, longitude
		}, {transaction: t}).then(direccion => {
			// Encripta la contraseña
			const hash = bcrypt.hashSync(contrasenia, 10);

			// Crea una nueva sucursal
			return Sucursales.create({
				retenido, contrasenia: hash, nombre, rut, cuenta_bancaria, cedula, numero_privado, numero_punto,
				idtienda,idmunicipio: municipio, costo_envio, comision, nombre_duenio: nombre_dueno,
				apellido_duenio: apellido_dueno, iddireccion: direccion.iddireccion, idcategoria: categoria, hora_inicial,
				hora_final, tiempo_entrega, soloempresarial: empresarial, listanegra: 'N', tipo, bajo_lluvia,
				ida_vuelta, valor_ida_vuelta
			}, {transaction: t});
		}).then(() => {
			return res.redirect('/administrador/sucursales?info=correct');
		});
	}).catch(err => {
		console.log(`Error al crear una sucursal`, err);
		return res.redirect('/administrador/sucursales/create?info=err');
	});

/*	connection
		.query('INSERT INTO direcciones (direccion, idmunicipio, notas, latitude, longitude) ' +
				' VALUES(?, ?, ?, ?, ?)', [ direccion, municipio, notas, latitude, longitude ], 
		(err, rows, fields) => {
			if (err) {
				console.log(err);
				return res.redirect('/administrador/sucursales?info=err');
			} else {
				let hash = bcrypt.hashSync(contrasenia, 10);
				connection.query('INSERT INTO sucursales (retenido, contrasenia, ' +
					' nombre, rut, cuenta_bancaria, cedula, numero_privado, numero_punto, ' +
					' idtienda, idmunicipio, costo_envio, comision, nombre_duenio, ' +
					' apellido_duenio, iddireccion, idcategoria, hora_inicial, ' +
					' hora_final, tiempo_entrega, soloempresarial, listanegra, tipo, bajo_lluvia) ' +
					' VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [ retenido, hash, 
						nombre, rut, cuenta_bancaria, cedula, numero_privado, numero_punto,
						idtienda, municipio, costo_envio, comision, nombre_dueno,
						apellido_dueno, rows.insertId, categoria, hora_inicial,
						hora_final, tiempo_entrega, empresarial, 'N', tipo, bajoLluvia], (err, rows, fields) => {
						if (err) {
							console.log(err);
							return res.redirect('/administrador/sucursales/crate?info=err');
						} else {
							return res.redirect('/administrador/sucursales?info=correct');
						}
					});
			}
		});
*/
});

// (DESTROY) Elimina una sucursal
router.get('/sucursales/delete/:id', (req, res) => {
	let id = req.params.id;

	Sucursales.destroy({
		where: { 
			idsucursal: id
		}
	}).then(() => {
		return res.redirect('/administrador/sucursales?info=eliminated');
	}).catch(err => {
		console.log(`Error al eliminar la sucursal ${id}`, err);
		return res.redirect('/administrador/sucursales?info=err');
	});
});


/** CRUD PRODUCTOS INIT**/
// (INDEX) Obtiene todos los producto de una sucursal
router.get('/productos/:idsucursal', (req, res) => {
	let info = req.query.info;
	let id = req.params.idsucursal;

	connection.query('SELECT * FROM productos WHERE idsucursal = ?', [id], (err, rows, fields) => {
		if (err) {
			console.log(err);
			res.redirect('/administrador/sucursales');
		} else {
			res.render('admin/pages/products/index', {
				productos: rows,
				idsucursal: id,
				info: info
			});
		}
	});
	
});
// (CREATE) Visualiza el formulario para agregar un producto
router.get('/productos/:idsucursal/create', (req, res) => {
	res.render('admin/pages/products/create', {
		idsucursal: req.params.idsucursal,
	});
});
// (SHOW) Permite visualizar un producto
router.get('/productos/:id/view', (req, res) => {
});
// (EDIT) Permite editar un producto
router.get('/productos/:id/edit', (req, res) => {
	let id = req.params.id;
	connection.query('SELECT * FROM productos WHERE idproducto = ?', [id], (err, rows, fields) => {
		if (err) {
			console.log(err);
			return res.redirect('/administrador/productos/' + id + '?info=err');
		} else {
			res.render('admin/pages/products/edit', {
				producto: rows[0]
			});
		}
	});
	
});
// (UPDATE) Permite editar un producto
router.put('/productos/:id', (req, res) => {
});
// (STORE) Agrega un nuevo producto
router.post('/productos', (req, res) => {

	let nombre = req.body.nombre;
	let descripcion_corta = req.body.descripcion_corta;
	let descripcion_larga = req.body.descripcion_larga;
	let precio = req.body.precio;
	let idsucursal = req.body.idsucursal;
	let tamanio_maletin = req.body.maletin;
	let categoria = req.body.categoria;

	let foto = req.files.imagen;
	let direccionImg;
	let name;

	let nombresucursal = req.body.nombresucursal;
	nombresucursal = nombresucursal.replace(" ", "%20");

	direccionImg = 'src/app/public/img/producto/default.png';
	name = 'default.png';
	
	let info = JSON.parse(req.body.info);

	let idproducto;

	connection
		.query('INSERT INTO productos (nombre, descripcion_corta, precio, imagen, ' +
			' idsucursal, descripcion_larga, tamanio_maletin, idcategoriaproducto) ' +
			' VALUES(?,?,?,?,?,?,?,?)', [nombre, descripcion_corta, precio, name, idsucursal, 
				descripcion_larga, tamanio_maletin, categoria], (err, rows, fields) => {
			if (err) {
				console.log(err);
		 	} else {
				idproducto = rows.insertId;
				if (foto.name != '') {
					let extension = foto.name.split('.').pop();
					direccionImg = 'src/app/public/img/producto/' + idproducto +'.' + extension;
					fs.renameSync(foto.path, direccionImg);
					name = idproducto + '.' + extension;
					connection.query('UPDATE productos SET imagen = ? WHERE idproducto = ?', [name, idproducto], (err, rows, fields) => {
						if (err) {
							console.log(err);
						}
					});
				}
				for (let i = 0; i < info.cantidadFactores; i++) {
					if (req.body["nombrefactor" + i].trim() != '') {
						agregarFactor(i, req, info, idproducto, (err) => {
							if (err) {
								console.log(err);
							}
						});
					}
				}

				for (let i = 0; i < info.cantidadElementos; i++) {
					if (req.body["elementonombre" + i].trim() != '' && req.body["elementoprecio" + i].trim() != '') {
						connection.query('INSERT INTO elementos (nombre, precio, idproducto) VALUES(?,?,?)',
							[req.body["elementonombre" + i], req.body["elementoprecio" + i], idproducto], (err, rows, fields) => {
								if(err)
									console.log(err);
								});
					}
				}

				for (let i = 0; i < info.cantidadAdiciones; i++) {
					if (req.body["adicionnombre" + i].trim() != '' && req.body["adicionprecio" + i].trim() != '') {
						connection.query('INSERT INTO adiciones (nombre, precio, idproducto) VALUES(?,?,?)',
							[req.body["adicionnombre" + i], req.body["adicionprecio" + i], idproducto], (err, rows, fields) => {
								if(err)
									console.log(err);
								});
					}
				}

				res.redirect('/administrador/productos/' + idsucursal + '?info=correct');
			}			
		});
});
// (DESTROY) Elimina una sucursal
router.get('/productos/delete/:id', (req, res) => {
	let id = req.params.id
	let id_sucursal = req.query.sucursal;
	connection.query('DELETE FROM productos WHERE idproducto = ?', [id], (err, rows, fields) => {
		if (err) {
			console.log(err);
			return res.redirect('/administrador/productos/' + id_sucursal + '?info=err');
		} else {
			return res.redirect('/administrador/productos/' + id_sucursal + '?info=eliminated');
		}
	})
});


/** CRUD ADMINISTRADORES */

// (INDEX) Obtiene todos los administradores
router.get('/administradores', (req, res) => {

	let info = req.query.info;

	connection.query('SELECT * FROM administradores', (err, rows, fields) =>{
		if (err) {
			console.log(err);
			res.redirect('/administrador');
		} else {
			for(let i = 0; i < rows.length; i++) {
				rows[i].fecha = new Date(rows[i].fecha).toLocaleString();
			}
			res.render('admin/pages/admins/index', {
				administradores: rows,
				info: info
			});
		}
	});
});
// (CREATE) Visualiza el formulario para agregar un administrador
router.get('/administradores/create', (req, res) => {
	res.render('admin/pages/admins/create');
});
// (SHOW) Permite visualizar un administrador
router.get('/administradores/:id', (req, res) => {
});
// (EDIT) Permite editar un administrador
router.get('/administradores/:id/edit', (req, res) => {
	let info = req.query.info;
	let usuario = req.params.id;

	connection.query('SELECT * FROM administradores WHERE cedula = ?', 
		[usuario], (err, rows, fields) => {
		if (err) {
			console.log(err);
			return res.redirect('/administrador/administradores?info=err');
		} else {
			let admin = rows[0];
			connection
				.query('SELECT * FROM direcciones d JOIN municipios m ' +
						' ON d.idmunicipio = m.idmunicipios JOIN ' +
						' departamentos dep ON dep.idDepartamento = m.idDepartamento JOIN ' +
						' paises p ON p.idPais = dep.idPais WHERE iddireccion = ?',
						[admin.iddireccion], (err, rows, fields) => {
							if (err) {
								console.log(err);
								return res.redirect('/administrador/administradores?info=err');
							} else {
								//console.log(rows[0]);
								res.render('admin/pages/admins/edit', {
									admin: admin,
									info: info,
									direccion: rows[0]
								});
							}
				});
		}
	});
});
// (UPDATE) Permite actulizar un administrador
router.put('/administradores/:id', (req, res) => {

	let cedula = req.params.id;
	let nombre = req.body.nombre;
	let apellido = req.body.apellido;
	let correo = req.body.correo;
	let pais = req.body.pais;
	let departamento = req.body.departamento;
	let municipio = req.body.municipio;
	let direccion = req.body.direccion;
	let notas = req.body.notas;
	let foto_perfil = req.body.foto_perfil;
	let direccion_anterior = req.body.direccion_anterior;

	let foto = req.files.foto;
	let anexos = req.body.anexos;
	let direccionImg;

	let name;

	direccionImg = 'src/app/public/img/admin/'+foto_perfil ;
	name = foto_perfil;

	if(foto.name != ''){
		let extension = foto.name.split('.').pop();
		let direccionImg = 'src/app/public/img/admin/' + cedula + '.' + extension;
		fs.renameSync(foto.path, direccionImg);

		name = cedula + '.' + extension;
	}
	
	db.query('SELECT * FROM administradores WHERE cedula = ?' , [cedula])
	.then(admin => {
		return db.query('UPDATE direcciones SET direccion = ?, idmunicipio = ?, notas = ? WHERE iddireccion = ?',
		[direccion, municipio, notas, admin[0].iddireccion]);
	})
	.then(() => {
		return db.query('UPDATE administradores SET nombre = ?, apellido = ?,' +
				' correo = ?, foto_perfil = ?, anexos = ? WHERE cedula = ?', 
				[nombre, apellido, correo, name, anexos, cedula]);
	})
	.then(() => {
		res.redirect('/administrador/administradores?info=update');
	})
	.catch(err => {
		console.log(err);
		res.redirect('/administrador/administradores/'+ cedula +'/edit?info=err');
	});
	
	/*connection.beginTransaction( (err) => {
		if (err) {
			console.log(err);
			return res.redirect('/administrador/administradores/'+ cedula +'/edit?info=err');
		} else{
			connection.query('INSERT INTO direcciones (direccion, idmunicipio, notas) VALUES(?, ?, ?)',
				[direccion, municipio, notas], (err, rows, fields) => {
					if (err) {
						connection.rollback( () => {
							console.log(err);
							return res.redirect('/administrador/administradores/'+ cedula +'/edit?info=err');
						});
					} else {
						connection
							.query('UPDATE administradores ' +
									' SET nombre = ?, apellido = ?,' +
									' correo = ?, foto_perfil = ?,' +
									' anexos = ?, iddireccion = ? WHERE cedula = ?', [nombre, apellido, correo,
									name, anexos, rows.insertId, cedula], (err, rows, fields) => {
								if (err) {
									connection.rollback(() => {
										console.log(err);
										return res.redirect('/administrador/administradores/'+ cedula +'/edit?info=err');
									});
								} else {
									connection
										.query('DELETE FROM direcciones WHERE iddireccion = ?', [direccion_anterior], 
										(err, rows, fields) => {
											if (err) {
												connection.rollback( () => {
													console.log(err);
													return res.redirect('/administrador/administradores/'+ cedula +'/edit?info=err');
												});
											} else {
												connection.commit( (err) => {
													if (err) {
														return res.redirect('/administrador/administradores/'+ cedula +'/edit?info=err');
													} else {
														return res.redirect('/administrador/administradores?info=update');
													}
												});
											}
									});
								}
							});
					}
				});
		}
	});*/

});
// (STORE) Agrega un nuevo administrador.
router.post('/administradores', (req, res) => {
	let cedula = req.body.cedula;
	let nombre = req.body.nombre;
	let apellido = req.body.apellido;
	let correo = req.body.correo;
	let contrasenia = cedula;
	let pais = req.body.pais;
	let departamento = req.body.departamento;
	let municipio = req.body.municipio;
	let genero = req.body.genero;
	let direccion = req.body.direccion;
	let notas = req.body.notas;

	let foto = req.files.foto;
	let anexos = req.body.anexos;
	let direccionImg;

	let name;

	// Verifica el genero del administrador para seleccionar una imagen de perfil por defecto
	if(genero == 'Masculino'){
		direccionImg = 'src/app/public/img/admin/defaultmas.jpg';
		name = 'defaultmas.jpg';
	}else{
		direccionImg = 'src/app/public/img/admin/defaultfem.jpg';
		name = 'defaultfem.jpg';
	}

	if (foto.name != '') {
		let extension = foto.name.split('.').pop();
		let direccionImg = 'src/app/public/img/admin/' + cedula + '.' + extension;
		fs.renameSync(foto.path, direccionImg);
		name = cedula + '.' + extension;
	}

	// Inserta una nueva direción del administrador
	connection.query('INSERT INTO direcciones (direccion, idmunicipio, notas) VALUES(?, ?, ?)', 
		[direccion, municipio, notas], (err, rows, fields) => {
			if (err) {
				console.log(err);
				return res.redirect('/administrador/administradores/create?info=err');
			} else {
				let hash = bcrypt.hashSync(contrasenia, 10);
				connection
					.query('INSERT INTO administradores ' +
						'( cedula, nombre, apellido, correo,' +
						' contrasenia, foto_perfil, anexos, iddireccion, genero) ' +
						' VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)', 
						[cedula, nombre, apellido, correo, hash, name, anexos, rows.insertId, genero],
						(err, rows, fields) => {
							if (err) {
								console.log(err);
								return res.redirect('/administrador/administradores/create?info=err');
							} else {
								return res.redirect('/administrador/administradores?info=correct');
							}
						});
			}
		});
});
// (DESTROY) Elimina un administrador
router.get('/administradores/delete/:id', (req, res) => {
	let cedula = req.params.id;

	connection.query('DELETE FROM administradores WHERE cedula = ?', [cedula],
		(err, rows, fields) => {
			if (err) {
				console.log(err);
				return res.redirect('/administrador/administradores?info=err');
			} else {
				return res.redirect('/administrador/administradores?info=eliminated');
			}
		});
});

// (CREATE)
router.get('/riders/create', (req, res) => {
	res.render('admin/pages/riders/create', {
		maletines: Constants.TIPO_MALETIN
	});
});

router.get('/riders/disable' ,(req, res) => {
	res.contentType('json');
	db.query('UPDATE riders SET activo = ?', ['N'])
	.then(()=> {
		res.send({
			msg: 'Riders desactivados' 
		});
	})
	.catch((err)=> {
		console.log(err);
		res.send({
			msg: 'Error al desactivar los riders' 
		});
	});
});

// (EDIT)
router.get('/riders/:id/edit', (req, res) => {
	
	// Se obtiene el id(cedula) del rider 
	let usuario = req.params.id;
	let info = req.query.info;	

	// Se obtiene el rider por el id
	db.query('SELECT * FROM riders WHERE cedula = ?', [usuario])
	.then(rows => {
		const rider = rows[0];
		return db.query('SELECT * FROM direcciones d' +
			' JOIN municipios m on d.idmunicipio = m.idmunicipios' +
			' JOIN departamentos dep on dep.idDepartamento = m.idDepartamento' + 
			' JOIN paises p on p.idPais = dep.idPais' + 
			' WHERE iddireccion = ?', [rider.iddireccion])
			.then(direction => {
				res.render('admin/pages/riders/edit', {
					rider: rider,
					info: info,
					direccion: direction[0],
					maletines: Constants.TIPO_MALETIN
				});
			});
	})
	.catch(err => {
		console.log(err);
		res.redirect('/administrador/riders/' + usuario + '/edit?info=err');
	});

	/*connection.query('SELECT * FROM riders WHERE cedula = ?', [usuario], (err, rows, fields) => {
		if (err) {
			return res.redirect('/administrador/riders/'+ usuario +'/edit?info=err');
		} else {
			let rider = rows[0];
			connection.query('SELECT * FROM direcciones d' +
							' JOIN municipios m on d.idmunicipio = m.idmunicipios' +
							' JOIN departamentos dep on dep.idDepartamento = m.idDepartamento' + 
							' JOIN paises p on p.idPais = dep.idPais' + 
							' WHERE iddireccion = ?', [rider.iddireccion],
				(err, rows, fields) => {
					if (err) {
						return res.redirect('/administrador/riders/'+ cedula +'/edit?info=err');
					} else {
						res.render('admin/pages/riders/edit', {
							rider: rider,
							info: info,
							direccion: rows[0],
							maletines: Constants.TIPO_MALETIN
						});
					}
				});
		}
	});*/
});
// (SHOW)
router.get('/riders/:id', (req, res) => {
});
// (UPDATE) falta validar si existe el rider
router.put('/riders/:id', (req, res) => {
	let cedula = req.params.id;

	let foto_perfil = req.body.foto_perfil;
	let direccion_anterior = req.body.direccion_anterior;
	let nombre = req.body.nombre;
	let apellido = req.body.apellido;
	let correo = req.body.correo;
	let telefono = req.body.telefono;
	let vehiculo = req.body.vehiculo;
	let maletin = req.body.maletin;
	console.log('Tamnño maletin seleccionado', maletin);
	let seguro = req.body.seguro;
	let foto = req.files.foto;
	let anexos = req.body.anexos;
	let listanegra = req.body.listanegra;
	let limite = req.body.limite;
	let pais = req.body.pais;
	let departamento = req.body.departamento;
	let municipio = req.body.municipio;
	let direccion = req.body.direccion;
	let notas = req.body.notas;
	let genero = req.body.genero;
	let fechaNacimiento = req.body.nacimiento;
	
	let direccionImg;

	let name;

	direccionImg = 'src/app/public/img/rider/'+foto_perfil ;
	name = foto_perfil;

	if (foto.name != '') {
		let extension = foto.name.split('.').pop();
		let direccionImg = 'src/app/public/img/rider/' + cedula + '.' + extension;
		fs.renameSync(foto.path, direccionImg);

		name = cedula + '.' + extension;
	}
	
	db.query('SELECT * FROM riders WHERE cedula = ?' , [cedula])
	.then(rider => {
		return db.query('UPDATE direcciones SET direccion = ?, idmunicipio = ?, notas = ? WHERE iddireccion = ?',
		[direccion, municipio, notas, rider[0].iddireccion]);
	})
	.then(() => {
		// Sentencia SQL
		let query;
		// Datos a actualizar
		let data;

		// Verifica si se actualial la imagen de perfil
		if (name) {
			query = 'UPDATE riders SET nombre = ?, apellido = ?, correo = ?, telefono = ?, tipo_vehiculo = ?, limitePedidos = ?, ' + 
				'tamanio_maletin = ?, seguro_traigo = ?, foto_perfil = ?, anexos = ?, listanegra = ? WHERE cedula = ?';
			data = [nombre, apellido, correo, telefono, vehiculo, limite, maletin, seguro, name, anexos, listanegra, cedula];
		} else {
			query = 'UPDATE riders SET nombre = ?, apellido = ?, correo = ?, telefono = ?, tipo_vehiculo = ?, limitePedidos = ?, ' + 
				'tamanio_maletin = ?, seguro_traigo = ?, anexos = ?, listanegra = ? WHERE cedula = ?';
			data = [nombre, apellido, correo, telefono, vehiculo, limite, maletin, seguro, anexos, listanegra, cedula];
		}

		return db.query(query, data);
	})
	.then(() => {
		res.redirect('/administrador/riders?info=update');
	})
	.catch(err => {
		console.log(err);
		res.redirect('/administrador/riders/'+ cedula +'/edit?info=err');
	});

	/*
	connection.beginTransaction((err) => {
		if( !name ){
			connection.query('SELECT foto_perfil FROM riders WHERE cedula = ?', [cedula], (err, rows, fields) => {
				if (err) {
					console.log(err);
					return res.redirect('/administrador/riders/'+ cedula +'/edit?info=err');
				} else {
					name = rows[0];
				}
			});
		}
		if (err) {
			console.log("Error editar", err);
			return res.redirect('/administrador/riders/'+ cedula +'/edit?info=err');
		} else {
			connection.query('INSERT INTO direcciones ' +
							' (direccion, idmunicipio, notas) VALUES(?, ?, ?)', [direccion, municipio, notas], 
				(err, rows, fields) => {
					if (err) {
						connection.rollback(() => {
							return res.redirect('/administrador/riders/'+ cedula +'/edit?info=err');
						});
					} else {
						connection.query('UPDATE riders' +
										' SET iddireccion = ?,' +
										' nombre = ?,' +
										' apellido = ?, ' +
										' correo = ?,' +
										' telefono = ?,' +
										' tipo_vehiculo = ?,' +
										' tamanio_maletin = ?,' +
										' seguro_traigo = ?,' +
										' foto_perfil = ?,' +
										' anexos = ?,' +
										' listanegra = ?' +
										' WHERE cedula = ?',
								[rows.insertId, nombre, apellido, correo, 
									telefono, vehiculo, maletin, seguro, name,
									anexos, listanegra, cedula
								], (err, rows, fields) => {
								if (err) {
									connection.rollback(() => {
										return res.redirect('/administrador/riders/'+ cedula +'/edit?info=err');
									});
								} else {
									connection.query('DELETE FROM direcciones WHERE iddireccion = ?', [direccion_anterior], (err, rows, fields) => {
										if(err){
											connection.rollback(() => {
												return res.redirect('/administrador/riders/'+ cedula +'/edit?info=err');
											});
										} else {
											connection.commit((err) => {
												if (err) {
													return res.redirect('/administrador/riders/'+ cedula +'/edit?info=err');
												} else {
													return res.redirect('/administrador/riders');
												}
											});
										}
									});
								}
							});
					}
				});
		}
	});*/
});
// (STORE)
router.post('/riders', (req, res) => {
	let cedula = req.body.cedula;
	let nombre = req.body.nombre;
	let apellido = req.body.apellido;
	let correo = req.body.correo;
	let telefono = req.body.telefono;
	let contrasenia = cedula;
	let vehiculo = req.body.vehiculo;
	let seguro = req.body.seguro;
	let nacimiento = req.body.nacimiento;
	let maletin = req.body.maletin;
	let foto = req.files.foto;
	let anexos = req.body.anexos;
	let genero = req.body.genero;

	let pais = req.body.pais;
	let departamento = req.body.departamento;
	let municipio = req.body.municipio;
	let direccion = req.body.direccion;
	let notas = req.body.notas;

	var limitePedidos = req.body.limite;

	let direccionImg;

	let name;

	if(genero == 'Masculino'){
		direccionImg = 'src/app/public/img/rider/defaultmas.jpg';
		name = 'defaultmas.jpg';
	}else{
		direccionImg = 'src/app/public/img/rider/defaultfem.jpg';
		name = 'defaultfem.jpg';
	}

	if(foto.name != ''){
		let extension = foto.name.split('.').pop();
		let direccionImg = 'src/app/public/img/rider/' + cedula + '.' + extension;
		fs.renameSync(foto.path, direccionImg);
		name = cedula + '.' + extension;
	}
	//INSERT INTO costos (costo_envio_deseo, comision_cliente_deseo, costo_envio_cliente, comision_rider_cliente, costo_envio_mandado, costo_paradas_mandado, comision_rider_mandado, comision_rider_empresarial, idmunicipio) VALUES(?,?,?,?,?,?,?,?,?)',
	db.query('INSERT INTO direcciones (direccion, idmunicipio, notas) VALUES(?, ?, ?)', [direccion, municipio, notas])
	.then(direction => {
		let hash = bcrypt.hashSync(contrasenia, 10);
		return db.query('INSERT INTO riders (cedula, nombre, apellido, telefono, correo, tipo_vehiculo, seguro_traigo, ' +
		'fecha_nacimiento, tamanio_maletin, foto_perfil, genero, iddireccion, contrasenia, anexos, listanegra, ' +
		'activo, latitude, longitude, limitePedidos) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ', [cedula, nombre, apellido, telefono, correo, vehiculo, seguro, 
			nacimiento, maletin, name, genero, direction.insertId, hash, anexos, 'N', 'N', 0, 0, limitePedidos]);
	})
	.then(() => {
		res.redirect('/administrador/riders?info=correct');
	})
	.catch(err => {
		console.log(err);
		res.redirect('/administrador/riders?info=err');
	});

	/*connection.query('INSERT INTO direcciones (direccion, idmunicipio, notas) VALUES(?, ?, ?)', [direccion, municipio, notas], 
		(err, rows, fields) => {
			if(err){
				console.log(err);
				return res.redirect('/administrador/riders?info=err');
			}else{
				
				connection.query('INSERT INTO riders VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
					[cedula, nombre, apellido, telefono, correo, vehiculo, seguro, nacimiento, maletin, name, genero, rows.insertId, contrasenia, anexos, 'N', 'N', 0, 0, null, null],
						(err, rows, fields) => {
							if(err){
								console.log(err);
								return res.redirect('/administrador/riders?info=err');
							}else{
								return res.redirect('/administrador/riders?info=correct');
							}
						});
			}
		});*/
});
// (DESTROY)
router.get('/riders/delete/:id', (req, res) => {

	let cedula = req.params.id;

	//console.log("Entra delete", cedula);

	connection.query('DELETE FROM riders WHERE cedula = ?', [cedula], (err, rows, fields) => {
		if(err){
			console.log(err);
			return res.redirect('/administrador/riders?info=err');
		}else{
			return res.redirect('/administrador/riders?info=eliminated');
		}
	});
});
// (INDEX) 
router.get('/riders', (req, res) => {

	let info = req.query.info;

	Riders.findAll()
	.then(riders => {
		let total = 0;
		let requests = 0;
		riders.forEach((rider, i) => {
			requests ++;
			obtenerCuentaRider(rider.cedula, (cuenta, index) => {
				rider.cuenta = cuenta;
				total += cuenta;
				requests--;
				if (requests == 0) {
					res.render('admin/pages/riders/index', {riders, info, total});
				}
			}, i);
		});
	})
	.catch(err => {
		console.log('Error al obtener todos los riders', err);
		res.redirect('/administrador');
	});

/*
	connection.query('SELECT * FROM riders', (err, rows, fields) =>{
		if (err) {
			console.log(err);
			res.redirect('/administrador');
		} else {
			let total = 0;
			let requests = 0;
			rows.forEach(async (row, i) => {
				requests++;
				await obtenerCuentaRider(row.cedula, (cuenta, index) => {
					rows[index].cuenta = cuenta;
					total += cuenta;
					requests--;
					if (requests == 0) {
						res.render('admin/pages/riders/index', {riders: rows, info: info, total: total});
					}
				}, i);
			});
		}
	});
	*/
});

/**
 * Obtiene los municipos por departamento
 */
router.get('/get-city-by-department/:department', (req, res) => {
	const department_id = req.params.department;

	Municipios.findAll({
		where: {
			idDepartamento: department_id
		}
	}).then(cities => {
		res.status(200).json(cities);
	})
	.catch(err => {
		console.log('Error al obtener ciudades', err);
		res.status(400).json('Error al obtener ciudades');
	});
});

/**
 * Obtiene los departamentos por pais
 */
router.get('/get-departments-by-country/:country', (req, res) => {
	const country_id = req.params.country;

	Departamentos.findAll({
		where: {
			idPais: country_id
		}
	}).then(departments => {
		res.status(200).json(departments);
	})
	.catch(err => {
		console.log('Error al obtener departamentos', err);
		res.status(400).json('Error al obtener departamentos');
	});
});

/**
 * Obtiene todos los paises
 */
router.get('/get-countries', (req, res) => {

	Paises.findAll()
	.then(countries => {
		res.status(200).json(countries);
	})
	.catch(err => {
		console.log('Error al obtener paises', err);
		res.status(400).json('Error al obtener paises');
	});
});

/*
function getRiderAccount(cedula, next, index){

	let orderBusiness, orderDesires, orderEntrust;

	db.query('SELECT pd.*, SUM(f.valor) AS totalfacturas FROM pedidos_deseos pd JOIN facturas f ON f.idpedido_deseos = pd.idpedido_deseo WHERE pd.idrider = ? AND (pd.estado = ? OR pd.estado = ?) GROUP BY pd.idpedido_deseo')
	.then(rows => {
		console.log(rows);
	})
	.catch();


	var deseos, pedidos, mandados, empresariales;
	connection.query('select pd.*, sum(f.valor) as totalfacturas from pedidos_deseos pd join facturas f on f.idpedido_deseos = pd.idpedido_deseo where pd.idrider = ? and (pd.estado = ? or pd.estado = ?) group by pd.idpedido_deseo'
		,[cedula, 'Entregado', 'Error'], (err, rows, fields) => {
			if(err){
				console.log(err);
				return res.send(err);
			}
			deseos = rows;
			for(var i = 0; i < deseos.length; i++){
				deseos[i].type = 'deseo';
			}
			connection.query('select pe.*, sum(p.costo) as totalparadas from pedidos_encomiendas pe left join paradas p on p.idpencomienda = pe.idpedido_encomienda where pe.idrider = ? and (pe.estado = ? or pe.estado = ?) and comision_entregada = ? group by pe.idpedido_encomienda'
				,[cedula, 'Entregado', 'Error', 'N'], (err, rows, fields) => {
					if(err){
						console.log(err);
						return res.send(err);
					}
					mandados = rows;
					for(var i = 0; i < mandados.length; i++){
						mandados[i].type = 'mandado';
					}
					connection.query('select * from pedidos_clientes pc where pc.idrider = ? and (pc.estado = ? or pc.estado = ?)'
						,[cedula, 'Entregado', 'Error'], (err, rows, fields) => {
							if(err){
								console.log(err);
								return res.send(err);
							}
							pedidos = rows;
							for(var i = 0; i < pedidos.length; i++){
								pedidos[i].type = 'tienda';
							}
							connection.query('select * from pedidos_empresariales pe where pe.idrider = ? and (pe.estado = ? or pe.estado = ?)'
								,[cedula, 'Entregado', 'Error'], (err, rows, fieds) => {
									if(err){
										console.log(err);
										return res.send(err);
									}
									empresariales = rows;
									for(var i = 0; i < empresariales.length; i++){
										empresariales[i].type = 'empresariales';
									}
									var todos = deseos.concat(pedidos, mandados, empresariales);
									obtenerValorEstadoActual(todos, (cuenta) => {
										next(cuenta, index);
									});
								});
						});
				});
		});
}*/

/**
 * Obtener el valor acumulado sin pagar de un rider
 * 
 * @param {*} riderId 
 */
function getAccumulatedValueRider (riderId) {
	
	// Obtener solicitud anterior
	db.query('SELECT * FROM solicitudes WHERE idrider = ? order by fecha desc limit 1', [riderId])
	.then(async request => {

		if (request.length > 0) {
			
			const coditions = [riderId, 'Entregado', 'Error', new Date(), request[0].fecha];

			// Total pedidos deseo
			let totalDesires = db.query('SELECT SUM(t.valor) AS total, t.comision FROM (SELECT f.valor AS valor, pd.comision_cliente as comision FROM pedidos_deseos pd JOIN facturas f ON f.idpedido_deseos = pd.idpedido_deseo WHERE pd.idrider = ? AND (pd.estado = ? OR pd.estado = ?) AND pd.fecha <= ? AND pd.fecha > ? GROUP BY pd.idpedido_deseo) t',
			coditions);

			// Total pedidos encomienda
			let totalEntrust = db.query('SELECT SUM(t.total) AS total, t.comision, SUM(t.costo_envio) AS costo_envio FROM (SELECT p.costo AS total, pe.costo_envio AS costo_envio, pe.comision_rider AS comision FROM pedidos_encomiendas pe JOIN paradas p ON p.idpencomienda = pe.idpedido_encomienda WHERE pe.idrider = ? AND (pe.estado = ? OR pe.estado = ?) AND pe.fecha <= ? AND pe.fecha > ? GROUP BY pe.idpedido_encomienda) t',
			coditions);

			// pedidos empresariales no retenidos
			let totalBusiness = db.query('SELECT (SUM(pe.costo_envio) + SUM(pe.valor_ida_vuelta)) AS total, pe.comision_rider FROM pedidos_empresariales pe WHERE pe.idrider = ? AND (pe.estado = ? OR pe.estado = ?) AND pe.fecha <= ? AND pe.fecha > ? AND pe.retenido = ?',
			[riderId, 'Entregado', 'Error', new Date(), request[0].fecha, 'N']);

			// pedidos empresariales retenidos
			let totalBusinessDetained = db.query('SELECT (SUM(pe.costo_envio) + SUM(pe.valor_ida_vuelta)) AS total, pe.comision_rider FROM pedidos_empresariales pe WHERE pe.idrider = ? AND (pe.estado = ? OR pe.estado = ?) AND pe.fecha <= ? AND pe.fecha > ? AND pe.retenido = ?',
			[riderId, 'Entregado', 'Error', new Date(), request[0].fecha, 'S']);

			// Operaciones de tabla
			let op = models.Sequelize.Op;

			// Obtiene los id's no nulos de ajustes de los pedidos empresariales
			let listBusinessOrdersAdjust = await PedidosEmpresariales.findAll({
				attributes: ['ajustes_id'],
				where: {
					idrider: riderId,
					ajustes_id: {
						[op.ne]: null
					},
					[op.or]: [
						{estado: 'Entregado'},
						{estado: 'Error'}
					],
					fecha: {
						[op.between]: [request[0].fecha, new Date()]
					}
				}
			})
			.then(pe => {
				// Obtiene una lista de identificadores de ajustes
				if (pe.length > 0 ) {
					const ids = pe.map((ajustes) => { return ajustes.ajustes_id; });
					// Pedidos empresariales ajustes
					return models.sequelize.query('SELECT IFNULL(SUM(a.valor), 0) valor FROM ajustes a WHERE a.id IN(?)',
					{ replacements: [ids], type: models.sequelize.QueryTypes.SELECT });
				} else {
					return false;
				}
			});


			// Procesar todos los totales
			await Promise.all([totalDesires, totalEntrust, totalBusiness, totalBusinessDetained, listBusinessOrdersAdjust])
			.then(orders => {

				let total = 0;
				const desires = orders[0][0];
				const entrust = orders[1][0];
				const business = orders[2][0];
				const businessDetained = orders[3][0];
				const businessAdjusts = (orders[4])? orders[4][0].valor : 0;

				// Sumar los pedidos deseo
				if (desires.total) {
					total -= desires.total * desires.comision / 100;
				}

				// Sumar los pedidos encomienda
				if (entrust.total) {
					total -= (entrust.total + entrust.costo_envio) * entrust.comision / 100;
				}

				// Sumar los pedidos empresariales
				if (business.total) {
					total -= business.total * business.comision_rider / 100;
				}

				// Sumar los pedidos empresariales retenidos
				if (businessDetained.total) {
					total += (businessDetained.total) * (100 - businessDetained.comision_rider) / 100;
				}

				// Suma los ajustes
				total += businessAdjusts;

				return total;
			})
			.catch(err => {
				console.log(err);
			});

		} else {
			return 0;
		}
	})
	.catch(err => {
		console.log(err);
	});
}


/**
 * Genera solicitudes no pagadas de un rider
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns id sulicitud | solicitud actual
 * get-all-payment-requests
 */
router.get('/get-payment-request/:id', (req, res) => {
	const riderId = req.params.id;

	db.query('SELECT idrider FROM solicitudes WHERE idrider = ? AND pagada = ?', [riderId ,'N'])
	.then(request => {
		if (request.length > 0) {
			console.log('No se puede generar más solicitudes de pago si ya tiene pendientes.')
			return res.redirect(`/administrador/riders/${riderId}/request?info=request`);	
		} else {
			// Se verifica si tiene cuentas pendientes
			if (getAccumulatedValueRider(riderId) != 0) {
				// Se crean nuevas solicitudes
				db.query('INSERT INTO solicitudes (idrider, fecha, pagada) VALUES(?,?,?)', [riderId, new Date(), 'N'])
				.then(() => {
					res.redirect(`/administrador/riders/${riderId}/request?info=correct`);
				})
				.catch(err => {
					console.log('Error al insertar nuevas solicitudes', err);
					res.redirect(`/administrador/riders/${riderId}/request?info=err`);
				});
			}
		}
	})
	.catch(err => {
		console.log('Error al insertar nuevas solicitudes', err);
		res.redirect(`/administrador/riders/${riderId}/request?info=err`);
	});
});


/**
 * Genera solicitudes no pagadas de todos los riders 
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns id sulicitud | solicitud actual
 * get-all-payment-requests
 */
router.get('/get-all-payment-requests', (req, res) => {

	// Obtenemos las solicitudes no pagadas
	db.query('SELECT idrider FROM solicitudes WHERE pagada = ?', ['N'])
	.then(request => {
		// Se obtiene los riders que pertenecen a esas solicitudes
		let ridersId = JSON.parse(JSON.stringify(
			request.map((obj) => {
				return obj.idrider;
		  	})
		  ));

		if (ridersId.length > 0) {
			// Se obtienen los riders que no tienen solicitudes pendientes
			return db.query('SELECT cedula FROM riders WHERE listanegra = ? AND cedula NOT IN(?)', ['N', ridersId]);
		} else {
			// Se obtienen los riders que no esten en lista negra
			return db.query('SELECT cedula FROM riders WHERE listanegra = ?', ['N']);	
		}
	})
	.then(async riders => {
		for (let i = 0; i < riders.length; i++) {
			const riderId = riders[i].cedula;
			
			// Se verifica si tiene cuentas pendientes
			if (getAccumulatedValueRider(riderId) > 0) {
				// Se crean nuevas solicitudes
				await db.query('INSERT INTO solicitudes (idrider, fecha, pagada) VALUES(?,?,?)', [riderId, new Date(), 'N'])
				.catch(err => {
					console.log('Error al insertar nuevas solicitudes', err);
					res.redirect('/administrador/solicitudes?info=err');
				});
			}
		}
		// Redireccionamos a vista de solicitudes
		res.redirect('/administrador/solicitudes?info=correct');
	})
	.catch(err => {
		console.log('Error al insertar nuevas solicitudes', err);
		res.redirect('/administrador/solicitudes?info=err');
	});
});


// Visualiza el historial de solicitudes de un rider
router.get('/riders/:id/request', (req, res) => {

	let idrider = req.params.id;

	// Busca todas las solicitudes pendientes
	Solicitudes.findAll({
		where: {
			pagada: 'S',
			idrider: idrider
		},
		include: [
			{
				model: Riders, as: 'rider',
				attributes: ['cedula', 'nombre', 'apellido', 'telefono']
			}
		]
	}).then(solicitudes => {
		
		for (let i= 0; i < solicitudes.length; i++) {
			
			let date = new Date(solicitudes[i].fecha).toLocaleString();
			let date_split = date.split(' ');
			solicitudes[i].fecha = date_split[0];
			solicitudes[i].hora = date_split[1];
		}

		obtenerCuentaRider(idrider, (cuenta) => {
			res.render('admin/pages/solicitudes', {
				info: undefined, 
				solicitudes: solicitudes, 
				rider: (solicitudes.length > 0)? solicitudes[0].rider : null,
				cuenta: cuenta
			});
		});	

		//res.render('admin/pages/solicitudes', {info: undefined, solicitudes: solicitudes});
	})
	.catch(err => {
		console.log('Error al buscar todas las solicitudes', err);
		res.redirect('/?info=err');
	});
/*
	connection.query('SELECT * FROM solicitudes s JOIN riders r ON r.cedula = s.idrider WHERE s.pagada = ? AND idrider = ?', ['S', idrider], (err, rows, fields) => {
		if (err) {
			console.log(err);
		} else {
			connection.query('SELECT * FROM riders r WHERE r.cedula = ?', [idrider], (err, rider, fields) => {
				if (err) {
					console.log(err);
				} else {
					for(let i= 0; i < rows.length; i++) {
						let date = new Date(rows[i].fecha).toLocaleString();
						let date_split =  date.split(" ");
						rows[i].fecha = date_split[0];
						rows[i].hora = date_split[1];
					}
					obtenerCuentaRider(idrider, (cuenta) => {
						res.render('admin/pages/solicitudes', {
							info: undefined, 
							solicitudes: rows, 
							rider: rider[0], 
							cuenta: cuenta
						});
					});	
				}
			});
		}
	});
*/
});


// Lista todos los moderadores
router.get('/moderadores', (req, res) => {

	let info = req.query.info;

	connection.query('select * from moderadores', (err, rows, fields) =>{
		if (err) {
			console.log(err);
			res.redirect('/administrador');
		} else {
			res.render('admin/pages/moderators/index', {
				moderadores: rows, info: info
			});
		}
	});
});

// Se habilita formulario para editar un moderador
router.get('/moderadores/:id/edit', (req, res) => {
	let info = req.query.info;
	let usuario = req.params.id;

	connection.query('select * from moderadores where cedula = ?', [usuario], (err, rows, fields) => {
		if (err) {
			console.log(err);
			return res.redirect('/administrador/moderadores?info=err');
		} else {
			let mod = rows[0];
			connection.query('select * from direcciones d join municipios m on d.idmunicipio = m.idmunicipios join departamentos dep on dep.idDepartamento = m.idDepartamento join paises p on p.idPais = dep.idPais where iddireccion = ?', 
				[mod.iddireccion], (err, rows, fields) => {
					if (err) {
						console.log(err);
						return res.redirect('/administrador/moderadores?info=err');
					} else {
						//console.log(rows[0])
						res.render('admin/pages/moderators/edit', {
							mod: mod,
							info: info,
							direccion: rows[0]
						});
					}
				});
		}
	});
});

// Agrega nuevos moderadores
router.post('/moderadores', (req, res) => {
	let cedula = req.body.cedula;
	let nombre = req.body.nombre;
	let apellido = req.body.apellido;
	let correo = req.body.correo;
	let contrasenia = cedula;
	let pais = req.body.pais;
	let departamento = req.body.departamento;
	let municipio = req.body.municipio;
	let genero = req.body.genero;
	let direccion = req.body.direccion;
	let notas = req.body.notas;

	let foto = req.files.foto;
	let anexos = req.body.anexos;
	let direccionImg;

	let name;

	if (genero == 'Masculino') {
		direccionImg = 'src/app/public/img/mod/defaultmas.jpg';
		name = 'defaultmas.jpg';
	} else {
		direccionImg = 'src/app/public/img/mod/defaultfem.jpg';
		name = 'defaultfem.jpg';
	}

	if (foto.name != '') {
		let extension = foto.name.split('.').pop();
		let direccionImg = 'src/app/public/img/mod/' + cedula + '.' + extension;
		fs.renameSync(foto.path, direccionImg);
		name = cedula + '.' + extension;
	}
	
	connection.query('insert into direcciones (direccion, idmunicipio, notas) values(?, ?, ?)', [direccion, municipio, notas], 
		(err, rows, fields) => {
			if (err) {
				console.log(err);
				return res.redirect('/administrador/moderadores?info=err');
			} else {
				let hash = bcrypt.hashSync(contrasenia, 10);
				connection.query('insert into moderadores (cedula, nombre, apellido, correo, contrasenia, foto_perfil, anexos, iddireccion, genero) values(?, ?, ?, ?, ?, ?, ?, ?, ?)', 
					[cedula, nombre,apellido, correo, hash, name, anexos, rows.insertId, genero], (err, rows, fields) => {
						if (err) {
							console.log(err);
							return res.redirect('/administrador/moderadores?info=err');
						} else {
							return res.redirect('/administrador/moderadores?info=correct');
						}
					});
			}
		});
});

// Modifica un moderador existente
router.put('/moderadores/:id', (req, res) => {
	let cedula = req.params.id;
	let nombre = req.body.nombre;
	let apellido = req.body.apellido;
	let correo = req.body.correo;
	let pais = req.body.pais;
	let departamento = req.body.departamento;
	let municipio = req.body.municipio;
	let direccion = req.body.direccion;
	let notas = req.body.notas;
	let foto_perfil = req.body.foto_perfil;
	let direccion_anterior = req.body.direccion_anterior;

	let foto = req.files.foto;
	let anexos = req.body.anexos;
	let direccionImg;

	let name;

	direccionImg = 'src/app/public/img/mod/'+foto_perfil ;
	name = foto_perfil;

	if (foto.name != '') {
		let extension = foto.name.split('.').pop();
		let direccionImg = 'src/app/public/img/mod/' + cedula + '.' + extension;
		fs.renameSync(foto.path, direccionImg);
		name = cedula + '.' + extension;
	}
	
	db.query('SELECT * FROM moderadores WHERE cedula = ?', [cedula])
	.then(mod => {
		return db.query('UPDATE direcciones SET direccion = ?, idmunicipio = ?, notas = ? WHERE iddireccion = ?',
		[direccion, municipio, notas, mod[0].iddireccion]);
	})
	.then(() => {
		return db.query('UPDATE moderadores SET nombre = ?, apellido = ?,' +
				' correo = ?, foto_perfil = ?, anexos = ? WHERE cedula = ?', 
				[nombre, apellido, correo, name, anexos, cedula]);
	})
	.then(() => {
		res.redirect('/administrador/moderadores?info=correct');
	})
	.catch(err => {
		console.log(err);
		res.redirect('/administrador/moderadores/'+ cedula +'/edit?info=err');
	});

	/*connection.beginTransaction((err) => {
		if (err) {
			return res.redirect('/administrador/moderadores/'+ cedula +'/edit?info=err');
		} else {
			connection.query('insert into direcciones (direccion, idmunicipio, notas) values(?, ?, ?)', [direccion, municipio, notas], 
				(err, rows, fields) => {
					if (err) {
						connection.rollback(() => {
							console.log(err);
							return res.redirect('/administrador/moderadores/'+ cedula +'/edit?info=err');
						});
					} else {
						connection.query('update moderadores set nombre = ?, apellido = ?, correo = ?, foto_perfil = ?, anexos = ?, iddireccion = ? where cedula = ?',
							[nombre, apellido, correo, name, anexos, rows.insertId, cedula], (err, rows, fields) => {
								if (err) {
									connection.rollback(() => {
										console.log(err);
										return res.redirect('/administrador/moderadores/'+ cedula +'/edit?info=err');
									});
								} else {
									connection.query('delete from direcciones where iddireccion = ?', [direccion_anterior], (err, rows, fields) => {
										if (err) {
											connection.rollback(() => {
												console.log(err);
												return res.redirect('/administrador/moderadores/'+ cedula +'/edit?info=err');
											});
										} else {
											connection.commit((err) => {
												if (err) {
													return res.redirect('/administrador/moderadores/'+ cedula +'/edit?info=err');
												} else {
													return res.redirect('/administrador/moderadores?info=correct');
												}
											});
										}
									});
								}
							});
					}
				});
		}
	});*/
});

// Elimina un moderador
router.get('/moderadores/delete/:id', (req, res) => {
	let cedula = req.params.id;

	connection.query('delete from moderadores where cedula = ?', [cedula], (err, rows, fields) => {
		if (err) {
			console.log(err);
			return res.redirect('/administrador/moderadores?info=err');
		} else {
			return res.redirect('/administrador/moderadores?info=eliminated');
		}
	})

});

router.post('/agregarsucursal', (req, res) => {
	let nombre = req.body.nombre;
	let idtienda = req.body.tienda;
	let categoria = req.body.categoria;
	let rut = req.body.rut;
	let empresarial = req.body.empresarial;
	let contrasenia = rut;
	let retenido = (req.body.retenido == 'on' ? 'S' : 'N');


	let latitude = req.body.latitude;
	let longitude = req.body.longitude;

	if(empresarial == 'Normal'){
		empresarial = 'N';
	}else{
		empresarial = 'Y';
	}

	let municipio = req.body.municipio;
	let direccion = req.body.direccion;
	let notas = req.body.notas;
	let hora_inicial = req.body.hora_inicial;
	let hora_final = req.body.hora_final;

	let tiempo_entrega = req.body.entrega;
	let telefono = req.body.entrega;
	let numero_punto = req.body.numero_punto;
	let costo_envio = req.body.costo_envio;
	let comision = req.body.comision;
	let cedula = req.body.cedula;
	let nombre_dueno = req.body.nombre_dueno;
	let apellido_dueno = req.body.apellido_dueno;
	let numero_privado = req.body.numero_privado;
	let cuenta_bancaria =  req.body.cuenta_bancaria;

	connection.query('insert into direcciones (direccion, idmunicipio, notas, latitude, longitude) values(?, ?, ?, ?, ?)', [direccion, municipio, notas, latitude, longitude], 
		(err, rows, fields) => {
			if(err){
				console.log(err);
				return res.redirect('/administrador/sucursales?info=err');
			}else{
				connection.query('insert into sucursales (retenido, contrasenia, nombre, rut, cuenta_bancaria, cedula, numero_privado, numero_punto, idtienda, idmunicipio, costo_envio, comision, nombre_duenio, apellido_duenio, iddireccion, idcategoria, hora_inicial, hora_final, tiempo_entrega, soloempresarial) values(?, ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'
					,[retenido, contrasenia, nombre, rut, cuenta_bancaria, cedula, numero_privado, numero_punto, idtienda, municipio, costo_envio, comision, nombre_dueno, apellido_dueno, rows.insertId, categoria, hora_inicial, hora_final, tiempo_entrega, empresarial], (err, rows, fields) => {
						if(err){
							console.log(err);
							return res.redirect('/administrador/sucursales?info=err');
						}else{
							return res.redirect('/administrador/sucursales?info=correct');
						}
					});
			}
		});

});

router.post('/agregarrider', (req, res) => {
	let cedula = req.body.cedula;
	let nombre = req.body.nombre;
	let apellido = req.body.apellido;
	let correo = req.body.correo;
	let telefono = req.body.telefono;
	let contrasenia = cedula;
	let vehiculo = req.body.vehiculo;
	let seguro = req.body.seguro;
	let nacimiento = req.body.nacimiento;
	let maletin = req.body.maletin;
	let foto = req.files.foto;
	let anexos = req.body.anexos;
	let genero = req.body.genero;

	let pais = req.body.pais;
	let departamento = req.body.departamento;
	let municipio = req.body.municipio;
	let direccion = req.body.direccion;
	let notas = req.body.notas;

	let direccionImg;

	let name;

	if(genero == 'Masculino'){
		direccionImg = 'src/app/public/img/rider/defaultmas.jpg';
		name = 'defaultmas.jpg';
	}else{
		direccionImg = 'src/app/public/img/rider/defaultfem.jpg';
		name = 'defaultfem.jpg';
	}

	if(foto.name != ''){
		let extension = foto.name.split('.').pop();
		let direccionImg = 'src/app/public/img/rider/' + cedula + '.' + extension;
		fs.renameSync(foto.path, direccionImg);
		name = cedula + '.' + extension;
	}
	
	connection.query('insert into direcciones (direccion, idmunicipio, notas) values(?, ?, ?)', [direccion, municipio, notas], 
		(err, rows, fields) => {
			if(err){
				console.log(err);
				return res.redirect('/administrador/riders?info=err');
			}else{
				connection.query('insert into riders values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'
					, [cedula, nombre, apellido, telefono, correo, vehiculo, seguro, nacimiento, maletin, name, genero, rows.insertId, contrasenia, anexos, 'N', 'N', 0, 0]
					, (err, rows, fields) => {
						if(err){
							console.log(err);
							return res.redirect('/administrador/riders?info=err');
						}else{
							return res.redirect('/administrador/riders?info=correct');
						}
					});
			}
		});

});

router.post('/modificarrider', (req, res) => {

	let cedula = req.body.cedula;

	let foto_perfil = req.body.foto_perfil;
	let direccion_anterior = req.body.direccion_anterior;
	let nombre = req.body.nombre;
	let apellido = req.body.apellido;
	let correo = req.body.correo;
	let telefono = req.body.telefono;
	let vehiculo = req.body.vehiculo;
	let maletin = req.body.maletin;
	let seguro = req.body.seguro;
	let foto = req.files.foto;
	let anexos = req.body.anexos;
	let listanegra = req.body.listanegra;
	
	let pais = req.body.pais;
	let departamento = req.body.departamento;
	let municipio = req.body.municipio;
	let direccion = req.body.direccion;
	let notas = req.body.notas;
	let genero = req.body.genero;
	
	
	let direccionImg;

	let name;

	direccionImg = 'src/app/public/img/rider/'+foto_perfil ;
	name = foto_perfil;

	if(foto.name != ''){
		let extension = foto.name.split('.').pop();
		let direccionImg = 'src/app/public/img/rider/' + cedula + '.' + extension;
		fs.renameSync(foto.path, direccionImg);

		name = cedula + '.' + extension;
	}
	
	connection.beginTransaction((err) => {
		if(err){
			return res.redirect('/administrador/rider?cedula='+ cedula +'&info=err');
		}else{
			connection.query('insert into direcciones (direccion, idmunicipio, notas) values(?, ?, ?)', [direccion, municipio, notas], 
				(err, rows, fields) => {
					if(err){
						connection.rollback(() => {
							console.log(err);
							return res.redirect('/administrador/rider?cedula='+ cedula +'&info=err');
						});
					}else{
						connection.query('update riders set iddireccion = ?, nombre = ?, apellido = ?, correo = ?, telefono = ?, tipo_vehiculo = ?, tamanio_maletin = ?, seguro_traigo = ?, foto_perfil = ?, anexos = ?, listanegra = ? where cedula = ?'
							, [rows.insertId, nombre, apellido, correo, telefono, vehiculo, maletin, seguro, name, anexos, listanegra, cedula], (err, rows, fields) => {
								if(err){
									connection.rollback(() => {
										console.log(err);
										return res.redirect('/administrador/rider?cedula='+ cedula +'&info=err');
									});
								}else{
									connection.query('delete from direcciones where iddireccion = ?', [direccion_anterior], (err, rows, fields) => {
										if(err){
											connection.rollback(() => {
												console.log(err);
												return res.redirect('/administrador/rider?cedula='+ cedula +'&info=err');
											});
										}else{
											connection.commit((err) => {
												if(err){
													return res.redirect('/administrador/rider?cedula='+ cedula +'&info=err');
												}else{
													return res.redirect('/administrador/rider?cedula='+ cedula +'&info=correct');
												}
											})
										}
									})
									
								}
							});
					}
				});
		}
	});

});

router.get('/eliminarrider', (req, res) => {
	let cedula = req.url.split('?')[1].split('=')[1];

	connection.query('delete from riders where cedula = ?', [cedula], (err, rows, fields) => {
		if(err){
			console.log(err);
			return res.redirect('/administrador/riders?info=err');
		}else{
			return res.redirect('/administrador/riders?info=eliminated');
		}
	});

});

router.get('/eliminarsucursal', (req, res) => {
	let id = req.url.split('?')[1].split('=')[1];

	connection.query('delete from sucursales where idsucursal = ?', [id], (err, rows, fields) => {
		if(err){
			console.log(err);
			return res.redirect('/administrador/sucursales?info=err');
		}else{
			return res.redirect('/administrador/sucursales?info=eliminated');
		}
	})

});


router.get('/eliminartienda', (req, res) => {
	let name = req.url.split('?')[1].split('=')[1];

	connection.query('delete from tiendas where nombre = ?', [name], (err, rows, fields) => {
		if(err){
			console.log(err);
			return res.redirect('/administrador/tiendas?info=err');
		}else{
			return res.redirect('/administrador/tiendas?info=eliminated');
		}
	})

});

router.get('/eliminarproducto', (req, res) => {
	let id = req.url.split('?')[1].split('&')[0].split("=")[1];
	let nombre = req.url.split('?')[1].split('&')[1].split("=")[1];
	connection.query('delete from productos where idproducto = ?', [id], (err, rows, fields) => {
		if(err){
			console.log(err);
			return res.redirect('/administrador/sucursales/'+nombre+'?info=err');
		}else{
			return res.redirect('/administrador/sucursales/'+nombre+'?info=eliminated');
		}
	})

});

router.get('/sucursales/:nombre', (req, res) => {

	let nombre = req.params.nombre;
	let sucursal;

	let params = req.url.split('?')[1];
	let info = undefined;
	if(params != undefined){
		info = params.split('=')[1];
	}

	connection.query('select s.*, d.direccion as direccion from sucursales s join direcciones d on d.iddireccion = s.iddireccion where s.nombre = ?', [nombre], (err, rows, fields) => {
		if(err){
			return res.redirect('/administrador/sucursales?info=err');
		}else{
			sucursal = rows[0];
			connection.query('select * from productos where idsucursal = ?', [sucursal.idsucursal], (err, productos, fields) => {
				if(err){
					return res.redirect('/administrador/sucursales?info=err');
				}else{
					res.render('admin/sucursal', {sucursal: sucursal, productos: productos, info: info});
				}
			});
		}
	});
});


router.get('/sucursal', (req, res) => {

	let nombre = req.params.nombre;
	let sucursal;

	let params = req.url.split('?')[1];
	let info = undefined;
	if(params != undefined){
		info = params.split('=')[1];
	}

	connection.query('select s.*, d.direccion as direccion from sucursales s join direcciones d on d.iddireccion = s.iddireccion where s.nombre = ?', [nombre], (err, rows, fields) => {
		if(err){
			return res.redirect('/administrador/sucursales?info=err');
		}else{
			sucursal = rows[0];
			connection.query('select * from productos where idsucursal = ?', [sucursal.idsucursal], (err, productos, fields) => {
				if(err){
					return res.redirect('/administrador/sucursales?info=err');
				}else{
					res.render('admin/sucursal', {sucursal: sucursal, productos: productos, info: info});
				}
			});
		}
	});
});


router.post('/agregarproducto', (req, res) => {

	let nombre = req.body.nombre;
	let descripcion_corta = req.body.descripcion_corta;
	let descripcion_larga = req.body.descripcion_larga;
	let precio = req.body.precio;
	let idsucursal = req.body.idsucursal;
	let tamanio_maletin = req.body.maletin;
	let categoria = req.body.categoria;

	let foto = req.files.imagen;
	let direccionImg;
	let name;

	let nombresucursal = req.body.nombresucursal;
	nombresucursal = nombresucursal.replace(" ", "%20");

	direccionImg = 'src/app/public/img/producto/default.png';
	name = 'default.png';
	
	


	let info = JSON.parse(req.body.info);

	let idproducto;

	connection.query('insert into productos (nombre, descripcion_corta, precio, imagen, idsucursal, descripcion_larga, tamanio_maletin, idcategoriaproducto) values(?,?,?,?,?,?,?,?)'
		, [nombre, descripcion_corta, precio, name, idsucursal, descripcion_larga, tamanio_maletin, categoria], (err, rows, fields) => {
			if(err){
				console.log(err);
			}else{
				idproducto = rows.insertId;

				if(foto.name != ''){
					let extension = foto.name.split('.').pop();
					let direccionImg = 'src/app/public/img/producto/' + idproducto +'.' + extension;
					fs.renameSync(foto.path, direccionImg);
					name = idproducto + '.' + extension;
					connection.query('update productos set imagen = ? where idproducto = ?', [name, idproducto], (err, rows, fields) => {
						if(err){
							console.log(err);
						}
					});
				}

				for(let i = 0; i < info.cantidadFactores; i++){
					if(req.body["nombrefactor" + i].trim() != ''){
						agregarFactor(i, req, info, idproducto, function(err){
							if(err){
								console.log(err);
							}
						});
					}
				}

				for(let i = 0; i < info.cantidadElementos; i++){

					if(req.body["elementonombre" + i].trim() != '' && req.body["elementoprecio" + i].trim() != ''){
						connection.query('insert into elementos (nombre, precio, idproducto) values(?,?,?)',
							[req.body["elementonombre" + i], req.body["elementoprecio" + i], idproducto], (err, rows, fields) => {
								if(err)
									console.log(err);
								});
					}
				}

				for(let i = 0; i < info.cantidadAdiciones; i++){
					if(req.body["adicionnombre" + i].trim() != '' && req.body["adicionprecio" + i].trim() != ''){
						connection.query('insert into adiciones (nombre, precio, idproducto) values(?,?,?)',
							[req.body["adicionnombre" + i], req.body["adicionprecio" + i], idproducto], (err, rows, fields) => {
								if(err)
									console.log(err);
								});
					}
				}


				res.redirect('/administrador/sucursales/' + nombresucursal);
			}
			
		});
});

router.get('/cerrarsesion', (req, res) => {
	req.session.user = undefined;
	res.redirect('/');
});

function agregarFactor(i, req, info, idproducto, next){
	connection.query('insert into factores (nombre, obligatorio, idproducto) values(?, ?, ?)',
		[req.body["nombrefactor" + i], (req.body["obligatorio" + i] == "on" ? "S" : "N"), idproducto], (err, result, fields) => {
			if(err){
				console.log(err);
				next(err);
			}else{
				idfactor = result.insertId;
				agregarOpciones(i, req, idfactor, info.cantidadOpciones[i], function(){
					console.log("good");
				});
				next(undefined);
			}
		});
}

function agregarOpciones(i, req, idfactor, cantidad, next){
	for(let j = 0; j < cantidad; j++){
		connection.query('insert into selecciones (nombre, precio, idfactor) values(?, ?, ?)',
			[req.body["nombre" + i + "opcion" + j], req.body["precio" + i + "opcion" + j], idfactor], (err, rows, fields) => {
				if(err){
					console.log(err);
				}
			});
	}
	next();
}


/**
 * Obtiene el valor actual de los pedido sin pagar y adicionales
 * 
 * @author Jhoan Sebastian Chilito Sanchez
 * @param {*} rider_id identificador del rider
 * @param {*} init_date fecha inicial intervalo
 * @param {*} end_date fecha final intervalo
 * @returns PedidosDeseo[0]
 * 			PedidosClientes[1]
 * 			PedidosEmpresariales[2]
 * 			PedidosEncomiendas[3] JOIN con ajustes 
 
 */
async function getCurrentOrdersAccountRider(rider_id, init_date, end_date, commision = 'N') {
	// Tipos de pedidos
	let deseos, encomiendas, clientes, empresariales;
	// Codiciones de consulta
	let condiciones;
	// Verificador de intervalo de fechas
	let isRange = false;
	// Consulta adicionada para intervalo de fechas
	let queryRange = '';

	// Condiciones de los filtros para las siguientes consultas
	condiciones = [rider_id, 'Entregado', 'Error', commision];

	// Verifica si vienen fechas de rango de filtro
	if (init_date && end_date) {
		// Se habilita rango de fechas para filtrar
		isRange = true;
		// Query adicional para filtro de fechas
		queryRange = `fecha BETWEEN '${init_date}' AND '${end_date}'`;
	}

	// Obtiene todos los pedidos deseos por rider
	deseos = models.sequelize.query(`SELECT pd.*, 'deseo' AS type, SUM(f.valor) AS totalfacturas FROM pedidos_deseos pd JOIN facturas f
	 ON f.idpedido_deseos = pd.idpedido_deseo WHERE pd.idrider = ? AND (pd.estado = ? OR pd.estado = ?) AND pd.comision_entregada = ?
	  ${isRange? `AND pd.${queryRange}` : ''} GROUP BY pd.idpedido_deseo`, { replacements: condiciones, type: models.sequelize.QueryTypes.SELECT });
	
	// Obtiene todos los pedidos realizados a una tienda por rider
	clientes = models.sequelize.query('SELECT *, \'tienda\' AS type FROM pedidos_clientes pc WHERE pc.idrider = ? AND (pc.estado = ? OR pc.estado = ?) AND pc.comision_entregada = ?',
	{ replacements: condiciones, type: models.sequelize.QueryTypes.SELECT });

	let op = models.Sequelize.Op;

	// Condiciones de pedido empresariales
	let conditionsBusinessOrder = {
		idrider: rider_id,
		comision_entregada: commision,
		[op.or]: [
			{estado: 'Entregado'},
			{estado: 'Error'}
		]
	};

	// Verifica si se hce el filtro por intervalo de fechas
	if (isRange) {
		// Se agrega la condicion de rango fechas
		conditionsBusinessOrder.fecha = {
			[op.between]: [
				init_date,
				end_date
			]
		}
	}

	//console.log('Condiciones', conditionsBusinessOrder);
	// Obtiene todos los pedidos empresariales por rider
	empresariales = PedidosEmpresariales.findAll({
		include: [
			{model: Ajustes, as: 'ajuste'}
		],
		where: conditionsBusinessOrder
	});
	/*empresariales = models.sequelize.query(`SELECT *, 'empresariales' AS type FROM pedidos_empresariales pe WHERE pe.idrider = ? 
	AND (pe.estado = ? OR pe.estado = ?) AND pe.comision_entregada = ? ${isRange? `AND pe.${queryRange}` : ''}`,
	{ replacements: condiciones, type: models.sequelize.QueryTypes.SELECT });*/

	// Obtiene todos los pedidos encomienda por rider
	encomiendas = models.sequelize.query(`SELECT pe.*, 'mandado' AS type, SUM(p.costo) AS totalparadas FROM pedidos_encomiendas pe 
	 LEFT JOIN paradas p ON p.idpencomienda = pe.idpedido_encomienda WHERE pe.idrider = ? AND (pe.estado = ? OR pe.estado = ?) 
	 AND pe.comision_entregada = ? ${isRange? `AND pe.${queryRange}` : ''} GROUP BY pe.idpedido_encomienda`, { replacements: condiciones, type: models.sequelize.QueryTypes.SELECT });


	
	/*// Obtiene los id's no nulos de ajustes de los pedidos empresariales
	let listBusinessOrdersAdjust = await PedidosEmpresariales.findAll({
		include: [
			{model: Ajustes, as: 'ajuste'}
		],
		attributes: ['ajustes_id'],
		where: {
			idrider: rider_id,
			ajustes_id: {
				[op.ne]: null
			},
			[op.or]: [
				{estado: 'Entregado'},
				{estado: 'Error'}
			]
		}
	})
	.then(pe => {
		console.log('Ajustes', pe);
		// Obtiene una lista de identificadores de ajustes
		if (pe.length > 0 ) {
			const ids = pe.map((ajustes) => { return ajustes.ajustes_id; });
			// Pedidos empresariales ajustes
			return models.sequelize.query(`SELECT IFNULL(SUM(a.valor), 0) valor FROM ajustes a WHERE a.id IN(?)`,
			{ replacements: [ids], type: models.sequelize.QueryTypes.SELECT });
		} else {
			return false;
		}
	});*/

	// Se ejecutan todas las consultas
	return Promise.all([deseos, clientes, empresariales, encomiendas]);
}

/**
 * Obtiene el valor de todos los pedidos realizados del rider
 */
async function obtenerCuentaRider(cedula, next, index) {

	// Obtiene el valor actual de los pedido sin pagar y adicionales
	getCurrentOrdersAccountRider(cedula)
	.then(pedidos => {
		// Se concatenan todods lo pedidos consultados
		let todos = pedidos[0].concat(pedidos[1], pedidos[2], pedidos[3]);
		//let ajustes = (pedidos[4])? pedidos[4][0].valor : 0;

		// Suma todos los valores actuales
		obtenerValorEstadoActual(todos, (cuenta) => {
			next(cuenta, index);
		});
	})
	.catch(err => {
		console.log('Error al',err);
		//res.send(err);
	});

	
/*
	connection.query('select pd.*, sum(f.valor) as totalfacturas from pedidos_deseos pd join facturas f on f.idpedido_deseos = pd.idpedido_deseo where pd.idrider = ? and (pd.estado = ? or pd.estado = ?) group by pd.idpedido_deseo'
		,[cedula, 'Entregado', 'Error'], (err, rows, fields) => {
			if(err){
				console.log(err);
				return res.send(err);
			}
			deseos = rows;
			for(let i = 0; i < deseos.length; i++){
				deseos[i].type = 'deseo';
			}
			connection.query('select pe.*, sum(p.costo) as totalparadas from pedidos_encomiendas pe left join paradas p on p.idpencomienda = pe.idpedido_encomienda where pe.idrider = ? and (pe.estado = ? or pe.estado = ?) and comision_entregada = ? group by pe.idpedido_encomienda'
				,[cedula, 'Entregado', 'Error', 'N'], (err, rows, fields) => {
					if(err){
						console.log(err);
						return res.send(err);
					}
					mandados = rows;
					for(let i = 0; i < mandados.length; i++){
						mandados[i].type = 'mandado';
					}
					connection.query('select * from pedidos_clientes pc where pc.idrider = ? and (pc.estado = ? or pc.estado = ?)'
						,[cedula, 'Entregado', 'Error'], (err, rows, fields) => {
							if(err){
								console.log(err);
								return res.send(err);
							}
							pedidos = rows;
							for(let i = 0; i < pedidos.length; i++){
								pedidos[i].type = 'tienda';
							}
							connection.query('select * from pedidos_empresariales pe where pe.idrider = ? and (pe.estado = ? or pe.estado = ?)'
								,[cedula, 'Entregado', 'Error'], (err, rows, fieds) => {
									if (err) {
										console.log(err);
										return res.send(err);
									}
									empresariales = rows;
									for (let i = 0; i < empresariales.length; i++) {
										empresariales[i].type = 'empresariales';
									}
									let todos = deseos.concat(pedidos, mandados, empresariales);
									obtenerValorEstadoActual(todos, (cuenta) => {
										next(cuenta, index);
									});
								});
						});
				});
		});
		*/
}

/**
 * Suma el valor de la cuenta de cada uno de los pedidos
 */
function obtenerValorEstadoActual(pedidos, next){
	let cuenta = 0;

	for (let i = 0; i < pedidos.length; i++) {
		if (pedidos[i].type == 'deseo') {
			//if (pedidos[i].comision_entregada == 'N') {
				cuenta -= pedidos[i].totalfacturas * pedidos[i].comision_cliente / 100;
			//}
		} else if(pedidos[i].type == 'mandado') {
			//if (pedidos[i].comision_entregada == 'N') {
				if (pedidos[i].totalparadas == null) {
					pedidos[i].totalparadas = 0;
				}
				cuenta -= (pedidos[i].totalparadas + pedidos[i].costo_envio) * pedidos[i].comision_rider / 100;
			//}
		} else if (pedidos[i].type == 'tienda') {
			if (pedidos[i].comision_entregada == 'N') {
				cuenta -= (pedidos[i].costo_envio) * pedidos[i].comision_rider / 100;
			}
		} else  {
			// Cuando los pedidos son empresariales
			//if (pedidos[i].comision_entregada == 'N') {
				if (pedidos[i].retenido == 'S') {
					cuenta += (pedidos[i].costo_envio + pedidos[i].valor_ida_vuelta) * (100 - pedidos[i].comision_rider) / 100;
				} else {
					cuenta -= (pedidos[i].costo_envio + pedidos[i].valor_ida_vuelta) * pedidos[i].comision_rider / 100;
				}
				if (pedidos[i].ajustes_id) {
					cuenta += pedidos[i].ajuste.valor;
				}
			//}
		}
	}
	next(cuenta);
}

/** CRUD NOTIFICATIONS INIT**/
// (INDEX) Obtiene tod...
router.get('/notifications', (req, res) => {

	let date = new Date()

	let day = date.getDate()
	let month = date.getMonth() + 1
	let year = date.getFullYear()

	let current = year + '-' + month + '-' + day;

	db.query('SELECT nd.title, nd.description FROM notifications n JOIN notifications_detail nd WHERE DATE(n.created_at) = ?',[current])
	.then(rows => {
		res.status(200).json({notifications: rows});
	})
	.catch(err => {
		console.log(err);
		res.status(400);
	});

});
// (CREATE) Visualiza el formulario para agregar un...
router.get('/notifications/create', (req, res) => {
});
// (SHOW) Permite visualizar un...
router.get('/notifications/:id', (req, res) => {
});
// (EDIT) Permite editar una sucursal
router.get('/notifications/:id/edit', (req, res) => {
});
// (UPDATE) Permite editar un...
router.put('/notifications/:id', (req, res) => {
});
// (STORE) Agrega una nueva...
router.post('/notifications', (req, res) => {

	/**
	 * Verifica si se toma el titulo y la descripcion de una notificacion creada
	 */
	let detail_id = req.body.detail_id;
	
	/**
	 * Tipo de notificacion
	 * 
	 * TODO definir los tipos en arreglo constante
	 */
	let type = req.body.type;
	
	/**
	 * Lista de sucursales a las que se le van a enviar
	 * vacio si es a todas
	 */
	let listOffices = req.body.offices;

	// datos de la notificacion
	let dataNotification;

	// Verifica si se envia una notificacion reciclada
	if (detail_id) {
		// Crea la notificacion
		dataNotification = [detail_id, type, new Date()];
		createNotification(dataNotification, listOffices, res);
	} else {
		// Titulo notificacion
		let title = req.body.title;
		// Descripcion notificacion
		let description = req.body.description;

		db.query('INSERT INTO notifications_detail(title, description) VALUES (?,?)', [title, description])
		.then(rows => {
			// datos de la notificacion
			dataNotification = [rows.insertId, type, new Date()];
			// Crea la notificacion
			createNotification(dataNotification, listOffices, res);
		})
		.catch(err => {
			console.log(err);
			res.status(400).json();	
		});
	}

	/**
	 * Inserta una nueva notificacion de tiendas
	 */
	function createNotification (notification, offices, res) {
		db.query('INSERT INTO notifications (notifications_detail_id, type, created_at) VALUES (?,?,?)', notification)
		.then(rows => {
			if (offices.length > 0) {
				// Se organiza en lista para la insercion
				for (let i = 0; i < offices.length; i++) {
					const element = offices[i];
					offices[i] = [rows.insertId, element];
				}
				// Insercion multiple
				db.query('INSERT INTO sucursales_notifications (notifications_id, sucursales_idsucursal) VALUES ?', offices)
				.then(() => {
					res.status(200).json();	
				})
				.catch(err => {
					console.log(err);
					res.status(400).json();	
				});
			} else {
				res.status(200).json();	
			}
		})
		.catch(err => {
			console.log(err);
			res.status(400).json();	
		});
	}

});
// (DESTROY) Elimina una sucursal
router.get('/notifications/delete/:id', (req, res) => {
});
/** CRUD EXAMPLE FINISH**/



/** CRUD PLANES INIT**/
// (INDEX) Obtiene todos los planes
router.get('/planes', (req, res) => {
	
	let info = req.query.info;
	
	Planes.findAll({
		include: [
			{model: TiendasPlanes, as: 'tiendas_planes'}
		]
	})
	.then(planes => {

		// Se recorren todos los planes
		planes.forEach(plan => {
			// Tipos de planes
			const TIPO_PLAN = Constants.TIPO_PLAN; 
			// Se obtiene el plan correspondiente al id
			let index = TIPO_PLAN.findIndex(x => x.id == plan.tipo);
			// Se obtiene el nombre del plan
			plan.tipo = TIPO_PLAN[index].name;

			plan.tiendas_planes = (plan.tiendas_planes.length > 0)? true : false;
		});
		
        res.render('admin/pages/plans/index', {
			planes: planes,
			info: info
		});
	})
    .catch(err => {
      	console.log('Error al obtener todos los planes', JSON.stringify(err));
		res.redirect('/administrador');
    });


});
// (CREATE) Visualiza el formulario para agregar un plan
router.get('/planes/create', (req, res) => {
	
	// Tipos de planes
	const TIPO_PLAN = Constants.TIPO_PLAN; 
	// Se obtiene el plan correspondiente al id
	let index = TIPO_PLAN.findIndex(x => x.name == 'Personalizado');
	// Elimina el [Plan personalizado]
	if (index > -1) {
		TIPO_PLAN.splice(index, 1);
	}
	res.render('admin/pages/plans/create', {
		types: TIPO_PLAN
	});
});
// (SHOW) Permite visualizar un plan
router.get('/planes/:id', (req, res) => {
});
// (EDIT) Permite editar un plan
router.get('/planes/:id/edit', (req, res) => {

	const id = req.params.id;
	const info = req.query.info;

	// Busca el plan por el ID
	Planes.findByPk(id)
	.then((plan) => {
		// Valida si existe el plan
		if (plan) {

			// Tipos de planes
			const TIPO_PLAN = Constants.TIPO_PLAN; 
			/*// Se obtiene el plan correspondiente al id
			let index = TIPO_PLAN.findIndex(x => x.name == 'Personalizado');
			// Elimina el [Plan personalizado]
			if (index > -1) {
				TIPO_PLAN.splice(index, 1);
			}*/
			// renderiza la vista con los datos del plan
			res.render('admin/pages/plans/edit', {
				plan: plan,
				types: TIPO_PLAN,
				info : info
			});	
		} else {
			return res.redirect('/administrador/planes?info=not_found');
		}
	})
	.catch(err => {
		console.log('Error al obtener el plan #' + id, JSON.stringify(err));
		return res.redirect('/administrador/planes?info=err');
	});		
	
});
// (UPDATE) Permite editar un plan
router.put('/planes/:id', (req, res) => {

	let id = req.params.id;
	let nombre = req.body.nombre;
	let descripcion = req.body.descripcion;
	let precio = req.body.precio;
	let num_pedidos = req.body.num_pedidos;
	let tipo = req.body.tipo;

	// Valida si fueron ingresados los campos requeridos
	if (!(nombre && precio && num_pedidos && tipo)) {
		return res.redirect(`/administrador/planes/${id}/edit?info=err`);
	}

	// Busca el plan por el ID
	Planes.findByPk(id)
	.then((plan) => {
		// Valida si existe el plan
		if (plan) {
			// Actuliza los datos del plan
			plan.nombre = nombre;
			plan.precio = precio;
			plan.num_pedidos = num_pedidos;
			plan.tipo = tipo;
			// Valida si se ingresa descripcion
			if (descripcion) {
				plan.descripcion = descripcion;
			}
			// Guarda informacion obtenida
			plan.save()
			.then(() => {
				return res.redirect('/administrador/planes?info=update');	
			})
			.catch(err => {
				console.log('Error al actualizar el plan #' + id, JSON.stringify(err));
				return res.redirect(`/administrador/planes/${id}/edit?info=err`);
			});
			
		} else {
			return res.redirect('/administrador/planes?info=not_found');
		}
	})
	.catch(err => {
		console.log('Error al obtener el plan #' + id, JSON.stringify(err));
		return res.redirect('/administrador/planes?info=err');
	});

});
// (STORE) Agrega un nuevo plan
router.post('/planes', (req, res) => {
	
	let nombre = req.body.nombre;
	let descripcion = req.body.descripcion;
	let precio = req.body.precio;
	let num_pedidos = req.body.num_pedidos;
	let tipo = req.body.tipo;
	
	// Valida si fueron ingresados los campos requeridos
	if (!(nombre && precio && num_pedidos && tipo)) {
		return res.redirect(`/administrador/planes/create?info=err`);
	}

	// Datos de plan
	let data = {
		nombre,
		precio,
		num_pedidos,
		tipo
	};

	// Verifica si se efectua una descripcion para agregarla a la creacion
	if (descripcion) {
		data.descripcion = descripcion;
	}

	// Se crea un nuevo plan
	Planes.create(data)
	.then(() => {
		return res.redirect('/administrador/planes?info=create');
	})
	.catch(err => {
		console.log('Error al guardar el plan', JSON.stringify(err));
		return res.redirect('/administrador/planes?info=err');
	});

});
// (DESTROY) Elimina un plan
router.get('/planes/delete/:id', (req, res) => {
	// Busca el plan por el ID
	let id= req.params.id;

	Planes.destroy({ where: { id : id } })
	.then(() => {
		return res.redirect('/administrador/planes?info=eliminated');
	})
	.catch(err => {
		console.log('Error al obtener el plan #' + id, JSON.stringify(err));
		return res.redirect('/administrador/planes?info=err');
	});

});
/** CRUD PLANES FINISH**/
router.get('/planes-tiendas',async (req, res) => {
	res.render('admin/pages/plans/current-plans-shops');
});

router.get('/get-planes-tiendas', async (req, res) => {
	// Obtiene todas los planes asignados a la tienda
	models.sequelize.query(`SELECT tp.total_pedidos, tp.adicionales, tp.estado, p.nombre AS plan, t.nombre AS tienda,
		t.idtienda FROM tiendas_planes tp JOIN tiendas t ON tp.tiendas_id = t.idtienda 
		JOIN planes p ON tp.planes_id = p.id`,
	{ type: models.sequelize.QueryTypes.SELECT })
	.map(result => {
		result.estado = findConstantById(Constants.ESTADO_TIENDAS_PLANES, result.estado).name;
		return result;
	})
	.then(tiendasPlanes => {
		res.status(200).json(tiendasPlanes);
	}).catch(err => {
		console.log('Error al obtener tiendas planes', err);
		res.status(400).json('Error al obtener tiendas planes');
	});
});


router.get('/planes-historial', async (req, res) => {
	let info = req.query.info;

	/**
	* link https://rstopup.com/sequelize-seleccionar-filas-distintas.html
	*/
	TiendasPlanes.aggregate('tiendas_id', 'DISTINCT', { plain: false })
    .map((row) => { return row.DISTINCT })
	.then(tiendas_id => {
		let op = models.Sequelize.Op;
		return Tiendas.findAll({
			attributes: ['idtienda', 'nombre'],
			where : {
				idtienda : {
					[op.in]: tiendas_id
				}
			}
		});
	})
	.then(tiendas => {
		res.render('admin/pages/plans/history', {
			tiendas: tiendas
		});
	})
	.catch(err => {
		console.log(err);
		res.redirect('/administrador?info=err')
	});

	


});

/**
 * Obtiene la cantidad de pedidos entregados de una tienda prepagada
 * 
 * @param id_tienda identificador de la tienda 
 */
function getQuantityOrdersDelivered(id_tienda) {

	// Obtiene el historial de la tienda
	HistorialTiendasPlanes.findAll({
		attributes: [
			[models.sequelize.fn('SUM', models.sequelize.col('cant_pedidos')), 'total'], 
			[models.sequelize.fn('SUM', models.sequelize.col('adicionales')), 'adicionales']
		],
		where: {
			tiendas_planes_tiendas_id: id_tienda
		}
	})
	.then(htp => {
		console.log('CAntidades', htp);
	})
	.catch(err => {
		console.log('Error', err);
	});
}


/** CRUD TIENDA PLANES INIT **/
// (INDEX) Obtiene todos los planes
router.get('/tiendas/:id/planes', (req, res) => {
	
	let id = req.params.id;
	let info = req.query.info;
	
	Tiendas.findByPk(id)
	.then(tienda => {
		// verifica si se ha encontrado la tienda
		if (tienda) {
			TiendasPlanes.findAll({
				where: {
					tiendas_id: id
				},
				raw: true
			})
			.then(tiendasPlanes => {
				// Lista de ids de planes
				const lista_planes_id = tiendasPlanes.map((tp) => { return tp.planes_id; })

				// Busca todos los planes por id
				Planes.findAll({
					where : {
						id: lista_planes_id
					}, raw: true
				}).then(async planes => {
					// Tipos de planes
					const TIPO_PLAN = Constants.TIPO_PLAN;
					
					// Se recorren todos los planes de la tienda
					for (const k in planes) {
						if (planes.hasOwnProperty(k)) {
							const plan = planes[k];
							
							//console.log('Tienda planes', plan);
							// Se obtiene el plan correspondiente al id
							plan.tipo = findConstantById(TIPO_PLAN, plan.tipo).name;	

							// Se obtiene tienda plan por plan id
							const tp = tiendasPlanes.find((element) => {
								return element.planes_id == plan.id;
							});

							plan.disponibles = tp.total_pedidos;
							plan.adicionales = tp.adicionales;
							plan.vencible = tp.vencible;
							plan.dias_habilitados = tp.dias_habilitados;
							plan.fecha_inicio = moment(tp.fecha_inicio).format('DD/MM/YYYY');
							plan.fecha_vencimiento = moment(tp.fecha_vencimiento).format('DD/MM/YYYY');
							plan.usados = (plan.num_pedidos + tp.adicionales) - tp.total_pedidos;

							await HistorialTiendasPlanes.findAll({
								where: {
									tiendas_planes_planes_id: plan.id,
									tiendas_planes_tiendas_id: id,
									// Estado pendiente 
									estado: 3
								}
							}).then(htp => {
								// Asigna el estado de tiendas planes a el plan
								plan.estado = findConstantById(Constants.ESTADO_TIENDAS_PLANES, tp.estado).name;
								// Valida si has mas de 1 historial con el estado pendiente
								plan.historial = htp.length;
							});							
						}
					}

					res.render('admin/pages/shop_plans/index', {
						tienda: tienda,
						tiendas_planes: tiendasPlanes,
						planes: planes,
						info: info
					});
				});
			})
			.catch(err => {
				console.log('Error al obtener todos los planes', JSON.stringify(err));
				res.redirect('/administrador');
			});
		} else {
			return res.redirect('/administrador/tiendas?info=not_found');
		}
	})
	.catch(err => {
		console.log('Error al obtener la tienda: ' + id, JSON.stringify(err));
		return res.redirect('/administrador/tiendas?info=err');
	});

});

/**
 * Obtiene el historial de planesd ela tienda por estado
 * @param tienda_id identificador de la tienda
 */
router.get('/get-history-tp/', (req, res) => {
	
	let plan_id = req.query.plan_id;
	let tienda_id = req.query.tienda_id;

	HistorialTiendasPlanes.findAll({
		attributes: [
			'adicionales', 'cant_pedidos',
			'estado', 'vencible', 'dias_habilitados', 
			'fecha_inicio', 'fecha_vencimiento', 
			'createdAt', 'updatedAt'
		], where: {
			tiendas_planes_planes_id: plan_id,
			tiendas_planes_tiendas_id: tienda_id
		}, order: [['createdAt']]
	})
	.then( historial => {
		// Se obtiene el valor del estado
		for (let i = 0; i < historial.length; i++) {
			historial[i].estado = findConstantById(Constants.ESTADO_HISTORIAL_PLANES, historial[i].estado).name;
		}
		res.status(200).json(historial);
	})
	.catch(err => {
		console.log('Error al obtener el historial', err);
		res.status(401).json('Error al obtener el historial');
	});
});

// (CREATE) Visualiza el formulario para crear un plan personalizado
router.get('/tiendas/:id/planes/assign', (req, res) => {

	let id = req.params.id;

	// Se busca la tienda por el ID
	Tiendas.findByPk(id)
	.then(tienda => {

		// verifica si se ha encontrado la tienda
		if (tienda) {
			// Operaciones de tabla
			let op = models.Sequelize.Op;

			// Se obtienen los planes asignados a la tienda
			TiendasPlanes.findAll({
				where: {
					tiendas_id: id,
					/*[op.or]: [
						{estado: 1}, // Planes activos
						{estado: 3}	 // Planes pendientes
					]*/
				},
				raw: true
			})
			.then(tiendasPlanes => {
				// Obtenemos los ids como arreglo
				console.log(JSON.stringify(tiendasPlanes));

				for (const key in tiendasPlanes) {
					if (tiendasPlanes.hasOwnProperty(key)) {
						const element = tiendasPlanes[key];
						const estado = Constants.ESTADO_TIENDAS_PLANES[Constants.ESTADO_TIENDAS_PLANES.findIndex(x => x.name == 'Pendiente')].id;
						
						// Verifica si el tipo de tienda plan es pendiente
						if (element.estado == estado) {	
							res.redirect(`/administrador/tiendas/${id}/planes?info=full`);
							break;
						}
					}
				}

				return tiendasPlanes.map((tp) => { return Number(tp.planes_id); })
			})
			.then(tplanes => {
				
				// Condiciones para consultar planes
				let conditions = {
					where: {
						// Se obtiene el ID del tipo de plan
						//tipo: Constants.TIPO_PLAN[Constants.TIPO_PLAN.findIndex(x => x.name == 'Por defecto')].id,
					}
				};

				// verifica si la tienda tiene asignado planes
				if (tplanes.length > 0) {
					// Carga los operadores del WHERE
					let op = models.Sequelize.Op;
					// Condicion para NO obtener los planes que ya tiene asignado la tienda
					conditions.where.id = {
						[op.notIn]: tplanes
					};
				}

				// Se obtienen todos los planes por defecto
				return Planes.findAll(conditions);
				
			})
			.then(planes => {
				res.render('admin/pages/shop_plans/assign', {
					tienda: tienda,
					planes: planes
				});
			})
			.catch(err => {
				console.log('Error al obtener todos los planes', JSON.stringify(err));
				res.redirect('/administrador');
			});
		} else {
			return res.redirect('/administrador/tiendas?info=not_found');
		}
	})
	.catch(err => {
		console.log('Error al obtener la tienda: ' + id, JSON.stringify(err));
		return res.redirect('/administrador/tiendas?info=err');
	});
	
});

// (POST) Guarda los planes asignados a la tienda
router.post('/tiendas/:id/planes/assign', (req, res) => {

	// Identificador de la tienda
	let tienda_id = req.params.id;
	// Plan asignado
	let plan_asignado = req.body.plan;
	// Pedidos adicionales
	let adicionales = (req.body.adicionales)? req.body.adicionales : 0;
	// Valida el plan es vencible
	let vencible = (req.body.vencible == 'on' ? true : false);
	// Cantidad de dias habilitados del plan
	let dias_habilitados = (req.body.dias_habilitados)? req.body.dias_habilitados : null;
	
	if (plan_asignado) {

		// Informacion del plan
		const plan_id = plan_asignado;

		TiendasPlanes.findAll({
			where: {
				tiendas_id: tienda_id
			}
		})
		.then(async tiendasPlanes => {

			// Todos lo estados de tiendas planes
			const ESTADO_TP = Constants.ESTADO_TIENDAS_PLANES;

			// Estado activo tiendas planes
			let EST = ESTADO_TP[ESTADO_TP.findIndex(x => x.name == 'Activo')].id;

			// Verifica si se encontraron tiendas planes
			if (tiendasPlanes.length > 0) {
				// Se recorrenc las tiendas planes
				for (const key in tiendasPlanes) {
					if (tiendasPlanes.hasOwnProperty(key)) {
						const element = tiendasPlanes[key];
						// Verifica si algun tienda plan tiene el estado activo
						if (element.estado == EST) {
							// Se asigna el estado pendiente
							EST = ESTADO_TP[ESTADO_TP.findIndex(x => x.name == 'Pendiente')].id;
							break;
						}
					}
				}
			}

			// Busca el plan por el id
			await Planes.findByPk(plan_id)
			.then(async plan => {

				// Unmanaged Transaction
				let tr = await models.sequelize.transaction();

				try {
					// Estado en curso
					const estado_tp = EST;
					
					// Estados de hostorial de asginacion de planes a las tiendas
					const C_ESTADO = Constants.ESTADO_HISTORIAL_PLANES;
					
					// Estado en curso de historial
					let estado_htp = C_ESTADO[C_ESTADO.findIndex(x => x.name == 'En curso')].id;

					// Datos del plan de la tienda
					let datosTiendasPlanes = {
						planes_id: plan.id,
						tiendas_id: tienda_id,
						total_pedidos: parseInt(plan.num_pedidos) + parseInt(adicionales), // Adicionales incluidos
						adicionales: adicionales,
						estado: estado_tp,
						vencible
					};

					// Datos del historial del plan de la tienda
					let datosHistorialTiendasPlanes = {
						tiendas_planes_planes_id: plan.id,
						tiendas_planes_tiendas_id: tienda_id,
						cant_pedidos: plan.num_pedidos,
						adicionales: adicionales,
						estado: estado_htp,
						vencible
					};

					// Valida si el plan a asignar es vencible
					if (vencible && dias_habilitados) {
						datosTiendasPlanes.dias_habilitados = dias_habilitados;
						datosHistorialTiendasPlanes.dias_habilitados = dias_habilitados;
					}

					// Verifica el estado pendiente de Tiendas planes a historial pendiente
					if (estado_tp === utilsController.getConstantByName('ESTADO_TIENDAS_PLANES', 'Pendiente').id) {
						datosHistorialTiendasPlanes.estado = utilsController.getConstantByName('ESTADO_HISTORIAL_PLANES', 'Pendiente').id;
					} else {
						// Valida si el plan a asignar es vencible
						if (vencible && dias_habilitados) {
							datosTiendasPlanes.fecha_inicio = new Date();
							datosTiendasPlanes.fecha_vencimiento = moment().add(dias_habilitados, 'days').calendar();
							datosHistorialTiendasPlanes.fecha_inicio = new Date();
							datosHistorialTiendasPlanes.fecha_vencimiento = moment().add(dias_habilitados, 'days').calendar();
						}
					}

					await TiendasPlanes.create(datosTiendasPlanes, {transaction: tr});
					// Se hace un registro de historial de los planes asignados
					await HistorialTiendasPlanes.create(datosHistorialTiendasPlanes, {transaction: tr});
					// always call commit at the end
					await tr.commit();

					res.redirect(`/administrador/tiendas/${tienda_id}/planes?info=correct`);

				} catch(err) {
					// always rollback 
					await tr.rollback();
					
					console.log('Error al guardar planes de la tienda: ' + tienda_id, JSON.stringify(err));
					return res.redirect(`/administrador/tiendas/${tienda_id}/planes?info=err`);
				}
			})	
		})
		.catch(err => {
			console.log('Error buscar el plan: ' + plan_id, JSON.stringify(err));
			return res.redirect(`/administrador/tiendas/${tienda_id}/planes?info=err`);
		});
	} else {
		return res.redirect(`/administrador/tiendas/${tienda_id}/planes?info=correct`);
	}

});

// (POST) Renueva un plan de tienda
router.post('/tiendas/:idt/planes/:idp/renew', async (req, res) => {

	const tienda_id = req.params.idt;
	const plan_id = req.params.idp;
	const adicionales = (req.body.adicionales)? req.body.adicionales : 0;
	const dias_habilitados = (req.body.dias_habilitados)? req.body.dias_habilitados : null;

	// Inicializacion de transaccion activa
	let trans = await models.sequelize.transaction();

	// Busca la tienda por el id
	Tiendas.findByPk(tienda_id)
	.then(tienda => {

		// Verifica si existe la tienda
		if (tienda) {
			
			// Busca la tienda plan 
			return TiendasPlanes.findOne({
				where: {
					tiendas_id: tienda_id,
					planes_id: plan_id
				}
			}, {transaction: trans});
		} else {
			console.log('RENOVACIÓN','No se encuentra la tienda');
			res.status(402).json({ msg : `No se encuentra la tienda ${tienda_id}`, state: false });
		}
	})
	.then(async tp => {

		// Vetifica si existe el plan asignado a la tienda
		if (tp) {

			let plan;
			
			// Se busca el plan por el id
			await Planes.findByPk(plan_id)
			.then(pl => plan = pl) // Se asigna el plan buscado para el manejo externo
			.catch(err => {
				console.log('ERROR EN RENOVACIÓN', err);
				res.status(402).json({ msg : `No se encuentra el plan ${plan_id}`, state: false });
			});

			// Estado Pendiente de historial
			let estado_htp = getConstant(Constants.ESTADO_HISTORIAL_PLANES, 'Pendiente').id;
			// Verifica si el estado que se va a renovar está en Inactivo
			if (tp.estado == getConstant(Constants.ESTADO_TIENDAS_PLANES, 'Inactivo').id) {

				try {
					// Busca un plan activo por tienda
					await TiendasPlanes.findOne({
						where: {
							tiendas_id: tienda_id,
							estado: getConstant(Constants.ESTADO_TIENDAS_PLANES, 'Activo').id
						}
					})
					.then(async tpa => {
						// Verifica si hay un plan de tienda activo
						if (tpa) {
							// Estado Pendiente para historial tienda plan
							estado_htp = getConstant(Constants.ESTADO_HISTORIAL_PLANES, 'Pendiente').id;
							// Estado Pendiente para tienda plan
							tp.estado = getConstant(Constants.ESTADO_TIENDAS_PLANES, 'Pendiente').id;
							// Si existen dias habilitados decimos que es vencible
							if (dias_habilitados) {
								tp.dias_habilitados = dias_habilitados;
								tp.vencible = true;
							}
						} else {
							// Estado activo para historial tienda plan
							estado_htp = getConstant(Constants.ESTADO_HISTORIAL_PLANES, 'En curso').id;
							// Datos de mondificación para tienda plan a renovación
							tp.estado = getConstant(Constants.ESTADO_TIENDAS_PLANES, 'Activo').id;
							tp.total_pedidos = parseInt(plan.num_pedidos) + parseInt(adicionales);
							tp.adicionales = parseInt(adicionales);
							// Si existen dias habilitados decimos que es vencible
							if (dias_habilitados) {
								tp.dias_habilitados = dias_habilitados;
								tp.vencible = true;
								tp.fecha_inicio = new Date();
								tp.fecha_vencimiento = moment().add(dias_habilitados, 'days').calendar();
							}
						}
						// Activa el estado de la tienda plan
						await tp.save();
					});
	
				} catch (err) {

					await trans.rollback();
					console.log('Error al guardar el estado de la tienda plan', err);
					res.status(402).json({ msg : 'Error al guardar el estado de la tienda plan.', state: false });
				}
				
			}

			try {
				// DAtos del historial de tiendas planes
				let dataHistorialTiendasPlanes = {
					tiendas_planes_planes_id: plan_id,
					tiendas_planes_tiendas_id: tienda_id,
					cant_pedidos: plan.num_pedidos,
					adicionales: adicionales,
					estado: estado_htp
				};

				// Valida si el plan es vencible cuando se ingresa los dias habilitados
				if (dias_habilitados) {
					dataHistorialTiendasPlanes.dias_habilitados = dias_habilitados;
					dataHistorialTiendasPlanes.vencible = true;
					// Valida si el estado del historial es en curso (Activo)
					if (estado_htp === getConstant(Constants.ESTADO_HISTORIAL_PLANES, 'En curso').id) {
						dataHistorialTiendasPlanes.fecha_inicio = new Date();
						dataHistorialTiendasPlanes.fecha_vencimiento = moment().add(dias_habilitados, 'days').calendar();
					}
				}

				// Crea un nuevo historial con estado pendiente
				await HistorialTiendasPlanes.create(dataHistorialTiendasPlanes, {transaction: trans}).then (async () => {
					await trans.commit();
					res.status(200).json({ msg: 'Plan renovado con exito', state: true });
				});	
			} catch (err) {
				await trans.rollback();
				console.log('RENOVACIÓN',err);
				res.status(402).json({ msg : 'Error al realizar la operación.', state: false });
			}

		} else {
			await trans.rollback();
			console.log('RENOVACIÓN',`No se encuentra el plan ${plan_id} asignado a la tienda ${tienda_id}`);
			res.status(402).json({ msg : `No se encuentra el plan ${plan_id} asignado a la tienda ${tienda_id}`, state: false });
		}
	})
	.catch(err => {
		console.log('RENOVACIÓN',err);
		res.status(402).json({ msg : 'Error al realizar la operación.', state: false });
	});

});

// (EDIT) Permite editar un plan NO USADO
router.get('/tiendas/:idt/planes/:idp/edit', (req, res) => {

	const id = req.params.id;
	const info = req.query.info;

	// Busca el plan por el ID
	Planes.findByPk(id)
	.then(plan => {
		if (plan) {
			// renderiza la vista con los datos del plan
			res.render('admin/pages/plans/edit', {
				plan: plan,
				types: Constants.TIPO_PLAN,
				info : info
			});	
		} else {
			return res.redirect('/administrador/planes?info=not_found');
		}
	})
	.catch(err => {
		console.log('Error al obtener el plan #' + id, JSON.stringify(err));
		return res.redirect('/administrador/planes?info=err');
	});		
	
});

/** CRUD TIENDA PLANES FINISH**/

/**
 * Obtiene la constante por el nombre
 */
function getConstant(constant, name) {
	return constant[constant.findIndex(x => x.name == name)];
}

// Buscar el elemento del la lista de constantes
function findConstantById (element, id) {
	return element[element.findIndex(x => x.id == id)];
}

module.exports = router;
