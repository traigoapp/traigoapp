const express = require('express');
const router = express.Router();
const fs = require('fs');
const nodemailer = require('nodemailer');
const verifier = require('email-verify');
//let infoCodes = verifier.infoCodes;
require('dotenv').config();
const jwt = require('jsonwebtoken');
let moment = require('moment');

const bcrypt = require('bcrypt');
const Constants = require('./constants');

const auth = require("../middlewares/auth");

const Database = require('../../config/database');
let db = new Database(); // Instancia de base de datos promise

const models = require('../models');
//const sequelize = models.index;
const Configuraciones = models.configuraciones; // new require for db object
const Rider = models.riders; // new require for db object
const Ajuste = models.ajustes; // new require for db object
const Planes = models.planes; // new require for db object planes
const TiendasPlanes = models.tiendas_planes; // new require for db object tiendas planes
const HistorialTiendasPlanes = models.historial_tiendas_planes; // new require for db object historial tiendas planes
const PedidosEmpresariales = models.pedidos_empresariales; // new require for db object predidos empresariales
const Sucursales = models.sucursales; // new require for db object sucrusales

const connection = db.getConnection();
let urlSocket = 'http://localhost:8002'

// Valida el tipo de entorno para saber la url del socket
if (process.env.NODE_ENV === 'production') {
	urlSocket = 'https://admin.traigo.com.co'; 
} else if (process.env.NODE_ENV === 'test') {
	urlSocket = 'http://traigo.com.co:8002'; 
}

const io = require('socket.io-client')(urlSocket);
const { Observable } = require('rxjs');

// Controlador de tiendas
router.use('/riders', require('../controllers/api/riders.controller'));
router.use('/orders', require('../controllers/api/orders.controller'));

// Test
/*let configMail = nodemailer.createTransport({
	host: 'smtp.mailtrap.io',
	port: 2525,
	//service: 'gmail',
	auth: {
		user: 'ddf73c02eca751',
		pass: 'faec8acbe7ffaf'
	},
	pool: true, // use pooled connection
	rateLimit: true, // enable to make sure we are limiting
	maxConnections: 1, // set limit to 1 connection only
	maxMessages: 3 // send 3 emails per second
});*/
// PROD
let configMail = nodemailer.createTransport({
	service: 'gmail',
	auth: {
		user: 'traigoapp@gmail.com',
		pass: 'traigoproyectohechoenelquindio'
	}
});

router.get('/encrptr', (req, res) => {
	let data = req.query.data;
	let hash = bcrypt.hashSync(data, 10);
	res.send(hash);
});


router.get('/example2', (req, res) => {
	res.send('Hollli puto');
});

/**
 *
 */
router.get('/test-riders', (req, res) => {

	/*return Rider.findAll({
		attributes: { exclude: ['contrasenia'] },
		include: 'direccion'
	})
	.then(contacts => {
		res.send(contacts);
	})
    .catch(err => {
      console.log('There was an error querying contacts', JSON.stringify(err))
      return res.send(err)
    });
*/

	let tienda_id = 2;
	/**
	* Obtienene la el plan de la tienda por estado
	* @param tienda_id identificador de la tienda
	* @param estado identificador del estado
	*/
	function getTiendaPlan(tienda_id, estado) {
		// Estados de planes de las tiendas
		const C_ESTADO = Constants.ESTADO_TIENDAS_PLANES;
		const C_ESTADO_HISTORIAL = Constants.ESTADO_HISTORIAL_PLANES;

		// Estado activo
		const estado_tp = C_ESTADO[C_ESTADO.findIndex(x => x.name == estado)].id;
		// Estado pendiente historial
		const estado_tph = C_ESTADO_HISTORIAL[C_ESTADO_HISTORIAL.findIndex(x => x.name == 'Pendiente')].id;

		return TiendasPlanes.findOne({
			where: {
				tiendas_id: tienda_id,
				estado: estado_tp
			}, include: [{
				model: HistorialTiendasPlanes,
				as: 'historial'
			}],
			order: [[{model: HistorialTiendasPlanes, as: 'historial'},'createdAt', 'desc']],
			limit: 1,
			subQuery: false,
		});
	}

	// Se buscan el plan activo asignado a la tienda 
	getTiendaPlan(tienda_id, 'Activo').then(tiendas_planes => {
		// Verifica si existe el plan asignado a la tienda
		console.log('Tiendas planes', tiendas_planes);
		return res.send(tiendas_planes);
	})
	.catch(err => {
		console.log('Error', err);
		return res.send(err);
	});

});


/**
 * Permite encriptar las contraseñas de las tablas
 * RIDERS, SUCURSALES, MODERADORES, ADMINISTRADORES y CLIENTES
 * 
 * @param req	parametros
 * @param res	opciones de respuesta
 */
router.get('/encriptar', (req, res) => {
	let promises = [];
	let namePromises = [];

	// Verifica la existencia del query params asocuado a la tabña deseada a converir
	if (req.query.r) {
		promises.push(db.query('SELECT * FROM riders'));
		namePromises.push({ name: 'riders', type: 'cedula' });
	}
	if (req.query.s) {
		promises.push(db.query('SELECT * FROM sucursales'));
		namePromises.push({ name: 'sucursales', type: 'idsucursal' });
	}
	if (req.query.m) {
		promises.push(db.query('SELECT * FROM moderadores'));
		namePromises.push({ name: 'moderadores', type: 'cedula' });
	}
	if (req.query.a) {
		promises.push(db.query('SELECT * FROM administradores'));
		namePromises.push({ name: 'administradores', type: 'cedula' });
	}
	if (req.query.c) {
		promises.push(db.query('SELECT * FROM clientes'));
		namePromises.push({ name: 'clientes', type: 'correo' });
	}

	// Verifica si no viene query params, se agregan todos para la encriptacion
	if (Object.keys(req.query).length === 0) {
		promises.push(db.query('SELECT * FROM riders'));
		namePromises.push({ name: 'riders', type: 'cedula' });

		promises.push(db.query('SELECT * FROM sucursales'));
		namePromises.push({ name: 'sucursales', type: 'idsucursal' });

		promises.push(db.query('SELECT * FROM moderadores'));
		namePromises.push({ name: 'moderadores', type: 'cedula' });

		promises.push(db.query('SELECT * FROM administradores'));
		namePromises.push({ name: 'administradores', type: 'cedula' });

		promises.push(db.query('SELECT * FROM clientes'));
		namePromises.push({ name: 'clientes', type: 'correo' });
	}

	// Ejecuta las consultas agregadas
	Promise.all(promises)
		.then(values => {
			for (let i = 0; i < values.length; i++) {
				const element = values[i];
				let cond = namePromises[i].type;
				let table = namePromises[i].name;

				for (let j = 0; j < element.length; j++) {
					const item = element[j];
					let pass = item.contrasenia;
					let condRes = item.cedula;

					if (cond === 'idsucursal') {
						condRes = item.idsucursal;
					} else if (cond === 'correo') {
						condRes = item.correo;
					}

					// Crea el hash del password
					let hash = bcrypt.hashSync(pass, 10);
					db.query('UPDATE ' + table + ' SET contrasenia = ? WHERE ' + cond + ' = ?', [hash, condRes])
						.catch(err => {
							console.log(err);
							res.send(err);
						});
				}
			}
			console.log('Termina encriptacion');
			res.send('Termina encriptacion');
		})
		.catch(err => {
			console.log(err);
			res.send(err);
		});

});

/**
 * Actuaiza las configuraciones adicionales en la actualizacion del software
 */
/*router.get('/configuraciones', (req, res) => {

	// Lista de promesas
	let promises = [];
	// Actualiza el tipo de sucursal a normal = 1
	promises.push(db.query('UPDATE sucursales SET tipo = ?', [1]));

	// Ejecuta las promesas
	Promise.all(promises)
		.then(() => {
			console.log('Termina actualizacion de tipo sucursales');
			res.send('Termina actualizacion de tipo sucursales');
		})
		.catch(err => {
			console.log(err);
			res.send(err);
		});

});
*/


/**
 * Obtiene el estado del rider por cedula
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns envia estado y la lista negra
 */
router.get('/obtenerestadorider', (req, res) => {
	let cedula = req.query.cedula;
	connection.query('select * from riders where cedula = ?',
		[cedula], (err, rows, fields) => {
			let rider = rows[0];
			if (err) {
				res.send(err);
			} else {
				let listanegra = false;
				if (rider.listanegra == 'S') {
					listanegra = true;
				}

				if (rider.activo == 'S') {
					res.send({
						estado: true,
						listanegra: listanegra
					});
				} else {
					res.send({
						estado: false,
						listanegra: listanegra
					});
				}
			}
		})
});

/**
 * Actualza la ubicacion actual de rider
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns error o correcto
 */
router.post('/actualizarcoordenadasrider', (req, res) => {
	let cedula = req.body.cedula;
	let latitude = req.body.latitude;
	let longitude = req.body.longitude;

	connection.query('update riders set latitude = ?, longitude = ? where cedula = ?',
		[latitude, longitude, cedula], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(err);
			} else {
				res.send({
					okey: 'okey'
				});
			}
		});
});


/**
 * Actualza la ubicacion actual de rider
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns error o correcto
 * Reforma de -> [actualizarcoordenadasrider]
 */
router.post('/update-actual-coordinates', (req, res) => {
	let id = req.body.id;
	let latitude = req.body.latitude;
	let longitude = req.body.longitude;

	db.query('UPDATE riders SET latitude = ?, longitude = ? WHERE cedula = ?', [id, latitude, longitude])
	.then(() => res.status(200).json('Ubicacion del rider actualizada'))
	.catch(err => {
		console.log(err);
		res.status(402).json('Error al actualizar las coordenadas del rider.');
	});
});


/**
 * Login riders
 * @deprecated esta funcionalidad ya existe en controlador de riders del api
 */
router.post('/login', (req, res) => {

	let user = req.body.username;
	let pass = req.body.password;

	console.log(req.body);

	if (!user || !pass) {
		return res.status(402).json({ error: 'Datos incompletos' });
	}

	//db.query('SELECT ri.*, di.direccion as direccion, di.idmunicipio as idmunicipio FROM riders ri JOIN direcciones di ON ri.iddireccion=di.iddireccion WHERE ri.cedula = ?', [user])
	db.query('SELECT ri.*, di.direccion as direccion, di.idmunicipio as idmunicipio, di.notas as notas FROM riders ri JOIN direcciones di ON ri.iddireccion=di.iddireccion WHERE ri.cedula = ?', [user])
		.then(rows => {	// Datos del rider
			// Verifica si no se encuentra el rider
			if (rows.length === 0) {
				console.log('Error', rows);
				return res.status(401).json({ error: 'No está registrado en nuestra plataforma.' });
			}

			// Rider obtenido
			const user = rows[0];

			/**
			 * Valida si el parametro es S = true o N = false
			 * @param {*} param 
			 */
			function validateBoolean(param) {
				return (param === 'S') ? true : false;
			}

			// Se valida si la contraseña es correcta
			if (bcrypt.compareSync(pass, user.contrasenia)) {
				// Se verifica si el rider está en lista negra
				if (user.listanegra == 'S') {
					return res.status(403).json({ error: 'No está habilitado para usar esta aplicación', message: user });
				}
				// Se crea el token con los datos de usuario
				jwt.sign({ user }, process.env.PRIVATE_JWT_KEY, (err, token) => {
					if (err) { // Error al crear el token
						return res.status(400).json({ error: 'Error al crear el token' });
					}

					/**
					 * Se Organiza el request para el envio de los datos
					 */
					user.cedula = parseInt(user.cedula)
					user.listanegra = validateBoolean(user.listanegra);
					user.activo = validateBoolean(user.activo);
					user.direction = {
						id: user.iddireccion,
						address: user.direccion,
						notes: user.notas,
						city_id: user.idmunicipio
					}

					// Se calcula la edad del rider
					user.edad = moment().diff(moment(user.fecha_nacimiento), 'years');

					// Se envía el token creado
					user.token = token;

					// Se eliminan datos innecesarios
					delete user.direccion;
					delete user.idmunicipio;
					delete user.notas;
					delete user.contrasenia;

					res.status(200).json(user);
				});
			} else { // Error cuando la contraseña no coincide
				res.status(402).json({ error: 'Contraseña invalida' });
			}
		}).catch(err => { // Error al obtener el rider
			console.log('Error', err);
			res.status(400).json({ error: err });
		});
});

/**
 * Inicio de session para el cliente
 */
router.post('/logincliente', (req, res) => {
	let user = req.body.username;
	let pass = req.body.password;

	if (user === 'undefined' || pass === 'undefined') {
		return res.status(402).json({ error: 'Datos incompletos' });
	}

	db.query('SELECT * FROM clientes WHERE correo = ?', [user])
		.then(rows => {
			// Verifica si no se encuentra el cliente
			if (rows.length === 0) {
				res.status(401).json({ error: 'No está registrado en nuestra plataforma.' });
			} else {
				const user = rows[0];
				// Se valida si la contraseña es correcta
				if (bcrypt.compareSync(pass, user.contrasenia)) {
					res.status(200).json({ message: user });
				} else { // Error cuando la contraseña no coincide
					res.status(402).json({ error: 'Contraseña invalida' });
				}
			}
		}).catch(err => { // Error al obtener el cliente
			console.log(err);
			res.status(400).json({ error: err });
		});

});


/**
 * Ejemplo de uso de recursos con token
 */
router.get('/exampleToken', verifyToken, (req, res) => {
	jwt.verify(req.token, process.env.PRIVATE_JWT_KEY, (err, authData) => {
		if (err) {
			res.sendStatus(403);
		} else {
			res.json({
				msm: 'Oki Doki',
				authData
			});
		}
	});
});

/**
 * Middleware para verificar el token enviado
 * 
 * Authotization: Bearer <token>  --Expresion del token [HEADER]
 * 
 * https://www.npmjs.com/package/jsonwebtoken
 * 
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
function verifyToken(req, res, next) {
	// Get auth token header value
	const bearerHeader = req.headers['authorization'];
	// Check if bearer is undefined
	if (typeof bearerHeader !== 'undefined') {
		// Split token space
		const bearer = bearerHeader.split(' ');
		// get token
		const token = bearer[1];
		// Send token
		req.token = token;
		// Next middleware
		next();
	} else {
		// Forbidden
		res.sendStatus(403);
	}

}

/**
 * Busca y obtiene el rider por la cedula (id)
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns envia los datos del rider
 */
router.get('/obtenerrider', (req, res) => {
	let usuario = req.query.cedula;

	connection.query('select * from riders where cedula = ?', [usuario], (err, rows, fields) => {
		if (err) {
			res.send(err);
		} else {
			res.send(rows);
		}
	});
});

/**
 * Ruta anterior [cambiarestadorider]
 * Cambia el estado del rider [Activo] - [Inactivo]
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns error o correcto
 */
router.post('/change-state-rider', auth, (req, res) => {

	let id = req.body.identfication;
	let state = req.body.state;

	db.query('UPDATE riders SET activo = ?, WHERE cedula = ?', [id, state])
		.then(res => {
			res.status(200).json('Se ha actualizado el estado del rider.');
		})
		.catch(err => {
			console.log(err);
			res.status(402).json('Error al actualizar el estado del rider.');
		});

});

/**
 * Obtiene todos los productos de la tienda
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns prodcutos del tienda
 */
router.get('/obtenerproductos', auth, (req, res) => {
	let idsucursal = req.query.tienda;
	connection.query('select * from productos p where p.idsucursal = ? and p.activo = ? and p.stock = ?',
		[idsucursal, 'S', 'S'], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(err);
			} else {
				res.send(rows);
			}
		});
});

/**
 * Obtiene las categorias de los productos
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns ecategorias de preductos
 */
router.get('/obtenercategoriasproductos', auth, (req, res) => {
	connection.query('select * from categoriasproductos', (err, rows, fields) => {
		if (err) {
			console.log(err);
			res.send(err);
		} else {
			res.send(rows);
		}
	});
});

/**
 * Obtiene las tiendas disponibles
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns lista de tiendas
 */
router.get('/obtenertiendas', auth, (req, res) => {
	let idcategoria = req.query.categoria;

	connection.query('SELECT s.*, (s.hora_inicial <= current_time() ' +
		'AND s.hora_final > current_time()) AS abierto, t.imagen AS imagen ' +
		'FROM sucursales s JOIN tiendas t ON t.idtienda = s.idtienda ' +
		'WHERE idcategoria = ? AND soloempresarial = ?',
		[idcategoria, 'N'], (err, rows, fields) => {
			if (err) {
				res.send(err);
			} else {
				res.send(rows);
			}
		});
})

function agregarImagenesDeseo(i, foto, idpedido, next) {
	if (foto.name != '') {
		let extension = foto.name.split('.').pop();
		let direccionImg = 'src/app/public/img/pedidosdeseos/' + idpedido + '-' + i + '.' + extension;
		fs.renameSync(foto.path, direccionImg);
		name = idpedido + '-' + i + '.' + extension;
		connection.query('insert into fotos (idpedido_deseo, foto) values(?, ?)',
			[idpedido, name], (err, rows, fields) => {
				if (err) {
					next(err);
				} else {
					next();
				}
			});
	}
}

function agregarImagenesMandado(i, foto, idpedido, next) {
	if (foto.name != '') {
		let extension = foto.name.split('.').pop();
		let direccionImg = 'src/app/public/img/pedidosmandados/' + idpedido + '-' + i + '.' + extension;
		fs.renameSync(foto.path, direccionImg);
		name = idpedido + '-' + i + '.' + extension;
		connection.query('insert into fotos (idpedido_encomienda, foto) values(?, ?)',
			[idpedido, name], (err, rows, fields) => {
				if (err) {
					next(err);
				} else {
					next();
				}
			});
	}
}

function agregarParada(parada, idpedido, costoparada, next) {
	connection.query('insert into paradas (iddireccion, costo, idpencomienda) values(?,?,?)',
		[parada.iddireccion, costoparada, idpedido], (err, rows, fields) => {
			next(err);
		});
}

/**
 * Permite crear un pedido mandado
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns error o correcto
 */
router.post('/realizarpedidomandado', auth, (req, res) => {
	let images = req.files;
	let paradas = JSON.parse(req.body.paradas);

	let dirOrigen = JSON.parse(req.body.direccionOrigen);
	let dirDestino = JSON.parse(req.body.direccionDestino);

	let costo_envio = req.body.costo_envio;
	let iddireccionorigen = dirOrigen.iddireccion;
	let iddirecciondestino = dirDestino.iddireccion;
	let comision_rider = req.body.comision_rider;
	let mandado = req.body.mandado;
	let correo = req.body.correo;
	let costoparada = req.body.costoparada;

	let imageslength = req.body.imageslength;
	connection.query('insert into pedidos_encomiendas (costo_envio, estado, iddireccionorigen, iddirecciondestino, comision_rider, comision_entregada, mandado, correocliente, fecha) values(?,?,?,?,?,?,?,?,?)',
		[costo_envio, 'En proceso', iddireccionorigen, iddirecciondestino, comision_rider, 'N', mandado, correo, new Date()], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(err);
			} else {
				let idpedido = rows.insertId;
				for (let i = 0; i < imageslength; i++) {
					agregarImagenesMandado(i, images['picture' + i], idpedido, (err) => {
						if (err) {
							console.log(err);
							return res.send(err);
						}
					});
				}
				for (let j = 0; j < paradas.length; j++) {
					agregarParada(paradas[j], idpedido, costoparada, (err) => {
						if (err) {
							console.log(err);
							return res.send(err);
						}
					});
				}
				res.send({
					okey: 'yes'
				});
			}
		});
});


/**
 * Permite crear un pedido deseo
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns error o correcto
 */
router.post('/realizarpedidodeseo', auth, (req, res) => {
	let images = req.files;
	let imageslength = req.body.imageslength;
	let correo = req.body.correo;
	let costo_envio = req.body.costo_envio;
	let comision = req.body.comision;
	let deseo = req.body.deseo;
	let direccion = JSON.parse(req.body.direccion);

	connection.query('insert into pedidos_deseos (costo_envio, comision_cliente, fecha, estado, iddireccion, deseo, comision_entregada, correocliente) values(?,?,?,?,?,?,?,?)',
		[costo_envio, comision, new Date(), 'En proceso', direccion.iddireccion, deseo, 'N', correo], (err, rows, fields) => {
			if (err) {
				res.send(err);
			} else {
				for (let i = 0; i < imageslength; i++) {
					agregarImagenesDeseo(i, images['picture' + i], rows.insertId, (err) => {
						if (err) {
							console.log(err);
						}
					});
				}
				res.send({
					okey: 'yes'
				});
			}
		});
});

/**
 * Obtiene los costos de traigo
 * TODO falta configurar en la aplicacion de riders, obtner por ciudad
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns error o correcto
 */
router.get('/costs', auth, (req, res) => {
	let idmunicipio = req.query.idmunicipio;
	let iddireccion = req.query.iddireccion;

	if (idmunicipio) {

		db.query('SELECT * FROM costos WHERE idmunicipio = ?', [idmunicipio])
			.then(rows => {
				if (rows[0]) {
					res.send(rows[0]);
				} else {
					getCosts(req, res);
				}
			})
			.catch(err => {
				console.log(err);
				res.status(400).json({ error: 'Error al obtener los costos' });
			});
	} else if (iddireccion) {

		db.query('SELECT idmunicipio FROM direcciones WHERE iddireccion = ?', [iddireccion])
			.then(rows => {
				if (rows.length === 0) {
					console.log(err);
					res.status(400).json({ error: 'Datos incorrectos' });
				}
				return db.query('SELECT * FROM costos WHERE idmunicipio = ?', [rows[0].idmunicipio]);
			})
			.then(rows => {
				if (rows[0]) {
					res.send(rows[0]);
				} else {
					getCosts(req, res);
				}
			})
			.catch(err => {
				console.log(err);
				res.status(400).json({ error: 'Error al obtener los costos' });
			});
	} else {
		getCosts(req, res);
	}

});

// Obtiene los primeros costos es decir el de armenia
function getCosts(req, res) {

	db.query('SELECT * FROM costos')
		.then(rows => {
			res.send(rows[0]);
		})
		.catch(err => {
			console.log(err);
			res.status(400).json({ error: 'Error al obtener los costos generales' });
		});

}

/**
 * Permite gregar una nueva dirección
 * TODO verificar agregar direccion son quemar el municipio
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns error o correcto
 */
router.post('/agregardireccion', (req, res) => {

	let correo = req.body.correo;
	let latitude = req.body.latitude;
	let longitude = req.body.longitude;
	let direccion = req.body.direccion;
	let notas = req.body.notas;
	let municipio = req.body.municipio;

	connection.query('insert into direcciones (idmunicipio, latitude, longitude, direccion, notas) values(?, ?,?,?,?)',
		[municipio, latitude, longitude, direccion, notas], (err, rows, fields) => {
			if (err) {
				res.send(err);
			} else {
				let iddireccion = rows.insertId;
				connection.query('insert into cliente_direccion (iddireccion, correocliente) values(?, ?)',
					[iddireccion, correo], (err, rows, fields) => {
						if (err) {
							console.log(err);
							res.send(err);
						} else {
							connection.query('select * from direcciones d join cliente_direccion cd on d.iddireccion = cd.iddireccion where cd.correocliente = ?', [correo], (err, rows, fields) => {
								if (err) {
									res.send(err);
								} else {
									res.send(rows);
								}
							});
						}
					});
			}
		});
});

/**
 * Obtiene todas las categorias de la tienda
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns lista de categorias
 */
router.get('/obtenercategorias', auth, (req, res) => {
	connection.query('select * from categorias', (err, rows, fields) => {
		if (err) {
			res.send(err);
		} else {
			res.send(rows);
		}
	});
});

/**
 * Obtiene la direccion del cliente por el correo (id cliente)
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns direccion del cliente
 */
router.get('/obtenerdireccionescliente', auth, (req, res) => {
	let correo = req.query.correo;
	connection.query('select * from direcciones d join cliente_direccion cd on d.iddireccion = cd.iddireccion where cd.correocliente = ?',
		[correo], (err, rows, fields) => {
			if (err) {
				res.send(err);
			} else {
				res.send(rows);
			}
		});
});

/**
 * Obtiene lista de pedidos en curso
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns lsita de pedidos en curso
 */
router.get('/obtenerpedidosencurso', auth, (req, res) => {

	let correo = req.query.usuario;

	let deseos, pedidos, mandados;
	connection.query('select pd.*, d.direccion as direccionentrega, d.notas as notasdireccionentrega ' +
		'from pedidos_deseos pd join direcciones d on d.iddireccion = pd.iddireccion join clientes c ' +
		'on c.correo = pd.correocliente where pd.estado != ? and pd.estado != ? ' +
		'and pd.estado != ? and c.correo = ? order by pd.fecha desc',
		['Entregado', 'Cancelado', 'Error', correo], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(err);
			} else {
				deseos = rows;
				for (let i = 0; i < deseos.length; i++) {
					deseos[i].type = 'deseo';
				}
				connection.query('select pd.*, c.*, do.direccion as direccionorigen, do.notas as notasdireccionorigen, ' +
					'do.latitude as latitudedireccionorigen, do.longitude as longitudedireccionorigen, dd.direccion ' +
					'as direcciondestino, dd.notas as notasdirecciondestino, dd.latitude as latitudedirecciondestino, ' +
					'dd.longitude as longitudedirecciondestino from pedidos_encomiendas pd join direcciones do ' +
					'on do.iddireccion = pd.iddireccionorigen join direcciones dd on dd.iddireccion = pd.iddirecciondestino ' +
					'join clientes c on c.correo = pd.correocliente where pd.estado != ? and pd.estado != ? ' +
					'and pd.estado != ? and c.correo = ? order by pd.fecha desc',
					['Entregado', 'Cancelado', 'Error', correo], (err, rows, fields) => {
						if (err) {
							console.log(err);
							res.send(err);
						} else {
							mandados = rows;
							for (let i = 0; i < mandados.length; i++) {
								mandados[i].type = 'mandado';
							}
							connection.query('select pd.*, c.*, d.*, ss.idsucursal as idsucursal, ss.nombre as nombresucursal, ' +
								'ss.tiempo_entrega as tiempo_entrega, count(p.idpclientes_productos) as cantidadproductos ' +
								'from pedidos_clientes pd join clientes c on c.correo = pd.correocliente join direcciones d ' +
								'on d.iddireccion = pd.iddireccion join pclientes_productos p on pd.idpedido_clientes = p.idpedido_clientes ' +
								'join productos pro on pro.idproducto = p.idproducto join sucursales ss on ss.idsucursal = pro.idsucursal ' +
								'where pd.estado != ? and pd.estado != ? and pd.estado != ? and c.correo = ? ' +
								'group by pd.idpedido_clientes, ss.idsucursal, ss.nombre, ss.tiempo_entrega order by pd.fecha desc',
								['Entregado', 'Cancelado', 'Error', correo], (err, rows, fields) => {
									if (err) {
										console.log(err);
										res.send(err);
									} else {
										pedidos = rows;
										for (let i = 0; i < pedidos.length; i++) {
											pedidos[i].type = 'tienda';
										}
										let todos = deseos.concat(pedidos, mandados);

										for (let i = 0; i < (todos.length - 1); i++) {
											for (let j = 0; j < (todos.length - i - 1); j++) {
												if (todos[j].fecha < todos[j + 1].fecha) {
													k = todos[j + 1];
													todos[j + 1] = todos[j];
													todos[j] = k;
												}
											}
										}
										res.send(todos);
									}
								});
						}
					});
			}
		});
});

/**
 * Valida la sesion de usuario
 */
router.get('/validate-session', (req, res) => {
	// Split token space
    // Get token
	const token = req.query.token;
	
	// Valida si se tiene el token por la url
	if (!token) {
		return res.status(400).send('No token');
	}

    try {
        //if can verify the token, set req.user and pass to next middleware
		const decoded = jwt.verify(token, process.env.PRIVATE_JWT_KEY);
		
		console.log('Valida token', decoded);
		
    } catch(err) {
        res.status(400).send('Invalid token.');
    }
});

/**
 * Obtiene todo los pedidos que estan en proceso
 * RIDERS
 * Filtrar por tipo de maletin, tipo de sucursal pedidos de la misma surucrsal especial
 * Ruta enterior [obtenerpedidosenproceso]
 * 
 */
router.get('/get-process-orders', (req, res) => {

	// Codigo de la aplicacion para validar
	const appcode = req.query.appcode;
	// Identificador del rider
	const riderId = req.query.rider;

	/*if (appcode != 1) {
		return res.status(401).json({ code: 1, err: 'Ups! Debes actualizar la aplicación para tomar pedidos' });
	} else if (!riderId) {
		return res.status(401).json('Datos enviados incompletos. Rider');
	}*/

	// Datos del rider consultado a consultar
	let dataRider;

	/**
	 * Inicia proceso de pedidos en curso
	 * 
	 * > Busca el rider por el ID
	 * > Obtiene la direccion del rider para el id del municipio
	 * > Ejecuta consultas de pedidos en curso
	 * > Verifica validaciones en los pedidos empresariales
	 */
	models.sequelize.query('SELECT tamanio_maletin, iddireccion FROM riders WHERE cedula = ?', { replacements: [riderId], type: models.sequelize.QueryTypes.SELECT })
		.then(async (rider) => {
			dataRider = rider[0];
			return await models.sequelize.query('SELECT idmunicipio FROM direcciones WHERE iddireccion = ?', { replacements: [dataRider.iddireccion], type: models.sequelize.QueryTypes.SELECT });
		})
		.then(async (direction) => {
			// Valida la direcion
			if (direction.length === 0) {
				console.log('Error direccion', riderId);
				res.status(401).json('Datos erroneos');
			}

			// Identificador del municipio
			const idmunicipio = direction[0].idmunicipio;
			// Condiciones de filtro
			const conditions = ['Entregado', 'Cancelado', 'Error', idmunicipio];

			/*pedidos = db.query('select pd.*, c.*, d.*, ss.idsucursal as idsucursal, ss.nombre as nombresucursal,' +
				' ss.tiempo_entrega as tiempo_entrega, count(p.idpclientes_productos) as cantidadproductos' +
				' from pedidos_clientes pd join clientes c on c.correo = pd.correocliente join direcciones d' +
				' on d.iddireccion = pd.iddireccion join pclientes_productos p on pd.idpedido_clientes = p.idpedido_clientes' +
				' join productos pro on pro.idproducto = p.idproducto join sucursales ss on ss.idsucursal = pro.idsucursal' +
				' where pd.estado != ? and pd.estado != ? and pd.estado != ? and d.idmunicipio = ? and isnull(idrider) group by pd.idpedido_clientes,' +
				' ss.idsucursal, ss.nombre, ss.tiempo_entrega order by pd.fecha desc',
				conditions);*/

			// Consulta de todos lo pedidos encomienda en curso
			const queryEntrust = `SELECT pen.idpedido_encomienda AS order_id, pen.estado AS state, pen.estados_pedido_id AS state_id, pen.correocliente AS customer_email,
						pen.mandado AS description, pen.idrider AS rider_id, pen.costo_envio AS shipping_cost, IF(pen.comision_entregada = 'S', true, false) AS deliver_commission,
						pen.comision_rider AS rider_commission, pen.fecha AS created_at, pen.iddirecciondestino AS destination_address_id, pen.iddireccionorigen AS origin_address_id,
						cli.nombre AS customer_name, cli.telefono AS customer_phone, odpe.iddireccion AS 'origin_direction.id', odpe.direccion AS 'origin_direction.address',
						odpe.notas AS 'origin_direction.notes', odpe.idmunicipio AS 'origin_direction.city_id', odpe.latitude AS 'origin_direction.latitude', odpe.longitude AS 'origin_direction.longitude',
						ddpe.iddireccion AS 'destination_direction.id', ddpe.direccion AS 'destination_direction.address', ddpe.notas AS 'destination_direction.notes', 
						ddpe.idmunicipio AS 'destination_direction.city_id', ddpe.latitude AS 'destination_direction.latitude', ddpe.longitude AS 'destination_direction.longitude'
						FROM pedidos_encomiendas pen JOIN clientes cli ON pen.correocliente = cli.correo JOIN direcciones odpe ON pen.iddireccionorigen = odpe.iddireccion
						JOIN direcciones ddpe ON pen.iddirecciondestino = ddpe.iddireccion WHERE pen.estado != ? AND pen.estado != ? AND pen.estado != ? AND ISNULL(pen.idrider) 
						AND ddpe.idmunicipio = ? ORDER BY pen.fecha ASC`;

			// Consulta de todos lo pedidos deseos en curso
			const queryOrderDesire = `SELECT ped.idpedido_deseo AS order_id, ped.correocliente AS customer_email, ped.comision_cliente AS rider_commission,
						ped.deseo AS description, ped.idrider AS rider_id, ped.estado AS state, ped.estados_pedido_id AS state_id,
						ped.iddireccion AS address_id, IF(ped.comision_entregada = 'S', true, false) AS deliver_commission, ped.costo_envio AS shipping_cost,
						ped.fecha AS created_at, cli.nombre AS customer_name, cli.telefono AS customer_phone,
						dpe.iddireccion AS 'direction.id', dpe.direccion AS 'direction.address', dpe.notas AS 'direction.notes', 
						dpe.idmunicipio AS 'direction.city_id', dpe.latitude AS 'direction.latitude', dpe.longitude AS 'direction.longitude'
						FROM pedidos_deseos ped JOIN clientes cli ON ped.correocliente = cli.correo JOIN direcciones dpe ON ped.iddireccion = dpe.iddireccion
						WHERE ped.estado != ? AND ped.estado != ? AND ped.estado != ? AND ISNULL(ped.idrider) AND dpe.idmunicipio = ? ORDER BY ped.fecha ASC`;

			// Consulta de todos lo pedidos empresariales en curso
			const queryOrderBussines = `SELECT pee.idpedido_empresarial AS order_id, pee.idsucursal AS office_id,  IF(pee.pagado = 'S', true, false) AS payed,
						pee.idrider AS rider_id, pee.comision_rider AS rider_commission, IF(pee.comision_entregada = 'S', true, false) AS deliver_commission, 
						IF(pee.pagado_sucursal = 'S', true, false) AS office_payed, IF(pee.datafono = 'S', true, false) AS dataphone, pee.fecha AS created_at,
						pee.nombre_cliente AS customer_name, pee.notas AS description, pee.valor AS value, pee.numero_factura AS num_invoice,
						pee.iddireccion AS address_id, pee.telefono AS customer_phone, pee.estado AS state, pee.estados_pedido_id AS state_id,
						pee.evidencia AS evidence, pee.tamanio_maletin AS size_bag, pee.ajustes_id AS adjust_id, IF(pee.retenido = 'S', true, false) AS detained,
						pee.ida_vuelta AS round_trip, pee.valor_ida_vuelta AS round_trip_value, pee.id_direccion_ida_vuelta AS address_id_round_trip,
						dpe.iddireccion AS 'direction.id', dpe.direccion AS 'direction.address', dpe.notas AS 'direction.notes', dpe.idmunicipio AS 'direction.city_id',
						dpe.latitude AS 'direction.latitude', dpe.longitude AS 'direction.longitude', dpiv.iddireccion AS 'round_trip_direction.id', 
						dpiv.direccion AS 'round_trip_direction.address', dpiv.notas AS 'round_trip_direction.notes', dpiv.idmunicipio AS 'round_trip_direction.city_id',
						dpiv.latitude AS 'round_trip_direction.latitude', dpiv.longitude AS 'round_trip_direction.longitude',
						suc.idsucursal AS 'office.id', suc.tipo AS 'office.type', suc.nombre AS 'office.name', suc.numero_punto AS 'office.phone',
						suc.tiempo_entrega AS 'office.delivery_time', suc.iddireccion AS 'office.address_id', IF(suc.retenido = 'S', true, false) AS 'office.detained',
						dsu.iddireccion AS 'office.direction.id', dsu.direccion AS 'office.direction.address', dsu.notas AS 'office.direction.notes',
						dsu.idmunicipio AS 'office.direction.city_id', dsu.latitude AS 'office.direction.latitude', dsu.longitude AS 'office.direction.longitude'
						FROM pedidos_empresariales pee JOIN direcciones dpe ON pee.iddireccion = dpe.iddireccion 
						LEFT JOIN direcciones dpiv ON pee.id_direccion_ida_vuelta = dpiv.iddireccion
						JOIN sucursales suc ON pee.idsucursal = suc.idsucursal
						JOIN direcciones dsu ON suc.iddireccion = dsu.iddireccion WHERE pee.estado != ? AND pee.estado != ? AND pee.estado != ? AND ISNULL(pee.idrider)
						AND dpe.idmunicipio = ? ORDER BY pee.fecha ASC LIMIT ?`;

			// Ejectua consulta de pedidos deseos
			const orderDesires = models.sequelize.query(queryOrderDesire, { replacements: conditions, nest: true, type: models.sequelize.QueryTypes.SELECT });
			// Ejectua consulta de pedidos encomienda
			const orderEntrust = models.sequelize.query(queryEntrust, { replacements: conditions, nest: true, type: models.sequelize.QueryTypes.SELECT });
			// Limite de pedidos para mostrar
			const limit_orders = await Configuraciones.findAll().then((config) => config[0].limit_business_orders);
			conditions.push(limit_orders);
			// Ejectua consulta de pedidos empresariales
			const orderBussines = models.sequelize.query(queryOrderBussines, { replacements: conditions, nest: true, type: models.sequelize.QueryTypes.SELECT });
			// Ejecuta promesas
			return Promise.all([orderDesires, orderEntrust, orderBussines]);

		})
		.then(async (result) => {

			// Se inicializan las listas de los pedidos
			const listDeseos = result[0];
			const listMandados = result[1]; 
			let listEmpresariales = [];

			// Id de sucursal
			let sucursal;
			
			// Obtiene el tipo de sucursal por el tipo de rider
			const officeType = await getOfficeIdByTypeOfRider(riderId, Constants.TIPO_SUCURSAL[1].id);
			// Valida si hay elementos
			if (officeType.length > 0) {
				// Asigna id de la sucursal tipo especial
				sucursal = officeType[0].idsucursal;
			}

			// Solo pedidos empresariales
			const vListBusinessOrders = result[2]; 
			// Validamos si hay pedidos tomads con la sucursal tipo 2
			if (sucursal) {
				for (let j = 0; j < vListBusinessOrders.length; j++) {
					// Verificamos si la sucursal de la orden tomada es igual
					// a la sucursal de las ordenes en curso
					if (sucursal === vListBusinessOrders[j].office_id) {
						// Se verifica viene con un tamaño de maletin establecido
						if (vListBusinessOrders[j].size_bag) {
							// Valida si el rider tiene tamano de maletin igual al de la orden o tiene cualquier tamano (0)
							if (vListBusinessOrders[j].size_bag === dataRider.tamanio_maletin 
								|| dataRider.tamanio_maletin === 0 
								|| dataRider.tamanio_maletin === null) {
								listEmpresariales.push(vListBusinessOrders[j]);
							}
						} else { // Para cualquier maletin
							listEmpresariales.push(vListBusinessOrders[j]);
						}
					}
				}
			} else {
				for (let j = 0; j < vListBusinessOrders.length; j++) {
					// Se verifica viene con un tamaño de maletin establecido
					if (vListBusinessOrders[j].size_bag) {
						
						// Valida si el rider tiene tamano de maletin igual al de la orden o tiene cualquier tamano (0)
						if (vListBusinessOrders[j].size_bag === dataRider.tamanio_maletin 
							|| dataRider.tamanio_maletin === 0 
							|| dataRider.tamanio_maletin === null) {
							listEmpresariales.push(vListBusinessOrders[j]);
						}
					} else { // Para cualquier maletin
						listEmpresariales.push(vListBusinessOrders[j]);
					}
				}
			}
			//console.log('Lista de pedidos empresariales', listEmpresariales);

			res.status(200).json({
				business: listEmpresariales,
				desire: (typeof listDeseos) ? listDeseos : [],
				entrust: (typeof listMandados) ? listMandados : []
			});
		})
		.catch((err) => {
			console.log('API Error al obtener las ordenes en proceso.', err);
			res.status(402).json('Error al obtener las ordenes en proceso.' + err);
		});

});

/**
 * Obtiene el primer pedido empresarial actual asocioado a el rider
 * filtrado por tipo de sucursal del pedido
 *
 * @param {String} riderId identificador del rider
 * @param {Number} officeType tipo de sucursal
 *
 * @returns id de sucursal
 */
function getOfficeIdByTypeOfRider(riderId, officeType) {
	return models.sequelize.query('SELECT pd.idsucursal FROM pedidos_empresariales pd JOIN sucursales s ON s.idsucursal = pd.idsucursal WHERE pd.estado != ? AND pd.estado != ? AND pd.estado != ? AND pd.idrider = ? AND s.tipo = ? ORDER BY pd.fecha DESC LIMIT 1 ', 
	{ replacements: ['Entregado', 'Cancelado', 'Error', riderId, officeType], type: models.sequelize.QueryTypes.SELECT });
	
}

// Ordenar fechas
// top-down implementation
function mergeSortTopDown(array) {
	if (array.length < 2) {
		return array;
	}

	let middle = Math.floor(array.length / 2);
	let left = array.slice(0, middle);
	let right = array.slice(middle);

	return mergeTopDown(mergeSortTopDown(left), mergeSortTopDown(right));
}

function mergeTopDown(left, right) {
	let array = [];

	while (left.length && right.length) {
		if (left[0].fecha < right[0].fecha) {
			array.push(left.shift());
		} else {
			array.push(right.shift());
		}
	}
	return array.concat(left.slice()).concat(right.slice());
}

//console.log(mergeSortTopDown(array.slice())); // => [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ]


router.get('/contarpedidostomados', auth, (req, res) => {
	let cedula = req.query.cedula;
	let total = 0;
	connection.query('select count(*) as total from pedidos_deseos pd  where pd.estado != ? and pd.estado != ? and pd.estado != ? and idrider = ?', ['Entregado', 'Cancelado', 'Error', cedula], (err, rows, fields) => {
		if (err) {
			res.send(err);
		} else {
			total += rows[0].total;
			connection.query('select count(*) as total from pedidos_encomiendas pd  where pd.estado != ? and pd.estado != ? and pd.estado != ? and idrider = ?', ['Entregado', 'Cancelado', 'Error', cedula], (err, rows, fields) => {
				if (err) {
					res.send(err);
				} else {
					total += rows[0].total;
					connection.query('select count(*) as total from pedidos_clientes pd where pd.estado != ? and pd.estado != ? and pd.estado != ? and idrider = ?', ['Entregado', 'Cancelado', 'Error', cedula], (err, rows, fields) => {
						if (err) {
							res.send(err);
						} else {
							total += rows[0].total;
							connection.query('select count(*) as total from pedidos_empresariales pd  where pd.estado != ? and pd.estado != ? and pd.estado != ?  and idrider = ?', ['Entregado', 'Cancelado', 'Error', cedula], (err, rows, fields) => {
								if (err) {
									console.log(err);
									res.send(err);
								} else {
									total += rows[0].total;
									res.send({ total: total });
								}
							});
						}
					});
				}
			});
		}
	});
});

/**
 * Sin uso
 * @deprecated
 */
router.get('/obtenerpedidos', auth, (req, res) => {
	let deseos, pedidos, mandados, empresariales;
	connection.query('select pd.*, d.direccion as direccionentrega, d.notas as notasdireccionentrega, d.latitude as latitudeentrega, d.longitude as longitudeentrega from pedidos_deseos pd join direcciones d on d.iddireccion = pd.iddireccion where pd.estado != ? and pd.estado != ? and pd.estado != ? and isnull(idrider) order by pd.fecha desc', ['Entregado', 'Cancelado', 'Error'], (err, rows, fields) => {
		if (err) {
			res.send(err);
		} else {
			deseos = rows;
			for (let i = 0; i < deseos.length; i++) {
				deseos[i].type = 'deseo';
			}
			connection.query('select pd.*, do.direccion as direccionorigen, do.notas as notasdireccionorigen, do.latitude as latitudedireccionorigen, do.longitude as longitudedireccionorigen, dd.direccion as direcciondestino, dd.notas as notasdirecciondestino, dd.latitude as latitudedirecciondestino, dd.longitude as longitudedirecciondestino from pedidos_encomiendas pd join direcciones do on do.iddireccion = pd.iddireccionorigen join direcciones dd on dd.iddireccion = pd.iddirecciondestino where pd.estado != ? and pd.estado != ? and pd.estado != ? and isnull(idrider) order by pd.fecha desc', ['Entregado', 'Cancelado', 'Error'], (err, rows, fields) => {
				if (err) {
					console.log(err);
					res.send(err);
				} else {
					mandados = rows;

					for (let i = 0; i < mandados.length; i++) {
						mandados[i].type = 'mandado';
					}
					connection.query('select pd.*, c.*, d.*, ss.idsucursal as idsucursal, ss.nombre as nombresucursal, ss.tiempo_entrega as tiempo_entrega, count(p.idpclientes_productos) as cantidadproductos from pedidos_clientes pd join clientes c on c.correo = pd.correocliente join direcciones d on d.iddireccion = pd.iddireccion join pclientes_productos p on pd.idpedido_clientes = p.idpedido_clientes join productos pro on pro.idproducto = p.idproducto join sucursales ss on ss.idsucursal = pro.idsucursal where pd.estado != ? and pd.estado != ? and pd.estado != ? and isnull(idrider) group by pd.idpedido_clientes, ss.idsucursal, ss.nombre, ss.tiempo_entrega order by pd.fecha desc', ['Entregado', 'Cancelado', 'Error'], (err, rows, fields) => {
						if (err) {
							res.send(err);
						} else {
							pedidos = rows;
							for (let i = 0; i < pedidos.length; i++) {
								pedidos[i].type = 'tienda';
							}

							connection.query('select pd.*, s.nombre as nombresucursal, s.numero_punto as numerosucursal, s.retenido as retiene, s.tiempo_entrega as tiempo_entrega, s.costo_envio as costoenvio, ds.iddireccion as iddireccionsucursal, ds.direccion as direccionsucursal, ds.notas as notasdireccionsucursal, ds.latitude as latitudesucursal, ds.longitude as longitudesucursal, d.iddireccion as iddireccionentrega, d.direccion as direccionentrega, d.notas as notasdireccionentrega from pedidos_empresariales pd join direcciones d on d.iddireccion = pd.iddireccion join sucursales s on s.idsucursal = pd.idsucursal join direcciones ds on ds.iddireccion = s.iddireccion where pd.estado != ? and pd.estado != ? and pd.estado != ?  and isnull(idrider) order by pd.fecha desc', ['Entregado', 'Cancelado', 'Error'], (err, rows, fields) => {
								if (err) {
									console.log(err);
									res.send(err);
								} else {
									empresariales = rows;

									for (let i = 0; i < empresariales.length; i++) {
										empresariales[i].type = 'empresariales';
									}

									let todos = deseos.concat(pedidos, mandados, empresariales);
									for (let i = 0; i < (todos.length - 1); i++) {
										for (let j = 0; j < (todos.length - i - 1); j++) {
											if (todos[j].fecha > todos[j + 1].fecha) {
												k = todos[j + 1];
												todos[j + 1] = todos[j];
												todos[j] = k;
											}
										}
									}
									res.send(todos);
								}
							});
						}
					});

				}
			});
		}
	});
});

/**
 * Obtiene lista de pedidos del rider
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns lsita de pedidos del rider
 */
router.get('/get-assigned-orders', async (req, res) => {

	// Identificador del rider
	const riderId = req.query.rider;

	// Codigo de la aplicacion para validar
	const appcode = req.query.appcode;

	/*if (appcode != 1) {
		return res.status(401).json({ code: 1, err: 'Ups! Debes actualizar la aplicación para tomar pedidos' });
	} else if (!riderId) {
		return res.status(401).json('Datos enviados incompletos. Rider');
	}*/

	// Datos del rider consultado a consultar
	let dataRider;

	/**
	 * Inicia proceso de pedidos en curso
	 * 
	 * > Busca el rider por el ID
	 * > Obtiene la direccion del rider para el id del municipio
	 * > Ejecuta consultas de pedidos en curso
	 * > Verifica validaciones en los pedidos empresariales
	 */
	models.sequelize.query('SELECT tamanio_maletin, iddireccion FROM riders WHERE cedula = ?', { replacements: [riderId], type: models.sequelize.QueryTypes.SELECT })
		.then(async (rider) => {
			dataRider = rider[0];
			return await models.sequelize.query('SELECT idmunicipio FROM direcciones WHERE iddireccion = ?', { replacements: [dataRider.iddireccion], type: models.sequelize.QueryTypes.SELECT });
		})
		.then(async (direction) => {
			// Valida la direcion
			if (direction.length === 0) {
				console.log('Error direccion', riderId);
				res.status(401).json('Datos erroneos');
			}

			// Identificador del municipio
			const idmunicipio = direction[0].idmunicipio;
			// Condiciones de filtro
			const conditions = ['Entregado', 'Cancelado', 'Error', riderId, idmunicipio];

			/*pedidos = db.query('select pd.*, c.*, d.*, ss.idsucursal as idsucursal, ss.nombre as nombresucursal,' +
				' ss.tiempo_entrega as tiempo_entrega, count(p.idpclientes_productos) as cantidadproductos' +
				' from pedidos_clientes pd join clientes c on c.correo = pd.correocliente join direcciones d' +
				' on d.iddireccion = pd.iddireccion join pclientes_productos p on pd.idpedido_clientes = p.idpedido_clientes' +
				' join productos pro on pro.idproducto = p.idproducto join sucursales ss on ss.idsucursal = pro.idsucursal' +
				' where pd.estado != ? and pd.estado != ? and pd.estado != ? and d.idmunicipio = ? and isnull(idrider) group by pd.idpedido_clientes,' +
				' ss.idsucursal, ss.nombre, ss.tiempo_entrega order by pd.fecha desc',
				conditions);*/

			// Consulta de todos lo pedidos encomienda en curso
			const queryEntrust = `SELECT pen.idpedido_encomienda AS order_id, pen.estado AS state, pen.estados_pedido_id AS state_id, pen.correocliente AS customer_email,
							pen.mandado AS description, pen.idrider AS rider_id, pen.costo_envio AS shipping_cost, IF(pen.comision_entregada = 'S', true, false) AS deliver_commission,
							pen.comision_rider AS rider_commission, pen.fecha AS created_at, pen.iddirecciondestino AS destination_address_id, pen.iddireccionorigen AS origin_address_id,
							cli.nombre AS customer_name, cli.telefono AS customer_phone, odpe.iddireccion AS 'origin_direction.id', odpe.direccion AS 'origin_direction.address',
							odpe.notas AS 'origin_direction.notes', odpe.idmunicipio AS 'origin_direction.city_id', odpe.latitude AS 'origin_direction.latitude', odpe.longitude AS 'origin_direction.longitude',
							ddpe.iddireccion AS 'destination_direction.id', ddpe.direccion AS 'destination_direction.address', ddpe.notas AS 'destination_direction.notes', 
							ddpe.idmunicipio AS 'destination_direction.city_id', ddpe.latitude AS 'destination_direction.latitude', ddpe.longitude AS 'destination_direction.longitude'
							FROM pedidos_encomiendas pen JOIN clientes cli ON pen.correocliente = cli.correo JOIN direcciones odpe ON pen.iddireccionorigen = odpe.iddireccion
							JOIN direcciones ddpe ON pen.iddirecciondestino = ddpe.iddireccion WHERE pen.estado != ? AND pen.estado != ? AND pen.estado != ? 
							AND pen.idrider = ? AND ddpe.idmunicipio = ? ORDER BY pen.fecha ASC`;
				
			// Consulta de todos lo pedidos deseos en curso
			const queryOrderDesire = `SELECT ped.idpedido_deseo AS order_id, ped.correocliente AS customer_email, ped.comision_cliente AS rider_commission,
							ped.deseo AS description, ped.idrider AS rider_id, ped.estado AS state, ped.estados_pedido_id AS state_id,
							ped.iddireccion AS address_id, IF(ped.comision_entregada = 'S', true, false) AS deliver_commission, ped.costo_envio AS shipping_cost,
							ped.fecha AS created_at, cli.nombre AS customer_name, cli.telefono AS customer_phone,
							dpe.iddireccion AS 'direction.id', dpe.direccion AS 'direction.address', dpe.notas AS 'direction.notes', 
							dpe.idmunicipio AS 'direction.city_id', dpe.latitude AS 'direction.latitude', dpe.longitude AS 'direction.longitude'
							FROM pedidos_deseos ped JOIN clientes cli ON ped.correocliente = cli.correo JOIN direcciones dpe ON ped.iddireccion = dpe.iddireccion
							WHERE ped.estado != ? AND ped.estado != ? AND ped.estado != ? AND ped.idrider = ? AND dpe.idmunicipio = ? ORDER BY ped.fecha ASC`;

			// Consulta de todos lo pedidos empresariales en curso
			const queryOrderBussines = `SELECT pee.idpedido_empresarial AS order_id, pee.idsucursal AS office_id,  IF(pee.pagado = 'S', true, false) AS payed,
							pee.idrider AS rider_id, pee.comision_rider AS rider_commission, IF(pee.comision_entregada = 'S', true, false) AS deliver_commission, 
							IF(pee.pagado_sucursal = 'S', true, false) AS office_payed, IF(pee.datafono = 'S', true, false) AS dataphone, pee.fecha AS created_at,
							suc.costo_envio AS shipping_cost, pee.nombre_cliente AS customer_name, pee.notas AS description, pee.valor AS value, pee.numero_factura AS num_invoice,
							pee.iddireccion AS address_id, pee.telefono AS customer_phone, pee.estado AS state, pee.estados_pedido_id AS state_id,
							pee.evidencia AS evidence, pee.tamanio_maletin AS size_bag, pee.ajustes_id AS adjust_id, IF(pee.retenido = 'S', true, false) AS detained,
							pee.ida_vuelta AS round_trip, pee.valor_ida_vuelta AS round_trip_value, pee.id_direccion_ida_vuelta AS address_id_round_trip,
							dpe.iddireccion AS 'direction.id', dpe.direccion AS 'direction.address', dpe.notas AS 'direction.notes', dpe.idmunicipio AS 'direction.city_id',
							dpe.latitude AS 'direction.latitude', dpe.longitude AS 'direction.longitude', dpiv.iddireccion AS 'round_trip_direction.id', 
							dpiv.direccion AS 'round_trip_direction.address', dpiv.notas AS 'round_trip_direction.notes', dpiv.idmunicipio AS 'round_trip_direction.city_id',
							dpiv.latitude AS 'round_trip_direction.latitude', dpiv.longitude AS 'round_trip_direction.longitude',
							suc.idsucursal AS 'office.id', suc.tipo AS 'office.type', suc.nombre AS 'office.name', suc.numero_punto AS 'office.phone',
							suc.tiempo_entrega AS 'office.delivery_time', suc.iddireccion AS 'office.address_id', IF(suc.retenido = 'S', true, false) AS 'office.detained',
							dsu.iddireccion AS 'office.direction.id', dsu.direccion AS 'office.direction.address', dsu.notas AS 'office.direction.notes',
							dsu.idmunicipio AS 'office.direction.city_id', dsu.latitude AS 'office.direction.latitude', dsu.longitude AS 'office.direction.longitude'
							FROM pedidos_empresariales pee JOIN direcciones dpe ON pee.iddireccion = dpe.iddireccion 
							LEFT JOIN direcciones dpiv ON pee.id_direccion_ida_vuelta = dpiv.iddireccion
							JOIN sucursales suc ON pee.idsucursal = suc.idsucursal
							JOIN direcciones dsu ON suc.iddireccion = dsu.iddireccion WHERE pee.estado != ? AND pee.estado != ? AND pee.estado != ?
							AND pee.idrider = ? ORDER BY pee.fecha ASC`;

			// Ejectua consulta de pedidos deseos
			const orderDesires = models.sequelize.query(queryOrderDesire, { replacements: conditions, nest: true, type: models.sequelize.QueryTypes.SELECT });
			// Ejectua consulta de pedidos encomienda
			const orderEntrust = models.sequelize.query(queryEntrust, { replacements: conditions, nest: true, type: models.sequelize.QueryTypes.SELECT });
			// Ejectua consulta de pedidos empresariales
			const orderBussines = models.sequelize.query(queryOrderBussines, { replacements: conditions, nest: true, type: models.sequelize.QueryTypes.SELECT });
			// Ejecuta promesas
			return Promise.all([orderDesires, orderEntrust, orderBussines]);

		})
		.then(async (result) => {

			// Se inicializan las listas de los pedidos
			const listDeseos = result[0];
			const listMandados = result[1]; 
			const listEmpresariales = result[2]; ;

			res.status(200).json({
				business: listEmpresariales,
				desire: (typeof listDeseos) ? listDeseos : [],
				entrust: (typeof listMandados) ? listMandados : []
			});
		})
		.catch((err) => {
			console.log('API Error al obtener las ordenes en proceso.', err);
			res.status(402).json('Error al obtener las ordenes en proceso.' + err);
		});

});

/**
 * Obtiene lista de pedidos del rider
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns lsita de pedidos del rider
 */
router.get('/obtenerpedidosrider', auth, (req, res) => {

	let cedula = req.query.cedula;

	/*if (cedula === 'undefined') {
		console.log(err);
		return res.send(err);
	}*/
	getOrdersRider(cedula)
		.then(result => {
			const orderType = ['deseo', 'mandado', 'tienda', 'empresariales'];
			let todos = [];

			for (let i = 0; i < result.length; i++) {
				let order = result[i];
				for (let j = 0; j < order.length; j++) {
					order[j].type = orderType[i];
					todos.push(order[j]);
				}
			}
			// Ordenar por fechas
			let order = mergeSortTopDown(todos.slice());
			res.send(order);
		})
		.catch(err => {
			console.log(err);
			res.send(err);
		});

});

/**
 * Obtiene todos los peidos asignados del rider
 * 
 * @param cedula identificador del rider
 * @returns promises  ['deseo', 'mandado', 'tienda', 'empresariales']
 */
function getOrdersRider(cedula) {
	let deseos, pedidos, mandados, empresariales;

	const conditions = ['Entregado', 'Cancelado', 'Error', cedula];

	deseos = db.query('select pd.*, d.direccion as direccionentrega, d.notas as notasdireccionentrega, d.latitude as latitudeentrega, d.longitude as longitudeentrega from pedidos_deseos pd join direcciones d on d.iddireccion = pd.iddireccion where pd.estado != ? and pd.estado != ? and pd.estado != ? and idrider = ? order by pd.fecha desc', conditions);
	mandados = db.query('select pd.*, do.direccion as direccionorigen, do.notas as notasdireccionorigen, do.latitude as latitudedireccionorigen, do.longitude as longitudedireccionorigen, dd.direccion as direcciondestino, dd.notas as notasdirecciondestino, dd.latitude as latitudedirecciondestino, dd.longitude as longitudedirecciondestino from pedidos_encomiendas pd join direcciones do on do.iddireccion = pd.iddireccionorigen join direcciones dd on dd.iddireccion = pd.iddirecciondestino where pd.estado != ? and pd.estado != ? and pd.estado != ? and idrider = ? order by pd.fecha desc', conditions);
	pedidos = db.query('select pd.*, c.*, d.*, ss.idsucursal as idsucursal, ss.nombre as nombresucursal, ss.tiempo_entrega as tiempo_entrega, count(p.idpclientes_productos) as cantidadproductos from pedidos_clientes pd join clientes c on c.correo = pd.correocliente join direcciones d on d.iddireccion = pd.iddireccion join pclientes_productos p on pd.idpedido_clientes = p.idpedido_clientes join productos pro on pro.idproducto = p.idproducto join sucursales ss on ss.idsucursal = pro.idsucursal where pd.estado != ? and pd.estado != ? and pd.estado != ? and idrider = ? group by pd.idpedido_clientes, ss.idsucursal, ss.nombre, ss.tiempo_entrega order by pd.fecha desc', conditions);
	empresariales = db.query('SELECT pd.idpedido_empresarial, pd.idsucursal, IF(pagado=\'S\', true, false) pagado, pd.idrider, pd.comision_rider, IF(pd.comision_entregada=\'S\', true, false) comision_entregada, IF(pd.pagado_sucursal=\'S\', true, false) pagado_sucursal, pd.nombre_cliente, pd.notas, pd.valor, pd.numero_factura, pd.iddireccion, pd.telefono, pd.estado, pd.fecha, IF(pd.datafono=\'S\', true, false) datafono, IF(pd.retenido=\'S\', true, false) retenido, pd.costo_envio, pd.estados_pedido_id, pd.evidencia, pd.tamanio_maletin, d.direccion AS direccionentrega, d.notas AS notasdireccionentrega, s.tipo AS tipoSucursal, s.nombre AS nombresucursal, IF(s.retenido = \'S\', true, false) retiene, s.tiempo_entrega AS tiempo_entrega,	s.numero_punto AS numero_sucursal, ds.iddireccion AS iddireccionsucursal, ds.direccion AS direccionsucursal, ds.notas AS notasdireccionsucursal, ds.idmunicipio as city_id, ds.latitude AS latitudesucursal, ds.longitude AS longitudesucursal FROM pedidos_empresariales pd JOIN direcciones d ON d.iddireccion = pd.iddireccion JOIN sucursales s ON s.idsucursal = pd.idsucursal JOIN direcciones ds ON ds.iddireccion = s.iddireccion WHERE pd.estado != ? AND pd.estado != ? AND pd.estado != ? AND idrider = ? ORDER BY pd.fecha DESC', conditions);

	return Promise.all([deseos, mandados, pedidos, empresariales]);
}

/**
 * Obtiene cliente por correo
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns cliente
 */
router.get('/obtenercliente', (req, res) => {
	let correo = req.query.correo;

	connection.query('select * from clientes where correo = ?', [correo], (err, rows, fields) => {
		if (err) {
			res.send(err);
		} else {
			res.send(rows);
		}
	});
});

/**
 * Se crea un nuevo cliente
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns direccion del cliente
 */
router.post('/agregarcliente', (req, res) => {

	let nombre = req.body.nombre;
	let correo = req.body.correo;
	let telefono = req.body.telefono;
	let contrasenia = req.body.contrasenia;
	let latitude = req.body.latitude;
	let longitude = req.body.longitude;
	let direccion = req.body.direccion;
	let notas = req.body.notas;
	let municipio = req.body.municipio;

	let iddireccion;

	let direccionObj = { idmunicipio: municipio, latitude: latitude, longitude: longitude, direccion: direccion, notas: notas };
	connection.query('insert into direcciones (idmunicipio, latitude, longitude, direccion, notas) values(?,?,?,?,?)',
		[municipio, latitude, longitude, direccion, notas], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(err);
			} else {
				iddireccion = rows.insertId;
				direccionObj['iddireccion'] = iddireccion;
				let hash = bcrypt.hashSync(contrasenia, 10);
				connection.query('insert into clientes (nombre, telefono, correo, contrasenia) values(?, ?, ?, ?)',
					[nombre, telefono, correo, hash], (err, rows, fields) => {
						if (err) {
							console.log(err);
							res.send(err);
						} else {
							connection.query('insert into cliente_direccion (iddireccion, correocliente) values(?, ?)',
								[iddireccion, correo], (err, rows, fields) => {
									if (err) {
										console.log(err);
										res.send(err);
									} else {
										connection.query('select * from clientes where correo = ?', [correo], (err, respuesta, fields) => {
											if (err) {
												console.log(err);
												res.send(err);
											} else {
												respuesta.push(direccionObj);
												res.send(respuesta);
											}
										});
									}
								});
						}
					});
			}
		});
});

/**
 * Obtiene la sucursal por id
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns sucursal
 */
router.get('/obtenersucursal', auth, (req, res) => {
	let idsucursal = req.query.idsucursal;
	connection.query('select * from sucursales s join direcciones d on d.iddireccion = s.iddireccion where idsucursal = ?',
		[idsucursal], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(err);
			} else {
				res.send(rows);
			}
		});
});

/**
 * Actualiza los datos del pedido empresarial
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns OK | ERR
 */
router.post('/actualizarpedidoempresarial', auth, (req, res) => {

	let id = req.body.id;
	let address_id = req.body.address_id;
	let customerName = req.body.customerName;
	let customerPhone = req.body.customerPhone;
	let value = req.body.value;
	let direction = req.body.direction;

	//let evidencia = req.body.evidencia; // Foto de la evidencia

	db.query('UPDATE pedidos_empresariales SET nombre_cliente = ?, telefono = ?, valor = ?' +
		' WHERE idpedido_empresarial = ?', [customerName, customerPhone, value, id])
		.then(() => {
			return db.query('UPDATE direcciones SET direccion = ? WHERE iddireccion = ?', [direction, address_id]);
		})
		.then(() => {
			res.status(200).json({ message: 'Pedido actualizado con exito' });
		})
		.catch(err => {
			console.log(err);
			res.status(402).json({ error: err });
		});

});

/**
 * Cambia el estado de pedido empresarial
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns pedido actualizado
 */
router.post('/cambiarestadopedidoempresarial', auth, (req, res) => {
	let idrider = req.body.cedularider;
	let idpedidoempresarial = req.body.idpedidoempresarial;
	let estado = req.body.estado;

	console.log(req.body);

	connection.query('update pedidos_empresariales set idrider = ?, estado = ? where idpedido_empresarial = ? and (isnull(idrider) or idrider = ?)',
		[idrider, estado, idpedidoempresarial, idrider], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(err);
			} else {
				if (io) {
					console.log("Emite cambio de estado");
					io.emit("ordershop");
				}
				res.send({
					okey: 'yes',
					update: rows.affectedRows
				});
			}
		});
});

/**
 * Cambia el estado de pedido del cliente
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns pedido actualizado
 */
router.post('/cambiarestadopedidocliente', auth, (req, res) => {
	let idrider = req.body.cedularider;
	let idpedido = req.body.idpedidocliente;
	let estado = req.body.estado;
	connection.query('update pedidos_clientes set idrider = ?, estado = ? where idpedido_clientes = ? and (isnull(idrider) or idrider = ?)',
		[idrider, estado, idpedido, idrider], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(err);
			} else {
				res.send({
					okey: 'yes',
					update: rows.affectedRows
				});
			}
		});
});

/**
 * Cambia el estado de pedido encomienda
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns pedido actualizado
 */
router.post('/cambiarestadopedidoencomienda', auth, (req, res) => {
	let idrider = req.body.cedularider;
	let idpedidoencomienda = req.body.idpedidoencomienda;
	let estado = req.body.estado;
	connection.query('update pedidos_encomiendas set idrider = ?, estado = ? where idpedido_encomienda = ? and (isnull(idrider) or idrider = ?)',
		[idrider, estado, idpedidoencomienda, idrider], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(err);
			} else {
				res.send({
					okey: 'yes',
					update: rows.affectedRows
				});
			}
		});
});

/**
 * Cambia el estado de pedido deseo
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns pedido actualizado
 */
router.post('/cambiarestadopedidodeseo', auth, (req, res) => {

	let idrider = req.body.cedularider;
	let idpedidodeseo = req.body.idpedidodeseo;
	let estado = req.body.estado;
	connection.query('update pedidos_deseos set idrider = ?, estado = ? where idpedido_deseo = ? and (isnull(idrider) or idrider = ?)',
		[idrider, estado, idpedidodeseo, idrider], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(err);
			} else {
				res.send({
					okey: 'yes',
					update: rows.affectedRows
				});
			}
		});
});

/**
 * Obtiene pedido empresarial por id
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns pedido empresarial
 */
router.get('/obtenerestadopedidoempresarial', auth, (req, res) => {
	let idpedido = req.query.pedido;
	connection.query('select * from pedidos_empresariales where idpedido_empresarial = ?',
		[idpedido], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(err);
			} else {
				res.send({ estado: rows[0] }.estado);
			}
		});
});

/**
 * Obtiene pedido deseo por id
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns pedido deseo
 */
router.get('/obtenerestadopedidodeseo', auth, (req, res) => {
	let idpedido = req.query.pedido;
	connection.query('select * from pedidos_deseos where idpedido_deseo = ?',
		[idpedido], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(err);
			} else {
				res.send({ estado: rows[0] }.estado);
			}
		});
});

/**
 * Obtiene pedido cliente por id
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns pedido cliente
 */
router.get('/obtenerestadopedidocliente', auth, (req, res) => {
	let idpedido = req.query.pedido;
	connection.query('select * from pedidos_clientes where idpedido_clientes = ?',
		[idpedido], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(err);
			} else {
				res.send({ estado: rows[0] }.estado);
			}
		});
});

/**
 * Obtiene los productos del pedido del cliente
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns productos del pedido cliente
 */
router.get('/obtenerproductospedido', auth, (req, res) => {
	let idpedido = req.query.pedido;
	connection.query('select pp.*, p.* from pclientes_productos pp join productos p on pp.idproducto = p.idproducto where pp.idpedido_clientes = ?',
		[idpedido], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(err);
			} else {
				res.send(rows);
			}
		});
});

/**
 * Obtiene el valor total del pedio del cliente
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns total del cliente
 */
router.get('/obtenertotalpedidocliente', auth, (req, res) => {
	let idpedido = req.query.pedido;
	let totalelementos = 0, totalselecciones = 0, totaladiciones = 0, totalproductos = 0;
	connection.query('select sum(e.precio) as totalelementos from pclientes_productos pp join pclientes_prod_elementos ppe on pp.idpclientes_productos = ppe.idpclientes_productos join elementos e on e.idelemento = ppe.idelemento where pp.idpedido_clientes = ? group by pp.idpedido_clientes',
		[idpedido], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(ee);
			} else {
				if (rows.length != 0) {
					totalelementos = rows[0].totalelementos;
				}
				connection.query('select sum(a.precio) as totaladiciones from pclientes_productos pp join pclientes_prod_adiciones ppe on pp.idpclientes_productos = ppe.idpclientes_productos join adiciones a on a.idadicion = ppe.idadicion where pp.idpedido_clientes = ? group by pp.idpedido_clientes',
					[idpedido], (err, rows, fields) => {
						if (err) {
							console.log(err);
							res.send(ee);
						} else {
							if (rows.length != 0) {
								totaladiciones = rows[0].totaladiciones;
							}
							connection.query('select sum(a.precio) as totalselecciones from pclientes_productos pp join pclientes_prod_selecciones ppe on pp.idpclientes_productos = ppe.idpclientes_productos join selecciones a on a.idseleccion = ppe.idseleccion where pp.idpedido_clientes = ? group by pp.idpedido_clientes',
								[idpedido], (err, rows, fields) => {
									if (err) {
										console.log(err);
										res.send(ee);
									} else {
										if (rows.length != 0) {
											totalselecciones = rows[0].totalselecciones;
										}
										connection.query('select sum(p.precio) as totalproductos from pclientes_productos pp join productos p on p.idproducto = pp.idproducto where pp.idpedido_clientes = ? group by pp.idpedido_clientes',
											[idpedido], (err, rows, fields) => {
												if (err) {
													console.log(err);
													res.send(err);
												} else {
													res.send({
														total: totaladiciones + totalelementos + totalselecciones + rows[0].totalproductos
													});
												}
											});
									}
								});
						}
					});
			}
		});
});

/**
 * Obtiene el estado del pedido encomienda
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns pedido o error
 */
router.get('/obtenerestadopedidoencomienda', auth, (req, res) => {
	let idpedido = req.query.pedido;
	connection.query('select * from pedidos_encomiendas where idpedido_encomienda = ?',
		[idpedido], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(err);
			} else {
				res.send({ estado: rows[0] }.estado);
			}
		});
});


// Obtiene los 
router.get('/get-orders-done-rider-account/:riderId', async (req, res) => {
	// Cedula del rider
	const riderId = req.params.riderId;

	let op = models.Sequelize.Op;

	// Obtiene el valor total de la cuenta de los pedidos empresariales pendiente por pagar
	const cuentaPedidosEmpresariales = await PedidosEmpresariales.findAll({
		attributes: ['costo_envio', 'comision_rider', 'retenido', 'valor_ida_vuelta', 'ajustes_id'],
		include: [{
			model: Ajuste, as: 'ajuste',
			attributes: ['valor']
		}],
		where: {
			comision_entregada: 'N',
			idrider: riderId,
			[op.or]: [
				{ estado: 'Entregado' },
				{ estado: 'Error' }
			],
		},
		nest: true,
		raw: true
	}).then((pedidosEmpresariales) => {
		let cuenta = 0;
		// console.log('Pedido E', pedidosEmpresariales);
		// Recorre  los pedidos empresariales
		pedidosEmpresariales.forEach((pedido) => {
			// Valida si el pedido es retenido
			if (pedido.retenido == 'S') {
				cuenta += (pedido.costo_envio + pedido.valor_ida_vuelta) * (100 - pedido.comision_rider) / 100;
			} else {
				cuenta -= (pedido.costo_envio + pedido.valor_ida_vuelta) * pedido.comision_rider / 100;
			}
			// Valida si el pedido viene con un ajuste
			if (pedido.ajustes_id) {
				// Agrega el valor del ajuste a el total de la cuenta
				cuenta += pedido.ajuste.valor;
			}
		});
		return cuenta;
	});

	return res.status(200).json({success: true, message: 'Cuenta del rider', data: cuentaPedidosEmpresariales});

});

/**
 * Obtiene todos lo spedidos realizados por el cliente
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns pedido del cliente
 */
router.get('/obtenerpedidosrealizadosrider', (req, res) => {
	let cedula = req.query.cedularider;

	let deseos, pedidos, mandados, empresariales;
	connection.query('select pd.*, \'deseo\' AS type, sum(f.valor) as totalfacturas from pedidos_deseos pd join facturas f on f.idpedido_deseos = pd.idpedido_deseo where pd.idrider = ? and (pd.estado = ? or pd.estado = ?) and pd.comision_entregada = ? group by pd.idpedido_deseo',
		[cedula, 'Entregado', 'Error', 'N'], (err, rows, fields) => {
			if (err) {
				console.log(err);
				return res.send(err);
			}
			deseos = rows;

			connection.query('select pe.*, \'mandado\' AS type, sum(p.costo) as totalparadas from pedidos_encomiendas pe left join paradas p on p.idpencomienda = pe.idpedido_encomienda where pe.idrider = ? and (pe.estado = ? or pe.estado = ?) and pe.comision_entregada = ? group by pe.idpedido_encomienda',
				[cedula, 'Entregado', 'Error', 'N'], (err, rows, fields) => {
					if (err) {
						console.log(err);
						return res.send(err);
					}
					mandados = rows;

					connection.query('select *, \'tienda\' AS type from pedidos_clientes pc where pc.idrider = ? and (pc.estado = ? or pc.estado = ?)',
						[cedula, 'Entregado', 'Error'], (err, rows, fields) => {
							if (err) {
								console.log(err);
								return res.send(err);
							}
							pedidos = rows;

							connection.query('select pe.*, \'empresariales\' AS type, IFNULL(aj.valor, 0) AS valor_ajuste from pedidos_empresariales pe LEFT JOIN ajustes aj ON aj.id = pe.ajustes_id where pe.idrider = ? and (pe.estado = ? or pe.estado = ?) and pe.comision_entregada = ?',
								[cedula, 'Entregado', 'Error', 'N'], async (err, rows, fieds) => {
									if (err) {
										console.log(err);
										return res.send(err);
									}
									empresariales = rows;

									let todos = deseos.concat(pedidos, mandados, empresariales);
									//console.log(mandados);
									res.send(todos);
								});
						});
				});
		});
});

/**
 * Busca las solicitudes no pagadas
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns id sulicitud | solicitud actual
 */
router.post('/solicitarpago', auth, (req, res) => {
	let cedularider = req.body.cedularider;

	connection.query('select * from solicitudes where idrider = ? and pagada = ?',
		[cedularider, 'N'], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(err);
			} else if (rows.length > 0) {
				res.send({ current: rows.length });
			} else {
				connection.query('insert into solicitudes (idrider, fecha, pagada) values(?,?,?)',
					[cedularider, new Date(), 'N'], (err, rows, fields) => {
						res.send({
							idsolicitud: rows.insertId
						});
					});
			}
		});
});

/**
 * Obtiene los factores, elemento y adiciones de un producto
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns id sulicitud | solicitud actual
 */
router.get('/obtenerfactoreselementosadiciones', auth, (req, res) => {
	let idproducto = req.query.idproducto;
	let response = [];
	let factores = [], elementos = [], adiciones = [];
	connection.beginTransaction((err) => {
		if (err) {
			console.log(err);
		} else {
			connection.query('select * from factores f where f.idproducto = ?', [idproducto], (err, rows, fields) => {
				if (err) {
					connection.rollback(() => {
						console.log(err);
						res.send(err);
					});
				} else {
					factores = rows;
					for (let i = 0; i < factores.length; i++) {
						factores[i].selecciones = [];
					}
					connection.query('select f.*, s.idseleccion as idseleccion, s.nombre as nombreseleccion, s.precio as precioseleccion from factores f join selecciones s on s.idfactor = f.idfactor where f.idproducto = ?', [idproducto], (err, rows, fields) => {
						if (err) {
							connection.rollback(() => {
								console.log(err);
								res.send(err);
							});
						} else {
							for (let i = 0; i < factores.length; i++) {
								for (let j = 0; j < rows.length; j++) {
									if (factores[i].idfactor == rows[j].idfactor) {
										factores[i].selecciones.push({
											idseleccion: rows[j].idseleccion,
											nombreseleccion: rows[j].nombreseleccion,
											precioseleccion: rows[j].precioseleccion
										});
									}
								}
							}
							connection.query('select * from elementos where idproducto = ?', [idproducto], (err, rows, fields) => {
								if (err) {
									connection.rollback(() => {
										console.log(err);
										res.send(err);
									});
								} else {
									elementos = rows;
									connection.query('select * from adiciones where idproducto = ?', [idproducto], (err, rows, fields) => {
										if (err) {
											connection.rollback(() => {
												console.log(err);
												res.send(err);
											});
										} else {
											adiciones = rows;
											connection.commit((err) => {
												if (err) {
													console.log(err);
													res.send(err);
												} else {
													response.push(factores);
													response.push(elementos);
													response.push(adiciones);
													res.send(response);
												}
											});
										}
									});
								}
							});
						}
					});
				}
			});
		}
	});
});


/**
 * Inserta un nuevo pedido del cliente
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns error | ok
 */
router.post('/realizarpedidocliente', auth, (req, res) => {
	let carrito = JSON.parse(req.body.carrito);
	let correocliente = req.body.correocliente;
	let costo_envio = req.body.costo_envio;
	let comision_rider = req.body.comision_rider;
	let iddireccion = req.body.iddireccion;
	let idpedido;

	connection.beginTransaction((err) => {
		if (err) {
			console.log(err);
			res.send(err);
		} else {
			connection.query('insert into pedidos_clientes (costo_envio, comision_rider, fecha, estado, iddireccion, comision_entregada, correocliente) values(?,?,?,?,?,?,?)',
				[costo_envio, comision_rider, new Date(), 'En proceso', iddireccion, 'N', correocliente], (err, rows, fields) => {
					if (err) {
						connection.rollback(() => {
							console.log(err);
							res.send(err);
						});
					} else {
						idpedido = rows.insertId;
						let values = [];
						for (let i = 0; i < carrito.length; i++) {
							let curr = [];
							curr.push(idpedido, carrito[i].idproducto);
							values.push(curr);
						}
						connection.query('insert into pclientes_productos (idpedido_clientes, idproducto) values ?',
							[values], (err, rows, fields) => {
								if (err) {
									connection.rollback(() => {
										console.log(err);
										res.send(err);
									});
								} else {
									let lastId = rows.insertId;
									let valuesElementos = [], valuesAdiciones = [], valuesSelecciones = [];
									for (let i = 0; i < carrito.length; i++) {
										for (let j = 0; j < carrito[i].elementos.length; j++) {
											let curr = [];
											curr.push(carrito[i].elementos[j].idelemento, lastId + i);
											valuesElementos.push(curr);
										}

										for (let j = 0; j < carrito[i].adiciones.length; j++) {
											let curr = [];
											curr.push(carrito[i].adiciones[j].idadicion, lastId + i);
											valuesAdiciones.push(curr);
										}

										for (let j = 0; j < carrito[i].factores.length; j++) {
											let curr = [];
											curr.push(carrito[i].factores[j].selecciones[carrito[i].factores[j].indexSelected].idseleccion, lastId + i);
											valuesSelecciones.push(curr);
										}
									}
									connection.query('insert into pclientes_prod_elementos (idelemento, idpclientes_productos) values ?',
										[valuesElementos], (err, rows, fields) => {
											if (err && valuesElementos.length != 0) {
												connection.rollback(() => {
													console.log(err);
													res.send(err);
												});
											} else {
												connection.query('insert into pclientes_prod_adiciones (idadicion, idpclientes_productos) values ?',
													[valuesAdiciones], (err, rows, fields) => {
														if (err && valuesAdiciones.length != 0) {
															connection.rollback(() => {
																console.log(err);
																res.send(err);
															});
														} else {
															connection.query('insert into pclientes_prod_selecciones (idseleccion, idpclientes_productos) values ?',
																[valuesSelecciones], (err, rows, fields) => {
																	if (err && valuesSelecciones.length != 0) {
																		connection.rollback(() => {
																			console.log(err);
																			res.send(err);
																		});
																	} else {
																		connection.commit((err) => {
																			if (err) {
																				console.log(err);
																				res.send(err);
																			} else {
																				res.send({
																					okey: 'yes'
																				});
																			}
																		});
																	}
																});
														}
													});
											}
										});
								}
							});
					}
				})
		}
	});
});

/**
 * Obtiene las paradas
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns error | paradas
 */
router.get('/obtenerparadas', auth, (req, res) => {
	connection.query('select * from paradas p join direcciones d on p.iddireccion = d.iddireccion', (err, rows, fields) => {
		if (err) {
			console.log(err);
			res.send(err);
		} else {
			res.send(rows);
		}
	});
});

/**
 * Obtiene las paradas del pedido
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns error | paradas
 */
router.get('/obtenerparadaspedido', auth, (req, res) => {
	let idpedido = req.query.pedido;
	connection.query('select * from paradas p join direcciones d on p.iddireccion = d.iddireccion where p.idpencomienda = ?', [idpedido], (err, rows, fields) => {
		if (err) {
			console.log(err);
			res.send(err);
		} else {
			res.send(rows);
		}
	});
});

/**
 * Obtiene la direccion del pedido encomienda
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns error | direccion pedio encomienda
 */
router.get('/obtenerdireccionesencomienda', auth, (req, res) => {
	let idpedido = req.query.pedido;
	let pedido;
	connection.query('select * from pedidos_encomiendas where idpedido_encomienda = ?', [idpedido], (err, rows, fields) => {
		if (err) {
			console.log(err);
			res.send(err);
		} else {
			pedido = rows[0];
			let direccionrecogida, direccionentrega;
			connection.query('select * from direcciones where iddireccion = ?',
				[pedido.iddireccionorigen], (err, rows, fields) => {
					if (err) {
						console.log(err);
						res.send(err);
					} else {
						direccionrecogida = rows[0];
						connection.query('select * from direcciones where iddireccion = ?',
							[pedido.iddirecciondestino], (err, rows, fields) => {
								if (err) {
									console.log(err);
									res.send(err);
								} else {
									direccionentrega = rows[0];
									let response = [];
									response.push(direccionrecogida, direccionentrega);
									res.send(response);
								}
							});
					}
				});
		}
	});
});

/**
 * Obtiene las facturas del pedido deseo
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns error | facturas
 */
router.get('/obtenerfacturas', auth, (req, res) => {
	let idpedido = req.query.pedido;
	connection.query('select * from facturas where idpedido_deseos = ?',
		[idpedido], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(err);
			} else {
				res.send(rows);
			}
		});
});

/**
 * Agrega una nueva factura
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns error | ok
 */
router.post('/agregarfactura', auth, (req, res) => {
	let idpedido = req.body.idpedido;
	let cantidadFacturas = req.body.cantidadFacturas;
	let foto = req.files.fotofactura;
	let valor = req.body.valor;
	let extension = foto.name.split('.').pop();
	let direccionImg = 'src/app/public/img/facturas/' + idpedido + '-' + cantidadFacturas + '.' + extension;
	fs.renameSync(foto.path, direccionImg);
	name = idpedido + '-' + cantidadFacturas + '.' + extension;

	connection.query('insert into facturas (foto, valor, idpedido_deseos) values(?, ?, ?)',
		[name, valor, idpedido], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(err);
			} else {
				res.send({
					okey: 'yes'
				});
			}
		});
});

/**
 * Obtiene el clente del pedido deseo
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns error | cliente
 */
router.get('/obtenerclientepedidodeseo', auth, (req, res) => {
	let idpedido = req.query.pedido;
	connection.query('select c.* from pedidos_deseos pd join clientes c on c.correo = pd.correocliente where pd.idpedido_deseo = ?',
		[idpedido], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(err);
			} else {
				res.send(rows[0]);
			}
		});
});


/**
 * Obtiene el clente del pedido encomienda
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns error | cliente
 */
router.get('/obtenerclientepedidoencomienda', auth, (req, res) => {
	let idpedido = req.query.pedido;
	connection.query('select c.* from pedidos_encomiendas pd join clientes c on c.correo = pd.correocliente where pd.idpedido_encomienda = ?',
		[idpedido], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(err);
			} else {
				res.send(rows[0]);
			}
		});
});

/**
 * Obtiene el clente del pedido clientes
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns error | cliente
 */
router.get('/obtenerclientepedidoclientes', auth, (req, res) => {
	let idpedido = req.query.pedido;
	connection.query('select c.* from pedidos_clientes pd join clientes c on c.correo = pd.correocliente where pd.idpedido_clientes = ?',
		[idpedido], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(err);
			} else {
				res.send(rows[0]);
			}
		});
});

/**
 * Envia correo de restablecimiento de contraseña a todos los riders
 */
router.get('/reset-all-passwords', (req, res) => {
	
	let ridersIsMailSend = [];
	db.query('SELECT correo, nombre, apellido, cedula FROM riders WHERE listanegra = ?', ['N'])
	.then(async rows => {
		for (let i = 0; i < rows.length; i++) {
			const rider = rows[i];
			await verifier.verify(rider.correo, async ( err, info ) => {
				if (err) {
					console.log('Error verificando correo '+ rider.correo, err);
					ridersIsMailSend.push({correo: rider.correo, enviado: 'Error al verificar el correo'});
				} else {
					// Verifica si el correo es correcto
					if (!info.success) {
						console.log(info.info);
						ridersIsMailSend.push({correo: rider.correo, enviado: 'Correo invalido'});
					} else {
						// Creacion de token
						let token = jwt.sign({
							data: rider
						}, process.env.PRIVATE_JWT_KEY, { expiresIn: '2d' });
						
						// Cuerpo del correo
						const msg = `Hola ${rider.nombre} ${rider.apellido}, nos enteramos de que olvidaste tu contraseña :( en Traigo estamos felices de tenerte! Por eso no queremos que pierdas tu acceso.\nPara recuperar tu contraseña ingresa al siguiente link:\n https://admin.traigo.com.co/reset-password/${token}`;
	
						// Envia el correo de cambio de contraseña
						await sendMail(rider.correo, 'Traigo: Cambio de contraseña', msg)
						.then(() => {
							console.log('Correo ' + rider.correo + ' enviado con exito.')
							ridersIsMailSend.push({correo: rider.correo, enviado: 'Enviado'});
						}) // Correo enviado
						.catch(err => { // Error al enviar el correo
							console.log(err);
							console.log('Error al enviar el correo' + rider.correo);
							ridersIsMailSend.push({correo: rider.correo, enviado: 'Error envio'});
							//return res.status(401).json({msg: 'Error al eviar correos' + rider.correo});
						});
						
					}
				}
			});
		}
		res.status(200).json(ridersIsMailSend);
	})
	.catch(err => {
		console.log(err);
		return res.status(401).json('Error al enviar correos');
	});
});

/**
 * Restablecer contraseña RIDER
 */
router.get('/password-reset', (req, res) => {
	// Id del rider
	let id = req.query.id;

	// Obtiene el rider por id
	db.query('SELECT correo, nombre, apellido, cedula FROM riders WHERE cedula = ? AND listanegra = ?', [id, 'N'])
	.then(rows => {
		// Verifica si vienen datos vacios
		if (rows.length === 0) {
			return res.status(400).json('El rider no existe');
		} else {
			// Datos del rider
			const rider = rows[0];
			// Verifica si la direccion del correo es valida
			verifier.verify(rider.correo, ( err, info ) => {
				if (err) {
					console.log(err);
					return res.status(401).json('Error al verificar el correo electronico.');
				} else {
					// Verifica si el correo es correcto
					if (!info.success) {
						console.log(info.info);
						return res.status(401).json('Correo invalido');
					} else {
						// Creacion de token
						let token = jwt.sign({
							data: rider
						}, process.env.PRIVATE_JWT_KEY, { expiresIn: '1h' });

						// Cuerpo del correo
						const msg = `Hola <b>${rider.nombre} ${rider.apellido}</b>, nos enteramos de que olvidaste tu contraseña :( en Traigo estamos felices de tenerte! Por eso no queremos que pierdas tu acceso.\nPara recuperar tu contraseña ingresa al siguiente link:\n <br><h3><a href="https://admin.traigo.com.co/reset-password/${token}">Pulsa aquí</a></h3>`;

						// Envia el correo de cambio de contraseña
						sendMail(rider.correo, 'Traigo: Cambio de contraseña', msg)
						.then(() => res.status(200).json('Correo enviado con exito.')) // Correo enviado
						.catch(err => { // Error al enviar el correo
							console.log(err);
							res.status(401).json('El correo no se ha podido enviar');
						});
					}
				}
			});
		}
	});
});

/**
 * Envia un correo 
 * 
 * @param {*} email desinatario
 * @param {*} subject 
 * @param {*} message 
 */
function sendMail(email, subject, message) {

	// Datos de envio
	const mailOptions = {
		from: 'traigoapp@gmail.com',// 'd223b852c6-bacf5f@inbox.mailtrap.io',
		to: email,
		subject: subject,
		text: message,
		html: message
	}

	// Envia el correo al desinatario
	return configMail.sendMail(mailOptions)
}

/**
 * Permite recuperar la contraseña del cliente
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns error | ok
 */
router.post('/recuperarcontrasenia', (req, res) => {
	let correo = req.body.correo;
	console.log('Hola');
	connection.query('select * from clientes where correo = ?', [correo], (err, rows, fields) => {
		if (err) {
			console.log(err);
			res.send(err);
		} else if (rows.length == 0) {
			res.send({
				notfound: 404
			})
		} else {
			let transporter = nodemailer.createTransport({
				service: 'gmail',
				auth: {
					user: 'traigoapp@gmail.com',
					pass: 'traigo2019todoloquenecesitas'
				}
			});
			let mailOptions = {
				from: 'traigoapp@gmail.com',
				to: rows[0].correo,
				subject: 'Traigo: Recuperación de contraseña',
				text: 'Hola ' + rows[0].nombre + ', nos enteramos de que olvidaste tu contraseña :( en Traigo estamos felices de tenerte! Por eso no queremos que pierdas tu acceso.\nTu contraseña es: ' + rows[0].contrasenia
			}

			transporter.sendMail(mailOptions, (err, info) => {
				if (err) {
					console.log(err);
					res.send(err);
				} else {
					res.send({
						correct: 'yes'
					});
				}
			});
		}
	});
});

/**
 * Permite obtener todos los departamentos
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns error | departamentos
 */
router.get('/departamentos', auth, (req, res) => {
	connection.query('SELECT d.idDepartamento as id, d.nombre FROM departamentos d',
		(err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(err);
			} else {
				res.send(rows);
			}
		});
});

/**
 * Permite obtener todos las ciudades por id
 * @param req lo que captura el request
 * @param res lo que se envía en el response
 * @returns error | ciudades
 */
router.get('/ciudades/:departamento', auth, (req, res) => {
	let idDepartamento = req.params.departamento;
	connection.query('SELECT m.idmunicipios as id, m.nombre FROM municipios m WHERE idDepartamento = ?',
		[idDepartamento], (err, rows, fields) => {
			if (err) {
				console.log(err);
				res.send(err);
			} else {
				res.send(rows);
			}
		});
});


module.exports = router;
