module.exports = (req, res, next) => {	
	if(req.session.user == undefined){
		return res.redirect("/");
	}else if(req.session.user.type == 'administrador'){
		res.locals.usuario =  req.session.user;
		return next();
	}else if(req.session.user.type == 'tienda'){
		return res.redirect("/tienda");
	}else if(req.session.user.type == 'moderador'){
		return res.redirect("/moderador");
	}
}