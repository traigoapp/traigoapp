const jwt = require("jsonwebtoken");
require('dotenv').config();

module.exports = (req, res, next) => {
  // Get auth token header value
  const bearerHeader = req.headers['authorization'];
  console.log('TOKEN', bearerHeader);

  // Check if bearer is undefined
  if (typeof bearerHeader !== 'undefined') {
      // Split token space
      const bearer = bearerHeader.split(' ');
      // get token
      const token = bearer[1];

      try {
          //if can verify the token, set req.user and pass to next middleware
          const decoded = jwt.verify(token, process.env.PRIVATE_JWT_KEY);
          // Send token
          req.user_app = decoded;
          // Next middleware
          next();
      } catch(err) {
          res.status(400).send({success: false, message: 'Token inválido.', code: 10});
      }
      
  } else {
      // Forbidden
      res.status(403).send({success: false, message: 'Acceso denegado, falta token de seguridad.', code: 11});
  }
  /*//get the token from the header if present
  const token = req.headers["x-access-token"] || req.headers["authorization"];
  //if no token found, return response (without going to the next middelware)
  if (!token) return res.status(401).send("Access denied. No token provided.");

  try {
    //if can verify the token, set req.user and pass to next middleware
    const decoded = jwt.verify(token, config.get("myprivatekey"));
    req.user = decoded;
    next();
  } catch (ex) {
    //if invalid token
    res.status(400).send("Invalid token.");
  }*/
};

