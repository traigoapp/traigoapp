module.exports = (req, res, next) => {
	if(req.session.user == undefined){
		res.redirect("/");
	}else if(req.session.user.type == 'administrador'){
		res.redirect("/administrador");
	}else if(req.session.user.type == 'tienda'){
		res.redirect("/tienda");
	}else if(req.session.user.type == 'moderador'){
		next();
	}
}