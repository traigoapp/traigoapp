/* jshint indent: 2 */
const moment = require('moment');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('historial_tiendas_planes', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    tiendas_planes_tiendas_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'tiendas_planes',
        key: 'tiendas_id'
      }
    },
    tiendas_planes_planes_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'tiendas_planes',
        key: 'planes_id'
      }
    },
    cant_pedidos: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '0'
    },
    adicionales: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '0'
    },
    estado: {
      type: DataTypes.CHAR(1),
      allowNull: false
    },
    vencible: {
      type: DataTypes.BOOLEAN(),
      defaultValue: false,
      allowNull: false
    },
    dias_habilitados: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: '0'
    },
    fecha_inicio: {
      field: 'fecha_inicio',
      type: DataTypes.DATE,
      allowNull: true,
      get() {
        return moment(this.getDataValue('fecha_inicio')).format('DD-MM-YYYY');
      }
    },
    fecha_vencimiento: {
      field: 'fecha_vencimiento',
      type: DataTypes.DATE,
      allowNull: true,
      get() {
        return moment(this.getDataValue('fecha_vencimiento')).format('DD-MM-YYYY');
      }
    },
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
      allowNull: true,
      get() {
        return moment(this.getDataValue('createdAt')).format('DD-MM-YYYY hh:mm A');
      }
    },
    updatedAt: {
      field: 'updated_at',
      type: DataTypes.DATE,
      allowNull: true,
      get() {
        return moment(this.getDataValue('updatedAt')).format('DD-MM-YYYY hh:mm A');
      }
    }
  }, {
    tableName: 'historial_tiendas_planes',
    timestamps: true
  });
};
