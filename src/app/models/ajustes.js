/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  let ajustes = sequelize.define('ajustes', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    valor: {
      type: "DOUBLE",
      allowNull: false
    },
    detalle: {
      type: DataTypes.STRING(500),
      allowNull: true
    },
    encargado_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    encargado_rol: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
      allowNull: true
    },
    updatedAt: {
      field: 'updated_at',
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'ajustes',
    timestamps: true
  });

  // Pedidos empresariales
  ajustes.associate = (models) => {
    ajustes.hasMany(models.pedidos_empresariales, {foreignKey: 'ajustes_id', sourceKey: 'id', as: 'pedidos_empresariales'})
  };

  return ajustes;
};

