/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('estados_pedido', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    estado_rider: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    estado_cliente: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    tiempo_minimo: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    tiempo_maximo: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    tiempo_demanda: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    descripcion: {
      type: DataTypes.STRING(300),
      allowNull: true
    },
    color: {
      type: DataTypes.STRING(45),
      allowNull: true
    }
  }, {
    tableName: 'estados_pedido'
  });
};
