/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('productos', {
    idproducto: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    nombre: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    descripcion_corta: {
      type: DataTypes.STRING(250),
      allowNull: true
    },
    precio: {
      type: "DOUBLE",
      allowNull: false
    },
    imagen: {
      type: DataTypes.STRING(500),
      allowNull: true
    },
    idsucursal: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'sucursales',
        key: 'idsucursal'
      }
    },
    descripcion_larga: {
      type: DataTypes.STRING(1000),
      allowNull: true
    },
    tamanio_maletin: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    idcategoriaproducto: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'categoriasproductos',
        key: 'idcategoriaproducto'
      }
    },
    activo: {
      type: DataTypes.STRING(1),
      allowNull: true,
      defaultValue: 'S'
    },
    stock: {
      type: DataTypes.STRING(1),
      allowNull: true,
      defaultValue: 'S'
    }
  }, {
    tableName: 'productos'
  });
};
