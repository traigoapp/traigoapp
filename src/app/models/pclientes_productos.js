/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('pclientes_productos', {
    idpedido_clientes: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'pedidos_clientes',
        key: 'idpedido_clientes'
      }
    },
    idproducto: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'productos',
        key: 'idproducto'
      }
    },
    nota: {
      type: DataTypes.STRING(5000),
      allowNull: true
    },
    idpclientes_productos: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    }
  }, {
    tableName: 'pclientes_productos'
  });
};
