/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('fotos', {
    idfoto: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    idpedido_deseo: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'pedidos_deseos',
        key: 'idpedido_deseo'
      }
    },
    foto: {
      type: DataTypes.STRING(1000),
      allowNull: false
    },
    idpedido_encomienda: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'pedidos_encomiendas',
        key: 'idpedido_encomienda'
      }
    }
  }, {
    tableName: 'fotos'
  });
};
