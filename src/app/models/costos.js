/* jshint indent: 2 */

module.exports = (sequelize, DataTypes) => {
  let costos = sequelize.define('costos', {
    idcosto: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    costo_envio_deseo: {
      type: "DOUBLE",
      allowNull: false
    },
    comision_cliente_deseo: {
      type: "DOUBLE",
      allowNull: false
    },
    costo_envio_cliente: {
      type: "DOUBLE",
      allowNull: false
    },
    comision_rider_cliente: {
      type: "DOUBLE",
      allowNull: false
    },
    costo_envio_mandado: {
      type: "DOUBLE",
      allowNull: false
    },
    costo_paradas_mandado: {
      type: "DOUBLE",
      allowNull: false
    },
    comision_rider_mandado: {
      type: "DOUBLE",
      allowNull: false
    },
    comision_rider_empresarial: {
      type: "DOUBLE",
      allowNull: false
    },
    costo_bajo_lluvia: {
      type: "DOUBLE",
      allowNull: true
    },
    idmunicipio: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'municipios',
        key: 'idmunicipios'
      },
      unique: true
    }
  }, {
    tableName: 'costos'
  });

  
  // Relacion con municipios
  costos.associate = (models) => {
    costos.belongsTo(models.municipios, {foreignKey: 'idmunicipio', targetKey: 'idmunicipios', as: 'city'});
  };

  return costos;
};
