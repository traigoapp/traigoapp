/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('historial_procesos', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    nombre_proceso: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    fecha_creacion: {
      type: DataTypes.DATE,
      allowNull: true
    },
    tiempo_ejecutado: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    id_pedido: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    contexto_pedidos_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'contexto_pedidos',
        key: 'id'
      }
    }
  }, {
    tableName: 'historial_procesos'
  });
};
