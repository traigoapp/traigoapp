/* jshint indent: 2 */

module.exports = (sequelize, DataTypes) => {
  let sucursales = sequelize.define('sucursales', {
    idsucursal: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    rut: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    cuenta_bancaria: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    cedula: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    numero_privado: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    numero_punto: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    idtienda: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'tiendas',
        key: 'idtienda'
      }
    },
    idmunicipio: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'municipios',
        key: 'idmunicipios'
      }
    },
    costo_envio: {
      type: "DOUBLE",
      allowNull: false
    },
    comision: {
      type: "DOUBLE",
      allowNull: false
    },
    nombre_duenio: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    apellido_duenio: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    iddireccion: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'direcciones',
        key: 'iddireccion'
      }
    },
    idcategoria: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'categorias',
        key: 'idcategoria'
      }
    },
    hora_inicial: {
      type: DataTypes.TIME,
      allowNull: false
    },
    hora_final: {
      type: DataTypes.TIME,
      allowNull: false
    },
    tiempo_entrega: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    soloempresarial: {
      type: DataTypes.STRING(1),
      allowNull: false
    },
    nombre: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    activo: {
      type: DataTypes.STRING(45),
      allowNull: true,
      defaultValue: 'S'
    },
    contrasenia: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    retenido: {
      type: DataTypes.STRING(1),
      allowNull: false
    },
    hora_inicialsabado: {
      type: DataTypes.TIME,
      allowNull: true
    },
    hora_finalsabado: {
      type: DataTypes.TIME,
      allowNull: true
    },
    hora_inicialdomingo: {
      type: DataTypes.TIME,
      allowNull: true
    },
    hora_finaldomingo: {
      type: DataTypes.TIME,
      allowNull: true
    },
    listanegra: {
      type: DataTypes.STRING(1),
      allowNull: false,
      defaultValue: 'N'
    },
    descripcion_listanegra: {
      type: DataTypes.STRING(300),
      allowNull: true
    },
    tipo: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '1'
    },
    bajo_lluvia: {
      type: DataTypes.INTEGER(1),
      allowNull: true
    },
    ida_vuelta: {
      type: DataTypes.BOOLEAN(),
      defaultValue: false,
      allowNull: false
    },
    valor_ida_vuelta: {
      type: "DOUBLE",
      defaultValue: 0,
      allowNull: false
    }
  }, {
    tableName: 'sucursales'
  });

  // Pedidos empresariales
  sucursales.associate = (models) => {
    sucursales.hasMany(models.pedidos_empresariales, {foreignKey: 'idsucursal', sourceKey: 'idsucursal', as: 'pedidos_empresariales'})
  };

  return sucursales;
};
