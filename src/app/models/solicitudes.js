/* jshint indent: 2 */
let moment = require('moment');

module.exports = function(sequelize, DataTypes) {
  let solicitudes =  sequelize.define('solicitudes', {
    idrider: {
      type: DataTypes.STRING(45),
      allowNull: false,
      references: {
        model: 'riders',
        key: 'cedula'
      }
    },
    idsolicitud: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    fecha: {
      type: DataTypes.DATE,
      allowNull: false,
      get() {
        return moment(this.getDataValue('fecha')).format('YYYY-MM-DD HH:mm:ss');
      }
    },
    pagada: {
      type: DataTypes.STRING(1),
      allowNull: false
    }
  }, {
    tableName: 'solicitudes'
  });

  solicitudes.associate = (models) => {
    solicitudes.belongsTo(models.riders, {foreignKey: 'idrider', as: 'rider'})
  };

  return solicitudes;
};
