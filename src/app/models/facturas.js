/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('facturas', {
    idfactura: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    foto: {
      type: DataTypes.STRING(1000),
      allowNull: false
    },
    valor: {
      type: "DOUBLE",
      allowNull: false
    },
    idpedido_deseos: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'pedidos_deseos',
        key: 'idpedido_deseo'
      }
    }
  }, {
    tableName: 'facturas'
  });
};
