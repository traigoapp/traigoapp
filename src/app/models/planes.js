/* jshint indent: 2 */
let moment = require('moment');

module.exports = (sequelize, DataTypes) => {
  let planes = sequelize.define('planes', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    nombre: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    descripcion: {
      type: DataTypes.STRING(300),
      allowNull: true
    },
    precio: {
      type: "DOUBLE",
      allowNull: false
    },
    num_pedidos: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    tipo: {
      type: DataTypes.CHAR(1),
      allowNull: false
    },
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
      allowNull: true,
      get() {
        return moment(this.getDataValue('createdAt')).format('DD-MM-YYYY');
      }
    },
    updatedAt: {
      field: 'updated_at',
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'planes',
    timestamps: true
  });

  /*planes.associate = (models) => {
    planes.hasMany(models.tiendas_planes, {foreignKey: 'tiendas_id', sourceKey: 'id', as: 'tiendas'})
  };*/

  planes.associate = (models) => {
    planes.hasMany(models.tiendas_planes, {foreignKey: 'planes_id', sourceKey: 'id', as: 'tiendas_planes'})
  };

  return planes;
};
