/* jshint indent: 2 */

module.exports = (sequelize, DataTypes) => {
  let alertas =  sequelize.define('alertas', {
    id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    nombre: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    img: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    tipo: {
      type: DataTypes.CHAR(1),
      allowNull: false
    },
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
      allowNull: true
    },
    updatedAt: {
      field: 'updated_at',
      type: DataTypes.DATE,
      allowNull: true
    },
    deletedAt: {
      field: 'deleted_at',
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'alertas',
    timestamps: true,
    paranoid: true
  });

  alertas.associate = (models) => {
    alertas.hasMany(models.historial_alertas, {foreignKey: 'alertas_id', sourceKey: 'id', as: 'historial_alerta'})
  };

  return alertas;
};
