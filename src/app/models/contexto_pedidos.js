/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('contexto_pedidos', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    tipo_pedido: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    slug: {
      type: DataTypes.STRING(45),
      allowNull: true
    }
  }, {
    tableName: 'contexto_pedidos'
  });
};
