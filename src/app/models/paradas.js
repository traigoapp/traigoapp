/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('paradas', {
    idparada: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    iddireccion: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'direcciones',
        key: 'iddireccion'
      }
    },
    descripcion: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    costo: {
      type: "DOUBLE",
      allowNull: false
    },
    idpencomienda: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'pedidos_encomiendas',
        key: 'idpedido_encomienda'
      }
    }
  }, {
    tableName: 'paradas'
  });
};
