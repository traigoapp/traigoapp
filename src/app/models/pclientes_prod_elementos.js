/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('pclientes_prod_elementos', {
    idelemento: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'elementos',
        key: 'idelemento'
      }
    },
    idpclientes_productos: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'pclientes_productos',
        key: 'idpclientes_productos'
      }
    }
  }, {
    tableName: 'pclientes_prod_elementos'
  });
};
