/* jshint indent: 2 */

module.exports = (sequelize, DataTypes) => {
  return sequelize.define('paises', {
    idPais: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    nombre: {
      type: DataTypes.STRING(100),
      allowNull: false
    }
  }, {
    tableName: 'paises'
  });
};
