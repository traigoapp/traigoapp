/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('categorias', {
    idcategoria: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    nombre: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    descripcion: {
      type: DataTypes.STRING(1000),
      allowNull: false
    },
    imagen: {
      type: DataTypes.STRING(500),
      allowNull: false
    }
  }, {
    tableName: 'categorias'
  });
};
