/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('adiciones', {
    idadicion: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    nombre: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    precio: {
      type: "DOUBLE",
      allowNull: false
    },
    idproducto: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'productos',
        key: 'idproducto'
      }
    }
  }, {
    tableName: 'adiciones'
  });
};
