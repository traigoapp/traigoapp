/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('selecciones', {
    idseleccion: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    nombre: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    precio: {
      type: "DOUBLE",
      allowNull: false
    },
    idfactor: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'factores',
        key: 'idfactor'
      }
    }
  }, {
    tableName: 'selecciones'
  });
};
