/* jshint indent: 2 */

module.exports = (sequelize, DataTypes) => {
  let configuraciones = sequelize.define('configuraciones', {
    img_login: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    limit_business_orders: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: '10'
    }
  }, {
    tableName: 'configuraciones'
  });
  configuraciones.removeAttribute('id');

  return configuraciones;
};
