/* jshint indent: 2 */

module.exports = (sequelize, DataTypes) => {
  const historial_alertas = sequelize.define('historial_alertas', {
    id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    alertas_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      references: {
        model: 'alertas',
        key: 'id'
      }
    },
    encargado_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    encargado_rol: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    tipo_envio: {
      type: DataTypes.CHAR(1),
      allowNull: false
    },
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
      allowNull: true
    },
    updatedAt: {
      field: 'updated_at',
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'historial_alertas',
    timestamps: true
  });

  historial_alertas.associate = (models) => {
    historial_alertas.belongsTo(models.alertas, {foreignKey: 'alertas_id', as: 'alerta'})
  };

  historial_alertas.associate = (models) => {
    historial_alertas.hasMany(models.tiendas_historial_alertas, {foreignKey: 'historial_alertas_id', sourceKey: 'id', as: 'tienda_historial_alertas'})
  };
  
  /*historial_alertas.associate = (models) => {
    historial_alertas.belongsToMany(models.tiendas, {
      through: 'historial',
      as: 'tiendas',
      foreignKey: 'historial_alertas_id',
      otherKey: 'tiendas_id'
    });
  };*/

  return historial_alertas;
};
