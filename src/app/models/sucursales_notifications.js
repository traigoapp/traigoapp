/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('sucursales_notifications', {
    sucursales_idsucursal: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'sucursales',
        key: 'idsucursal'
      }
    },
    notifications_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'notifications',
        key: 'id'
      }
    }
  }, {
    tableName: 'sucursales_notifications'
  });
};
