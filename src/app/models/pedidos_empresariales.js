/* jshint indent: 2 */

module.exports = (sequelize, DataTypes) => {
  let pedidos_empresariales = sequelize.define('pedidos_empresariales', {
    idpedido_empresarial: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    idsucursal: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'sucursales',
        key: 'idsucursal'
      }
    },
    pagado: {
      type: DataTypes.STRING(1),
      allowNull: false
    },
    idrider: {
      type: DataTypes.STRING(45),
      allowNull: true,
      references: {
        model: 'riders',
        key: 'cedula'
      }
    },
    comision_rider: {
      type: "DOUBLE",
      allowNull: false
    },
    comision_entregada: {
      type: DataTypes.STRING(1),
      allowNull: false
    },
    pagado_sucursal: {
      type: DataTypes.STRING(1),
      allowNull: false
    },
    nombre_cliente: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    notas: {
      type: DataTypes.STRING(500),
      allowNull: true
    },
    valor: {
      type: "DOUBLE",
      allowNull: false
    },
    numero_factura: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    iddireccion: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'direcciones',
        key: 'iddireccion'
      }
    },
    id_direccion_ida_vuelta: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'direcciones',
        key: 'iddireccion'
      }
    },
    telefono: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    estado: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    fecha: {
      type: DataTypes.DATE,
      allowNull: false
    },
    datafono: {
      type: DataTypes.STRING(1),
      allowNull: false
    },
    retenido: {
      type: DataTypes.STRING(1),
      allowNull: false
    },
    costo_envio: {
      type: "DOUBLE",
      allowNull: false
    },
    estados_pedido_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'estados_pedido',
        key: 'id'
      }
    },
    evidencia: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    tamanio_maletin: {
      type: DataTypes.INTEGER(4),
      allowNull: true
    },
    ajustes_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'ajustes',
        key: 'id'
      }
    },
    ida_vuelta: {
      type: DataTypes.BOOLEAN(),
      defaultValue: false,
      allowNull: false
    },
    valor_ida_vuelta: {
      type: "DOUBLE",
      defaultValue: 0,
      allowNull: false
    }
  }, {
    tableName: 'pedidos_empresariales'
  });

  // Relacion con sucursales
  pedidos_empresariales.associate = (models) => {
    pedidos_empresariales.belongsTo(models.sucursales, {foreignKey: 'idsucursal', targetKey: 'idsucursal', as: 'sucursaless'})
  };

  // Relacion con riders
  pedidos_empresariales.associate = (models) => {
    pedidos_empresariales.belongsTo(models.riders, {foreignKey: 'idrider', targetKey: 'cedula', as: 'rider'});
  };

  // Relacion con direcciones
  pedidos_empresariales.associate = (models) => {
    pedidos_empresariales.belongsTo(models.direcciones, {foreignKey: 'iddireccion', as: 'direccion'});
  };

  // Relacion con estadis de pedido
  pedidos_empresariales.associate = (models) => {
    pedidos_empresariales.belongsTo(models.estados_pedido, {foreignKey: 'estados_pedido_id', as: 'estado_pedido'});
  };

  // Relacion con ajustes
  pedidos_empresariales.associate = (models) => {
    pedidos_empresariales.belongsTo(models.ajustes, {foreignKey: 'ajustes_id', as: 'ajuste'});
  };

  return pedidos_empresariales;
};
