/* jshint indent: 2 */

module.exports = (sequelize, DataTypes) => {
  return sequelize.define('cliente_direccion', {
    iddireccion: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'direcciones',
        key: 'iddireccion'
      }
    },
    correocliente: {
      type: DataTypes.STRING(100),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'clientes',
        key: 'correo'
      }
    }
  }, {
    tableName: 'cliente_direccion'
  });
};
