/* jshint indent: 2 */

module.exports = (sequelize, DataTypes) => {
  return sequelize.define('departamentos', {
    idDepartamento: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    nombre: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    idPais: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'paises',
        key: 'idPais'
      }
    }
  }, {
    tableName: 'departamentos'
  });
};
