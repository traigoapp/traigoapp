/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('contexto_estados_pedido', {
    contexto_pedidos_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'contexto_pedidos',
        key: 'id'
      }
    },
    estados_pedido_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'estados_pedido',
        key: 'id'
      }
    },
    estado_anterior: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    }
  }, {
    tableName: 'contexto_estados_pedido'
  });
};
