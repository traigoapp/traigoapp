/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  let tiendas = sequelize.define('tiendas', {
    idtienda: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    nombre: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    descripcion: {
      type: DataTypes.STRING(250),
      allowNull: true
    },
    imagen: {
      type: DataTypes.STRING(500),
      allowNull: true
    },
    posicion_pagina: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '1'
    },
    activo: {
      type: DataTypes.STRING(45),
      allowNull: true,
      defaultValue: 'S'
    },
    num_pedidos: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    }
  }, {
    tableName: 'tiendas'
  });

  tiendas.associate = (models) => {
    tiendas.hasMany(models.tiendas_planes, {foreignKey: 'tiendas_id', sourceKey: 'idtienda', as: 'tiendas_planes'})
  };

  tiendas.associate = (models) => {
    tiendas.belongsToMany(models.historial_alertas, {
      through: 'tiendas_historial_alertas',
      as: 'historial_alertas',
      foreignKey: 'tiendas_id',
      otherKey: 'historial_alertas_id'
    });
  };

  return tiendas;
};
