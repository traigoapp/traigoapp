/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('notifications', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    notifications_detail_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'notifications_detail',
        key: 'id'
      }
    },
    type: {
      type: DataTypes.CHAR(1),
      allowNull: true
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'notifications'
  });
};
