/* jshint indent: 2 */

module.exports = (sequelize, DataTypes) => {
  return sequelize.define('municipios', {
    idmunicipios: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    nombre: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    idDepartamento: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'departamentos',
        key: 'idDepartamento'
      }
    }
  }, {
    tableName: 'municipios'
  });
};
