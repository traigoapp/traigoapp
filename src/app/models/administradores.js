/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('administradores', {
    cedula: {
      type: DataTypes.STRING(45),
      allowNull: false,
      primaryKey: true
    },
    nombre: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    apellido: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    correo: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    contrasenia: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    foto_perfil: {
      type: DataTypes.STRING(500),
      allowNull: true
    },
    anexos: {
      type: DataTypes.STRING(500),
      allowNull: true
    },
    iddireccion: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'direcciones',
        key: 'iddireccion'
      }
    },
    genero: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    ultimo_acceso: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'administradores'
  });
};
