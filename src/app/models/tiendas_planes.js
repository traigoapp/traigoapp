/* jshint indent: 2 */
const moment = require('moment');
module.exports = function(sequelize, DataTypes) {
  let tiendas_planes = sequelize.define('tiendas_planes', {
    tiendas_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'tiendas',
        key: 'idtienda'
      }
    },
    planes_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'planes',
        key: 'id'
      }
    },
    total_pedidos: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '0'
    },
    adicionales: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '0'
    },
    estado: {
      type: DataTypes.CHAR(1),
      allowNull: false
    },
    vencible: {
      type: DataTypes.BOOLEAN(),
      defaultValue: false,
      allowNull: false
    },
    dias_habilitados: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: '0'
    },
    fecha_inicio: {
      field: 'fecha_inicio',
      type: DataTypes.DATE,
      allowNull: true,
      get() {
        return moment(this.getDataValue('fecha_inicio')).format('YYYY-MM-DD');
      }
    },
    fecha_vencimiento: {
      field: 'fecha_vencimiento',
      type: DataTypes.DATE,
      allowNull: true,
      get() {
        return moment(this.getDataValue('fecha_vencimiento')).format('YYYY-MM-DD');
      }
    },
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
      allowNull: true,
      get() {
        return moment(this.getDataValue('createdAt')).format('DD-MM-YYYY');
      }
    },
    updatedAt: {
      field: 'updated_at',
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'tiendas_planes',
    timestamps: true,
    /*classMethods: {
      associate: function (models) {
        //tiendas_planes.belongsTo(models.tiendas, {foreignKey: 'tiendas_id', as: 'tienda'});
        //tiendas_planes.belongsTo(models.planes, {foreignKey: 'planes_id', sourceKey: 'id', as: 'plan'});
    }*/
  });

  // Obtiene historial
  tiendas_planes.associate = (models) => {
    tiendas_planes. hasMany(models.historial_tiendas_planes, {              
      foreignKey: 'tiendas_planes_tiendas_id',
      sourceKey: 'tiendas_id',
      scope: { 
          tiendas_planes_planes_id: sequelize.where(sequelize.col('historial.tiendas_planes_planes_id'), '=', sequelize.col('tiendas_planes.planes_id'))
        }, 
      as: 'historial'
    });
  };

  // Obtiene tiendas
  tiendas_planes.associate = (models) => {
    tiendas_planes.belongsTo(models.tiendas, {foreignKey: 'tiendas_id', as: 'tienda'})
  };
/*
  // Obtienen planes
  tiendas_planes.associate = (models) => {
    tiendas_planes.belongsTo(models.planes, {foreignKey: 'planes_id', sourceKey: 'id', as: 'plan'})
  };*/

  

  return tiendas_planes;
};
