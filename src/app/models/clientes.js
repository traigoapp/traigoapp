/* jshint indent: 2 */

module.exports = (sequelize, DataTypes) => {
  let clientes = sequelize.define('clientes', {
    correo: {
      type: DataTypes.STRING(100),
      allowNull: false,
      primaryKey: true
    },
    nombre: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    telefono: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    fecha_nacimiento: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    genero: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    contrasenia: {
      type: DataTypes.STRING(200),
      allowNull: false
    }
  }, {
    tableName: 'clientes'
  });

  return clientes;
};
