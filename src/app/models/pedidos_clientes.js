/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('pedidos_clientes', {
    idpedido_clientes: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    costo_envio: {
      type: "DOUBLE",
      allowNull: false
    },
    comision_rider: {
      type: "DOUBLE",
      allowNull: false
    },
    fecha: {
      type: DataTypes.DATE,
      allowNull: false
    },
    estado: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    iddireccion: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'direcciones',
        key: 'iddireccion'
      }
    },
    idrider: {
      type: DataTypes.STRING(45),
      allowNull: true,
      references: {
        model: 'riders',
        key: 'cedula'
      }
    },
    comision_entregada: {
      type: DataTypes.STRING(1),
      allowNull: false
    },
    correocliente: {
      type: DataTypes.STRING(100),
      allowNull: false,
      references: {
        model: 'clientes',
        key: 'correo'
      }
    },
    estados_pedido_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'estados_pedido',
        key: 'id'
      }
    }
  }, {
    tableName: 'pedidos_clientes'
  });
};
