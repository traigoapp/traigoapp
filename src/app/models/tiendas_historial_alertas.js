/* jshint indent: 2 */

module.exports = (sequelize, DataTypes) => {
  return sequelize.define('tiendas_historial_alertas', {
    tiendas_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'tiendas',
        key: 'idtienda'
      }
    },
    historial_alertas_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'historial_alertas',
        key: 'id'
      }
    },
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE,
      allowNull: true
    },
    updatedAt: {
      field: 'updated_at',
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'tiendas_historial_alertas',
    timestamps: true
  });
};
