/* jshint indent: 2 */

module.exports = (sequelize, DataTypes) => {
  let riders =  sequelize.define('riders', {
    cedula: {
      type: DataTypes.STRING(45),
      allowNull: false,
      primaryKey: true
    },
    nombre: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    apellido: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    telefono: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    correo: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    tipo_vehiculo: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    seguro_traigo: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    fecha_nacimiento: {
      type: DataTypes.DATEONLY,
      allowNull: false
    },
    tamanio_maletin: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    foto_perfil: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    genero: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    iddireccion: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'direcciones',
        key: 'iddireccion'
      }
    },
    contrasenia: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    anexos: {
      type: DataTypes.STRING(500),
      allowNull: true
    },
    listanegra: {
      type: DataTypes.STRING(1),
      allowNull: false,
      defaultValue: 'N'
    },
    activo: {
      type: DataTypes.STRING(45),
      allowNull: false,
      defaultValue: 'N'
    },
    latitude: {
      type: "DOUBLE",
      allowNull: true
    },
    longitude: {
      type: "DOUBLE",
      allowNull: true
    },
    limitePedidos: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: '0'
    }
  }, {
    tableName: 'riders'
  });

  riders.associate = (models) => {
    riders.belongsTo(models.direcciones, {foreignKey: 'iddireccion', as: 'direccion'})
  };

  riders.associate = (models) => {
    riders.hasMany(models.pedidos_empresariales, {foreignKey: 'idrider', sourceKey: 'cedula', as: 'pedidos_empresariales'})
  };

  return riders;
};
