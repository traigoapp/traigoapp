/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('pedidos_encomiendas', {
    idpedido_encomienda: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    costo_envio: {
      type: "DOUBLE",
      allowNull: false
    },
    estado: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    idrider: {
      type: DataTypes.STRING(45),
      allowNull: true,
      references: {
        model: 'riders',
        key: 'cedula'
      }
    },
    iddireccionorigen: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'direcciones',
        key: 'iddireccion'
      }
    },
    iddirecciondestino: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'direcciones',
        key: 'iddireccion'
      }
    },
    comision_rider: {
      type: "DOUBLE",
      allowNull: false
    },
    comision_entregada: {
      type: DataTypes.STRING(1),
      allowNull: false
    },
    mandado: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    correocliente: {
      type: DataTypes.STRING(100),
      allowNull: false,
      references: {
        model: 'clientes',
        key: 'correo'
      }
    },
    fecha: {
      type: DataTypes.DATE,
      allowNull: false
    },
    estados_pedido_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'estados_pedido',
        key: 'id'
      }
    }
  }, {
    tableName: 'pedidos_encomiendas'
  });
};
