/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('factores', {
    idfactor: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    nombre: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    obligatorio: {
      type: DataTypes.STRING(1),
      allowNull: false
    },
    idproducto: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'productos',
        key: 'idproducto'
      }
    }
  }, {
    tableName: 'factores'
  });
};
