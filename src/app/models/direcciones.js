/* jshint indent: 2 */

module.exports = (sequelize, DataTypes) => {
  let direcciones = sequelize.define('direcciones', {
    iddireccion: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    direccion: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    idmunicipio: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'municipios',
        key: 'idmunicipios'
      }
    },
    notas: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    latitude: {
      type: "DOUBLE",
      allowNull: true
    },
    longitude: {
      type: "DOUBLE",
      allowNull: true
    }
  }, {
    tableName: 'direcciones'
  });

  // Relacion con estadis de pedido
  direcciones.associate = (models) => {
    direcciones.belongsTo(models.municipios, {foreignKey: 'idmunicipio', as: 'municipio'});
  };

  return direcciones;
};
