'use strict';

var dbm;
var type;
var seed;
var async = require('async');
/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  let cols = ['tipo_pedido', 'slug'];
  let table = 'contexto_pedidos';

  db.insert(table, cols, ['pedidos_empresariales', 'Pedidos empresariales']);
  db.insert(table, cols, ['pedidos_encomiendas', 'Pedidos encomiendas']),
  db.insert(table, cols, ['pedidos_deseos', 'Pedidos deseo']),
  db.insert(table, cols, ['pedidos_clientes', 'Pedidos clientes']);
  return db;
  /*return async.series([
  db.insert(table, cols, ['pedidos_empresariales', 'Pedidos empresariales']),
  db.insert(table, cols, ['pedidos_encomiendas', 'Pedidos encomiendas']),
  db.insert(table, cols, ['pedidos_deseos', 'Pedidos deseo']),
  db.insert(table, cols, ['pedidos_clientes', 'Pedidos clientes'])
  ], (error) => {console.log(error);
  });*/
    
  //return null;
};

exports.down = function(db) {
  return null;
};

exports._meta = {
  "version": 1
};
